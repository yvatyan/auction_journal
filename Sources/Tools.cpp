#include "Headers/Tools.h"
#include "Headers/Options.h"
#include <QFontMetrics>
#include <QDate>
#include <QTextEdit>

QString Tools::ChangeStringCase(const QString& str, Tools::StringCase mode) {
    QString result;
    if(mode == StringCase::FirstUpper) {
        bool complete = false;
        for(auto ch : str) {
            if(isSpecialCharacter(ch.toLatin1()) || complete) {
                result += ch;
            } else {
                complete = true;
                result += ch.toUpper();
            }
        }
    }
    if(mode == StringCase::AllUpper) {
        result = str.toUpper();
    }
    if(mode == StringCase::AllLower) {
        result = str.toLower();
    }
    if(mode == StringCase::AllFirstUpper) {
        bool complete = false;
        for(auto ch : str) {
            if(isSpaceCharacter(ch.toLatin1())) {
                complete = false;
                result += ch;
                continue;
            }
            if(isSpecialCharacter(ch.toLatin1()) || complete) {
                result += ch;
            } else {
                complete = true;
                result += ch.toUpper();
            }
        }
    }
    return applySpecialSlashB(result);
}
QSize Tools::StringRenderSize(const QString& str, const QFont& font) {
    QFontMetrics fm(font);
    QRect boundingRectangle(fm.boundingRect(str));
    return QSize(boundingRectangle.width(), boundingRectangle.height());
}
QSize Tools::TextRenderSizeOld(const QString& text, const QFont& font, int widthLimit) {
    QTextEdit* temp = new QTextEdit;
    temp->setFont(font);
    temp->resize(QSize(widthLimit, -1));
    temp->setHtml(text);
    temp->show();
    QSize size(temp->document()->size().toSize());
    temp->hide();
    delete temp;
    return size;
}
// Note: Assumed that this function returns size that won't be less than required size.
QSize Tools::TextRenderSize(const QString& text, const QFont& font, int widthLimit) {
    QFontMetrics fm(font);
    int spaceWidth = 5;
    QStringList words = text.split(" ", QString::SplitBehavior::SkipEmptyParts);
    int rowCount = static_cast<int>(words.size() != 0);
    int rowLength = 0;
    for(auto word : words) {
        rowLength += fm.boundingRect(word).width();
        if(rowLength >= widthLimit) {
            rowLength = fm.boundingRect(word).width();
            rowCount ++;
        }
        rowLength += spaceWidth;
    }
    int textHeight = rowCount == 1 ? fm.lineSpacing() : rowCount * fm.lineSpacing();
    textHeight += static_cast<int>(fm.lineSpacing() / 2.);
    return QSize(widthLimit, textHeight);
}
int Tools::CompareNumberedStrings(const QString& str1, const QString& str2) { // Note: Who's greater
    QString ztr1(str1 + char(0));
    QString ztr2(str2 + char(0));
    for(int z1 = 0, z2 = 0; z1 < ztr1.size() && z2 < ztr2.size(); ++z1, ++z2) {
        if(ztr1[z1].isDigit() && !ztr2[z2].isDigit()) {
            return 2;
        }
        if(!ztr1[z1].isDigit() && ztr2[z2].isDigit()) {
            return 1;
        }
        if(ztr1[z1].isDigit() && ztr2[z2].isDigit()) {
            int l1, l2;
            int i1 = extractIntegerAt(ztr1, z1, l1);
            int i2 = extractIntegerAt(ztr2, z2, l2);
            if(i1 > i2) {
                return 1;
            } else if(i1 < i2) {
                return 2;
            } else {
                z1 += l1 - 1;
                z2 += l2 - 1;
                continue;
            }
        }
        if(!ztr1[z1].isDigit() && !ztr2[z2].isDigit()) {
            if(ztr1[z1] > ztr2[z2]) {
                return 1;
            } else if(ztr1[z1] < ztr2[z2]) {
                return 2;
            }
        }
    }
    return 0;
}
bool Tools::IsInStorageDir(const QString path) {
    return path.indexOf(Options::Instance().StorageDirectory()) == 0;
}
QString Tools::GenerateRandomName(int length, int portion, const QString& delemiter, const QString& extension) {
    QString result;
    srand(static_cast<unsigned int>(time(nullptr)));
    const QString possibleCharacters("ABCDEFGHIJKLMNOPQRTSUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
    for(int i = 0; i < length; ++i) {
        if(portion != 0) {
            if(i % portion == 0 && i != 0) {
                result += delemiter;
            }
        }
        int index = rand() % possibleCharacters.length();
        QChar randChar = possibleCharacters.at(index);
        result += randChar;
    }
    result += (extension.isEmpty() ? "" : "." + extension);
    return result;
}
QString Tools::FileExtension(const QString& path) {
    int index = path.lastIndexOf(QChar('.'));
    return path.right(path.length() - 1 - index);
}
bool Tools::isSpecialCharacter(char ch) {
    static QString specialString("\a\b\n\r\t");
    return specialString.indexOf(ch) != -1;
}
bool Tools::isSpaceCharacter(char ch) {
    static QString spaceString("\n\r\t ");
    return spaceString.indexOf(ch) != -1;
}
int Tools::extractIntegerAt(const QString& str, int index, int& length) {
    int result = 0;
    length = 0;
    while(str[index].isDigit() && index < str.size()) {
        result *= 10;
        result += (str.at(index).toLatin1() - '0');
        length ++;
        index ++;
    }
    return result;
}
QString Tools::applySpecialSlashB(const QString& str) {
    QString result;
    for(auto ch : str) {
        if(ch == '\b') {
            result.remove(result.size() - 1, 1);
            continue;
        }
        result += ch;
    }
    return result;
}
bool Tools::IsQMouseEvent(QEvent* event) {
    return event->type() == QEvent::NonClientAreaMouseButtonDblClick ||
           event->type() == QEvent::NonClientAreaMouseButtonPress ||
           event->type() == QEvent::NonClientAreaMouseButtonRelease ||
           event->type() == QEvent::NonClientAreaMouseMove ||
           event->type() == QEvent::MouseButtonDblClick ||
           event->type() == QEvent::MouseButtonPress ||
           event->type() == QEvent::MouseButtonRelease ||
           event->type() == QEvent::MouseMove;
}
uint qHash(const QVariant& v, uint seed) {
    switch(v.type()) {
        case QVariant::Int			: return qHash(v.toInt(), seed);
        case QVariant::LongLong 	: return qHash(v.toLongLong(), seed);
        case QVariant::UInt			: return qHash(v.toUInt(), seed);
        case QVariant::ULongLong	: return qHash(v.toULongLong(), seed);
        case QVariant::Date			: return qHash(v.toDate(), seed);
        case QVariant::String		: return qHash(v.toString(), seed);
        default		 				: return INT_MAX;
    }
}
