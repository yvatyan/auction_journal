#include "Headers/Comment.h"

Comment::Comment()
{
}
Comment::Comment(size_t recordId, const QString& text, const QDate& createdDate, const bool edited, const QDate& editedDate)
    : m_recordId(recordId)
    , m_text(text)
    , m_createdDate(createdDate)
    , m_edited(edited)
    , m_editedDate(editedDate)
    , m_id(0)
{
}
