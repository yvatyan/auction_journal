#include "Headers/Database/SqlCommands/SqlSelect.h"
#include <QtSql/QSqlRecord>

SqlSelect::SqlSelect(Database* database, const QString& selectWhat, const QString& tableName)
    : ISqlCommand(database)
    , m_selectWhat(selectWhat)
    , m_tableName(tableName)
    , m_condition("")
    , m_orderBy("")
    , m_groupBy("")
    , m_isDistinct(false)
{
}
QList<QVariantList> SqlSelect::Execute() {
    QString queryString = "SELECT " + (m_isDistinct ? " DISTINCT " : QString()) + m_selectWhat;
    queryString += " FROM " + m_tableName;
    queryString += m_condition.isEmpty() ? "" : " WHERE " + m_condition;
    queryString += m_groupBy.isEmpty() ? "" : " GROUP BY " + m_groupBy;
    queryString += m_orderBy.isEmpty() ? "" : " ORDER BY " + m_orderBy;
    queryString += ";";
    QSqlQuery query = executeQuery(queryString);
    if(query.isActive()) {
        QList<QVariantList> result;
        while(query.next()) {
            QVariantList recordValues;
            QSqlRecord record(query.record());
            for(int i = 0; i < record.count(); ++i) {
                recordValues.append(record.value(i));
            }
            result.append(recordValues);
        }
        m_state = true;
        return result;
    }
    m_state = false;
    return QList<QVariantList>();
}
void SqlSelect::SetCondition(const QString& condition) {
    m_condition = condition;
}
void SqlSelect::SetOrderBy(const QString& orderBy) {
    m_orderBy = orderBy;
}
void SqlSelect::SetGroupBy(const QString& groupBy) {
    m_groupBy = groupBy;
}
void SqlSelect::SetDistinctFlag(bool turnOn) {
    m_isDistinct = turnOn;
}
