#include "Headers/Database/SqlCommands/SqlCreateTable.h"
#include <QtSql/QSqlError>

SqlCreateTable::SqlCreateTable(Database* database, const QString& tableName, const QString& primaryKey, const FieldVector& fields, const ForeignKeyList& foreignKeys)
    : ISqlCommand(database)
    , m_tableName(tableName)
    , m_primaryKey(primaryKey)
    , m_fields(fields)
    , m_foreignKeys(foreignKeys)
{
}
QList<QVariantList> SqlCreateTable::Execute() {
    QString queryString = "CREATE TABLE `" + m_tableName + "` ( ";
    for(int i = 0; i < m_fields.size(); ++i) {
        QString attributeStr;
        for(auto attrib : std::get<2>(m_fields.at(i))) {
            attributeStr += SqlAttributes::ToString(attrib) + " ";
        }
        queryString += "`" + std::get<0>(m_fields.at(i)) + "` "
                    + SqlTypes::ToString(std::get<1>(m_fields.at(i)))
                    + (attributeStr.isEmpty() ? "" : " " + attributeStr.trimmed())
                    + ", ";
    }
    queryString += "PRIMARY KEY(`" + m_primaryKey + "`)";
    if(m_foreignKeys.size() != 0) {
        queryString += ", ";
        for(int i = 0; i < m_foreignKeys.size(); ++i) {
            auto fKey = m_foreignKeys.at(i);
            queryString += "FOREIGN KEY (`" + std::get<0>(fKey) +  "`) "
                        + "REFERENCES " + std::get<1>(fKey) + "(`" + std::get<2>(fKey) + "`)";
            if(i != m_foreignKeys.size() - 1) {
                queryString += ", ";
            }
        }
    }
    queryString += ");";
    QSqlQuery query = executeQuery(queryString);
    m_state = query.isActive();
    return QList<QVariantList>();
}
