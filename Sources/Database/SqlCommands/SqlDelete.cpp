#include "Headers/Database/SqlCommands/SqlDelete.h"


SqlDelete::SqlDelete(Database* database, const QString& tableName, const QString& condition)
    : ISqlCommand(database)
    , m_tableName(tableName)
    , m_condition(condition)
{
}
QList<QVariantList> SqlDelete::Execute() {
    QString queryString = "DELETE FROM " + m_tableName;
    queryString += " WHERE " + m_condition + ";";
    QSqlQuery query = executeQuery(queryString);
    m_state = query.isActive();
    return QList<QVariantList>();
}
