#include "Headers/Database/SqlCommands/SqlUpdate.h"

SqlUpdate::SqlUpdate(Database* database, const QString& tableName, const QList<QPair<QString, QString>>& fields)
    : ISqlCommand(database)
    , m_tableName(tableName)
    , m_fields(fields)
{
}
QList<QVariantList> SqlUpdate::Execute() {
    QString queryString("UPDATE ");
    queryString += m_tableName + " SET ";
    for(int i = 0; i < m_fields.size(); ++i) {
        queryString += m_fields.at(i).first + "=";
        if(m_fields.at(i).second.toLower() == "null") {
            queryString += m_fields.at(i).second;
        } else {
            queryString += "\"" + m_fields.at(i).second + "\"";
        }
        if(i != m_fields.size() - 1) {
            queryString += ", ";
        }
    }
    if(!m_condition.isEmpty()) {
        queryString += " WHERE " + m_condition;
    }
    queryString += ";";
    QSqlQuery query = executeQuery(queryString);
    m_state = query.isActive();
    return QList<QVariantList>();
}
void SqlUpdate::SetCondition(const QString& condition) {
    m_condition = condition;
}
