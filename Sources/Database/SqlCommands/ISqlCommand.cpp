#include "Headers/Database/SqlCommands/ISqlCommand.h"
#include <QtSql/QSqlError>
#include <QDebug>

ISqlCommand::ISqlCommand(Database* database)
    : m_state(false)
    , m_db(database)
{
}
ISqlCommand::~ISqlCommand()
{
}
bool ISqlCommand::IsValid() const {
    return m_state;
}
QSqlQuery ISqlCommand::executeQuery(const QString &queryString) {
    QSqlQuery query(m_db->SqlDatabase());
    query.prepare(queryString);
    if(!query.exec()) {
        qDebug() << "Sql execution failed:\n\tQuery:" << queryString
                 << "\n\tError:" << query.lastError().text();
    }
    return query;
}
