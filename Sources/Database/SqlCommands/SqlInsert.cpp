#include "Headers/Database/SqlCommands/SqlInsert.h"
#include <QtSql/QSqlError>

SqlInsert::SqlInsert(Database* database, const QString& tableName, const Values& values)
    : ISqlCommand(database)
    , m_tableName(tableName)
    , m_values(values)
{
}
QList<QVariantList> SqlInsert::Execute() {
    if(m_values.first.size() != m_values.second.size()) {
        m_state = false;
        return QList<QVariantList>();
    }
    QString queryString = "INSERT INTO ";
    queryString += "`" + m_tableName + "`(";
    QString columns, values;
    for(int i = 0; i < m_values.first.size(); ++i) {
        columns += "`" + m_values.first.at(i) + "`";
        if(m_values.second.at(i).toLower() == "null") {
            values += m_values.second.at(i);
        } else {
            values += "\"" + m_values.second.at(i) + "\"";
        }
        if(i != m_values.first.size() - 1) {
            columns += ", ";
            values += ", ";
        }
    }
    queryString += columns + ") " + "VALUES (" + values + ");";
    QSqlQuery query = executeQuery(queryString);
    m_state = query.isActive();
    return QList<QVariantList>();
}
