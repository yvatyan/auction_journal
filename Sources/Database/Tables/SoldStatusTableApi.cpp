#include "Headers/Database/Tables/SoldStatusTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/SqlCommands/SqlCreateTable.h"
#include "Headers/Database/SqlCommands/SqlInsert.h"
#include "Headers/Database/SqlCommands/SqlSelect.h"

QMap<SoldStatusTable::Fields, QPair<size_t, QString>> SoldStatusTable::m_fields = {
     std::pair<Fields, QPair<size_t, QString>>(Fields::Id, QPair<size_t, QString>(0, "Id")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Name, QPair<size_t, QString>(1, "Name")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Finished, QPair<size_t, QString>(2, "Finished"))
};
SoldStatusTable::Migration::Migration(ITableApi *table)
    : IMigration(table)
{
}
bool SoldStatusTable::Migration::Migrate() const {
    FieldVector fields(3);
    fields[static_cast<int>(FieldIndex(Fields::Id))] =
        Field(FieldName(Fields::Id), SqlType::Integer, {});
    fields[static_cast<int>(FieldIndex(Fields::Name))] =
        Field(FieldName(Fields::Name), SqlType::Text, {SqlAttribute::NotNull, SqlAttribute::Unique});
    fields[static_cast<int>(FieldIndex(Fields::Finished))] =
        Field(FieldName(Fields::Finished), SqlType::Integer, {SqlAttribute::NotNull});

    QList<QStringList> data = {
            {QString::number(Sold), "sold", "1"},
            {QString::number(NotSold), "not sold", "0"},
            {QString::number(WA), "waiting approval", "0"},
            {QString::number(Pending), "pending", "2"},
            {QString::number(Canceled), "canceled", "1"},
            {QString::number(Moved), "moved", "0"}
    };
    return m_table->CreateDataTable(fields, data, SoldStatusTable::FieldName(SoldStatusTable::Fields::Id));
}
bool SoldStatusTable::Migration::Rollback() const {
    return false;
}
QString SoldStatusTable::Migration::Description() const {
    return SOLDSTATUS_TABLE_M_v1_DESCRIPTION;
}
size_t SoldStatusTable::Migration::Id() const {
    return SOLDSTATUS_TABLE_M_v1_ID;
}
SoldStatusTable::IApi::IApi(ITableApi* owningTable)
    : m_table(owningTable)
{
}
QString SoldStatusTable::IApi::GetSoldStatusNameById(size_t id) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetSoldStatusNameById call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            FieldName(Fields::Name),
                            m_table->Name());
    selectCommand.SetCondition("Id=" + QString::number(id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        if(rawResult.size() != 1) {
            throw std::runtime_error("GetSoldStatusNameById call: Multiple records or empty data returned.");
        }
        return rawResult.at(0).at(0).toString();
    } else {
        throw std::runtime_error("GetSoldStatusNameById call: Failed.");
    }
}

QList<QMap<SoldStatusTable::Fields, QVariant> > SoldStatusTable::IApi::GetSoldStatusData() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetSoldStatusData call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            FieldName(SoldStatusTable::Fields::Id) + "," +
                            FieldName(SoldStatusTable::Fields::Name),
                            m_table->Name());
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        QList<QMap<Fields, QVariant>> result;
        for(auto row : rawResult) {
            QMap<Fields, QVariant> record;
            record.insert(Fields::Id, row.at(0));
            record.insert(Fields::Name, row.at(1));
            result.append(record);
        }
        return result;
    } else {
        throw std::runtime_error("GetSoldStatusData call: Failed.");
    }
}
QList<QMap<SoldStatusTable::Fields, QVariant> > SoldStatusTable::IApi::GetFinishedSoldStatusData() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetFinishedSoldStatusData call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            FieldName(SoldStatusTable::Fields::Id) + "," +
                            FieldName(SoldStatusTable::Fields::Name),
                            m_table->Name());
    selectCommand.SetCondition("Finished=1");
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        QList<QMap<Fields, QVariant>> result;
        for(auto row : rawResult) {
            QMap<Fields, QVariant> record;
            record.insert(Fields::Id, row.at(0));
            record.insert(Fields::Name, row.at(1));
            result.append(record);
        }
        return result;
    } else {
        throw std::runtime_error("GetFinishedSoldStatusData call: Failed.");
    }
}
QList<QMap<SoldStatusTable::Fields, QVariant> > SoldStatusTable::IApi::GetUnfinishedSoldStatusData() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetUnfinishedSoldStatusData call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            FieldName(SoldStatusTable::Fields::Id) + "," +
                            FieldName(SoldStatusTable::Fields::Name),
                            m_table->Name());
    selectCommand.SetCondition(FieldName(Fields::Finished) + "=0");
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        QList<QMap<Fields, QVariant>> result;
        for(auto row : rawResult) {
            QMap<Fields, QVariant> record;
            record.insert(Fields::Id, row.at(0));
            record.insert(Fields::Name, row.at(1));
            result.append(record);
        }
        return result;
    } else {
        throw std::runtime_error("GetUnfinishedSoldStatusData call: Failed.");
    }
}
SoldStatusTable::SoldStatusTable(Database* database)
    : ITableApi(new Migration(this), database)
    , m_api(this)
{
}
QString SoldStatusTable::Name() const {
    return SOLDSTATUS_TABLE_NAME;
}
const SoldStatusTable::IApi& SoldStatusTable::Api() const {
    return m_api;
}
size_t SoldStatusTable::FieldIndex(SoldStatusTable::Fields f) {
    return m_fields[f].first;
}
QString SoldStatusTable::FieldName(SoldStatusTable::Fields f) {
    return m_fields[f].second;
}
