#include "Headers/Database/Tables/TransmissionTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/SqlCommands/SqlSelect.h"

QMap<TransmissionTable::Fields, QPair<size_t, QString>> TransmissionTable::m_fields = {
     std::pair<Fields, QPair<size_t, QString>>(Fields::Id, QPair<size_t, QString>(0, "Id")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Name, QPair<size_t, QString>(1, "Name"))
};
TransmissionTable::Migration::Migration(ITableApi* table)
    : IMigration(table)
{
}
bool TransmissionTable::Migration::Migrate() const {
    FieldVector fields(2);
    fields[static_cast<int>(TransmissionTable::FieldIndex(TransmissionTable::Fields::Id))] =
        Field(TransmissionTable::FieldName(TransmissionTable::Fields::Id), SqlType::Integer, {});
    fields[static_cast<int>(TransmissionTable::FieldIndex(TransmissionTable::Fields::Name))] =
        Field(TransmissionTable::FieldName(TransmissionTable::Fields::Name), SqlType::Text, {SqlAttribute::NotNull, SqlAttribute::Unique});

    QList<QStringList> data = {
            {"1", "automiatic"},
            {"2", "dual-clutch"},
            {"3", "manual"},
            {"4", "semi-automatic"}
    };
    return m_table->CreateDataTable(fields, data, TransmissionTable::FieldName(TransmissionTable::Fields::Id));
}
bool TransmissionTable::Migration::Rollback() const {
    return false;
}
QString TransmissionTable::Migration::Description() const {
    return TRANSMISSION_TABLE_M_v1_DESCRIPTION;
}
size_t TransmissionTable::Migration::Id() const {
    return TRANSMISSION_TABLE_M_v1_ID;
}
TransmissionTable::IApi::IApi(ITableApi* owningTable)
    : m_table(owningTable)
{
}
QList<QMap<TransmissionTable::Fields, QVariant>> TransmissionTable::IApi::GetTransmissionData() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetTransmissionData call: Api call when table is missing from db.");
    }
    QString fieldsToRetrieve =
            TransmissionTable::FieldName(TransmissionTable::Fields::Id) + ", " +
            TransmissionTable::FieldName(TransmissionTable::Fields::Name);
    SqlSelect selectCommand(m_table->GetDatabase(),
                            fieldsToRetrieve,
                            m_table->Name());
    selectCommand.SetOrderBy(TransmissionTable::FieldName(TransmissionTable::Fields::Id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        QList<QMap<TransmissionTable::Fields, QVariant>> result;
        for(auto entry : rawResult) {
            QMap<TransmissionTable::Fields, QVariant> singleRecord;
            singleRecord.insert(TransmissionTable::Fields::Id, entry.at(0));
            singleRecord.insert(TransmissionTable::Fields::Name, entry.at(1));
            result.append(singleRecord);
        }
        return result;
   } else {
        throw std::runtime_error("GetTransmissionData call: Failed.");
    }
}
QString TransmissionTable::IApi::GetTransmissionNameById(size_t id) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetTransmissionNameById call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            TransmissionTable::FieldName(TransmissionTable::Fields::Name),
                            m_table->Name());
    selectCommand.SetCondition("Id=" + QString::number(id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        if(rawResult.size() != 1) {
            throw std::runtime_error("GetTransmissionNameById call: Multiple records or empty data returned.");
        }
        return rawResult.at(0).at(0).toString();
    } else {
        throw std::runtime_error("GetTransmissionNameById call: Failed.");
    }
}
TransmissionTable::TransmissionTable(Database* database)
    : ITableApi(new Migration(this), database)
    , m_api(this)
{
}
QString TransmissionTable::Name() const {
    return TRANSMISSION_TABLE_NAME;
}
const TransmissionTable::IApi& TransmissionTable::Api() const {
    return m_api;
}
size_t TransmissionTable::FieldIndex(TransmissionTable::Fields f) {
    return m_fields[f].first;
}
QString TransmissionTable::FieldName(TransmissionTable::Fields f) {
    return m_fields[f].second;
}
