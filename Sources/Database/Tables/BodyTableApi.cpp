#include "Headers/Database/Tables/BodyTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/SqlCommands/SqlSelect.h"

QMap<BodyTable::Fields, QPair<size_t, QString>> BodyTable::m_fields = {
     std::pair<Fields, QPair<size_t, QString>>(Fields::Id, QPair<size_t, QString>(0, "Id")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Name, QPair<size_t, QString>(1, "Name"))
};
BodyTable::Migration::Migration(ITableApi* table)
    : IMigration(table)
{
}
bool BodyTable::Migration::Migrate() const {
    FieldVector fields(2);
    fields[static_cast<int>(BodyTable::FieldIndex(BodyTable::Fields::Id))] =
        Field(BodyTable::FieldName(BodyTable::Fields::Id), SqlType::Integer, {});
    fields[static_cast<int>(BodyTable::FieldIndex(BodyTable::Fields::Name))] =
        Field(BodyTable::FieldName(BodyTable::Fields::Name), SqlType::Text, {SqlAttribute::NotNull});

    QList<QStringList> data = {
        {"1",  "2-door convertible"},
        {"2",  "4-door convertible"},
        {"3",  "2-door coupe"},
        {"4",  "2-door hatchback"},
        {"5",  "4-door hatchback"},
        {"6",  "2-door pickup"},
        {"7",  "4-door pickup"},
        {"8",  "2-door roadster"},
        {"9",  "2-door sedan"},
        {"10", "4-door sedan"},
        {"11", "2-door station wagon"},
        {"12", "4-door station wagon"},
        {"13", "2-door suv"},
        {"14", "4-door suv"}
    };
    return m_table->CreateDataTable(fields, data, BodyTable::FieldName(BodyTable::Fields::Id));
}
bool BodyTable::Migration::Rollback() const {
    return false;
}
QString BodyTable::Migration::Description() const {
    return BODY_TABLE_M_v1_DESCRIPTION;
}
size_t BodyTable::Migration::Id() const {
    return BODY_TABLE_M_v1_ID;
}
BodyTable::IApi::IApi(ITableApi* owningTable)
    : m_table(owningTable)
{
}
QList<QMap<BodyTable::Fields, QVariant>> BodyTable::IApi::GetBodyData() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetBodyData call: Api call when table is missing from db.");
    }
    QString fieldsToRetrieve =
            BodyTable::FieldName(BodyTable::Fields::Id) + ", " +
            BodyTable::FieldName(BodyTable::Fields::Name);
    SqlSelect selectCommand(m_table->GetDatabase(),
                            fieldsToRetrieve,
                            m_table->Name());
    selectCommand.SetOrderBy(BodyTable::FieldName(BodyTable::Fields::Id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        QList<QMap<BodyTable::Fields, QVariant>> result;
        for(auto entry : rawResult) {
            QMap<BodyTable::Fields, QVariant> singleRecord;
            singleRecord.insert(BodyTable::Fields::Id, entry.at(0));
            singleRecord.insert(BodyTable::Fields::Name, entry.at(1));
            result.append(singleRecord);
        }
        return result;
   } else {
        throw std::runtime_error("GetBodyData call: Failed.");
    }
}
QString BodyTable::IApi::GetBodyNameById(size_t id) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetBodyNameById call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            BodyTable::FieldName(BodyTable::Fields::Name),
                            m_table->Name());
    selectCommand.SetCondition("Id=" + QString::number(id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        if(rawResult.size() != 1) {
            throw std::runtime_error("GetBodyNameById call: Multiple records or empty data returned.");
        }
        return rawResult.at(0).at(0).toString();
    } else {
        throw std::runtime_error("GetBodyNameById call: Failed.");
    }
}
BodyTable::BodyTable(Database* database)
    : ITableApi(new Migration(this), database)
    , m_api(this)
{
}
BodyTable::~BodyTable()
{
}
QString BodyTable::Name() const {
    return BODY_TABLE_NAME;
}
const BodyTable::IApi& BodyTable::Api() const {
    return m_api;
}
size_t BodyTable::FieldIndex(Fields f) {
    return m_fields[f].first;
}
QString BodyTable::FieldName(Fields f) {
    return m_fields[f].second;
}
