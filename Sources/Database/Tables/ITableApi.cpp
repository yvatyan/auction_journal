#include "Headers/Database/Tables/ITableApi.h"
#include "Headers/Database/Tables/MigrationTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/SqlCommands/SqlCreateTable.h"
#include "Headers/Database/SqlCommands/SqlInsert.h"

ITableApi::ITableApi(IMigration* migration, Database* database)
    : m_migration(migration)
    , m_database(database)
{
}
ITableApi::~ITableApi()
{
}
void ITableApi::ApplyMigration() {
    if(m_migration->Migrate()) {
        MigrationTable* migrationTable = m_database->GetMigrationTable();
        size_t regId;
        try {
            regId = migrationTable->Api().RegisterMigration(m_migration->Description(), Name());
        } catch (const std::runtime_error& err) {
            if(m_migration->Rollback()) {
                throw std::runtime_error(std::string("Rollback succeed. Error origin: ") + err.what());
            } else {
                throw std::runtime_error(std::string("Rollback failed. Error origin: ") + err.what());
            }
        }
        if(regId != m_migration->Id()) {
            QString message = "Id discrepancy of table \"" + Name() + "\".\n" +
                              "RegistrationID " + QString::number(regId) +
                              " not equals to migrationID " + QString::number(m_migration->Id()) + ".";
            if(m_migration->Rollback()) {
                throw std::runtime_error("Rollback succeed. Error origin: " + message.toStdString());
            } else {
                throw std::runtime_error("Rollback failed. Error origin: " + message.toStdString());
            }
        }
    } else {
        QString message = "Failed to migrate table \"" + Name() + "\".";
        if(m_migration->Rollback()) {
            throw std::runtime_error("Rollback succeed. Error origin: " + message.toStdString());
        } else {
            throw std::runtime_error("Rollback failed. Error origin: " + message.toStdString());
        }
    }
}
Database* ITableApi::GetDatabase() {
    return m_database;
}
unsigned int ITableApi::MigrationId() const {
    return m_migration->Id();
}
QString ITableApi::MigrationDescription() const {
    return m_migration->Description();
}
bool ITableApi::ExistsInDb() {
    return m_database->SqlDatabase().tables().indexOf(Name()) != -1;
}
bool ITableApi::CreateDataTable(const FieldVector& fields, const QList<QStringList>& data, const QString& primaryKey, const ForeignKeyList& foreignKeys) const {
    SqlCreateTable createTableCommnand(m_database, Name(), primaryKey, fields, foreignKeys);
    createTableCommnand.Execute();
    if(createTableCommnand.IsValid()) {
        QStringList fieldNames;
        for(auto f : fields) {
            fieldNames.append(std::get<0>(f));
        }
        QList<Values> values;
        for(auto record : data) {
            values.append(Values(fieldNames, record));
        }
        for(auto v : values) {
            SqlInsert insertCommand(m_database, Name(), v);
            insertCommand.Execute();
            if(!insertCommand.IsValid()) {
                return false;
            }
        }
    } else {
        return false;
    }
    return true;
}
