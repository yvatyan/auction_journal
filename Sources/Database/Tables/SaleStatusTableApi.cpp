#include "Headers/Database/Tables/SaleStatusTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/SqlCommands/SqlSelect.h"

QMap<SaleStatusTable::Fields, QPair<size_t, QString>> SaleStatusTable::m_fields = {
     std::pair<Fields, QPair<size_t, QString>>(Fields::Id, QPair<size_t, QString>(0, "Id")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Name, QPair<size_t, QString>(1, "Name"))
};
SaleStatusTable::Migration::Migration(ITableApi *table)
    : IMigration(table)
{
}
bool SaleStatusTable::Migration::Migrate() const {
    FieldVector fields(2);
    fields[static_cast<int>(SaleStatusTable::FieldIndex(SaleStatusTable::Fields::Id))] =
        Field(SaleStatusTable::FieldName(SaleStatusTable::Fields::Id), SqlType::Integer, {});
    fields[static_cast<int>(SaleStatusTable::FieldIndex(SaleStatusTable::Fields::Name))] =
        Field(SaleStatusTable::FieldName(SaleStatusTable::Fields::Name), SqlType::Text, {SqlAttribute::NotNull, SqlAttribute::Unique});

    QList<QStringList> data = {
        {"1", "pure sale"},
        {QString::number(MinimumBid), "minimum bid"},
        {QString::number(OnApproval), "on approval"}
    };
    return m_table->CreateDataTable(fields, data, SaleStatusTable::FieldName(SaleStatusTable::Fields::Id));
}
bool SaleStatusTable::Migration::Rollback() const {
    return false;
}
QString SaleStatusTable::Migration::Description() const {
    return SALESTATUS_TABLE_M_v1_DESCRIPTION;
}
size_t SaleStatusTable::Migration::Id() const {
    return SALESTATUS_TABLE_M_v1_ID;
}
SaleStatusTable::IApi::IApi(ITableApi* owningTable)
    : m_table(owningTable)
{
}
QList<QMap<SaleStatusTable::Fields, QVariant>> SaleStatusTable::IApi::GetSaleStatusData() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetSaleStatusData call: Api call when table is missing from db.");
    }
    QString fieldsToRetrieve =
            SaleStatusTable::FieldName(SaleStatusTable::Fields::Id) + ", " +
            SaleStatusTable::FieldName(SaleStatusTable::Fields::Name);
    SqlSelect selectCommand(m_table->GetDatabase(),
                            fieldsToRetrieve,
                            m_table->Name());
    selectCommand.SetOrderBy(SaleStatusTable::FieldName(SaleStatusTable::Fields::Id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        QList<QMap<SaleStatusTable::Fields, QVariant>> result;
        for(auto entry : rawResult) {
            QMap<SaleStatusTable::Fields, QVariant> singleRecord;
            singleRecord.insert(SaleStatusTable::Fields::Id, entry.at(0));
            singleRecord.insert(SaleStatusTable::Fields::Name, entry.at(1));
            result.append(singleRecord);
        }
        return result;
   } else {
        throw std::runtime_error("GetSaleStatusData call: Failed.");
    }
}
QString SaleStatusTable::IApi::GetSaleStatusNameById(size_t id) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetSaleStatusNameById call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            SaleStatusTable::FieldName(SaleStatusTable::Fields::Name),
                            m_table->Name());
    selectCommand.SetCondition("Id=" + QString::number(id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        if(rawResult.size() != 1) {
            throw std::runtime_error("GetSaleStatusNameById call: Multiple records or empty data returned.");
        }
        return rawResult.at(0).at(0).toString();
    } else {
        throw std::runtime_error("GetSaleSattusNameById call: Failed.");
    }
}
SaleStatusTable::SaleStatusTable(Database* database)
    : ITableApi(new Migration(this), database)
    , m_api(this)
{
}
QString SaleStatusTable::Name() const {
    return SALESTATUS_TABLE_NAME;
}
const SaleStatusTable::IApi& SaleStatusTable::Api() const {
    return m_api;
}
size_t SaleStatusTable::FieldIndex(SaleStatusTable::Fields f) {
    return m_fields[f].first;
}
QString SaleStatusTable::FieldName(SaleStatusTable::Fields f) {
    return m_fields[f].second;
}
