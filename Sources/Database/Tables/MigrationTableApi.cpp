#include "Headers/Database/Tables/MigrationTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/SqlCommands/SqlCreateTable.h"
#include "Headers/Database/SqlCommands/SqlSelect.h"
#include "Headers/Database/SqlCommands/SqlInsert.h"

QMap<MigrationTable::Fields, QPair<size_t, QString>> MigrationTable::m_fields = {
     std::pair<Fields, QPair<size_t, QString>>(Fields::Id, QPair<size_t, QString>(0, "Id")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Description, QPair<size_t, QString>(1, "Description")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::TargetTable, QPair<size_t, QString>(2, "TargetTable"))
};
MigrationTable::Migration::Migration(ITableApi *table)
    : IMigration(table)
{
}
bool MigrationTable::Migration::Migrate() const {
    FieldVector fields(3);
    fields[static_cast<int>(MigrationTable::FieldIndex(MigrationTable::Fields::Id))] =
        Field(MigrationTable::FieldName(MigrationTable::Fields::Id), SqlType::Integer, {SqlAttribute::AutoIncrement});
    fields[static_cast<int>(MigrationTable::FieldIndex(MigrationTable::Fields::Description))] =
        Field(MigrationTable::FieldName(MigrationTable::Fields::Description), SqlType::Text, {});
    fields[static_cast<int>(MigrationTable::FieldIndex(MigrationTable::Fields::TargetTable))] =
        Field(MigrationTable::FieldName(MigrationTable::Fields::TargetTable), SqlType::Text, {SqlAttribute::NotNull});

    SqlCreateTable createTableCommnand(m_table->GetDatabase(),
                                       m_table->Name(),
                                       MigrationTable::FieldName(MigrationTable::Fields::Id),
                                       fields);
    createTableCommnand.Execute();
    return createTableCommnand.IsValid();
}
bool MigrationTable::Migration::Rollback() const {
    return false;
}
QString MigrationTable::Migration::Description() const {
    return MIGRATION_TABLE_M_v1_DESCRIPTION;
}
size_t MigrationTable::Migration::Id() const {
    return MIGRATION_TABLE_M_v1_ID;
}
MigrationTable::IApi::IApi(ITableApi* owningTable)
    : m_table(owningTable)
{
}
size_t MigrationTable::IApi::LastAppliedMigrationId() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("LastAppliedMigrationId call: Api call when table is missing from db.");
    }
    QString idFieldName = MigrationTable::FieldName(MigrationTable::Fields::Id);
    SqlSelect selectCommand(m_table->GetDatabase(),
                            idFieldName,
                            m_table->Name());
    selectCommand.SetOrderBy(idFieldName + QString(" DESC LIMIT 1"));
    QList<QVariantList> result = selectCommand.Execute();
    if(selectCommand.IsValid() && result.size() == 1 && result.at(0).size() == 1) {
        return result.at(0).at(0).toUInt();
    } else {
        throw std::runtime_error("LastAppliedMigrationId call: Failed.");
    }
    return 0;
}
size_t MigrationTable::IApi::RegisterMigration(const QString& description, const QString& tableName) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("RegisterMigration call: Api call when table is missing from db.");
    }
    Values values({MigrationTable::FieldName(MigrationTable::Fields::Description),
                   MigrationTable::FieldName(MigrationTable::Fields::TargetTable)}, {description, tableName});
    SqlInsert insertCommand(m_table->GetDatabase(), MIGRATION_TABLE_NAME, values);
    insertCommand.Execute();
    if(!insertCommand.IsValid()) {
        throw std::runtime_error("RegisterMigration call: Failed.");
    }
    return LastAppliedMigrationId();
}
MigrationTable::MigrationTable(Database* database)
    : ITableApi(new Migration(this), database)
    , m_api(this)
{
}
QString MigrationTable::Name() const {
    return MIGRATION_TABLE_NAME;
}
const MigrationTable::IApi& MigrationTable::Api() const {
    return m_api;
}
size_t MigrationTable::FieldIndex(MigrationTable::Fields f) {
    return m_fields[f].first;
}
QString MigrationTable::FieldName(MigrationTable::Fields f) {
    return m_fields[f].second;
}
