#include "Headers/Database/Tables/LocationTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/SqlCommands/SqlSelect.h"

QMap<LocationTable::Fields, QPair<size_t, QString>> LocationTable::m_fields = {
     std::pair<Fields, QPair<size_t, QString>>(Fields::Id, QPair<size_t, QString>(0, "Id")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Abbreviation, QPair<size_t, QString>(1, "Abbreviation")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Name, QPair<size_t, QString>(2, "Name"))
};
LocationTable::Migration::Migration(ITableApi* table)
    : IMigration(table)
{
}
bool LocationTable::Migration::Migrate() const {
    FieldVector fields(3);
    fields[static_cast<int>(LocationTable::FieldIndex(LocationTable::Fields::Id))] =
        Field(LocationTable::FieldName(LocationTable::Fields::Id), SqlType::Integer, {});
    fields[static_cast<int>(LocationTable::FieldIndex(LocationTable::Fields::Abbreviation))] =
        Field(LocationTable::FieldName(LocationTable::Fields::Abbreviation), SqlType::Text, {SqlAttribute::NotNull});
    fields[static_cast<int>(LocationTable::FieldIndex((LocationTable::Fields::Name)))] =
        Field(LocationTable::FieldName(LocationTable::Fields::Name), SqlType::Text, {SqlAttribute::NotNull});

    QList<QStringList> data = {
        {"1", "al", "alabama"},        {"2", "ak", "alaska"},          {"3", "az", "arizona"},
        {"4", "ar", "arkansas"},       {"5", "ca", "california"},      {"6", "co", "colorado"},
        {"7", "ct", "connecticut"},    {"8", "de", "delaware"},        {"9", "fl", "florida"},
        {"10", "ga", "georgia"},       {"11", "hi", "hawaii"},         {"12", "id", "idaho"},
        {"13", "il", "illinois"},      {"14", "in", "indiana"},        {"15", "ia", "iowa"},
        {"16", "ks", "kansas"},        {"17", "ky", "kentucky"},       {"18", "la", "louisiana"},
        {"19", "me", "maine"},         {"20", "md", "maryland"},       {"21", "ma", "massachusetts"},
        {"22", "mi", "michigan"},      {"23", "mn", "minnesota"},      {"24", "ms", "mississippi"},
        {"25", "mo", "missouri"},      {"26", "mt", "montana"},        {"27", "ne", "nebraska"},
        {"28", "nv", "nevada"},        {"29", "nh", "new hampshire"},  {"30", "nj", "new jersey"},
        {"31", "nm", "new mexico"},    {"32", "ny", "new york"},       {"33", "nc", "north carolina"},
        {"34", "nd", "north dakota"},  {"35", "oh", "ohio"},           {"36", "ok", "oklahoma"},
        {"37", "or", "oregon"},        {"38", "pa", "pennsylvania"},   {"39", "ri", "rhode island"},
        {"40", "sc", "south carolina"},{"41", "sd", "south dakota"},   {"42", "tn", "tennessee"},
        {"43", "tx", "texas"},         {"44", "ut", "utah"},           {"45", "vt", "vermont"},
        {"46", "va", "virginia"},      {"47", "wa", "washington"},     {"48", "wv", "west virginia"},
        {"49", "wi", "wisconsin"},     {"50", "wy", "wyoming"},        {"51", "vaal", "alberta"},
        {"52", "wimt", "montreal"},    {"53", "njnb", "new brunswick"},{"54", "caon", "ontario"},
        {"55", "cosd", "south denver"}
    };
    return m_table->CreateDataTable(fields, data, LocationTable::FieldName(LocationTable::Fields::Id));
}
bool LocationTable::Migration::Rollback() const {
    return false;
}
QString LocationTable::Migration::Description() const {
    return LOCATION_TABLE_M_v1_DESCRIPTION;
}
size_t LocationTable::Migration::Id() const {
    return LOCATION_TABLE_M_v1_ID;
}
LocationTable::IApi::IApi(ITableApi* owningTable)
    : m_table(owningTable)
{
}
QList<QMap<LocationTable::Fields, QVariant>> LocationTable::IApi::GetLocationData() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetLocationData call: Api call when table is missing from db.");
    }
    QString fieldsToRetrieve =
            LocationTable::FieldName(LocationTable::Fields::Id) + ", " +
            LocationTable::FieldName(LocationTable::Fields::Abbreviation) + ", " +
            LocationTable::FieldName(LocationTable::Fields::Name);
    SqlSelect selectCommand(m_table->GetDatabase(),
                            fieldsToRetrieve,
                            m_table->Name());
    selectCommand.SetOrderBy(LocationTable::FieldName(LocationTable::Fields::Id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        QList<QMap<LocationTable::Fields, QVariant>> result;
        for(auto entry : rawResult) {
            QMap<LocationTable::Fields, QVariant> singleRecord;
            singleRecord.insert(LocationTable::Fields::Id, entry.at(0));
            singleRecord.insert(LocationTable::Fields::Abbreviation, entry.at(1));
            singleRecord.insert(LocationTable::Fields::Name, entry.at(2));
            result.append(singleRecord);
        }
        return result;
   } else {
        throw std::runtime_error("GetLocationData call: Failed.");
    }
}
QString LocationTable::IApi::GetLocationAbbreviationById(size_t id) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetLocationAbbreviationById call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            LocationTable::FieldName(LocationTable::Fields::Abbreviation),
                            m_table->Name());
    selectCommand.SetCondition("Id=" + QString::number(id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        if(rawResult.size() != 1) {
            throw std::runtime_error("GetLocationAbbreviationById call: Multiple records or empty data returned.");
        }
        return rawResult.at(0).at(0).toString();
    } else {
        throw std::runtime_error("GetLocationAbbreviationById call: Failed.");
    }
}
QString LocationTable::IApi::GetLocationNameById(size_t id) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetLocationNameById call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            LocationTable::FieldName(LocationTable::Fields::Name),
                            m_table->Name());
    selectCommand.SetCondition("Id=" + QString::number(id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        if(rawResult.size() != 1) {
            throw std::runtime_error("GetLocaitonNameById call: Multiple records or empty data returned.");
        }
        return rawResult.at(0).at(0).toString();
    } else {
        throw std::runtime_error("GetLocationNameById call: Failed.");
    }
}
LocationTable::LocationTable(Database* database)
    : ITableApi(new Migration(this), database)
    , m_api(this)
{
}
LocationTable::~LocationTable()
{
}
QString LocationTable::Name() const {
    return LOCATION_TABLE_NAME;
}
const LocationTable::IApi& LocationTable::Api() const {
    return m_api;
}
size_t LocationTable::FieldIndex(LocationTable::Fields f) {
    return m_fields[f].first;
}
QString LocationTable::FieldName(LocationTable::Fields f) {
    return m_fields[f].second;
}
