#include "Headers/Database/Tables/LotCommentsTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/Tables/LotRecordsTableApi.h"
#include "Headers/Database/SqlCommands/SqlSelect.h"
#include "Headers/Database/SqlCommands/SqlDelete.h"
#include "Headers/Database/SqlCommands/SqlUpdate.h"
#include "Headers/Database/SqlCommands/SqlInsert.h"
#include "Headers/Tools.h"
#include <QMutex>

QMap<LotCommentsTable::Fields, QPair<size_t, QString>> LotCommentsTable::m_fields = {
     std::pair<Fields, QPair<size_t, QString>>(Fields::Id, QPair<size_t, QString>(0, "Id")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::RecordId, QPair<size_t, QString>(1, "RecordId")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::CommentDate, QPair<size_t, QString>(2, "CommentDate")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Comment, QPair<size_t, QString>(3, "Comment")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::EditDate, QPair<size_t, QString>(4, "EditDate"))
};
LotCommentsTable::Migration::Migration(ITableApi *table)
    : IMigration(table)
{
}
bool LotCommentsTable::Migration::Migrate() const {
    FieldVector fields(5);
    fields[static_cast<int>(LotCommentsTable::FieldIndex(LotCommentsTable::Fields::Id))] =
        Field(LotCommentsTable::FieldName(LotCommentsTable::Fields::Id), SqlType::Integer, {});
    fields[static_cast<int>(LotCommentsTable::FieldIndex(LotCommentsTable::Fields::RecordId))] =
        Field(LotCommentsTable::FieldName(LotCommentsTable::Fields::RecordId), SqlType::Integer, {});
    fields[static_cast<int>(LotCommentsTable::FieldIndex(LotCommentsTable::Fields::CommentDate))] =
        Field(LotCommentsTable::FieldName(LotCommentsTable::Fields::CommentDate), SqlType::Date, {SqlAttribute::NotNull});
    fields[static_cast<int>(LotCommentsTable::FieldIndex(LotCommentsTable::Fields::Comment))] =
        Field(LotCommentsTable::FieldName((LotCommentsTable::Fields::Comment)), SqlType::Text, {SqlAttribute::NotNull});
    fields[static_cast<int>(LotCommentsTable::FieldIndex(LotCommentsTable::Fields::EditDate))] =
        Field(LotCommentsTable::FieldName(LotCommentsTable::Fields::EditDate), SqlType::Date, {});

    ForeignKeyList fKeys = {
        ForeignKey(LotCommentsTable::FieldName(LotCommentsTable::Fields::RecordId),
                   LOTRECORDS_TABLE_NAME,
                   LotRecordsTable::FieldName(LotRecordsTable::Fields::Id))
    };
    return m_table->CreateDataTable(fields, {}, LotCommentsTable::FieldName(LotCommentsTable::Fields::Id), fKeys);
}
bool LotCommentsTable::Migration::Rollback() const {
    return false;
}
QString LotCommentsTable::Migration::Description() const {
    return LOTCOMMENTS_TABLE_M_v1_DESCRIPTION;
}
size_t LotCommentsTable::Migration::Id() const {
    return LOTCOMMENTS_TABLE_M_v1_ID;
}
LotCommentsTable::IApi::IApi(ITableApi* owningTable)
    : m_table(owningTable)
{
}
QList<Comment> LotCommentsTable::IApi::GetCommentsByRecordId(size_t recordId) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetCommentsByRecordId call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            commentFieldsToRetrieve(),
                            m_table->Name());
    selectCommand.SetOrderBy(FieldName(Fields::Id));
    selectCommand.SetCondition(FieldName(Fields::RecordId) + "=" + QString::number(recordId));
    QList<QVariantList> commandResult = selectCommand.Execute();
    if(!selectCommand.IsValid()) {
        throw std::runtime_error("GetCommentsByRecordId call: Failed.");
    } else {
        QList<Comment> result;
        for(auto var : commandResult) {
            result.push_back(buildComment(var));
        }
        return result;
    }
}
Comment LotCommentsTable::IApi::GetCommentById(size_t id) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetCommentById call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            commentFieldsToRetrieve(),
                            m_table->Name());
    selectCommand.SetCondition(FieldName(Fields::Id) + "=" + QString::number(id));
    QList<QVariantList> commandResult = selectCommand.Execute();
    if(!selectCommand.IsValid()) {
        throw std::runtime_error("GetCommentById call: Failed.");
    } else {
        if(commandResult.size() != 1) {
            throw std::runtime_error("GetCommentById call: Id duplication.");
        }
        return buildComment(commandResult.at(0));
    }

}
void LotCommentsTable::IApi::RemoveCommentByRecordId(size_t recordId) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("RemoveCommentByRecordId call: Api call when table is missing from db.");
    }
    SqlDelete deleteCommand(m_table->GetDatabase(),
                            m_table->Name(),
                            FieldName(Fields::RecordId) + "=" + QString::number(recordId));
    deleteCommand.Execute();
    if(!deleteCommand.IsValid()) {
        throw std::runtime_error("RemoveCommentByRecordId call: Failed.");
    }
}
void LotCommentsTable::IApi::RemoveCommentById(size_t id) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("RemoveCommentById call: Api call when table is missing from db.");
    }
    SqlDelete deleteCommand(m_table->GetDatabase(),
                            m_table->Name(),
                            FieldName(Fields::Id) + "=" + QString::number(id));
    deleteCommand.Execute();
    if(!deleteCommand.IsValid()) {
        throw std::runtime_error("RemoveCommentById call: Failed.");
    }
}
void LotCommentsTable::IApi::EditComment(size_t id, const QString& newText, const QDate& editDate) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("EditComment call: Api call when table is missing from db.");
    }
    QList<QPair<QString, QString>> fields;
    fields.append(QPair<QString, QString>(FieldName(Fields::Comment), newText.toUtf8().toBase64()));
    fields.append(QPair<QString, QString>(FieldName(Fields::EditDate), editDate.toString(Qt::ISODate)));
    SqlUpdate updateCommand(m_table->GetDatabase(),
                            m_table->Name(),
                            fields);
    updateCommand.SetCondition(FieldName(Fields::Id) + "=" + QString::number(id));
    updateCommand.Execute();
    if(!updateCommand.IsValid()) {
        throw std::runtime_error("EditComemnt call: Failed.");
    }
}
size_t LotCommentsTable::IApi::InsertComment(const Comment& comment) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("InsertComment call: Api call when table is missing from db.");
    }
    static QMutex mutex;
    mutex.lock();
    QStringList fields = {
        FieldName(Fields::RecordId),
        FieldName(Fields::Comment),
        FieldName(Fields::CommentDate)
    };
    QStringList fieldValues = {
        QString::number(comment.RecordId()), comment.Text().toUtf8().toBase64(), comment.CreatedDate().toString(Qt::ISODate)
    };
    Values values(fields, fieldValues);
    SqlInsert insertCommand(m_table->GetDatabase(), m_table->Name(), values);
    insertCommand.Execute();
    if(!insertCommand.IsValid()) {
        mutex.unlock();
        throw std::runtime_error("InsertComment call (stage 1): Failed.");
    } else {
        SqlSelect selectCommand(m_table->GetDatabase(),
                                FieldName(Fields::Id),
                                m_table->Name());
        selectCommand.SetOrderBy(FieldName(Fields::Id) + " DESC LIMIT 1");
        QList<QVariantList> rawResult = selectCommand.Execute();
        if(selectCommand.IsValid() && rawResult.size() == 1 && rawResult.at(0).size() == 1) {
            mutex.unlock();
            return rawResult.at(0).at(0).toUInt();
        } else {
            mutex.unlock();
            throw std::runtime_error("InsertComment call (stage 2): Failed.");
        }
    }
}
size_t LotCommentsTable::IApi::GetCommentQtyForRecord(const size_t recordId) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetCommentQtyForRecord call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            "COUNT(" + FieldName(Fields::Id) + ")",
                            m_table->Name());
    selectCommand.SetCondition(FieldName(Fields::RecordId) + "=" + QString::number(recordId));
    QList<QVariantList> result = selectCommand.Execute();
    if(selectCommand.IsValid() && result.size() == 1 && result.at(0).size() == 1) {
        return result.at(0).at(0).toUInt();
    } else {
        throw std::runtime_error("GetCommentQtyForRecord call: Failed.");
    }
}
QString LotCommentsTable::IApi::commentFieldsToRetrieve() const {
    static QString fieldsToRetrieve =
        FieldName(Fields::Id) + ", " +
        FieldName(Fields::RecordId) + ", " +
        FieldName(Fields::CommentDate) + ", " +
        FieldName(Fields::Comment) + ", " +
        FieldName(Fields::EditDate);
     return fieldsToRetrieve;
}
Comment LotCommentsTable::IApi::buildComment(const QVariantList& rawComment) const {
    int fieldOffset = 0;
    QDate editDate;
    bool isEdited = false;
    if(!rawComment.at(fieldOffset + 4).isNull()) {
        isEdited = true;
        editDate = rawComment.at(fieldOffset + 4).toDate();
    }
    Comment result = Comment(rawComment.at(fieldOffset + 1).toUInt(),
                             QByteArray::fromBase64(rawComment.at(fieldOffset + 3).toString().toUtf8()),
                             rawComment.at(fieldOffset + 2).toDate(),
                             isEdited,
                             editDate);
    result.SetId(rawComment.at(fieldOffset + 0).toUInt());
    return result;
}
LotCommentsTable::LotCommentsTable(Database* database)
    : ITableApi(new Migration(this), database)
    , m_api(this)
{
}
LotCommentsTable::~LotCommentsTable()
{
}
QString LotCommentsTable::Name() const {
    return LOTCOMMENTS_TABLE_NAME;
}
const LotCommentsTable::IApi& LotCommentsTable::Api() const {
    return m_api;
}
size_t LotCommentsTable::FieldIndex(LotCommentsTable::Fields f) {
    return m_fields[f].first;
}
QString LotCommentsTable::FieldName(LotCommentsTable::Fields f) {
    return m_fields[f].second;
}
