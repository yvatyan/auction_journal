#include "Headers/Database/Tables/LotRecordsTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/SqlCommands/SqlInsert.h"
#include "Headers/Database/SqlCommands/SqlSelect.h"
#include "Headers/Database/SqlCommands/SqlDelete.h"
#include "Headers/Database/SqlCommands/SqlUpdate.h"
#include "Headers/LotBuilder.h"
#include "Headers/Database/Tables/MakeTableApi.h"
#include "Headers/Database/Tables/ModelTableApi.h"
#include "Headers/Database/Tables/TransmissionTableApi.h"
#include "Headers/Database/Tables/BodyTableApi.h"
#include "Headers/Database/Tables/PurposeTableApi.h"
#include "Headers/Database/Tables/LocationTableApi.h"
#include "Headers/Database/Tables/SaleStatusTableApi.h"
#include "Headers/Database/Tables/SoldStatusTableApi.h"

QMap<LotRecordsTable::Fields, QPair<size_t, QString>> LotRecordsTable::m_fields = {
     std::pair<Fields, QPair<size_t, QString>>(Fields::Id, QPair<size_t, QString>(0, "Id")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::LotId, QPair<size_t, QString>(1, "LotId")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::MakeId, QPair<size_t, QString>(2, "MakeId")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::ModelId, QPair<size_t, QString>(3, "ModelId")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Year, QPair<size_t, QString>(4, "Year")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::TransmissionId, QPair<size_t, QString>(5, "TransmissionId")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::BodyId, QPair<size_t, QString>(6, "BodyId")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::PurposeId, QPair<size_t, QString>(7, "PurposeId")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::LocationId, QPair<size_t, QString>(8, "LocationId")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::WatchDate, QPair<size_t, QString>(9, "WatchDate")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::AuctionDate, QPair<size_t, QString>(10, "AuctionDate")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::SaleStatusId, QPair<size_t, QString>(11, "SaleStatusId")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::SoldStatusId, QPair<size_t, QString>(12, "SoldStatusId")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::SoldPrice, QPair<size_t, QString>(13, "SoldPrice")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::WitPrice, QPair<size_t, QString>(14, "WitPrice")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::WorksheetPath, QPair<size_t, QString>(15, "WorksheetPath")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Missed, QPair<size_t, QString>(16, "Missed"))
};
LotRecordsTable::Migration::Migration(ITableApi* table)
    : IMigration(table)
{
}
bool LotRecordsTable::Migration::Migrate() const {
    FieldVector fields(17);
    fields[static_cast<int>(FieldIndex(Fields::Id))] =
        Field(FieldName(Fields::Id), SqlType::Integer, {SqlAttribute::AutoIncrement});
    fields[static_cast<int>(FieldIndex(Fields::LotId))] =
        Field(FieldName(Fields::LotId), SqlType::Integer, {});
    fields[static_cast<int>(FieldIndex(Fields::MakeId))] =
        Field(FieldName(Fields::MakeId), SqlType::Integer, {});
    fields[static_cast<int>(FieldIndex(Fields::ModelId))] =
        Field(FieldName(Fields::ModelId), SqlType::Integer, {});
    fields[static_cast<int>(FieldIndex(Fields::Year))] =
        Field(FieldName(Fields::Year), SqlType::Integer, {SqlAttribute::NotNull});
    fields[static_cast<int>(FieldIndex(Fields::TransmissionId))] =
        Field(FieldName(Fields::TransmissionId), SqlType::Integer, {});
    fields[static_cast<int>(FieldIndex(Fields::BodyId))] =
        Field(FieldName(Fields::BodyId), SqlType::Integer, {});
    fields[static_cast<int>(FieldIndex(Fields::PurposeId))] =
        Field(FieldName(Fields::PurposeId), SqlType::Integer, {});
    fields[static_cast<int>(FieldIndex(Fields::LocationId))] =
        Field(FieldName(Fields::LocationId), SqlType::Integer, {});
    fields[static_cast<int>(FieldIndex(Fields::WatchDate))] =
        Field(FieldName(Fields::WatchDate), SqlType::Date, {SqlAttribute::NotNull});
    fields[static_cast<int>(FieldIndex(Fields::AuctionDate))] =
        Field(FieldName(Fields::AuctionDate), SqlType::Date, {});
    fields[static_cast<int>(FieldIndex(Fields::SaleStatusId))] =
        Field(FieldName(Fields::SaleStatusId), SqlType::Integer, {});
    fields[static_cast<int>(FieldIndex(Fields::SoldStatusId))] =
        Field(FieldName(Fields::SoldStatusId), SqlType::Integer, {});
    fields[static_cast<int>(FieldIndex(Fields::SoldPrice))] =
        Field(FieldName(Fields::SoldPrice), SqlType::Integer, {});
    fields[static_cast<int>(FieldIndex(Fields::WitPrice))] =
        Field(FieldName(Fields::WitPrice), SqlType::Integer, {});
    fields[static_cast<int>(FieldIndex(Fields::WorksheetPath))] =
        Field(FieldName(Fields::WorksheetPath), SqlType::Text, {SqlAttribute::NotNull});
    fields[static_cast<int>(FieldIndex(Fields::Missed))] =
        Field(FieldName(Fields::Missed), SqlType::Boolean, {SqlAttribute::NotNull});

    ForeignKeyList fKeys = {
        ForeignKey(FieldName(Fields::MakeId),
                   MAKE_TABLE_NAME,
                   MakeTable::FieldName(MakeTable::Fields::Id)),
        ForeignKey(FieldName(Fields::MakeId),
                   MODEL_TABLE_NAME,
                   ModelTable::FieldName(ModelTable::Fields::Id)),
        ForeignKey(FieldName(Fields::TransmissionId),
                   TRANSMISSION_TABLE_NAME,
                   TransmissionTable::FieldName(TransmissionTable::Fields::Id)),
        ForeignKey(FieldName(Fields::BodyId),
                   BODY_TABLE_NAME,
                   BodyTable::FieldName(BodyTable::Fields::Id)),
        ForeignKey(FieldName(Fields::PurposeId),
                   PURPOSE_TABLE_NAME,
                   PurposeTable::FieldName(PurposeTable::Fields::Id)),
        ForeignKey(FieldName(Fields::LocationId),
                   LOCATION_TABLE_NAME,
                   LocationTable::FieldName(LocationTable::Fields::Id)),
        ForeignKey(FieldName(Fields::SaleStatusId),
                   SALESTATUS_TABLE_NAME,
                   SaleStatusTable::FieldName(SaleStatusTable::Fields::Id)),
        ForeignKey(FieldName(Fields::SoldStatusId),
                   SOLDSTATUS_TABLE_NAME,
                   SoldStatusTable::FieldName(SoldStatusTable::Fields::Id))
    };
    return m_table->CreateDataTable(fields, {}, FieldName(Fields::Id), fKeys);
}
bool LotRecordsTable::Migration::Rollback() const {
    return false;
}
QString LotRecordsTable::Migration::Description() const {
    return LOTRECORDS_TABLE_M_v1_DESCRIPTION;
}
size_t LotRecordsTable::Migration::Id() const {
    return LOTRECORDS_TABLE_M_v1_ID;
}
LotRecordsTable::IApi::IApi(ITableApi* owningTable)
    : m_table(owningTable)
{
}
size_t LotRecordsTable::IApi::InsertLot(const Lot& lot) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("InsertLot call: Api call when table is missing from db.");
    }
    QStringList fields = {
        FieldName(Fields::LotId),
        FieldName(Fields::MakeId),
        FieldName(Fields::ModelId),
        FieldName(Fields::Year),
        FieldName(Fields::TransmissionId),
        FieldName(Fields::BodyId),
        FieldName(Fields::PurposeId),
        FieldName(Fields::LocationId),
        FieldName(Fields::WatchDate),
        FieldName(Fields::AuctionDate),
        FieldName(Fields::SaleStatusId),
        FieldName(Fields::SoldStatusId),
        FieldName(Fields::SoldPrice),
        FieldName(Fields::WitPrice),
        FieldName(Fields::WorksheetPath),
        FieldName(Fields::Missed)
    };
    QStringList fieldValues = {
        QString::number(lot.LotId()), QString::number(lot.MakeId()), QString::number(lot.ModelId()),
        QString::number(lot.Year()), QString::number(lot.TransmissionId()), QString::number(lot.BodyId()),
        QString::number(lot.PurposeId()), QString::number(lot.LocationId()), lot.WatchDate().toString(Qt::ISODate),
        lot.AuctionDate().toString(Qt::ISODate), QString::number(lot.SaleStatusId()), QString::number(lot.SoldStatusId()),
        QString::number(lot.SoldPrice()), QString::number(lot.WantItTodayPrice()), lot.WorksheetPath(),
        QString::number(static_cast<int>(lot.IsAuctionMissed()))
    };
    Values values(fields, fieldValues);
    SqlInsert insertCommand(m_table->GetDatabase(), m_table->Name(), values);
    insertCommand.Execute();
    if(!insertCommand.IsValid()) {
        throw std::runtime_error("InsertLot call (stage 1): Failed.");
    } else {
        SqlSelect selectCommand(m_table->GetDatabase(),
                                FieldName(Fields::Id),
                                m_table->Name());
        selectCommand.SetCondition(FieldName(Fields::LotId) + "=" + QString::number(lot.LotId()));
        selectCommand.SetOrderBy(FieldName(Fields::Id) + " DESC LIMIT 1");
        QList<QVariantList> rawResult = selectCommand.Execute();
        if(selectCommand.IsValid() && rawResult.size() == 1 && rawResult.at(0).size() == 1) {
            return rawResult.at(0).at(0).toUInt();
        } else {
            throw std::runtime_error("InsertLot call (stage 2): Failed.");
        }
    }
}
QList<Lot> LotRecordsTable::IApi::GetAllLastLots() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetAllLastLots call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            lotFieldsToRetrieve(),
                            m_table->Name());
    selectCommand.SetOrderBy(FieldName(Fields::Id));
    selectCommand.SetCondition(FieldName(Fields::SoldStatusId) + "=" + QString::number(SoldStatusTable::Sold) +
                      " or " + FieldName(Fields::SoldStatusId) + "=" + QString::number(SoldStatusTable::Canceled) +
                      " or " + FieldName(Fields::SoldStatusId) + "=" + QString::number(SoldStatusTable::WA) +
                      " or " + FieldName(Fields::SoldStatusId) + "=" + QString::number(SoldStatusTable::Pending));

    QList<QVariantList> commandResult = selectCommand.Execute();
    if(!selectCommand.IsValid()) {
        throw std::runtime_error("GetAllLastLots call: Failed.");
    } else {
        return buildLot(commandResult, 0);
    }
}
bool LotRecordsTable::IApi::LotExistInDb(size_t lotId) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetLot call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            FieldName(Fields::LotId),
                            m_table->Name());
    selectCommand.SetCondition(FieldName(Fields::LotId) + "=" + QString::number(lotId));
    QList<QVariantList> result = selectCommand.Execute();
    if(!selectCommand.IsValid()) {
        throw std::runtime_error("SelectLot call: Failed.");
    }
    return result.size() > 0;
}
void LotRecordsTable::IApi::RemoveLotEntries(size_t lotId) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("RemoveLotEntries call: Api call when table is missing from db.");
    }
    SqlDelete deleteCommand(m_table->GetDatabase(),
                            m_table->Name(),
                            FieldName(Fields::LotId) + "=" + QString::number(lotId));
    deleteCommand.Execute();
    if(!deleteCommand.IsValid()) {
        throw std::runtime_error("RemoveLotEntries call: Failed.");
    }
}
void LotRecordsTable::IApi::RemoveLotRecord(size_t recordId) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("RemoveLotRecord call: Api call when table is missing from db.");
    }
    SqlDelete deleteCommand(m_table->GetDatabase(),
                            m_table->Name(),
                            FieldName(Fields::Id) + "=" + QString::number(recordId));
    deleteCommand.Execute();
    if(!deleteCommand.IsValid()) {
        throw std::runtime_error("RemoveLotRecord call: Failed.");
    }
}
size_t LotRecordsTable::IApi::GetLotEntriesQty(size_t lotId) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetLotEntriesQty call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            "COUNT(" + FieldName(Fields::LotId) + ")",
                            m_table->Name());
    selectCommand.SetCondition(FieldName(Fields::LotId) + "=" + QString::number(lotId));
    QList<QVariantList> result = selectCommand.Execute();
    if(selectCommand.IsValid() && result.size() == 1 && result.at(0).size() == 1) {
        return result.at(0).at(0).toUInt();
    } else {
        throw std::runtime_error("GetLotEntriesQty call: Failed.");
    }
}
QList<Lot> LotRecordsTable::IApi::GetAllLotEntries(size_t lotId) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetAllLotEntries call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            lotFieldsToRetrieve(),
                            m_table->Name());
    selectCommand.SetOrderBy(FieldName(Fields::Id));
    selectCommand.SetCondition(FieldName(Fields::LotId) + "=" + QString::number(lotId));
    QList<QVariantList> commandResult = selectCommand.Execute();
    if(!selectCommand.IsValid()) {
        throw std::runtime_error("GetAllLotEntries call: Failed.");
    } else {
        return buildLot(commandResult, 0);
    }
}
void LotRecordsTable::IApi::AlterFinalFields(const Lot& finalizedLot) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("AlterFinalFields call: Api call when table is missing from db.");
    }
    QList<QPair<QString, QString>> fields;
    fields.append(QPair<QString, QString>(FieldName(Fields::SoldStatusId), QString::number(finalizedLot.SoldStatusId())));
    fields.append(QPair<QString, QString>(FieldName(Fields::SoldPrice), QString::number(finalizedLot.SoldPrice())));
    fields.append(QPair<QString, QString>(FieldName(Fields::Missed), QString::number(static_cast<int>(finalizedLot.IsAuctionMissed()))));
    SqlUpdate updateCommand(m_table->GetDatabase(),
                            m_table->Name(),
                            fields);
    updateCommand.SetCondition(FieldName(Fields::Id) + "=" + QString::number(finalizedLot.GetRecordId()));
    updateCommand.Execute();
    if(!updateCommand.IsValid()) {
        throw std::runtime_error("AlterFinalFields call: Failed.");
    }
}
QList<size_t> LotRecordsTable::IApi::GetRecordIdsForLotId(size_t lotId) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetRecordIdsForLotId call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            FieldName(Fields::Id),
                            m_table->Name());
    selectCommand.SetOrderBy(FieldName(Fields::Id));
    selectCommand.SetCondition(FieldName(Fields::LotId) + "=" + QString::number(lotId));
    QList<QVariantList> commandResult = selectCommand.Execute();
    if(!selectCommand.IsValid()) {
        throw std::runtime_error("GetRecordIdsForLotId call: Failed.");
    } else {
        QList<size_t> result;
        for(auto recordId : commandResult) {
            if(recordId.size() != 1) {
                throw std::runtime_error("GetRecordIdsForLotId call: Invalid result.");
            }
            result.push_back(recordId.at(0).toUInt());
        }
        return result;
    }
}
QString LotRecordsTable::IApi::GetWorksheetPathByRecordId(size_t recordId) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetWorksheetPathByRecordId call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            FieldName(Fields::WorksheetPath),
                            m_table->Name());
    selectCommand.SetCondition(FieldName(Fields::Id) + "=" + QString::number(recordId));
    QList<QVariantList> commandResult = selectCommand.Execute();
    if(!selectCommand.IsValid() || commandResult.size() == 0 || commandResult.at(0).size() == 0) {
        throw std::runtime_error("GetWorksheetPathByRecordId call: Failed.");
    } else {
        return commandResult.at(0).at(0).toString();
    }
}
QList<size_t> LotRecordsTable::IApi::GetAllDistinctLotIds() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetAllDifferentLotIds call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            FieldName(Fields::LotId),
                            m_table->Name());
    selectCommand.SetOrderBy(FieldName(Fields::LotId));
    selectCommand.SetDistinctFlag(true);
    QList<QVariantList> commandResult = selectCommand.Execute();
    if(!selectCommand.IsValid()) {
        throw std::runtime_error("GetAllDifferentLotIds call: Failed.");
    } else {
        QList<size_t> result;
        for(int i = 0; i < commandResult.size(); ++i) {
            if(commandResult.at(i).size() != 1) {
                throw std::runtime_error("GetAllDifferentLotIds call: Invalid result.");
            }
            result.append(commandResult.at(i).at(0).toUInt());
        }
        return result;
    }
}
QString LotRecordsTable::IApi::lotFieldsToRetrieve() const {
     static QString fieldsToRetrieve =
        FieldName(Fields::LotId) + ", " +
        FieldName(Fields::MakeId) + ", " +
        FieldName(Fields::ModelId) + ", " +
        FieldName(Fields::Year) + ", " +
        FieldName(Fields::TransmissionId) + ", " +
        FieldName(Fields::BodyId) + ", " +
        FieldName(Fields::PurposeId) + ", " +
        FieldName(Fields::LocationId) + ", " +
        FieldName(Fields::WatchDate) + ", " +
        FieldName(Fields::AuctionDate) + ", " +
        FieldName(Fields::SaleStatusId) + ", " +
        FieldName(Fields::SoldStatusId) + ", " +
        FieldName(Fields::SoldPrice) + ", " +
        FieldName(Fields::WitPrice) + ", " +
        FieldName(Fields::WorksheetPath) + ", " +
        FieldName(Fields::Missed) + ", " +
        FieldName(Fields::Id);
     return fieldsToRetrieve;
}

QList<Lot> LotRecordsTable::IApi::buildLot(const QList<QVariantList>& commandResult, int fieldOffset) const {
    LotBuilder builder;
    QList<Lot> result;
    // NOTE: Field indicies should match with the ones in lotFieldsToRetrieve() function.
    for(auto varList : commandResult) {
       builder.SetLotId(varList.at(fieldOffset).toUInt());
       builder.SetMakeId(varList.at(fieldOffset + 1).toInt());
       builder.SetModelId(varList.at(fieldOffset + 2).toInt());
       builder.SetYear(varList.at(fieldOffset + 3).toUInt());
       builder.SetTransmissionId(varList.at(fieldOffset + 4).toUInt());
       builder.SetBodyId(varList.at(fieldOffset + 5).toUInt());
       builder.SetPurposeId(varList.at(fieldOffset + 6).toUInt());
       builder.SetLocationId(varList.at(fieldOffset + 7).toUInt());
       builder.SetWatchDate(varList.at(fieldOffset + 8).toDate());
       builder.SetAuctionDate(varList.at(fieldOffset + 9).toDate());
       builder.SetSaleStatusId(varList.at(fieldOffset + 10).toUInt());
       builder.SetSoldStatusId(varList.at(fieldOffset + 11).toUInt());
       builder.SetSoldPrice(varList.at(fieldOffset + 12).toUInt());
       builder.SetWantItTodayPrice(varList.at(fieldOffset + 13).toUInt());
       builder.SetWorksheetPath(varList.at(fieldOffset + 14).toString());
       builder.SetMissedFlag(varList.at(fieldOffset + 15).toBool());

       Lot lot = builder.ConstructLot();
       lot.SetRecordId(varList.at(fieldOffset + 16).toUInt());
       result.append(lot);
       builder.ResetBuilder();
    }
    return result;
}
LotRecordsTable::LotRecordsTable(Database* database)
    : ITableApi(new Migration(this), database)
    , m_api(this)
{
}
LotRecordsTable::~LotRecordsTable()
{
}
QString LotRecordsTable::Name() const {
    return LOTRECORDS_TABLE_NAME;
}
const LotRecordsTable::IApi& LotRecordsTable::Api() const {
    return m_api;
}
size_t LotRecordsTable::FieldIndex(LotRecordsTable::Fields f) {
    return m_fields[f].first;
}
QString LotRecordsTable::FieldName(LotRecordsTable::Fields f) {
    return m_fields[f].second;
}
