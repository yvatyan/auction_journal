#include "Headers/Database/Tables/ModelTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/SqlCommands/SqlSelect.h"
#include "Headers/Database/SqlCommands/SqlInsert.h"
#include "Headers/Database/Tables/MakeTableApi.h"

QMap<ModelTable::Fields, QPair<size_t, QString>> ModelTable::m_fields = {
     std::pair<Fields, QPair<size_t, QString>>(Fields::Id, QPair<size_t, QString>(0, "Id")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Name, QPair<size_t, QString>(1, "Name")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::MakeId, QPair<size_t, QString>(2, "MakeId"))
};
ModelTable::Migration::Migration(ITableApi *table)
    : IMigration(table)
{
}
bool ModelTable::Migration::Migrate() const {
    FieldVector fields(3);
    fields[static_cast<int>(ModelTable::FieldIndex(ModelTable::Fields::Id))] =
        Field(ModelTable::FieldName(ModelTable::Fields::Id), SqlType::Integer, {});
    fields[static_cast<int>(ModelTable::FieldIndex(ModelTable::Fields::Name))] =
        Field(ModelTable::FieldName(ModelTable::Fields::Name), SqlType::Text, {SqlAttribute::NotNull});
    fields[static_cast<int>(ModelTable::FieldIndex(ModelTable::Fields::MakeId))] =
        Field(ModelTable::FieldName(ModelTable::Fields::MakeId), SqlType::Integer, {SqlAttribute::NotNull});

    QList<QStringList> data = {
        {"1", "db9", "1"},
        {"2", "db11", "1"},
        {"3", "gran \bturismo", "5"},
        {"4", "gran \bturismo m \bc", "5"},
        {"5", "gran \bturismo m \bc sport line", "5"},
        {"6", "gran \bturismo m \bc stradale", "5"},
        {"7", "gran \bturismo s", "5"},
        {"8", "gran \bturismo sport", "5"},
        {"9", "raptor", "3"},
        {"10", "wrangler gen.1( \by \bj)", "4"},
        {"11", "wrangler gen.2( \bt \bj)", "4"},
        {"12", "wrangler gen.3( \bj \bk), rubicon", "4"},
        {"13", "wrangler gen.3( \bj \bk), sahara", "4"},
        {"14", "wrangler gen.3( \bj \bk), unlimited rubicon", "4"},
        {"15", "wrangler gen.3( \bj \bk), unlimited sport", "4"},
        {"16", "wrangler gen.3( \bj \bk), x", "4"},
        {"17", "wrangler gen.4( \bj \bl), unlimited rubicon", "4"},
        {"18", "wrangler gen.4( \bj \bl), unlimited sport", "4"}
    };
    ForeignKeyList fKeys = {
        ForeignKey(ModelTable::FieldName(ModelTable::Fields::MakeId),
                   MAKE_TABLE_NAME,
                   MakeTable::FieldName(MakeTable::Fields::Id))
    };
    return m_table->CreateDataTable(fields, data, ModelTable::FieldName(ModelTable::Fields::Id), fKeys);
}
bool ModelTable::Migration::Rollback() const {
    return false;
}
QString ModelTable::Migration::Description() const {
    return MODEL_TABLE_M_v1_DESCRIPTION;
}
size_t ModelTable::Migration::Id() const {
    return MODEL_TABLE_M_v1_ID;
}
ModelTable::IApi::IApi(ITableApi* owningTable)
    : m_table(owningTable)
{
}
QList<QMap<ModelTable::Fields, QVariant>> ModelTable::IApi::GetModelDataByMakeId(size_t makeId) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetModelDataByMake call: Api call when table is missing from db.");
    }
    QString fieldsToRetrieve =
            ModelTable::FieldName(ModelTable::Fields::Id) + ", " +
            ModelTable::FieldName(ModelTable::Fields::Name) + ", " +
            ModelTable::FieldName(ModelTable::Fields::MakeId);
    SqlSelect selectCommand(m_table->GetDatabase(),
                             fieldsToRetrieve,
                             m_table->Name());
    selectCommand.SetCondition(ModelTable::FieldName(ModelTable::Fields::MakeId) + "=" + QString::number(makeId));
    selectCommand.SetOrderBy(ModelTable::FieldName(ModelTable::Fields::Id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
       QList<QMap<ModelTable::Fields, QVariant>> result;
       for(auto entry : rawResult) {
           QMap<ModelTable::Fields, QVariant> singleRecord;
           singleRecord.insert(ModelTable::Fields::Id, entry.at(0));
           singleRecord.insert(ModelTable::Fields::Name, entry.at(1));
           singleRecord.insert(ModelTable::Fields::MakeId, entry.at(2));
           result.append(singleRecord);
       }
       return result;
    } else {
        throw std::runtime_error("GetModelDataByMake call: Failed.");
    }
}
QString ModelTable::IApi::GetModelNameById(size_t id) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetModelNameById call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            ModelTable::FieldName(ModelTable::Fields::Name),
                            m_table->Name());
    selectCommand.SetCondition("Id=" + QString::number(id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        if(rawResult.size() != 1) {
            throw std::runtime_error("GetModelNameById call: Multiple records or empty data returned.");
        }
        return rawResult.at(0).at(0).toString();
    } else {
        throw std::runtime_error("GetModelNameById call: Failed.");
    }
}
void ModelTable::IApi::AddNewModel(int makeId, const QString& modelName) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("AddNewModel call: Api call when table is missing from db.");
    }
    Values value = {
        {ModelTable::FieldName(ModelTable::Fields::Name), ModelTable::FieldName(ModelTable::Fields::MakeId)},
        {modelName, QString::number(makeId)}
    };
    SqlInsert insertCommand(m_table->GetDatabase(),
                            m_table->Name(),
                            value);
    insertCommand.Execute();
    if(!insertCommand.IsValid()) {
        throw std::runtime_error("AddNewModel call: Failed.");
    }
}
int ModelTable::IApi::GetModelIdByName(const QString& modelName) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetModelIdByName call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            ModelTable::FieldName(ModelTable::Fields::Id),
                            m_table->Name());
    selectCommand.SetCondition(ModelTable::FieldName(ModelTable::Fields::Name) + "=\"" + modelName + "\"");
    QList<QVariantList> result = selectCommand.Execute();
    if(selectCommand.IsValid() && result.size() == 1 && result.at(0).size() == 1) {
        return result.at(0).at(0).toInt();
    } else {
        throw std::runtime_error("GetModelIdByName call: Failed.");
    }
    return 0;
}
int ModelTable::IApi::GetMakeIdByModelId(int modelId) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetMakeIdByModelId call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            ModelTable::FieldName(ModelTable::Fields::MakeId),
                            m_table->Name());
    selectCommand.SetCondition(ModelTable::FieldName(ModelTable::Fields::Id) + "=\"" + QString::number(modelId) + "\"");
    QList<QVariantList> result = selectCommand.Execute();
    if(selectCommand.IsValid() && result.size() == 1 && result.at(0).size() == 1) {
        return result.at(0).at(0).toInt();
    } else {
        throw std::runtime_error("GetMakeIdByModelId call: Failed.");
    }
    return 0;
}
ModelTable::ModelTable(Database* database)
    : ITableApi(new Migration(this), database)
    , m_api(this)
{
}
QString ModelTable::Name() const {
    return MODEL_TABLE_NAME;
}
const ModelTable::IApi& ModelTable::Api() const {
    return m_api;
}
size_t ModelTable::FieldIndex(ModelTable::Fields f) {
    return m_fields[f].first;
}
QString ModelTable::FieldName(ModelTable::Fields f) {
    return m_fields[f].second;
}
