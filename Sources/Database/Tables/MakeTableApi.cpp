#include "Headers/Database/Tables/MakeTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/SqlCommands/SqlSelect.h"
#include "Headers/Database/SqlCommands/SqlInsert.h"

QMap<MakeTable::Fields, QPair<size_t, QString>> MakeTable::m_fields = {
     std::pair<Fields, QPair<size_t, QString>>(Fields::Id, QPair<size_t, QString>(0, "Id")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Name, QPair<size_t, QString>(1, "Name"))
};
MakeTable::Migration::Migration(ITableApi *table)
    : IMigration(table)
{
}
bool MakeTable::Migration::Migrate() const {
    FieldVector fields(2);
    fields[static_cast<int>(MakeTable::FieldIndex(MakeTable::Fields::Id))] =
        Field(MakeTable::FieldName(MakeTable::Fields::Id), SqlType::Integer, {});
    fields[static_cast<int>(MakeTable::FieldIndex(MakeTable::Fields::Name))] =
        Field(MakeTable::FieldName(MakeTable::Fields::Name), SqlType::Text, {SqlAttribute::NotNull, SqlAttribute::Unique});

    QList<QStringList> data = {
        {"1", "aston martin"},
        {"2", "bmw"},
        {"3", "ford"},
        {"4", "jeep"},
        {"5", "maserati"},
        {"6", "mersedes"}
    };
    return m_table->CreateDataTable(fields, data, MakeTable::FieldName(MakeTable::Fields::Id));
}
bool MakeTable::Migration::Rollback() const {
    return false;
}
QString MakeTable::Migration::Description() const {
    return MAKE_TABLE_M_v1_DESCRIPTION;
}
size_t MakeTable::Migration::Id() const {
    return MAKE_TABLE_M_v1_ID;
}
MakeTable::IApi::IApi(ITableApi* owningTable)
    : m_table(owningTable)
{
}
QList<QMap<MakeTable::Fields, QVariant>> MakeTable::IApi::GetMakeData() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetMakeData call: Api call when table is missing from db.");
    }
    QString fieldsToRetrieve =
            MakeTable::FieldName(MakeTable::Fields::Id) + ", " +
            MakeTable::FieldName(MakeTable::Fields::Name);
    SqlSelect selectCommand(m_table->GetDatabase(),
                            fieldsToRetrieve,
                            m_table->Name());
    selectCommand.SetOrderBy(MakeTable::FieldName(MakeTable::Fields::Id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        QList<QMap<MakeTable::Fields, QVariant>> result;
        for(auto entry : rawResult) {
            QMap<MakeTable::Fields, QVariant> singleRecord;
            singleRecord.insert(MakeTable::Fields::Id, entry.at(0));
            singleRecord.insert(MakeTable::Fields::Name, entry.at(1));
            result.append(singleRecord);
        }
        return result;
   } else {
        throw std::runtime_error("GetMakeData call: Failed.");
    }
}
QString MakeTable::IApi::GetMakeNameById(size_t id) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetMakeNameById call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            MakeTable::FieldName(MakeTable::Fields::Name),
                            m_table->Name());
    selectCommand.SetCondition("Id=" + QString::number(id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        if(rawResult.size() != 1) {
            throw std::runtime_error("GetMakeNameById call: Multiple records or empty data returned.");
        }
        return rawResult.at(0).at(0).toString();
    } else {
        throw std::runtime_error("GetMakeNameById call: Failed.");
    }
}
void MakeTable::IApi::AddNewMake(const QString& makeName) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("AddNewMake call: Api call when table is missing from db.");
    }
    Values value = {
        {MakeTable::FieldName(MakeTable::Fields::Name)}, {makeName}
    };
    SqlInsert insertCommand(m_table->GetDatabase(),
                            m_table->Name(),
                            value);
    insertCommand.Execute();
    if(!insertCommand.IsValid()) {
        throw std::runtime_error("AddNewMake call: Failed.");
    }
}
int MakeTable::IApi::GetMakeIdByName(const QString& makeName) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetMakeIdByName call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            MakeTable::FieldName(MakeTable::Fields::Id),
                            m_table->Name());
    selectCommand.SetCondition(MakeTable::FieldName(MakeTable::Fields::Name) + "=\"" + makeName + "\"");
    QList<QVariantList> result = selectCommand.Execute();
    if(selectCommand.IsValid() && result.size() == 1 && result.at(0).size() == 1) {
        return result.at(0).at(0).toInt();
    } else {
        throw std::runtime_error("GetMakeIdByName call: Failed.");
    }
    return 0;
}
MakeTable::MakeTable(Database* database)
    : ITableApi(new Migration(this), database)
    , m_api(this)
{
}
QString MakeTable::Name() const {
    return MAKE_TABLE_NAME;
}
const MakeTable::IApi& MakeTable::Api() const {
    return m_api;
}
size_t MakeTable::FieldIndex(MakeTable::Fields f) {
    return m_fields[f].first;
}
QString MakeTable::FieldName(MakeTable::Fields f) {
    return m_fields[f].second;
}
