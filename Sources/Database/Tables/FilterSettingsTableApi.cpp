#include "Headers/Database/Tables/FilterSettingsTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/SqlCommands/SqlInsert.h"
#include "Headers/Database/SqlCommands/SqlUpdate.h"
#include "Headers/Database/SqlCommands/SqlSelect.h"
#include "Headers/Options.h"
#include "Headers/Tools.h"
#include <QDate>

QMap<FilterSettingsTable::Fields, QPair<size_t, QString>> FilterSettingsTable::m_fields = {
     std::pair<Fields, QPair<size_t, QString>>(Fields::Id, QPair<size_t, QString>(0, "Id")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::UseFilter, QPair<size_t, QString>(1, "UseFilter")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::LotIds, QPair<size_t, QString>(2, "LotIds")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::MakeIds, QPair<size_t, QString>(3, "MakeIds")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::ModelIds, QPair<size_t, QString>(4, "ModelIds")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Years, QPair<size_t, QString>(5, "Years")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::TransmissionIds, QPair<size_t, QString>(6, "TransmissionIds")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::BodyIds, QPair<size_t, QString>(7, "BodyIds")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::PurposeIds, QPair<size_t, QString>(8, "PurposeIds")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::LocationIds, QPair<size_t, QString>(9, "LocationIds")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::WatchDates, QPair<size_t, QString>(10, "WatchDates")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::AuctionDates, QPair<size_t, QString>(11, "AuctionDates")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::SaleStatusIds, QPair<size_t, QString>(12, "SaleStatusIds")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::SoldStatusIds, QPair<size_t, QString>(13, "SoldStatusIds")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::SoldPrices, QPair<size_t, QString>(14, "SoldPrices")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::WitPrices, QPair<size_t, QString>(15, "WitPrices")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Missed, QPair<size_t, QString>(16, "Missed"))
};
FilterSettingsTable::Migration::Migration(ITableApi *table)
    : IMigration(table)
{
}
bool FilterSettingsTable::Migration::Migrate() const {
    FieldVector fields(17);
    fields[static_cast<int>(FieldIndex(Fields::Id))] =
        Field(FieldName(Fields::Id), SqlType::Integer, {SqlAttribute::AutoIncrement});
    fields[static_cast<int>(FieldIndex(Fields::UseFilter))] =
        Field(FieldName(Fields::UseFilter), SqlType::Boolean, {SqlAttribute::NotNull});
    fields[static_cast<int>(FieldIndex(Fields::LotIds))] =
        Field(FieldName(Fields::LotIds), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::MakeIds))] =
        Field(FieldName(Fields::MakeIds), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::ModelIds))] =
        Field(FieldName(Fields::ModelIds), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::Years))] =
        Field(FieldName(Fields::Years), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::TransmissionIds))] =
        Field(FieldName(Fields::TransmissionIds), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::BodyIds))] =
        Field(FieldName(Fields::BodyIds), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::PurposeIds))] =
        Field(FieldName(Fields::PurposeIds), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::LocationIds))] =
        Field(FieldName(Fields::LocationIds), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::WatchDates))] =
        Field(FieldName(Fields::WatchDates), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::AuctionDates))] =
        Field(FieldName(Fields::AuctionDates), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::SaleStatusIds))] =
        Field(FieldName(Fields::SaleStatusIds), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::SoldStatusIds))] =
        Field(FieldName(Fields::SoldStatusIds), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::SoldPrices))] =
        Field(FieldName(Fields::SoldPrices), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::WitPrices))] =
        Field(FieldName(Fields::WitPrices), SqlType::Text, {});
    fields[static_cast<int>(FieldIndex(Fields::Missed))] =
        Field(FieldName(Fields::Missed), SqlType::Integer, {});

    QList<QStringList> data = {
        {"1", "0", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null"}
    };

    return m_table->CreateDataTable(fields, data, FilterSettingsTable::FieldName(FilterSettingsTable::Fields::Id));
}
bool FilterSettingsTable::Migration::Rollback() const {
    return false;
}
QString FilterSettingsTable::Migration::Description() const {
    return FILTERSETTINGS_TABLE_M_v1_DESCRIPTION;
}
size_t FilterSettingsTable::Migration::Id() const {
    return FILTERSETTINGS_TABLE_M_v1_ID;
}
FilterSettingsTable::IApi::IApi(ITableApi* owningTable)
    : m_table(owningTable)
{
}
void FilterSettingsTable::IApi::SaveFilter(const LotFilterDialog::FilterCollection& filter) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("SaveFilter call: Api call when table is missing from db.");
    }

    QMap<Fields, QString> filterData;
    packFilterData(filter, filterData);
    QList<QPair<QString, QString>> fields;
    fields.append(QPair<QString, QString>(FieldName(Fields::UseFilter), QString::number(static_cast<int>(true))));
    fields.append(QPair<QString, QString>(FieldName(Fields::LotIds), filterData[Fields::LotIds]));
    fields.append(QPair<QString, QString>(FieldName(Fields::MakeIds), filterData[Fields::MakeIds]));
    fields.append(QPair<QString, QString>(FieldName(Fields::ModelIds), filterData[Fields::ModelIds]));
    fields.append(QPair<QString, QString>(FieldName(Fields::Years), filterData[Fields::Years]));
    fields.append(QPair<QString, QString>(FieldName(Fields::TransmissionIds), filterData[Fields::TransmissionIds]));
    fields.append(QPair<QString, QString>(FieldName(Fields::BodyIds), filterData[Fields::BodyIds]));
    fields.append(QPair<QString, QString>(FieldName(Fields::PurposeIds), filterData[Fields::PurposeIds]));
    fields.append(QPair<QString, QString>(FieldName(Fields::LocationIds), filterData[Fields::LocationIds]));
    fields.append(QPair<QString, QString>(FieldName(Fields::WatchDates), filterData[Fields::WatchDates]));
    fields.append(QPair<QString, QString>(FieldName(Fields::AuctionDates), filterData[Fields::AuctionDates]));
    fields.append(QPair<QString, QString>(FieldName(Fields::SaleStatusIds), filterData[Fields::SaleStatusIds]));
    fields.append(QPair<QString, QString>(FieldName(Fields::SoldStatusIds), filterData[Fields::SoldStatusIds]));
    fields.append(QPair<QString, QString>(FieldName(Fields::SoldPrices), filterData[Fields::SoldPrices]));
    fields.append(QPair<QString, QString>(FieldName(Fields::WitPrices), filterData[Fields::WitPrices]));
    fields.append(QPair<QString, QString>(FieldName(Fields::Missed), filterData[Fields::Missed]));

    SqlUpdate updateCommand(m_table->GetDatabase(),
                            m_table->Name(),
                            fields);
    updateCommand.SetCondition(FieldName(Fields::Id) + "=" + QString::number(1));
    updateCommand.Execute();
    if(!updateCommand.IsValid()) {
        throw std::runtime_error("SaveFilter call: Failed.");
    }
}
LotFilterDialog::FilterCollection FilterSettingsTable::IApi::GetFilter() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetFilter call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            fieldsToRetrieve(),
                            m_table->Name());
    selectCommand.SetCondition("Id=" + QString::number(1));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        if(rawResult.size() != 1) {
            throw std::runtime_error("GetFilter call: Multiple records or empty data returned.");
        }
        auto lambda_monthNumber = [] (const QString& monthName) -> int {
            static QStringList months = {"jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"};
            for(int i = 0; i < months.size(); ++i) {
                if(monthName.toLower() == months.at(i)) {
                    return i + 1;
                }
            }
            return 0;
        };
        auto lambda_splitDate = [] (const QString& date) -> QStringList {
            QString day, month, year;
            int componentSwitch = 0;
            for(auto ch : date) {
                if(componentSwitch < 2 && ch == "-") {
                    componentSwitch ++;
                    continue;
                }
                if(componentSwitch == 0) {
                    day += ch;
                } else if(componentSwitch == 1) {
                    month += ch;
                } else {
                    year += ch;
                }
            }
            return {day, month, year};
        };
        LotFilterDialog::FilterCollection filter;
        // NOTE: Field indicies should match with the ones in fieldsToRetrieve() function.
        if(!rawResult.at(0).at(0).isNull()) {
            if(rawResult.at(0).at(0).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Lot id.");
            }
            SelectionFilter<QVariant>* lotFilter = new SelectionFilter<QVariant>;
            for(auto lotId : rawResult.at(0).at(0).toString().split(",", QString::SkipEmptyParts)) {
                lotFilter->AddData(QVariant(lotId.toUInt()));
            }
            filter.insert(LotFilterDialog::Field::LotId, QSharedPointer<IFilter<QVariant>>(lotFilter));
        }

        if(!rawResult.at(0).at(1).isNull()) {
            if(rawResult.at(0).at(1).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Make.");
            }
            SelectionFilter<QVariant>* makeFilter = new SelectionFilter<QVariant>;
            for(auto makeId : rawResult.at(0).at(1).toString().split(",", QString::SkipEmptyParts)) {
                makeFilter->AddData(QVariant(makeId.toInt()));
            }
            filter.insert(LotFilterDialog::Field::Make, QSharedPointer<IFilter<QVariant>>(makeFilter));
        }

        if(!rawResult.at(0).at(2).isNull()) {
            if(rawResult.at(0).at(2).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Model.");
            }
            SelectionFilter<QVariant>* modelFilter = new SelectionFilter<QVariant>;
            for(auto modelId : rawResult.at(0).at(2).toString().split(",", QString::SkipEmptyParts)) {
                modelFilter->AddData(QVariant(modelId.toInt()));
            }
            filter.insert(LotFilterDialog::Field::Model, QSharedPointer<IFilter<QVariant>>(modelFilter));
        }

        if(!rawResult.at(0).at(3).isNull()) {
            if(rawResult.at(0).at(3).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Year.");
            }
            RangeFilter<QVariant>* yearFilter = new RangeFilter<QVariant>;
            for(auto yearRange : rawResult.at(0).at(3).toString().split(",", QString::SkipEmptyParts)) {
                QStringList rangePair = yearRange.split("..", QString::SkipEmptyParts);
                yearFilter->AddData(QVariant(rangePair.at(0).toInt()), QVariant(rangePair.at(1).toInt()));
            }
            filter.insert(LotFilterDialog::Field::Year, QSharedPointer<IFilter<QVariant>>(yearFilter));
        }

        if(!rawResult.at(0).at(4).isNull()) {
            if(rawResult.at(0).at(4).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Transmission.");
            }
            SelectionFilter<QVariant>* transmissionFilter = new SelectionFilter<QVariant>;
            for(auto transmissionId : rawResult.at(0).at(4).toString().split(",", QString::SkipEmptyParts)) {
                transmissionFilter->AddData(QVariant(transmissionId.toUInt()));
            }
            filter.insert(LotFilterDialog::Field::Transmission, QSharedPointer<IFilter<QVariant>>(transmissionFilter));
        }

        if(!rawResult.at(0).at(5).isNull()) {
            if(rawResult.at(0).at(5).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Body.");
            }
            SelectionFilter<QVariant>* bodyFilter = new SelectionFilter<QVariant>;
            for(auto bodyId : rawResult.at(0).at(5).toString().split(",", QString::SkipEmptyParts)) {
                bodyFilter->AddData(QVariant(bodyId.toUInt()));
            }
            filter.insert(LotFilterDialog::Field::Body, QSharedPointer<IFilter<QVariant>>(bodyFilter));
        }

        if(!rawResult.at(0).at(6).isNull()) {
            if(rawResult.at(0).at(6).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Purpose.");
            }
            SelectionFilter<QVariant>* purposeFilter = new SelectionFilter<QVariant>;
            for(auto purposeId : rawResult.at(0).at(6).toString().split(",", QString::SkipEmptyParts)) {
                purposeFilter->AddData(QVariant(purposeId.toUInt()));
            }
            filter.insert(LotFilterDialog::Field::Purpose, QSharedPointer<IFilter<QVariant>>(purposeFilter));
        }

        if(!rawResult.at(0).at(7).isNull()) {
            if(rawResult.at(0).at(7).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Location.");
            }
            SelectionFilter<QVariant>* locationFilter = new SelectionFilter<QVariant>;
            for(auto locationId : rawResult.at(0).at(7).toString().split(",", QString::SkipEmptyParts)) {
                locationFilter->AddData(QVariant(locationId.toUInt()));
            }
            filter.insert(LotFilterDialog::Field::Location, QSharedPointer<IFilter<QVariant>>(locationFilter));
        }

        if(!rawResult.at(0).at(8).isNull()) {
            if(rawResult.at(0).at(8).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Watch date.");
            }
            RangeFilter<QVariant>* watchDateFilter = new RangeFilter<QVariant>;
            for(auto dateRange : rawResult.at(0).at(8).toString().split(",", QString::SkipEmptyParts)) {
                QStringList rangePair = dateRange.split("..", QString::SkipEmptyParts);
                // NOTE: [QT BUG:] QDate::fromString and QDateEdit can't work with dates above year 7999.
                QStringList first = lambda_splitDate(rangePair.at(0));
                QStringList second = lambda_splitDate(rangePair.at(1));
                QDate dateFrom(first.at(2).toInt(), lambda_monthNumber(first.at(1)), first.at(0).toInt());
                QDate dateTo(second.at(2).toInt(), lambda_monthNumber(second.at(1)), second.at(0).toInt());
                watchDateFilter->AddData(QVariant(dateFrom), QVariant(dateTo));
            }
            filter.insert(LotFilterDialog::Field::WatchDate, QSharedPointer<IFilter<QVariant>>(watchDateFilter));
        }

        if(!rawResult.at(0).at(9).isNull()) {
            if(rawResult.at(0).at(9).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Auction date.");
            }
            RangeFilter<QVariant>* auctionDateFilter = new RangeFilter<QVariant>;
            for(auto dateRange : rawResult.at(0).at(9).toString().split(",", QString::SkipEmptyParts)) {
                QStringList rangePair = dateRange.split("..", QString::SkipEmptyParts);
                // NOTE: [QT BUG:] QDate::fromString and QDateEdit can't work with dates above year 7999.
                QStringList first = lambda_splitDate(rangePair.at(0));
                QStringList second = lambda_splitDate(rangePair.at(1));
                QDate dateFrom(first.at(2).toInt(), lambda_monthNumber(first.at(1)), first.at(0).toInt());
                QDate dateTo(second.at(2).toInt(), lambda_monthNumber(second.at(1)), second.at(0).toInt());
                auctionDateFilter->AddData(QVariant(dateFrom), QVariant(dateTo));
            }
            filter.insert(LotFilterDialog::Field::AuctionDate, QSharedPointer<IFilter<QVariant>>(auctionDateFilter));
        }

        if(!rawResult.at(0).at(10).isNull()) {
            if(rawResult.at(0).at(10).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Sale status.");
            }
            SelectionFilter<QVariant>* saleStatusFilter = new SelectionFilter<QVariant>;
            for(auto saleStatusId : rawResult.at(0).at(10).toString().split(",", QString::SkipEmptyParts)) {
                saleStatusFilter->AddData(QVariant(saleStatusId.toUInt()));
            }
            filter.insert(LotFilterDialog::Field::SaleStatus, QSharedPointer<IFilter<QVariant>>(saleStatusFilter));
        }

        if(!rawResult.at(0).at(11).isNull()) {
            if(rawResult.at(0).at(11).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Sold status.");
            }
            SelectionFilter<QVariant>* soldStatusFilter = new SelectionFilter<QVariant>;
            for(auto soldStatusId : rawResult.at(0).at(11).toString().split(",", QString::SkipEmptyParts)) {
                soldStatusFilter->AddData(QVariant(soldStatusId.toUInt()));
            }
            filter.insert(LotFilterDialog::Field::SoldStatus, QSharedPointer<IFilter<QVariant>>(soldStatusFilter));
        }

        if(!rawResult.at(0).at(12).isNull()) {
            if(rawResult.at(0).at(12).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Sold price.");
            }
            RangeFilter<QVariant>* soldPriceFilter = new RangeFilter<QVariant>;
            for(auto integerRange : rawResult.at(0).at(12).toString().split(",", QString::SkipEmptyParts)) {
                QStringList rangePair = integerRange.split("..", QString::SkipEmptyParts);
                soldPriceFilter->AddData(QVariant(rangePair.at(0).toInt()), QVariant(rangePair.at(1).toInt()));
            }
            filter.insert(LotFilterDialog::Field::SoldPrice, QSharedPointer<IFilter<QVariant>>(soldPriceFilter));
        }

        if(!rawResult.at(0).at(13).isNull()) {
            if(rawResult.at(0).at(13).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Wit price.");
            }
            RangeFilter<QVariant>* witPriceFilter = new RangeFilter<QVariant>;
            for(auto integerRange : rawResult.at(0).at(13).toString().split(",", QString::SkipEmptyParts)) {
                QStringList rangePair = integerRange.split("..", QString::SkipEmptyParts);
                witPriceFilter->AddData(QVariant(rangePair.at(0).toInt()), QVariant(rangePair.at(1).toInt()));
            }
            filter.insert(LotFilterDialog::Field::Wit, QSharedPointer<IFilter<QVariant>>(witPriceFilter));
        }

        if(!rawResult.at(0).at(14).isNull()) {
            if(rawResult.at(0).at(14).toString().isEmpty()) {
                throw std::runtime_error("GetFilter call: Empty Missed.");
            }
            SelectionFilter<QVariant>* missedFilter = new SelectionFilter<QVariant>;
            missedFilter->AddData(QVariant(rawResult.at(0).at(14).toBool()));
            filter.insert(LotFilterDialog::Field::Missed, QSharedPointer<IFilter<QVariant>>(missedFilter));
        }

        return filter;
    } else {
        throw std::runtime_error("GetFilter call: Failed.");
    }
}
void FilterSettingsTable::IApi::TurnOffFilter() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("TurnOffFilter call: Api call when table is missing from db.");
    }

    SqlUpdate updateCommand(m_table->GetDatabase(),
                            m_table->Name(),
                            {QPair<QString, QString>(FieldName(Fields::UseFilter), QString::number(static_cast<int>(false)))});
    updateCommand.SetCondition(FieldName(Fields::Id) + "=" + QString::number(1));
    updateCommand.Execute();
    if(!updateCommand.IsValid()) {
        throw std::runtime_error("TurnOffFilter call: Failed.");
    }
}
bool FilterSettingsTable::IApi::IsFilterTurnedOn() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("IsFilterTurnedOn call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            FilterSettingsTable::FieldName(FilterSettingsTable::Fields::UseFilter),
                            m_table->Name());
    selectCommand.SetCondition("Id=" + QString::number(1));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        if(rawResult.size() != 1) {
            throw std::runtime_error("IsFilterTurnedOn call: Multiple records or empty data returned.");
        }
        return rawResult.at(0).at(0).toBool();
    } else {
        throw std::runtime_error("IsFilterTurnedOn call: Failed.");
    }
}
void FilterSettingsTable::IApi::packFilterData(const LotFilterDialog::FilterCollection& filter, QMap<FilterSettingsTable::Fields, QString>& packedFilter) const {
    if(filter.contains(LotFilterDialog::Field::LotId)) {
        QVariantList data =
                 qSharedPointerCast<SelectionFilter<QVariant>>(filter[LotFilterDialog::Field::LotId])->GetData();
        QString joinedData;
        for(auto lotId : data) {
            joinedData.append(lotId.toString() + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::LotIds, joinedData);
    } else {
        packedFilter.insert(Fields::LotIds, "null");
    }

    if(filter.contains(LotFilterDialog::Field::Make)) {
        QVariantList data =
                 qSharedPointerCast<SelectionFilter<QVariant>>(filter[LotFilterDialog::Field::Make])->GetData();
        QString joinedData;
        for(auto makeId : data) {
            joinedData.append(makeId.toString() + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::MakeIds, joinedData);
    } else {
        packedFilter.insert(Fields::MakeIds, "null");
    }

    if(filter.contains(LotFilterDialog::Field::Model)) {
        QVariantList data =
                 qSharedPointerCast<SelectionFilter<QVariant>>(filter[LotFilterDialog::Field::Model])->GetData();
        QString joinedData;
        for(auto modelId : data) {
            joinedData.append(modelId.toString() + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::ModelIds, joinedData);
    } else {
        packedFilter.insert(Fields::ModelIds, "null");
    }

    if(filter.contains(LotFilterDialog::Field::Year)) {
        QList<QPair<QVariant, QVariant>> data =
                qSharedPointerCast<RangeFilter<QVariant>>(filter[LotFilterDialog::Field::Year])->GetData();
        QString joinedData;
        for(auto yearRange : data) {
            joinedData.append(yearRange.first.toString() + ".." + yearRange.second.toString() + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::Years, joinedData);
    } else {
        packedFilter.insert(Fields::Years, "null");
    }

    if(filter.contains(LotFilterDialog::Field::Transmission)) {
        QVariantList data =
                 qSharedPointerCast<SelectionFilter<QVariant>>(filter[LotFilterDialog::Field::Transmission])->GetData();
        QString joinedData;
        for(auto transmissionId : data) {
            joinedData.append(transmissionId.toString() + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::TransmissionIds, joinedData);
    } else {
        packedFilter.insert(Fields::TransmissionIds, "null");
    }

    if(filter.contains(LotFilterDialog::Field::Body)) {
        QVariantList data =
                 qSharedPointerCast<SelectionFilter<QVariant>>(filter[LotFilterDialog::Field::Body])->GetData();
        QString joinedData;
        for(auto bodyId : data) {
            joinedData.append(bodyId.toString() + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::BodyIds, joinedData);
    } else {
        packedFilter.insert(Fields::BodyIds, "null");
    }

    if(filter.contains(LotFilterDialog::Field::Purpose)) {
        QVariantList data =
                 qSharedPointerCast<SelectionFilter<QVariant>>(filter[LotFilterDialog::Field::Purpose])->GetData();
        QString joinedData;
        for(auto purposeId : data) {
            joinedData.append(purposeId.toString() + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::PurposeIds, joinedData);
    } else {
        packedFilter.insert(Fields::PurposeIds, "null");
    }

    if(filter.contains(LotFilterDialog::Field::Location)) {
        QVariantList data =
                 qSharedPointerCast<SelectionFilter<QVariant>>(filter[LotFilterDialog::Field::Location])->GetData();
        QString joinedData;
        for(auto locationId : data) {
            joinedData.append(locationId.toString() + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::LocationIds, joinedData);
    } else {
        packedFilter.insert(Fields::LocationIds, "null");
    }

    if(filter.contains(LotFilterDialog::Field::WatchDate)) {
        QList<QPair<QVariant, QVariant>> data =
                qSharedPointerCast<RangeFilter<QVariant>>(filter[LotFilterDialog::Field::WatchDate])->GetData();
        QString joinedData;
        for(auto watchDateRange : data) {
            joinedData.append(watchDateRange.first.toDate().toString(Options::Instance().DateFormat()) + ".." +
                              watchDateRange.second.toDate().toString(Options::Instance().DateFormat()) + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::WatchDates, joinedData);
    } else {
        packedFilter.insert(Fields::WatchDates, "null");
    }

    if(filter.contains(LotFilterDialog::Field::AuctionDate)) {
        QList<QPair<QVariant, QVariant>> data =
                qSharedPointerCast<RangeFilter<QVariant>>(filter[LotFilterDialog::Field::AuctionDate])->GetData();
        QString joinedData;
        for(auto auctionDateRange : data) {
            joinedData.append(auctionDateRange.first.toDate().toString(Options::Instance().DateFormat()) + ".." +
                              auctionDateRange.second.toDate().toString(Options::Instance().DateFormat()) + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::AuctionDates, joinedData);
    } else {
        packedFilter.insert(Fields::AuctionDates, "null");
    }

    if(filter.contains(LotFilterDialog::Field::SaleStatus)) {
        QVariantList data =
                 qSharedPointerCast<SelectionFilter<QVariant>>(filter[LotFilterDialog::Field::SaleStatus])->GetData();
        QString joinedData;
        for(auto saleStatusId : data) {
            joinedData.append(saleStatusId.toString() + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::SaleStatusIds, joinedData);
    } else {
        packedFilter.insert(Fields::SaleStatusIds, "null");
    }

    if(filter.contains(LotFilterDialog::Field::SoldStatus)) {
        QVariantList data =
                 qSharedPointerCast<SelectionFilter<QVariant>>(filter[LotFilterDialog::Field::SoldStatus])->GetData();
        QString joinedData;
        for(auto soldStatusId : data) {
            joinedData.append(soldStatusId.toString() + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::SoldStatusIds, joinedData);
    } else {
        packedFilter.insert(Fields::SoldStatusIds, "null");
    }

    if(filter.contains(LotFilterDialog::Field::SoldPrice)) {
        QList<QPair<QVariant, QVariant>> data =
                qSharedPointerCast<RangeFilter<QVariant>>(filter[LotFilterDialog::Field::SoldPrice])->GetData();
        QString joinedData;
        for(auto soldPriceRange : data) {
            joinedData.append(soldPriceRange.first.toString() + ".." + soldPriceRange.second.toString() + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::SoldPrices, joinedData);
    } else {
        packedFilter.insert(Fields::SoldPrices, "null");
    }

    if(filter.contains(LotFilterDialog::Field::Wit)) {
        QList<QPair<QVariant, QVariant>> data =
                qSharedPointerCast<RangeFilter<QVariant>>(filter[LotFilterDialog::Field::Wit])->GetData();
        QString joinedData;
        for(auto witRange : data) {
            joinedData.append(witRange.first.toString() + ".." + witRange.second.toString() + ",");
        }
        joinedData.remove(joinedData.size() - 1, 1);
        packedFilter.insert(Fields::WitPrices, joinedData);
    } else {
        packedFilter.insert(Fields::WitPrices, "null");
    }

    if(filter.contains(LotFilterDialog::Field::Missed)) {
        QVariantList data =
                 qSharedPointerCast<SelectionFilter<QVariant>>(filter[LotFilterDialog::Field::Missed])->GetData();
        if(data.size() != 1) {
            throw std::runtime_error("Filter contains multiple or no \"missed\" values.");
        }
        packedFilter.insert(Fields::Missed, data.at(0).toBool() ? "1" : "0");
    } else {
        packedFilter.insert(Fields::Missed, "null");
    }
}
QString FilterSettingsTable::IApi::fieldsToRetrieve() const {
    static QString fields =
            FieldName(Fields::LotIds) + ", " +
            FieldName(Fields::MakeIds) + ", " +
            FieldName(Fields::ModelIds) + ", " +
            FieldName(Fields::Years) + ", " +
            FieldName(Fields::TransmissionIds) + ", " +
            FieldName(Fields::BodyIds) + ", " +
            FieldName(Fields::PurposeIds) + ", " +
            FieldName(Fields::LocationIds) + ", " +
            FieldName(Fields::WatchDates) + ", " +
            FieldName(Fields::AuctionDates) + ", " +
            FieldName(Fields::SaleStatusIds) + ", " +
            FieldName(Fields::SoldStatusIds) + ", " +
            FieldName(Fields::SoldPrices) + ", " +
            FieldName(Fields::WitPrices) + ", " +
            FieldName(Fields::Missed);
    return fields;
}
FilterSettingsTable::FilterSettingsTable(Database* database)
    : ITableApi(new Migration(this), database)
    , m_api(this)
{
}
QString FilterSettingsTable::Name() const {
    return FILTERSETTINGS_TABLE_NAME;
}
const FilterSettingsTable::IApi& FilterSettingsTable::Api() const {
    return m_api;
}
size_t FilterSettingsTable::FieldIndex(FilterSettingsTable::Fields f) {
    return m_fields[f].first;
}
QString FilterSettingsTable::FieldName(FilterSettingsTable::Fields f) {
    return m_fields[f].second;
}
