#include "Headers/Database/Tables/PurposeTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/SqlCommands/SqlSelect.h"

QMap<PurposeTable::Fields, QPair<size_t, QString>> PurposeTable::m_fields = {
     std::pair<Fields, QPair<size_t, QString>>(Fields::Id, QPair<size_t, QString>(0, "Id")),
     std::pair<Fields, QPair<size_t, QString>>(Fields::Name, QPair<size_t, QString>(1, "Name"))
};
PurposeTable::Migration::Migration(ITableApi *table)
    : IMigration(table)
{
}
bool PurposeTable::Migration::Migrate() const {
    FieldVector fields(2);
    fields[static_cast<int>(PurposeTable::FieldIndex(PurposeTable::Fields::Id))] =
        Field(PurposeTable::FieldName(PurposeTable::Fields::Id), SqlType::Integer, {});
    fields[static_cast<int>(PurposeTable::FieldIndex(PurposeTable::Fields::Name))] =
        Field(PurposeTable::FieldName(PurposeTable::Fields::Name), SqlType::Text, {SqlAttribute::NotNull, SqlAttribute::Unique});

    QList<QStringList> data = {
        {"1", "car base"},
        {"2", "car parts"},
        {"3", "interest"},
        {"4", "statistics"},
        {"5", "target"}
    };
    return m_table->CreateDataTable(fields, data, PurposeTable::FieldName(PurposeTable::Fields::Id));
}
bool PurposeTable::Migration::Rollback() const {
    return false;
}
QString PurposeTable::Migration::Description() const {
    return PURPOSE_TABLE_M_v1_DESCRIPTION;
}
size_t PurposeTable::Migration::Id() const {
    return PURPOSE_TABLE_M_v1_ID;
}
PurposeTable::IApi::IApi(ITableApi* owningTable)
    : m_table(owningTable)
{
}
QList<QMap<PurposeTable::Fields, QVariant>> PurposeTable::IApi::GetPurposeData() const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetPurposeData call: Api call when table is missing from db.");
    }
    QString fieldsToRetrieve =
            PurposeTable::FieldName(PurposeTable::Fields::Id) + ", " +
            PurposeTable::FieldName(PurposeTable::Fields::Name);
    SqlSelect selectCommand(m_table->GetDatabase(),
                            fieldsToRetrieve,
                            m_table->Name());
   selectCommand.SetOrderBy(PurposeTable::FieldName(PurposeTable::Fields::Id));
   QList<QVariantList> rawResult = selectCommand.Execute();
   if(selectCommand.IsValid()) {
       QList<QMap<PurposeTable::Fields, QVariant>> result;
       for(auto entry : rawResult) {
           QMap<PurposeTable::Fields, QVariant> singleRecord;
           singleRecord.insert(PurposeTable::Fields::Id, entry.at(0));
           singleRecord.insert(PurposeTable::Fields::Name, entry.at(1));
           result.append(singleRecord);
       }
       return result;
   } else {
       throw std::runtime_error("GetPurposeData call: Failed.");
   }
}
QString PurposeTable::IApi::GetPurposeNameById(size_t id) const {
    if(!m_table->ExistsInDb()) {
        throw std::runtime_error("GetPurposeNameById call: Api call when table is missing from db.");
    }
    SqlSelect selectCommand(m_table->GetDatabase(),
                            PurposeTable::FieldName(PurposeTable::Fields::Name),
                            m_table->Name());
    selectCommand.SetCondition("Id=" + QString::number(id));
    QList<QVariantList> rawResult = selectCommand.Execute();
    if(selectCommand.IsValid()) {
        if(rawResult.size() != 1) {
            throw std::runtime_error("GetPurposeNameById call: Multiple records or empty data returned.");
        }
        return rawResult.at(0).at(0).toString();
    } else {
        throw std::runtime_error("GetPurposeNameById call: Failed.");
    }
}
PurposeTable::PurposeTable(Database* database)
    : ITableApi(new Migration(this), database)
    , m_api(this)
{
}
QString PurposeTable::Name() const {
    return PURPOSE_TABLE_NAME;
}
const PurposeTable::IApi& PurposeTable::Api() const {
    return m_api;
}
size_t PurposeTable::FieldIndex(PurposeTable::Fields f) {
    return m_fields[f].first;
}
QString PurposeTable::FieldName(PurposeTable::Fields f) {
    return m_fields[f].second;
}
