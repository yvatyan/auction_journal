#include "Headers/Database/Database.h"
#include "Headers/Database/Tables/ITableApi.h"
#include "Headers/Database/Tables/MigrationTableApi.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/NotificationManager.h"
#include <QtSql/QSqlError>

Database::Database(const QString& name)
    : m_db(QSqlDatabase::addDatabase("QSQLITE", name + "_connection"))
    , m_tables()
    , m_migrationApplyHistory()
    , m_name(name)
{
}
Database::~Database() {
    for(int i = m_migrationApplyHistory.size() - 1; i >= 0; --i) {
        delete m_migrationApplyHistory.at(i);
    }
    m_db.close();
}
void Database::OpenConnection(const QString& path) {
    m_db.setDatabaseName(path + "/" + m_name);
    if(!m_db.isValid()) {
        throw std::runtime_error("QSqlDatabase driver is not valid.");
    }
    if(!m_db.open()) {
        throw std::runtime_error(m_db.lastError().text().toStdString());
    }
}
void Database::AddTable(ITableApi* table) {
    if(static_cast<size_t>(m_migrationApplyHistory.size()) != table->MigrationId() - 1) {
        QString message = "Table " + table->Name() + " can\'t be added.";
        throw std::runtime_error(message.toStdString());
    }
    m_migrationApplyHistory.append(table);
    m_tables.insert(table->Name(), table);
}
ITableApi* Database::Table(const QString& name) const {
    if(m_tables.find(name) == m_tables.end()) {
        return nullptr;
    }
    return m_tables[name];
}
MigrationTable* Database::GetMigrationTable() {
    return static_cast<MigrationTable*>(Table(MIGRATION_TABLE_NAME));
}
const QSqlDatabase& Database::SqlDatabase() const {
    return m_db;
}
void Database::Migrate() {
    size_t lastMigrationId = 0;
    if(migrationTableExistsInDb()) {
        MigrationTable* migrationTable = GetMigrationTable();
        lastMigrationId = migrationTable->Api().LastAppliedMigrationId();
        // NOTE: LastAppliedMigrationId - Only function in migration critical region.
        // Migration critical region is the region between db.AddTable(table) and table->Migrate(),
        // in this region api is not synchronized with database and any api call is not correct.
    }

    QString jobName("Appling migrations");
    size_t stepQty = static_cast<size_t>(m_migrationApplyHistory.size()) - lastMigrationId + 1;
    QString stepName;
    size_t stepIndex;
    QString stepMsg("");
    ProgressInfo::Status stepStatus = ProgressInfo::Status::Pending;

    for(size_t i = lastMigrationId + 1; i <= static_cast<size_t>(m_migrationApplyHistory.size()); ++i) {
        ITableApi* table = m_migrationApplyHistory.at(static_cast<int>(i - 1));
        stepName = "Table \"" + table->Name() + "\"";
        stepIndex = i;
        try {
            stepStatus = ProgressInfo::Status::Pending;
            emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
            table->ApplyMigration();
            stepStatus = ProgressInfo::Status::Succeed;
            emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
        } catch (const std::runtime_error& err) {
            NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
            QString message = "Following migration \"" + table->MigrationDescription() +
                              "\" has not been applied.";
            stepStatus = ProgressInfo::Status::Failed;
            emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
            throw std::runtime_error(message.toStdString());
        }
    }
}
bool Database::migrationTableExistsInDb() const {
    return m_db.tables().indexOf(MIGRATION_TABLE_NAME) != -1;
}
