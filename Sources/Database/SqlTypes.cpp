#include "Headers/Database/SqlTypes.h"

QString SqlTypes::ToString(SqlType type) {
    switch(type) {
        case SqlType::Integer : return "INTEGER";
        case SqlType::Blob    : return "BLOB";
        case SqlType::Text    : return "TEXT";
        case SqlType::Real    : return "REAL";
        case SqlType::Numeric : return "NUMERIC";
        case SqlType::Date    : return "DATETIME";
        case SqlType::Boolean : return "BOOLEAN";
    };
    return "";
}
QString SqlAttributes::ToString(SqlAttribute attribute) {
    switch(attribute) {
        case SqlAttribute::Unique        : return "UNIQUE";
        case SqlAttribute::NotNull       : return "NOT NULL";
        case SqlAttribute::AutoIncrement : return ""; // Is used only with INTEGER PRIMARY KEY
                                                      // which has ROWID already.
    }
    return "";
}
