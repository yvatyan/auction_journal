#include "Headers/Database/DatabaseManager.h"
#include "Headers/NotificationManager.h"
#include "Headers/Options.h"

DatabaseManager& DatabaseManager::Instance() {
    static DatabaseManager instance;
    return instance;
}
DatabaseManager::DatabaseManager()
    : m_recordsDb(Options::Instance().StorageDbName())
    , m_settingsDb(Options::Instance().SettingsDbName())
{
}
DatabaseManager::~DatabaseManager()
{
}
bool DatabaseManager::Initialize() {
    QString jobName("DBManager");
    size_t stepQty = (STORAGE_DB_TABLE_QTY + 2) + (SETTINGS_DB_TABLE_QTY + 2);
    bool result;

    result = initializeStorageDatabase(jobName, stepQty, 0);
    if(!result) {
        return result;
    }
    result = initializeSettingsDatabase(jobName, stepQty, STORAGE_DB_TABLE_QTY + 2);

    return result;
}
bool DatabaseManager::initializeStorageDatabase(const QString& jobName, size_t stepQty, size_t stepOffset) {
    QString stepName;
    size_t stepIndex;
    QString stepMsg("");
    ProgressInfo::Status stepStatus = ProgressInfo::Status::Pending;

    stepName = "Creating Database \"Storage\"";
    stepIndex = 0 + stepOffset;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createStorageDatabase()) {
       stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    // Migrations critical region, BEGIN
    // NOTE: No API calls are permited in between
    stepName = "Adding table \"Migrations\"";
    stepIndex++;
    if(createTableForStorageMigrations()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Adding table \"Transmission\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createTableForTransmission()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Adding table \"Body\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createTableForBody()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Adding table \"Location\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createTableForLocation()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Adding table \"Sale Status\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createTableForSaleStatus()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Adding table \"Sold Status\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createTableForSoldStatus()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Adding table \"Purpose\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createTableForPurpose()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Adding table \"Make\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createTableForMake()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Adding table \"Model\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createTableForModel()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Adding table \"Lot Records\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createTableForLotRecords()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Adding table \"Lot Comments\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createTableForLotComments()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Migrating Database \"Storage\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(migrateStorageDatabase()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }
    // Migration critical region, END
    return true;
}
bool DatabaseManager::initializeSettingsDatabase(const QString& jobName, size_t stepQty, size_t stepOffset) {
    QString stepName;
    size_t stepIndex;
    QString stepMsg("");
    ProgressInfo::Status stepStatus = ProgressInfo::Status::Pending;

    stepName = "Creating Database \"Settings\"";
    stepIndex = 0 + stepOffset;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createSettingsDatabase()) {
       stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    // Migrations critical region, BEGIN
    // NOTE: No API calls are permited in between
    stepName = "Adding table \"Migrations\"";
    stepIndex++;
    if(createTableForSettingsMigrations()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Adding table \"Filter\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(createTableForFilter()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }

    stepName = "Migrating Database \"Settings\"";
    stepIndex++;
    stepStatus = ProgressInfo::Status::Pending;
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(migrateSettingsDatabase()) {
        stepStatus = ProgressInfo::Status::Succeed;
    } else {
        stepStatus = ProgressInfo::Status::Failed;
    }
    emit sigProgressChanged(ProgressInfo(jobName, stepQty, stepName, stepIndex, stepMsg, stepStatus));
    if(stepStatus == ProgressInfo::Status::Failed) {
        return false;
    }
    // Migration critical region, END
    return true;
}
bool DatabaseManager::createStorageDatabase() {
    try {
        m_recordsDb.OpenConnection(Options::Instance().StorageDbPath());
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForStorageMigrations() {
    try {
        m_recordsDb.AddTable(new MigrationTable(&m_recordsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForTransmission() {
    try {
        m_recordsDb.AddTable(new TransmissionTable(&m_recordsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForBody() {
    try {
        m_recordsDb.AddTable(new BodyTable(&m_recordsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForLocation() {
try {
        m_recordsDb.AddTable(new LocationTable(&m_recordsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForSaleStatus() {
try {
        m_recordsDb.AddTable(new SaleStatusTable(&m_recordsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForSoldStatus() {
    try {
        m_recordsDb.AddTable(new SoldStatusTable(&m_recordsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForPurpose() {
    try {
        m_recordsDb.AddTable(new PurposeTable(&m_recordsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForMake() {
    try {
        m_recordsDb.AddTable(new MakeTable(&m_recordsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForModel() {
    try {
        m_recordsDb.AddTable(new ModelTable(&m_recordsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForLotRecords() {
    try {
        m_recordsDb.AddTable(new LotRecordsTable(&m_recordsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForLotComments() {
    try {
        m_recordsDb.AddTable(new LotCommentsTable(&m_recordsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::migrateStorageDatabase() {
    try {
        m_recordsDb.Migrate();
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createSettingsDatabase() {
    try {
        m_settingsDb.OpenConnection(Options::Instance().SettingsDbPath());
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForSettingsMigrations() {
    try {
        m_settingsDb.AddTable(new MigrationTable(&m_settingsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::createTableForFilter() {
    try {
        m_settingsDb.AddTable(new FilterSettingsTable(&m_settingsDb));
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
bool DatabaseManager::migrateSettingsDatabase() {
    try {
        m_settingsDb.Migrate();
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return false;
    }
    return true;
}
