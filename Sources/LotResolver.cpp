#include "Headers/LotResolver.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/Tables/MakeTableApi.h"
#include "Headers/Database/Tables/ModelTableApi.h"
#include "Headers/Database/Tables/TransmissionTableApi.h"
#include "Headers/Database/Tables/BodyTableApi.h"
#include "Headers/Database/Tables/PurposeTableApi.h"
#include "Headers/Database/Tables/LocationTableApi.h"
#include "Headers/Database/Tables/SaleStatusTableApi.h"
#include "Headers/Database/Tables/SoldStatusTableApi.h"

LotResolver::LotResolver(const Lot& lot, Database* db)
    : m_lot(lot)
    , m_db(db)
{
}
size_t LotResolver::LotId() const {
    return m_lot.LotId();
}
QString LotResolver::MakeName() const {
    if(m_db == nullptr) {
        throw std::runtime_error("Make resolving failed: Database is null.");
    }
    MakeTable* makeTable = static_cast<MakeTable*>(m_db->Table(MAKE_TABLE_NAME));
    return makeTable->Api().GetMakeNameById(static_cast<size_t>(m_lot.MakeId()));
}
QString LotResolver::ModelName() const {
    if(m_db == nullptr) {
        throw std::runtime_error("Model resolving failed: Database is null.");
    }
    ModelTable* modelTable = static_cast<ModelTable*>(m_db->Table(MODEL_TABLE_NAME));
    return modelTable->Api().GetModelNameById(static_cast<size_t>(m_lot.ModelId()));
}
size_t LotResolver::Year() const {
    return m_lot.Year();
}
QString LotResolver::TransmissionName() const {
    if(m_db == nullptr) {
        throw std::runtime_error("Transmission resolving failed: Database is null.");
    }
    TransmissionTable* transmissionTable = static_cast<TransmissionTable*>(m_db->Table(TRANSMISSION_TABLE_NAME));
    return transmissionTable->Api().GetTransmissionNameById(m_lot.TransmissionId());
}
QString LotResolver::BodyName() const {
    if(m_db == nullptr) {
        throw std::runtime_error("Body resolving failed: Database is null.");
    }
    BodyTable* bodyTable = static_cast<BodyTable*>(m_db->Table(BODY_TABLE_NAME));
    return bodyTable->Api().GetBodyNameById(m_lot.BodyId());
}
QString LotResolver::PurposeName() const {
    if(m_db == nullptr) {
        throw std::runtime_error("Purpose resolving failed: Database is null.");
    }
    PurposeTable* purposeTable = static_cast<PurposeTable*>(m_db->Table(PURPOSE_TABLE_NAME));
    return purposeTable->Api().GetPurposeNameById(m_lot.PurposeId());
}
QString LotResolver::LocationName() const {
    if(m_db == nullptr) {
        throw std::runtime_error("Location resolving failed: Database is null.");
    }
    LocationTable* locationTable = static_cast<LocationTable*>(m_db->Table(LOCATION_TABLE_NAME));
    return locationTable->Api().GetLocationNameById(m_lot.LocationId());
}
QString LotResolver::LocationAbbreviation() const {
    if(m_db == nullptr) {
        throw std::runtime_error("Location Abbreviation resolving failed: Database is null.");
    }
    LocationTable* locationTable = static_cast<LocationTable*>(m_db->Table(LOCATION_TABLE_NAME));
    return locationTable->Api().GetLocationAbbreviationById(m_lot.LocationId());
}
QDate LotResolver::WatchDate() const {
    return m_lot.WatchDate();
}
QDate LotResolver::AuctionDate() const {
    return m_lot.AuctionDate();
}
QString LotResolver::SaleStatus() const {
    if(m_db == nullptr) {
        throw std::runtime_error("Sale Status resolving failed: Database is null.");
    }
    SaleStatusTable* saleStatusTable = static_cast<SaleStatusTable*>(m_db->Table(SALESTATUS_TABLE_NAME));
    return saleStatusTable->Api().GetSaleStatusNameById(m_lot.SaleStatusId());
}
QString LotResolver::SoldStatus() const {
    if(m_db == nullptr) {
        throw std::runtime_error("Sold Status resolving failed: Database is null.");
    }
    SoldStatusTable* soldStatusTable = static_cast<SoldStatusTable*>(m_db->Table(SOLDSTATUS_TABLE_NAME));
    return soldStatusTable->Api().GetSoldStatusNameById(m_lot.SoldStatusId());
}
size_t LotResolver::SoldPrice() const {
    return m_lot.SoldPrice();
}
size_t LotResolver::WitPrice() const {
    return m_lot.WantItTodayPrice();
}
bool LotResolver::IsMissed() const {
    return m_lot.IsAuctionMissed();
}
QVariant LotResolver::GetField(LotRecordsTable::Fields fieldName) const {
    switch (fieldName) {
        case LotRecordsTable::Fields::LotId : return QVariant(LotId());
        case LotRecordsTable::Fields::MakeId : return QVariant(MakeName());
        case LotRecordsTable::Fields::ModelId : return QVariant(ModelName());
        case LotRecordsTable::Fields::Year : return QVariant(Year());
        case LotRecordsTable::Fields::TransmissionId : return QVariant(TransmissionName());
        case LotRecordsTable::Fields::BodyId : return QVariant(BodyName());
        case LotRecordsTable::Fields::PurposeId : return QVariant(PurposeName());
        case LotRecordsTable::Fields::LocationId : return QVariant(LocationName());
        case LotRecordsTable::Fields::WatchDate : return QVariant(WatchDate());
        case LotRecordsTable::Fields::AuctionDate : return QVariant(AuctionDate());
        case LotRecordsTable::Fields::SaleStatusId : return QVariant(SaleStatus());
        case LotRecordsTable::Fields::SoldStatusId : return QVariant(SoldStatus());
        case LotRecordsTable::Fields::SoldPrice : return QVariant(SoldPrice());
        case LotRecordsTable::Fields::WitPrice : return QVariant(WitPrice());
        case LotRecordsTable::Fields::Missed: return QVariant(IsMissed());
        default : return QVariant();
    }
}
