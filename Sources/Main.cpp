#include "Headers/Ui/StartupWindow.h"
#include "Headers/Ui/MainWindow.h"
#include "Headers/NotificationManager.h"
#include "Headers/Database/DatabaseManager.h"
#include <QApplication>

void CustomQtMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg) {
    NotificationManager::Instance().QtMessagesLogger(type, context, msg);
}

void SetupStartupWindow(StartupWindow* startup) {
    // startup->setWindowFlag(Qt::WindowStaysOnTopHint, true); // Warning: Don't work in X11, take into account when porting to linux
    startup->resize(360, 240);
    startup->SetBackgroundPixmap(":/Images/startup_bg.png");
    startup->SetForegroundPixmap(":/Images/startup_fg/winter_1_fg.jpg", QRect(0, 0, 240, 240));
    startup->SubscribeWorker(&DatabaseManager::Instance());
    startup->SubscribeWorker(DatabaseManager::Instance().GetRecordsDatabase());
    startup->show();
}

int main(int argc, char* argv[]) {
    QApplication a(argc, argv);
    NotificationManager::Instance();
    qInstallMessageHandler(CustomQtMessageHandler);

    StartupWindow* startup = new StartupWindow();
    SetupStartupWindow(startup);

    bool succeed = DatabaseManager::Instance().Initialize();
    if(!succeed) {
        return -1;
    }

    MainWindow w(DatabaseManager::Instance().GetRecordsDatabase());
    w.showMaximized();
    startup->finish(&w);
    return a.exec();
}
