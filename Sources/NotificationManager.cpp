#include "Headers/NotificationManager.h"
#include "Headers/Options.h"
#include <QDateTime>

const NotificationManager& NotificationManager::Instance() {
    static NotificationManager instance;
    return instance;
}
int NotificationManager::ShowNotification(const QString& message, NotificationManager::Type type) const {
    switch (type) {
        case Type::Question : return showQuestion(message);
        case Type::Question3 : return showQuestion(message, true);
        case Type::Warning : return static_cast<int>(showWarning(message));
        case Type::Error : return static_cast<int>(showError(message));
        case Type::Notification : return static_cast<int>(showNotification(message));
    }
    return false;
}
void NotificationManager::QtMessagesLogger(QtMsgType type, const QMessageLogContext &context, const QString &msg) const {
    QString logMessageTemplate = "[%1], %2: %3 (%4:%5, %6).\n";
    QString dateStr = QDateTime::currentDateTime().toString();
    QString logSeverityStr;
    switch(type) {
        case QtDebugMsg : logSeverityStr = "Debug"; break;
        case QtInfoMsg : logSeverityStr = "Info"; break;
        case QtWarningMsg : logSeverityStr = "Warning"; break;
        case QtCriticalMsg : logSeverityStr = "Critical"; break;
        case QtFatalMsg : logSeverityStr = "Fatal"; break;
    }
    QString fileName = context.file;
    QString fileLine = QString::number(context.line);
    QString functionName = context.function;
    m_logFile.write(logMessageTemplate.
                        arg(dateStr).
                        arg(logSeverityStr).
                        arg(msg).
                        arg(fileName).
                        arg(fileLine).
                        arg(functionName).toUtf8());
    m_logFile.flush();
}
NotificationManager::NotificationManager()
    : m_logFile(Options::Instance().LogFilePathAndName())
{
    if(!m_logFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {
        ShowNotification("Can\'t open log file " + m_logFile.fileName() + " for writing.", Type::Warning);
    } else {
        QString dateStr = QDateTime::currentDateTime().toString();
        m_logFile.write(QString("----NEW SESSION [%1]----\n").arg(dateStr).toUtf8());
    }
}
NotificationManager::NotificationManager(const NotificationManager& copy) {
    Q_UNUSED(copy);
}
const NotificationManager& NotificationManager::operator=(const NotificationManager& copy) {
    Q_UNUSED(copy);
    return *this;
}
bool NotificationManager::showError(const QString& message) const {
    showNotificationMessageBox(QMessageBox::Critical, QObject::tr("Error"), message);
    return true;
}

bool NotificationManager::showNotification(const QString &message) const {
    showNotificationMessageBox(QMessageBox::Information, QObject::tr("Notification"), message);
    return true;
}
void NotificationManager::showNotificationMessageBox(QMessageBox::Icon icon, const QString& title, const QString& message) const {
    QMessageBox msgBox;
    msgBox.setWindowIcon(Options::Instance().ApplicationIcon());
    msgBox.setIcon(icon);
    msgBox.setWindowTitle(title);
    msgBox.setText(message);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.exec();
}
bool NotificationManager::showWarning(const QString& message) const {
    showNotificationMessageBox(QMessageBox::Warning, QObject::tr("Warning"), message);
    return true;
}
int NotificationManager::showQuestion(const QString& message, bool triState) const {
    QMessageBox msgBox;
    msgBox.setWindowIcon(Options::Instance().ApplicationIcon());
    msgBox.setIcon(QMessageBox::Question);
    msgBox.setWindowTitle(QObject::tr("Question"));
    msgBox.setText(message);
    if(triState) {
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Cancel);
    } else {
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::No);
    }
    int result = msgBox.exec();
    if(result == QMessageBox::Yes) {
        return 1;
    } else if(result == QMessageBox::No) {
        return 0;
    } else if(result == QMessageBox::Cancel) {
        return -1;
    }
    return -1;
}
