#include "Headers/NewLotBuilder.h"
#include "Headers/Options.h"
#include "Headers/Tools.h"
#include "Headers/Database/Tables/SoldStatusTableApi.h"

NewLotBuilder::NewLotBuilder()
    : m_product()
    , m_modified(true)
    , m_requiredFieldsMask(ResetMask)
{
}
Lot NewLotBuilder::ConstructLot() {
    if(!AreAllRequiredFieldsFilled()) {
        throw std::runtime_error("Not all required fields are filled.");
    }
    if(m_modified) {
        m_product = Lot(
                        m_lotId,
                        m_makeId,
                        m_modelId,
                        m_year,
                        m_transmissionId,
                        m_bodyId,
                        m_purposeId,
                        m_locationId,
                        QDateTime::currentDateTime().date(),
                        m_auctionDate,
                        m_saleStatusId,
                        SoldStatusTable::Pending,
                        0,
                        m_wantItTodayPrice,
                        m_worksheetPath,
                        false
                    );
        m_modified = false;
        if(m_makeId == -1) {
            m_product.SetNewMakeName(m_makeName);
        }
        if(m_modelId == -1) {
            m_product.SetNewModelName(m_modelName);
        }
    }
    return m_product;
}
void NewLotBuilder::SetLot(const Lot& lot) {
    m_modified = true;
    SetLotId(lot.LotId());
    SetMakeId(lot.MakeId());
    SetModelId(lot.ModelId());
    SetYear(lot.Year());
    SetTransmissionId(lot.TransmissionId());
    SetBodyId(lot.BodyId());
    SetPurposeId(lot.PurposeId());
    SetLocationId(lot.LocationId());
    SetAuctionDate(lot.AuctionDate());
    SetSaleStatusId(lot.SaleStatusId());
    SetWantItTodayPrice(lot.WantItTodayPrice());
    SetWorksheetPath(lot.WorksheetPath());
}
void NewLotBuilder::ResetBuilder() {
    m_modified = true;
    m_requiredFieldsMask = ResetMask;
    // NOTE: Required field data is not nulled, because
    //       new object can be built only when all required fields are set again.
    m_wantItTodayPrice = 0;
}
bool NewLotBuilder::AreAllRequiredFieldsFilled() const {
    return m_requiredFieldsMask == AllowedMask;
}
QList<LotRecordsTable::Fields> NewLotBuilder::UnfilledRequiredFields() const {
    QList<LotRecordsTable::Fields> result;
    if((m_requiredFieldsMask & LotId) == 0) {
        result += LotRecordsTable::Fields::LotId;
    }
    if((m_requiredFieldsMask & Make) == 0) {
        result += LotRecordsTable::Fields::MakeId;
    }
    if((m_requiredFieldsMask & Model) == 0) {
        result += LotRecordsTable::Fields::ModelId;
    }
    if((m_requiredFieldsMask & Year) == 0) {
        result += LotRecordsTable::Fields::Year;
    }
    if((m_requiredFieldsMask & Transmission) == 0) {
        result += LotRecordsTable::Fields::TransmissionId;
    }
    if((m_requiredFieldsMask & Body) == 0) {
        result += LotRecordsTable::Fields::BodyId;
    }
    if((m_requiredFieldsMask & Purpose) == 0) {
        result += LotRecordsTable::Fields::PurposeId;
    }
    if((m_requiredFieldsMask & Location) == 0) {
        result += LotRecordsTable::Fields::LocationId;
    }
    if((m_requiredFieldsMask & AuctionDate) == 0) {
        result += LotRecordsTable::Fields::AuctionDate;
    }
    if((m_requiredFieldsMask & SaleStatus) == 0) {
        result += LotRecordsTable::Fields::SaleStatusId;
    }
    if((m_requiredFieldsMask & WorksheetFile) == 0) {
        result += LotRecordsTable::Fields::WorksheetPath;
    }
    return result;
}
void NewLotBuilder::SetLotId(unsigned int lotId) {
    if(lotId == 0 || QString::number(lotId).size() != Options::Instance().ValidLotIdLength()) {
        return;
    }
    m_modified = true;
    m_requiredFieldsMask |= LotId;
    m_lotId = lotId;
}
void NewLotBuilder::SetMakeId(int makeId) {
    if(makeId <= 0) {
        return;
    }
    m_modified = true;
    m_requiredFieldsMask |= Make;
    m_makeId = makeId;
}
void NewLotBuilder::SetMakeName(const QString& makeName) {
    if(makeName.isEmpty()) {
        return;
    }
    m_modified = true;
    m_requiredFieldsMask |= Make;
    m_makeId = -1;
    m_makeName = makeName;
}
void NewLotBuilder::SetModelId(int modelId) {
    if(modelId <= 0) {
        return;
    }
    m_modified = true;
    m_requiredFieldsMask |= Model;
    m_modelId = modelId;
}
void NewLotBuilder::SetModelName(const QString& modelName) {
    if(modelName.isEmpty()) {
        return;
    }
    m_modified = true;
    m_requiredFieldsMask |= Model;
    m_modelId = -1;
    m_modelName = modelName;
}
void NewLotBuilder::SetYear(unsigned int year) {
    if(year == 0) {
        return;
    }
    m_modified = true;
    m_requiredFieldsMask |= Year;
    m_year = year;
}
void NewLotBuilder::SetTransmissionId(unsigned int transmissionId) {
    if(transmissionId == 0) {
        return;
    }
    m_modified = true;
    m_requiredFieldsMask |= Transmission;
    m_transmissionId = transmissionId;
}
void NewLotBuilder::SetBodyId(unsigned int bodyId) {
    if(bodyId == 0) {
        return;
    }
    m_modified = true;
    m_requiredFieldsMask |= Body;
    m_bodyId = bodyId;
}
void NewLotBuilder::SetPurposeId(unsigned int purposeId) {
    if(purposeId == 0) {
        return;
    }
    m_modified = true;
    m_requiredFieldsMask |= Purpose;
    m_purposeId = purposeId;
}
void NewLotBuilder::SetLocationId(unsigned int locationId) {
    if(locationId == 0) {
        return;
    }
    m_modified = true;
    m_requiredFieldsMask |= Location;
    m_locationId = locationId;
}
void NewLotBuilder::SetAuctionDate(const QDate& auctionDate) {
    m_modified = true;
    m_requiredFieldsMask |= AuctionDate;
    m_auctionDate = auctionDate;
}
void NewLotBuilder::SetSaleStatusId(unsigned int saleStatusId) {
    if(saleStatusId == 0) {
        return;
    }
    m_modified = true;
    m_requiredFieldsMask |= SaleStatus;
    m_saleStatusId = saleStatusId;
}
void NewLotBuilder::SetWantItTodayPrice(unsigned int wantItTodayPrice) {
    m_modified = true;
    m_wantItTodayPrice = wantItTodayPrice;
}
void NewLotBuilder::SetWorksheetPath(const QString& worksheetPath) {
    if(worksheetPath.isEmpty() || Tools::FileExtension(worksheetPath) != "pdf") {
        return;
    }
    QString filePath;
    if(!Tools::IsInStorageDir(worksheetPath)) {
        QString randomName = Tools::GenerateRandomName(15, 5, "-", "pdf");
        filePath = Options::Instance().StorageDirectory() + "/" + randomName;
        QFile::copy(worksheetPath, filePath);
    } else {
        filePath = worksheetPath;
    }
    m_modified = true;
    m_requiredFieldsMask |= WorksheetFile;
    m_worksheetPath = filePath;
}
