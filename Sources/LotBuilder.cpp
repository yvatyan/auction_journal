#include "Headers/LotBuilder.h"

LotBuilder::LotBuilder()
    : m_product()
    , m_modified(true)
    , m_requiredFieldsMask(ResetMask)
    , m_missedFlag(false)
{
}
Lot LotBuilder::ConstructLot() {
    if(m_modified) {
        if(!AreAllRequiredFieldsFilled()) {
            throw std::runtime_error("Not all required fields are filled (stage 1).");
        }
        Lot tempLot;
        try {
            tempLot = NewLotBuilder::ConstructLot();
        } catch (const std::runtime_error&) {
            throw std::runtime_error("Not all required fields are filled (stage 2).");
        }
        m_product = Lot(
                        tempLot.LotId(),
                        tempLot.MakeId(),
                        tempLot.ModelId(),
                        tempLot.Year(),
                        tempLot.TransmissionId(),
                        tempLot.BodyId(),
                        tempLot.PurposeId(),
                        tempLot.LocationId(),
                        m_watchDate,
                        tempLot.AuctionDate(),
                        tempLot.SaleStatusId(),
                        m_soldStatusId,
                        m_soldPrice,
                        tempLot.WantItTodayPrice(),
                        tempLot.WorksheetPath(),
                        m_missedFlag
                    );
        m_modified = false;
    }
    return m_product;
}
void LotBuilder::SetLot(const Lot& lot) {
    m_modified = true;
    NewLotBuilder::SetLot(lot);
    SetWatchDate(lot.WatchDate());
}
void LotBuilder::ResetBuilder() {
    m_modified = true;
    m_requiredFieldsMask = ResetMask;
    NewLotBuilder::ResetBuilder();
    // NOTE: Required field data is not nulled, because
    //       new object can be built only when all required fields are set again.
    m_soldStatusId = 0;
    m_soldPrice = 0;
    m_missedFlag = false;
}
bool LotBuilder::AreAllRequiredFieldsFilled() const {
     return m_requiredFieldsMask == AllowedMask;
}
QList<LotRecordsTable::Fields> LotBuilder::UnfilledRequiredFields() const {
    QList<LotRecordsTable::Fields> result;
    if((m_requiredFieldsMask & WatchDate) == 0) {
        result.append(LotRecordsTable::Fields::WatchDate);
    }
    return result;
}
void LotBuilder::SetWatchDate(const QDate& watchDate) {
    m_modified = true;
    m_requiredFieldsMask |= WatchDate;
    m_watchDate = watchDate;
}
void LotBuilder::SetSoldStatusId(unsigned int soldStatusId) {
    m_modified = true;
    m_soldStatusId = soldStatusId;
}
void LotBuilder::SetSoldPrice(unsigned int soldPrice) {
    m_modified = true;
    m_soldPrice = soldPrice;
}
void LotBuilder::SetMissedFlag(bool isMissed) {
    m_modified = true;
    m_missedFlag = isMissed;
}
