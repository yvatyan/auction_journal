#include "Headers/Lot.h"
#include "Headers/Database/Tables/SoldStatusTableApi.h"

Lot::Lot()
    : m_isValid(false)
{
}
Lot::Lot(unsigned int lotId, int makeId, int modelId, unsigned int year, unsigned int transmissionId, unsigned int bodyId, unsigned int purposeId, unsigned int locationId, QDate watchDate, QDate auctionDate, unsigned int saleStatusId, unsigned int soldStatusId, unsigned int soldPrice, unsigned int wantItTodayPrice, QString worksheetPath, bool isAuctionMissed)
    : m_lotId(lotId)
    , m_makeId(makeId)
    , m_modelId(modelId)
    , m_year(year)
    , m_transmissionId(transmissionId)
    , m_bodyId(bodyId)
    , m_purposeId(purposeId)
    , m_locationId(locationId)
    , m_watchDate(watchDate)
    , m_auctionDate(auctionDate)
    , m_saleStatusId(saleStatusId)
    , m_soldStatusId(soldStatusId)
    , m_soldPrice(soldPrice)
    , m_wantItTodayPrice(wantItTodayPrice)
    , m_worksheetPath(worksheetPath)
    , m_missed(isAuctionMissed)
    , m_isValid(true)
    , m_recordId(0)
{
}
bool Lot::IsEnded() const {
    return m_soldStatusId != SoldStatusTable::Pending &&
            m_soldStatusId != SoldStatusTable::WA;
}
void Lot::UpdateMakeId(int makeId) {
    m_makeId = makeId;
    m_newMakeName = QString();
}
void Lot::UpdateModelId(int modelId) {
    m_modelId = modelId;
    m_newModelName = QString();
}
