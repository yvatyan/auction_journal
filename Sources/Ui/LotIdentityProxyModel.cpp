#include <Headers/Ui/LotIdentityProxyModel.h>
#include "Headers/Ui/LotModel.h"
#include "Headers/Options.h"
#include "Headers/Database/Tables/SoldStatusTableApi.h"
#include <QColor>
#include <QBrush>
#include <QFont>

LotIdentityProxyModel::LotIdentityProxyModel(QObject* parent)
    : QIdentityProxyModel(parent)
    , m_rootModel(nullptr)
{
}
QVariant LotIdentityProxyModel::data(const QModelIndex& index, int role) const {
    if (role == Qt::BackgroundRole) {
        QColor color;
        size_t soldStatusId = static_cast<LotModel*>(SourceRootModel())->SoldStatusIdByRow(index.row());
        QDate auctionDate = static_cast<LotModel*>(SourceRootModel())->AuctionDateByRow(index.row());
        QDate today = QDateTime::currentDateTime().date();
        if(auctionDate.year() == 9999 && soldStatusId == SoldStatusTable::Pending) {  // Future sale
            color = QColor(0xcc, 0xff, 0xeb);
        } else if(auctionDate > today && soldStatusId == SoldStatusTable::Pending) {  // Pending sale
            color = QColor(0xe0, 0xcc, 0xff);
        } else if(auctionDate == today && soldStatusId == SoldStatusTable::Pending) { // Today's sale
            color = QColor(0xcc, 0xf2, 0xff);
        } else if(auctionDate < today && soldStatusId == SoldStatusTable::Pending) {  // Sale should be finalized
            color = QColor(0xff, 0xff, 0xcc);
        } else if(soldStatusId == SoldStatusTable::Moved ||
                  soldStatusId == SoldStatusTable::Canceled ||
                  (auctionDate <= today &&
                    (soldStatusId == SoldStatusTable::Sold ||
                     soldStatusId == SoldStatusTable::NotSold))) {					  // Past sale
            color = QColor(0xee, 0xee, 0xee);
        } else if(auctionDate <= today && soldStatusId == SoldStatusTable::WA) {      // Waiting approval sale
            color = QColor(0xff, 0xbf, 0xbc);
        } else {											   						  // Undefined
            color = QColor(Qt::white);
        }
        return QBrush(color);
    } else if(role == Qt::FontRole) {
        return Options::Instance().LotRecordsTableCellFont();
    }
    return QIdentityProxyModel::data(index, role);
}
QAbstractItemModel* LotIdentityProxyModel::SourceRootModel() const {
    return m_rootModel;
}
void LotIdentityProxyModel::SetSourceRootModel(QAbstractItemModel* rootModel) {
    m_rootModel = rootModel;
}
