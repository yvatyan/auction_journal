#include "Headers/Ui/CustomListWidgetItem.h"
#include "Headers/Tools.h"

CustomListWidgetItem::CustomListWidgetItem(QListWidget* parent, int type)
    : QListWidgetItem(parent, type)
{
}
CustomListWidgetItem::CustomListWidgetItem(const QString& text, QListWidget* parent, int type)
    : QListWidgetItem(text, parent, type)
{
}
CustomListWidgetItem::CustomListWidgetItem(const QIcon& icon, const QString& text, QListWidget* parent, int type)
    : QListWidgetItem(icon, text, parent, type)
{
}
CustomListWidgetItem::CustomListWidgetItem(const QListWidgetItem& other)
    : QListWidgetItem(other)
{
}
CustomListWidgetItem::~CustomListWidgetItem()
{
}
bool CustomListWidgetItem::operator<(const QListWidgetItem& other) const {
    return Tools::CompareNumberedStrings(text(), other.text()) == 2;
}
