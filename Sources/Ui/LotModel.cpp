#include "Headers/Ui/LotModel.h"
#include "Headers/Options.h"
#include "Headers/LotResolver.h"
#include "Headers/Tools.h"
#include <QFont>

LotModel::LotModel(QObject* parent)
    : QAbstractTableModel(parent)
    , m_database(nullptr)
{
    initializeColumnDescription();
}
int LotModel::rowCount(const QModelIndex& parent) const {
    Q_UNUSED(parent);
    return m_lotRows.size();
}
int LotModel::columnCount(const QModelIndex& parent) const {
    Q_UNUSED(parent);
    return m_columns.size();
}
QVariant LotModel::data(const QModelIndex& index, int role) const {
    Lot lot = m_lotRows.at(index.row());
    LotResolver resolver(lot, m_database);
    if(role == Qt::DisplayRole) {
        switch(m_columns[index.column()].first) {
            case LotRecordsTable::Fields::Id : break;
            case LotRecordsTable::Fields::LotId : {
                return QVariant(resolver.LotId());
            }
            case LotRecordsTable::Fields::MakeId : {
                QString makeName = resolver.MakeName();
                return QVariant(Tools::ChangeStringCase(makeName, Tools::StringCase::AllFirstUpper));
            }
            case LotRecordsTable::Fields::ModelId : {
                QString modelName = resolver.ModelName();
                return QVariant(Tools::ChangeStringCase(modelName, Tools::StringCase::AllFirstUpper));
            }
            case LotRecordsTable::Fields::Year : {
                return QVariant(resolver.Year());
            }
            case LotRecordsTable::Fields::TransmissionId : {
                QString transmissionName = resolver.TransmissionName();
                return QVariant(Tools::ChangeStringCase(transmissionName, Tools::StringCase::AllFirstUpper));
            }
            case LotRecordsTable::Fields::BodyId : {
                QString bodyName = resolver.BodyName();
                return QVariant(Tools::ChangeStringCase(bodyName, Tools::StringCase::AllFirstUpper));
            }
            case LotRecordsTable::Fields::PurposeId : {
                QString purposeName = resolver.PurposeName();
                return QVariant(Tools::ChangeStringCase(purposeName, Tools::StringCase::FirstUpper));
            }
            case LotRecordsTable::Fields::LocationId : {
                QString locationAbbr = resolver.LocationAbbreviation();
                return QVariant(Tools::ChangeStringCase(locationAbbr, Tools::StringCase::AllUpper));
            }
            case LotRecordsTable::Fields::WatchDate : {
                QDate date = resolver.WatchDate();
                if(date == QDateTime::currentDateTime().date()) {
                    return QVariant("Today");
                } else {
                    return QVariant(date.toString(Options::Instance().DateFormat()));
                }
            }
            case LotRecordsTable::Fields::AuctionDate : {
                QDate date = resolver.AuctionDate();
                if(date.year() == 9999) {
                    return QVariant("Future");
                } else if(date == QDateTime::currentDateTime().date()) {
                    return QVariant("Today");
                } else {
                    return QVariant(date.toString(Options::Instance().DateFormat()));
                }
            }
            case LotRecordsTable::Fields::SaleStatusId : {
                QString saleStatusName = resolver.SaleStatus();
                return QVariant(Tools::ChangeStringCase(saleStatusName, Tools::StringCase::FirstUpper));
            }
            case LotRecordsTable::Fields::SoldStatusId : {
                QString soldStatusName = resolver.SoldStatus();
                if(resolver.IsMissed()) {
                    soldStatusName.prepend("missed, ");
                }
                return QVariant(Tools::ChangeStringCase(soldStatusName, Tools::StringCase::FirstUpper));
            }
            case LotRecordsTable::Fields::SoldPrice : {
                size_t price = resolver.SoldPrice();
                if(price == 0) {
                    return QString("–"); // UNICODE
                } else {
                    return QVariant("$" + QString::number(price));
                }
            }
            case LotRecordsTable::Fields::WitPrice : {
                size_t price = resolver.WitPrice();
                if(price == 0) {
                    return QString("–"); // UNICODE
                } else {
                    return QVariant("$" + QString::number(price));
                }
            }
            case LotRecordsTable::Fields::WorksheetPath : break;
            case LotRecordsTable::Fields::Missed : break;
            default: break;
        };
    }
    if(role == Qt::ToolTipRole) {
        switch(m_columns[index.column()].first) {
            case LotRecordsTable::Fields::LocationId : {
                QString locationName = resolver.LocationName();
                return QVariant(Tools::ChangeStringCase(locationName, Tools::StringCase::AllFirstUpper));
            }
            default: break;
        }
    }
    return QVariant();
}
QVariant LotModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if(role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        return m_columns[section].second;
    } else if(role == Qt::DisplayRole && orientation == Qt::Vertical) {
        return QVariant(section + 1);
    } else if(role == Qt::FontRole) {
        return Options::Instance().LotRecordsTableHeaderFont();
    }
    return QVariant();
}
void LotModel::SetDatabase(Database* database) {
    m_database = database;
}
QDate LotModel::AuctionDateByRow(int row) {
    return m_lotRows.at(row).AuctionDate();
}
size_t LotModel::SoldStatusIdByRow(int row) {
    return m_lotRows.at(row).SoldStatusId();
}
void LotModel::InsertLot(const Lot &lot) {
    bool inserted = false;
    for(int i = 0; i < m_lotRows.size(); ++i) {
        if(lot.AuctionDate() > m_lotRows.at(i).AuctionDate()) {
            beginInsertRows(QModelIndex(), i, i);
            m_lotRows.insert(i, lot);
            endInsertRows();
            inserted = true;
            break;
        }
    }
    if(!inserted) {
        beginInsertRows(QModelIndex(), m_lotRows.size(), m_lotRows.size());
        m_lotRows.append(lot);
        endInsertRows();
    }
}
void LotModel::RemoveRow(int index) {
    beginRemoveRows(QModelIndex(), index, index);
    m_lotRows.removeAt(index);
    endRemoveRows();
}
Lot LotModel::GetLotByModelIndex(const QModelIndex &mIndex) const {
    return m_lotRows.at(mIndex.row());
}
Lot LotModel::GetLotByRow(int row) const {
    return m_lotRows.at(row);
}
void LotModel::UpdateRecord(size_t recordId, const Lot& lot) {
    for(int i = 0; i < m_lotRows.size(); ++i) {
        if(m_lotRows.at(i).GetRecordId() == recordId) {
            if(lot.GetRecordId() == recordId) { // Update of the recrd
                m_lotRows.replace(i, lot);
                emit dataChanged(createIndex(i, 0), createIndex(i, m_columns.size() - 1));
            } else {							// Insert new entry
               RemoveRow(i);
               InsertLot(lot);
            }
            break;
        }
    }
}
void LotModel::DropData() {
    if(!m_lotRows.isEmpty()) {
        beginRemoveRows(QModelIndex(), 0, m_lotRows.size() - 1);
        int rowQty = m_lotRows.size();
        for(int i = 0; i < rowQty; ++i) {
            m_lotRows.removeLast();
        }
        endRemoveRows();
    }
}
LotRecordsTable::Fields LotModel::GetFieldByColumn(int column) const {
    return m_columns[column].first;
}
int LotModel::GetColumnByField(LotRecordsTable::Fields field) const {
    QMapIterator<int, QPair<LotRecordsTable::Fields, QString>> iter(m_columns);
    while(iter.hasNext()) {
        iter.next();
        if(iter.value().first == field) {
            return iter.key();
        }
    }
    return -1;
}
void LotModel::initializeColumnDescription() {
    m_columns.insert(0, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::LotId, "Lot #"));
    m_columns.insert(1, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::MakeId, "Make"));
    m_columns.insert(2, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::ModelId, "Model"));
    m_columns.insert(3, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::Year, "Year"));
    m_columns.insert(4, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::TransmissionId, "Transmission"));
    m_columns.insert(5, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::BodyId, "Body"));
    m_columns.insert(6, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::PurposeId, "Purpose"));
    m_columns.insert(7, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::LocationId, "Location"));
    m_columns.insert(8, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::WatchDate, "Watching Since"));
    m_columns.insert(9, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::AuctionDate, "Auction Date"));
    m_columns.insert(10, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::SaleStatusId, "Sale Status"));
    m_columns.insert(11, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::SoldStatusId, "Sold Status"));
    m_columns.insert(12, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::SoldPrice, "Sold Price"));
    m_columns.insert(13, QPair<LotRecordsTable::Fields, QString>(LotRecordsTable::Fields::WitPrice, "Want It Today"));
}
