#include "Headers/Ui/ResizeNotifyingButton.h"

ResizeNotifyingButton::ResizeNotifyingButton(QWidget* parent)
    : QPushButton(parent)
{
}
ResizeNotifyingButton::ResizeNotifyingButton(const QString& text, QWidget* parent)
    : QPushButton(text, parent)
{
}
ResizeNotifyingButton::ResizeNotifyingButton(const QIcon& icon, const QString& text, QWidget* parent)
    : QPushButton(icon, text, parent)
{
}
ResizeNotifyingButton::~ResizeNotifyingButton()
{
}
void ResizeNotifyingButton::resizeEvent(QResizeEvent* event) {
    QPushButton::resizeEvent(event);
    emit buttonResized();
}
