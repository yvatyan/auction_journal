#include "Headers/Ui/MainWindow.h"
#include "Headers/Database/DatabaseManager.h"
#include "Headers/Ui/NewVehicleDialog.h"
#include "Headers/Ui/EndAuctionDialog.h"
#include "Headers/Ui/NewEntryDialog.h"
#include "ui_MainWindow.h"
#include "ui_CommentWindow.h"
#include "ui_PdfViewerWindow.h"
#include "Headers/Options.h"
#include "Headers/NotificationManager.h"
#include "Headers/Tools.h"
#include "Headers/Ui/ShapeWidget.h"
#include "Headers/EndedLotBuilder.h"
#include "Headers/Ui/CommentItemDelegate.h"
#include "Headers/Ui/AboutDialog.h"
#include "Headers/Ui/LotFilterDialog.h"
#include "Headers/Ui/TemplateQMainWindow.h"
#include "Headers/Configuration/Version.h"
#include <QFileInfo>

MainWindow::MainWindow(Database* recordsDb, QWidget* parent)
    : QMainWindow(parent)
    , m_mainUi(nullptr)
    , m_commentsUi(nullptr)
    , m_pdfViewerUi(nullptr)
    , m_recordsDb(recordsDb)
    , m_criticalErrorOccurred(false)
    , m_timelineCount(nullptr)
    , m_commentCount(nullptr)
    , m_timelineMode(false)
    , m_currentTabButton(TabButton::All)
    , m_commentModel(nullptr)
{
    setWindowIcon(Options::Instance().ApplicationIcon());
    setWindowTitle(Options::Instance().ApplicationName() + " (" + Version_Short + ")");
    setupUiStack();
}
MainWindow::~MainWindow() {
    clearAllUi();
}
bool MainWindow::CriticalErrorOccurred() const {
    return m_criticalErrorOccurred;
}
void MainWindow::resizeEvent(QResizeEvent* event) {
    Q_UNUSED(event);
    if(m_timelineCount != nullptr && m_commentCount != nullptr) {
        adjustCountersPosition();
    }
}
void MainWindow::switchToMainWindow() {
    if(m_commentModel != nullptr) {
        delete m_commentModel;
        m_commentModel = nullptr;
    }
    m_pdfDocument.close();
    m_stackedUi.setCurrentIndex(m_mainUiIndex);
    adjustCountersPosition();
}
void MainWindow::switchToCommentsWindow() {
    QList<size_t> commentLot = m_mainUi->lotTableWidget->GetSelectedRecordIds();
    if(commentLot.size() != 1) {
        throw std::runtime_error("Can\'t show comments for multiple records.");
    }
    m_commentRecordId = commentLot.at(0);
    LotCommentsTable* commentsTable = static_cast<LotCommentsTable*>(m_recordsDb->Table(LOTCOMMENTS_TABLE_NAME));
    QList<Comment> commentList = commentsTable->Api().GetCommentsByRecordId(m_commentRecordId);
    m_commentModel = new CommentItemModel(commentList);
    m_commentsUi->commentItemView->setModel(m_commentModel);

    m_stackedUi.setCurrentIndex(m_commentsUiIndex);
}
void MainWindow::switchToPdfViewerWindow(const QString& pdfPath) {
    if(pdfPath.isEmpty()) {
        QList<size_t> lot = m_mainUi->lotTableWidget->GetSelectedRecordIds();
        if(lot.size() != 1) {
            throw std::runtime_error("Can\'t show pdf worksheet for multiple records.");
        }

        size_t recordId = lot.at(0);
        LotRecordsTable* recordsTable = static_cast<LotRecordsTable*>(m_recordsDb->Table(LOTRECORDS_TABLE_NAME));
        QString recordWorksheetPath(recordsTable->Api().GetWorksheetPathByRecordId(recordId));
        setupPdfViewer(recordWorksheetPath);
    } else {
        QFileInfo file(pdfPath);
        if(file.exists() && file.isFile() && file.isReadable()) {
            setupPdfViewer(pdfPath);
        } else {
            if(!file.exists()) {
                throw std::runtime_error(QString("File " + pdfPath + " does\'t exists.").toStdString());
            }
            if(!file.isFile()) {
                throw std::runtime_error(QString("File " + pdfPath + " is not a file.").toStdString());
            }
            if(!file.isReadable()) {
                throw std::runtime_error(QString("File " + pdfPath + " is not readable.").toStdString());
            }
        }
    }
    m_stackedUi.setCurrentIndex(m_pdfViewerUiIndex);
}
void MainWindow::onAddNewVehicleButtonClicked() {
    NewVehicleDialog newVehicleDialog(m_recordsDb);
    if(newVehicleDialog.exec() == QDialog::Accepted) {
        // Warning: Worksheet file is copied when dialog is accepted, but is not deleted if operation fails.
        Lot newVehicle = newVehicleDialog.GetResult();
        if(newVehicle.ContainsUnregisteredData()) {
            try {
                registerNewDataFromLot(newVehicle);
            } catch(const std::runtime_error& err) {
                QString message = "Can\'t register new make\\model.\nError: ";
                message += err.what();
                NotificationManager::Instance().ShowNotification(message, NotificationManager::Type::Error);
                return;
            }
            try {
                updateUnregisteredDataForLot(newVehicle);
            } catch (const std::runtime_error& err) {
                QString message = "Can\'t retrieve recently registered make\\model ID.\nError: ";
                message += err.what();
                NotificationManager::Instance().ShowNotification(message, NotificationManager::Type::Error);
                return;
            }
        }
        LotRecordsTable* recordsTable = static_cast<LotRecordsTable*>(m_recordsDb->Table(LOTRECORDS_TABLE_NAME));
        try {
            size_t recordId = recordsTable->Api().InsertLot(newVehicle);
            newVehicle.SetRecordId(recordId);
            if(newVehicleDialog.GetCommentDocument()->toPlainText().length() != 0) {
                registerNewComment(recordId, newVehicleDialog.GetCommentDocument()->toHtml());
            }
        } catch (const std::runtime_error& err) {
            QString message = "Can\'t add new vehicle record.\nError: ";
            message += err.what();
            NotificationManager::Instance().ShowNotification(message, NotificationManager::Type::Error);
            return;
        }
        m_mainUi->lotTableWidget->AddNewRecord(newVehicle);
    }
}
void MainWindow::onAuctionEndButtonClicked() {
    QList<Lot> selectedLots(m_mainUi->lotTableWidget->GetSelectedLots());
    if(selectedLots.size() != 1) {
        throw std::runtime_error("Multiple or no record\' to end requested.");
    }
    Lot targetLot = selectedLots.at(0);
    if(targetLot.SoldStatusId() == SoldStatusTable::WA) {
        try {
            finalizeWaitingApprovalLot(targetLot);
        } catch (const std::runtime_error& err) {
            QString message = "Can\'t approve sale.\nError: ";
            message += err.what();
            NotificationManager::Instance().ShowNotification(message, NotificationManager::Type::Error);
            return;
        }
    } else {
        try {
            finalizeCommonLot(targetLot);
        } catch (const std::runtime_error& err) {
            QString message = "Can\'t finilize auction.\nError: ";
            message += err.what();
            NotificationManager::Instance().ShowNotification(message, NotificationManager::Type::Error);
            return;
        }
    }
}
void MainWindow::onTimelineButtonClicked() {
    m_timelineMode = true;
    QList<size_t> selectedLot = m_mainUi->lotTableWidget->GetSelectedLotIds();
    if(selectedLot.size() != 1) {
        throw std::runtime_error("Multiple or no record\' timeline requested.");
    }
    LotRecordsTable* records = static_cast<LotRecordsTable*>(m_recordsDb->Table(LOTRECORDS_TABLE_NAME));
    QList<Lot> lotList = records->Api().GetAllLotEntries(selectedLot.at(0));
    m_mainUi->lotTableWidget->ClearData();
    for(auto lot : lotList) {
        m_mainUi->lotTableWidget->AddNewRecord(lot);
    }
    m_timelineCount->hide();
    m_mainUi->timelineB->setEnabled(false);
    QString buttonText = m_mainUi->b_closeTimeline->text();
    buttonText.remove(buttonText.size() - Options::Instance().ValidLotIdLength(), Options::Instance().ValidLotIdLength());
    buttonText.append(QString::number(selectedLot.at(0)));
    m_mainUi->b_closeTimeline->setText(buttonText);
    m_mainUi->b_closeTimeline->show();
}
void MainWindow::onCommentsButtonClicked() {
    switchToCommentsWindow();
}
void MainWindow::onPdfViewerButtonClicked() {
    switchToPdfViewerWindow();
}
void MainWindow::onDeleteRecordButtonClicked() {
    if(m_mainUi->lotTableWidget->HasSelection()) {
        QString question = "Do you really want to delete the selected lot(s)?";
        if(NotificationManager::Instance().ShowNotification(question, NotificationManager::Type::Question)) {
            LotRecordsTable* recordTable = static_cast<LotRecordsTable*>(m_recordsDb->Table(LOTRECORDS_TABLE_NAME));
            LotCommentsTable* commentsTable = static_cast<LotCommentsTable*>(m_recordsDb->Table(LOTCOMMENTS_TABLE_NAME));
            if(!m_timelineMode) {
                QList<size_t> lotsToDelete = m_mainUi->lotTableWidget->GetSelectedLotIds();
                for(auto lotId : lotsToDelete) {
                    QList<size_t> recordIds = recordTable->Api().GetRecordIdsForLotId(lotId);
                    for(auto recordId : recordIds) {
                        commentsTable->Api().RemoveCommentByRecordId(recordId);
                        QFile file(recordTable->Api().GetWorksheetPathByRecordId(recordId));
                        file.remove();
                    }
                    recordTable->Api().RemoveLotEntries(lotId);
                }
            } else {
                QList<size_t> recordsToDelete = m_mainUi->lotTableWidget->GetSelectedRecordIds();
                for(auto recordId : recordsToDelete) {
                    commentsTable->Api().RemoveCommentByRecordId(recordId);
                    recordTable->Api().RemoveLotRecord(recordId);
                    QFile file(recordTable->Api().GetWorksheetPathByRecordId(recordId));
                    file.remove();
                }
            }
            m_mainUi->lotTableWidget->DeleteSelectedRecords();
        }
    }
}
void MainWindow::onExitButtonClicked() {
    QApplication::quit();
}
void MainWindow::adjustCountersPosition() {
    if(m_timelineCount != nullptr && m_commentCount != nullptr) {
        m_timelineCount->ChangeCenter(QPoint(m_mainUi->timelineB->x() + m_mainUi->timelineB->width(),
                                             m_mainUi->timelineB->y() + m_mainUi->timelineB->height()));

        m_commentCount->ChangeCenter(QPoint(m_mainUi->commentsB->x() + m_mainUi->commentsB->width(),
                                            m_mainUi->commentsB->y() + m_mainUi->commentsB->height()));
    }
}
void MainWindow::onTableWidgetDeselected() {
    m_timelineCount->hide();
    m_commentCount->hide();
    m_mainUi->auctionEndB->setEnabled(false);
    m_mainUi->timelineB->setEnabled(false);
    m_mainUi->commentsB->setEnabled(false);
    m_mainUi->pdfViewerB->setEnabled(false);
    m_mainUi->deleteRecordB->setEnabled(false);
}
void MainWindow::onTableWidgetOneRowSelected(int row) {
    m_mainUi->commentsB->setEnabled(true);
    m_mainUi->pdfViewerB->setEnabled(true);
    m_mainUi->deleteRecordB->setEnabled(true);
    Lot lot = m_mainUi->lotTableWidget->GetLotByRow(row);
    LotRecordsTable* recordsTable = static_cast<LotRecordsTable*>(m_recordsDb->Table(LOTRECORDS_TABLE_NAME));
    size_t lotEntriesCount = recordsTable->Api().GetLotEntriesQty(lot.LotId()) - 1;
    m_mainUi->auctionEndB->setEnabled(!lot.IsEnded());
    if(!m_timelineMode) {
        m_timelineCount->show();
        if(lotEntriesCount == 0) {
            m_timelineCount->ChangeText("0");
            m_mainUi->timelineB->setEnabled(false);
        } else if(lotEntriesCount <= 99) {
            m_timelineCount->ChangeText(QString::number(lotEntriesCount));
            m_mainUi->timelineB->setEnabled(true);
        } else {
            m_timelineCount->ChangeText("99+");
            m_mainUi->timelineB->setEnabled(true);
        }
        m_timelineCount->update();
    }
    LotCommentsTable* commentsTable = static_cast<LotCommentsTable*>(m_recordsDb->Table(LOTCOMMENTS_TABLE_NAME));
    size_t commentEntriesCount = commentsTable->Api().GetCommentQtyForRecord(lot.GetRecordId());
    m_commentCount->show();
    if(commentEntriesCount <= 99) {
        m_commentCount->ChangeText(QString::number(commentEntriesCount));
    } else {
        m_commentCount->ChangeText("99+");
    }
    m_commentCount->update();
}
void MainWindow::onTableWidgetMultiRowsSelected(QList<int> rows) {
    Q_UNUSED(rows);
    m_timelineCount->hide();
    m_commentCount->hide();
    m_mainUi->auctionEndB->setEnabled(false);
    m_mainUi->timelineB->setEnabled(false);
    m_mainUi->commentsB->setEnabled(false);
    m_mainUi->pdfViewerB->setEnabled(false);
    m_mainUi->deleteRecordB->setEnabled(true);
}
void MainWindow::onNewComment(const QString& text) {
    Comment comment = registerNewComment(m_commentRecordId, text);
    m_commentModel->AddNewComment(comment);
}
void MainWindow::onEditComment(const QModelIndex& modelIndex, const QString& text) {
    if(!text.isEmpty()) {
        LotCommentsTable* commentsTable = static_cast<LotCommentsTable*>(m_recordsDb->Table(LOTCOMMENTS_TABLE_NAME));
        size_t id = qvariant_cast<Comment>(modelIndex.data()).GetId();
        commentsTable->Api().EditComment(id, text, QDateTime::currentDateTime().date());
        Comment comment = commentsTable->Api().GetCommentById(id);
        m_commentModel->ChangeComment(modelIndex, comment);
    }
}
void MainWindow::onDeleteComment(const QModelIndexList& modelIndices) {
    LotCommentsTable* commentsTable = static_cast<LotCommentsTable*>(m_recordsDb->Table(LOTCOMMENTS_TABLE_NAME));
    QModelIndexList sortedList(modelIndices);
    std::sort(sortedList.begin(), sortedList.end(), [](const QModelIndex& lhv, const QModelIndex& rhv) {
        return lhv.row() > rhv.row();
    });
    for(auto modelIndex : sortedList) {
        size_t id = qvariant_cast<Comment>(modelIndex.data()).GetId();
        commentsTable->Api().RemoveCommentById(id);
        m_commentModel->RemoveComment(modelIndex);
    }
}
void MainWindow::onStateLicensingClicked() {
    switchToPdfViewerWindow(Options::Instance().StateLicensingFilePath());
}
void MainWindow::onMemberFeesClicked() {
    switchToPdfViewerWindow(Options::Instance().MemberFeesFilePath());
}
void MainWindow::onAboutClicked() {
    AboutDialog aboutDialog;
    aboutDialog.exec();
}
void MainWindow::onUpcommingTabButtonClicked() {
    m_mainUi->lotTableWidget->SetFilter(m_upcommingFilter);
    m_currentTabButton = TabButton::Upcomming;
}
void MainWindow::onEndedTabButtonClicked() {
    m_mainUi->lotTableWidget->SetFilter(m_endedFilters.at(0));
    for(int i = 1; i < m_endedFilters.size(); ++i) {
        m_mainUi->lotTableWidget->AddFilter(m_endedFilters.at(i));
    }
    m_currentTabButton = TabButton::Ended;
}
void MainWindow::onPastTabButtonClicked() {
    m_mainUi->lotTableWidget->SetFilter(m_pastFilters.at(0));
    for(int i = 1; i < m_pastFilters.size(); ++i) {
        m_mainUi->lotTableWidget->AddFilter(m_pastFilters.at(i));
    }
    m_currentTabButton = TabButton::Past;
}
void MainWindow::onAllTabButtonClicked() {
    m_mainUi->lotTableWidget->SetFilter(LotFilterDialog::FilterCollection());
    m_currentTabButton = TabButton::All;
}
void MainWindow::onFilterTabButtonClicked() {
    if(m_customFilter.count() > 0) {
        m_mainUi->lotTableWidget->SetFilter(m_customFilter);
        m_currentTabButton = TabButton::Custom;
    } else {
        onFilterTabButtonDoubleClicked();
    }
}
void MainWindow::onFilterTabButtonDoubleClicked() {
    LotFilterDialog filterDialog(&m_customFilter);
    if(filterDialog.exec() == QDialog::Accepted) {
        m_customFilter = filterDialog.GetResult();
        Database* db = DatabaseManager::Instance().GetSettingsDatabase();
        FilterSettingsTable* filterTable = static_cast<FilterSettingsTable*>(db->Table(FILTERSETTINGS_TABLE_NAME));
        if(filterDialog.ApplyFiltersOnStartup()) {
            filterTable->Api().SaveFilter(m_customFilter);
        } else {
            filterTable->Api().TurnOffFilter();
        }
        m_mainUi->lotTableWidget->SetFilter(m_customFilter);
        m_currentTabButton = TabButton::Custom;
    }
}
void MainWindow::onCloseTimelineButtonClicked() {
    m_timelineMode = false;
    m_mainUi->lotTableWidget->ClearData();
    loadRecordTable();
    m_mainUi->b_closeTimeline->hide();
}
void MainWindow::setupUiStack() {
    setupMainWindow();
    setupCommentsWindow();
    setupPdfViewerWindow();
    setCentralWidget(&m_stackedUi);
    m_stackedUi.setCurrentIndex(m_mainUiIndex);
}
void MainWindow::setupMainWindow() {
    TemplateQMainWindow<Ui::mainWindow>* mainUiWidget = new TemplateQMainWindow<Ui::mainWindow>();
    m_mainUi = mainUiWidget->m_ui;
    m_mainUiIndex = m_stackedUi.addWidget(mainUiWidget);
    m_mainUi->b_closeTimeline->hide();
    QString dummyLotId;
    dummyLotId.fill('*', Options::Instance().ValidLotIdLength());
    m_mainUi->b_closeTimeline->setText(m_mainUi->b_closeTimeline->text() + dummyLotId);
    setupMainUiExclusiveButtonGroup();
    m_timelineCount = new ShapeWidget(ShapeWidget::Ellipse, m_mainUi->centralWidget);
    m_timelineCount->ChangeSize(QSize(22, 22));
    m_timelineCount->ChangeColor(QColor(0xff, 0x00, 0x1e));
    m_timelineCount->hide();
    m_commentCount = new ShapeWidget(ShapeWidget::Ellipse, m_mainUi->centralWidget);
    m_commentCount->ChangeSize(QSize(22, 22));
    m_commentCount->ChangeColor(QColor(0xff, 0x00, 0x1e));
    m_commentCount->hide();

    QObject::connect(m_mainUi->addNewVehicleB, SIGNAL(clicked()), this, SLOT(onAddNewVehicleButtonClicked()));
    QObject::connect(m_mainUi->auctionEndB, SIGNAL(clicked()), this, SLOT(onAuctionEndButtonClicked()));
    QObject::connect(m_mainUi->timelineB, SIGNAL(clicked()), this, SLOT(onTimelineButtonClicked()));
    QObject::connect(m_mainUi->commentsB, SIGNAL(clicked()), this, SLOT(onCommentsButtonClicked()));
    QObject::connect(m_mainUi->pdfViewerB, SIGNAL(clicked()), this, SLOT(onPdfViewerButtonClicked()));
    QObject::connect(m_mainUi->deleteRecordB, SIGNAL(clicked()), this, SLOT(onDeleteRecordButtonClicked()));
    QObject::connect(m_mainUi->exitB, SIGNAL(clicked()), this, SLOT(onExitButtonClicked()));
    QObject::connect(m_mainUi->timelineB, SIGNAL(buttonResized()), this, SLOT(adjustCountersPosition()));
    QObject::connect(m_mainUi->lotTableWidget, SIGNAL(rowsDeselected()), this, SLOT(onTableWidgetDeselected()));
    QObject::connect(m_mainUi->lotTableWidget, SIGNAL(oneRowSelected(int)), this, SLOT(onTableWidgetOneRowSelected(int)));
    QObject::connect(m_mainUi->lotTableWidget, SIGNAL(multiRowsSelected(QList<int>)), this, SLOT(onTableWidgetMultiRowsSelected(QList<int>)));
    QObject::connect(m_mainUi->l_stateLicensing, SIGNAL(clicked()), this, SLOT(onStateLicensingClicked()));
    QObject::connect(m_mainUi->l_memberFees, SIGNAL(clicked()), this, SLOT(onMemberFeesClicked()));
    QObject::connect(m_mainUi->l_about, SIGNAL(clicked()), this, SLOT(onAboutClicked()));
    QObject::connect(m_mainUi->upcomingB, SIGNAL(clicked()), this, SLOT(onUpcommingTabButtonClicked()));
    QObject::connect(m_mainUi->endedB, SIGNAL(clicked()), this, SLOT(onEndedTabButtonClicked()));
    QObject::connect(m_mainUi->pastB, SIGNAL(clicked()), this, SLOT(onPastTabButtonClicked()));
    QObject::connect(m_mainUi->allB, SIGNAL(clicked()), this, SLOT(onAllTabButtonClicked()));
    QObject::connect(m_mainUi->b_custom, SIGNAL(clicked()), this, SLOT(onFilterTabButtonClicked()));
    QObject::connect(m_mainUi->b_custom, SIGNAL(doubleClicked()), this, SLOT(onFilterTabButtonDoubleClicked()));
    QObject::connect(m_mainUi->b_closeTimeline, SIGNAL(clicked()), this, SLOT(onCloseTimelineButtonClicked()));

    m_mainUi->lotTableWidget->SetDatabase(m_recordsDb);
    loadRecordTable();
    setupFilters();
}
void MainWindow::setupCommentsWindow() {
    TemplateQMainWindow<Ui::commentsWindow>* commentsUiWidget = new TemplateQMainWindow<Ui::commentsWindow>();
    m_commentsUi = commentsUiWidget->m_ui;
    m_commentsUiIndex = m_stackedUi.addWidget(commentsUiWidget);
    m_commentsUi->commentItemView->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    m_commentsUi->commentItemView->SetDelegateEditor(m_commentsUi->w_commentEditor);

    QObject::connect(m_commentsUi->backB, SIGNAL(clicked()), this, SLOT(switchToMainWindow()));
    QObject::connect(m_commentsUi->w_commentEditor, SIGNAL(sigNewTextSubmited(const QString&)), this, SLOT(onNewComment(const QString&)));
    QObject::connect(m_commentsUi->w_commentEditor, SIGNAL(sigEditedTextSubmited(const QModelIndex&, const QString&)), this, SLOT(onEditComment(const QModelIndex&, const QString&)));
    QObject::connect(m_commentsUi->w_commentEditor, SIGNAL(sigDeleteAccepted(const QModelIndexList&)), this, SLOT(onDeleteComment(const QModelIndexList&)));
}
void MainWindow::setupPdfViewerWindow() {
    TemplateQMainWindow<Ui::pdfViewerWindow>* pdfViewerUiWidget = new TemplateQMainWindow<Ui::pdfViewerWindow>();
    m_pdfViewerUi = pdfViewerUiWidget->m_ui;
    m_pdfViewerUiIndex = m_stackedUi.addWidget(pdfViewerUiWidget);

    QObject::connect(m_pdfViewerUi->backB, SIGNAL(clicked()), this, SLOT(switchToMainWindow()));
}
void MainWindow::clearAllUi() {
    if(m_commentsUi != nullptr) {
        delete m_commentsUi;
        delete m_commentModel;
        m_commentsUi = nullptr;
        m_commentModel = nullptr;
    }
    if(m_mainUi != nullptr) {
        delete m_mainUi;
        delete m_timelineCount;
        delete m_commentCount;
        m_mainUi = nullptr;
        m_timelineCount = nullptr;
        m_commentCount = nullptr;
    }
    if(m_pdfViewerUi != nullptr) {
        delete m_pdfViewerUi;
        m_pdfViewerUi = nullptr;
    }
}
void MainWindow::loadRecordTable() {
    LotRecordsTable* records = static_cast<LotRecordsTable*>(m_recordsDb->Table(LOTRECORDS_TABLE_NAME));
    QList<Lot> lotList = records->Api().GetAllLastLots();
    for(auto lot : lotList) {
        m_mainUi->lotTableWidget->AddNewRecord(lot);
    }
    m_mainUi->lotTableWidget->DefaultSort();
}
void MainWindow::finalizeWaitingApprovalLot(const Lot& targetLot) {
    QString question = "Has the sale of Lot #" + QString::number(targetLot.LotId()) + " been approved?";
    int result = NotificationManager::Instance().ShowNotification(question, NotificationManager::Type::Question3);
    if(result == 1) {
        saleApproved(targetLot);
    } else if(result == 0){
        saleRejected(targetLot);
    }
}
void MainWindow::finalizeCommonLot(const Lot& targetLot) {
    EndAuctionDialog endAuctionDialog(m_recordsDb, targetLot);
    if(endAuctionDialog.exec() == QDialog::Accepted) {
        Lot finalizedLot = endAuctionDialog.GetResult();
        finalizedLot.SetRecordId(targetLot.GetRecordId());
        if(endAuctionDialog.GetCommentDocument()->toPlainText().length() != 0) {
            registerNewComment(targetLot.GetRecordId(), endAuctionDialog.GetCommentDocument()->toHtml());
        }

        size_t soldStatusId = finalizedLot.SoldStatusId();
        if(soldStatusId == SoldStatusTable::Sold ||
           soldStatusId == SoldStatusTable::WA ||
           soldStatusId == SoldStatusTable::Canceled)
        {
            saleEnded(finalizedLot);
        } else if(soldStatusId == SoldStatusTable::NotSold ||
                  soldStatusId == SoldStatusTable::Moved)
        {
            saleContinues(finalizedLot);
        }
    }
}
void MainWindow::saleApproved(const Lot& targetLot) const {
    EndedLotBuilder builder;
    builder.SetLot(targetLot);
    builder.SetSoldPriceRequired(!targetLot.IsAuctionMissed());
    builder.SetSoldPrice(targetLot.SoldPrice());
    builder.SetSoldStatusId(SoldStatusTable::Sold);
    builder.SetMissedFlag(targetLot.IsAuctionMissed());
    Lot finalizedLot = builder.ConstructLot();
    LotRecordsTable* recordsTable = static_cast<LotRecordsTable*>(m_recordsDb->Table(LOTRECORDS_TABLE_NAME));
    recordsTable->Api().AlterFinalFields(finalizedLot);
    m_mainUi->lotTableWidget->UpdateRecordByRecordId(finalizedLot);
}
void MainWindow::saleRejected(const Lot& targetLot) {
    EndedLotBuilder builder;
    builder.SetLot(targetLot);
    builder.SetSoldPriceRequired(!targetLot.IsAuctionMissed());
    builder.SetSoldPrice(targetLot.SoldPrice());
    builder.SetSoldStatusId(SoldStatusTable::NotSold);
    builder.SetMissedFlag(targetLot.IsAuctionMissed());
    Lot finalizedLot = builder.ConstructLot();
    addNewEntry(finalizedLot);
}
void MainWindow::saleEnded(const Lot& finalizedLot) const {
    LotRecordsTable* recordsTable = static_cast<LotRecordsTable*>(m_recordsDb->Table(LOTRECORDS_TABLE_NAME));
    recordsTable->Api().AlterFinalFields(finalizedLot);
    m_mainUi->lotTableWidget->UpdateRecordByRecordId(finalizedLot);
}
void MainWindow::saleContinues(const Lot& finalizedLot) {
    addNewEntry(finalizedLot);
}
void MainWindow::addNewEntry(const Lot& finalizedLot) {
    NewEntryDialog newEntryDialog(m_recordsDb, finalizedLot);
    if(newEntryDialog.exec() == QDialog::Accepted) {
        // Warning: Worksheet file is copied when dialog is accepted, but is not deleted if operation fails.
        Lot newEntry = newEntryDialog.GetResult();
        LotRecordsTable* recordsTable = static_cast<LotRecordsTable*>(m_recordsDb->Table(LOTRECORDS_TABLE_NAME));
        recordsTable->Api().AlterFinalFields(finalizedLot);
        size_t recordId = recordsTable->Api().InsertLot(newEntry);
        newEntry.SetRecordId(recordId);
        if(newEntryDialog.GetCommentDocument()->toPlainText().length() != 0) {
            registerNewComment(recordId, newEntryDialog.GetCommentDocument()->toHtml());
        }
        if(!m_timelineMode) {
            m_mainUi->lotTableWidget->ReplaceRecordByRecordId(finalizedLot.GetRecordId(), newEntry);
        } else {
            m_mainUi->lotTableWidget->UpdateRecordByRecordId(finalizedLot);
            m_mainUi->lotTableWidget->AddNewRecord(newEntry);
        }
    }
}
void MainWindow::setupMainUiExclusiveButtonGroup() {
    m_mainUi->upcomingB->setAutoExclusive(true);
    m_mainUi->upcomingB->setCheckable(true);
    m_mainUi->endedB->setAutoExclusive(true);
    m_mainUi->endedB->setCheckable(true);
    m_mainUi->pastB->setAutoExclusive(true);
    m_mainUi->pastB->setCheckable(true);
    m_mainUi->allB->setAutoExclusive(true);
    m_mainUi->allB->setCheckable(true);
    m_mainUi->b_custom->setAutoExclusive(true);
    m_mainUi->b_custom->setCheckable(true);
    m_mainUi->allB->click();
}
void MainWindow::registerNewDataFromLot(const Lot& lot) {
    QString makeName = lot.GetNewMakeName();
    QString modelName = lot.GetNewModelName();
    if(modelName.isEmpty()) {
        throw std::runtime_error("New model name is empty.");
    }
    MakeTable* makeTable = static_cast<MakeTable*>(m_recordsDb->Table(MAKE_TABLE_NAME));
    ModelTable* modelTable = static_cast<ModelTable*>(m_recordsDb->Table(MODEL_TABLE_NAME));
    int makeId = -1;
    if(makeName.isEmpty()) {
        makeId = lot.MakeId();
    } else {
        makeTable->Api().AddNewMake(makeName);
        makeId = makeTable->Api().GetMakeIdByName(makeName);
    }
    if(makeId < 0) {
        throw std::runtime_error("Negative make ID to register.");
    }
    modelTable->Api().AddNewModel(makeId, modelName);
}
void MainWindow::updateUnregisteredDataForLot(Lot& lot) {
    MakeTable* makeTable = static_cast<MakeTable*>(m_recordsDb->Table(MAKE_TABLE_NAME));
    int makeId = lot.MakeId();
    if(makeId == -1) {
       makeId = makeTable->Api().GetMakeIdByName(lot.GetNewMakeName());
        if(makeId == 0) {
            throw std::runtime_error("Make ID for make " + lot.GetNewMakeName().toStdString() + " is invalid.");
        } else {
            lot.UpdateMakeId(makeId);
        }
    }

    ModelTable* modelTable = static_cast<ModelTable*>(m_recordsDb->Table(MODEL_TABLE_NAME));
    int modelId = lot.ModelId();
    if(modelId == -1) {
        modelId = modelTable->Api().GetModelIdByName(lot.GetNewModelName());
        if(modelId == 0) {
            throw std::runtime_error("Model ID for model " + lot.GetNewModelName().toStdString() + " is invalid.");
        } else {
            lot.UpdateModelId(modelId);
        }
    }
}
Comment MainWindow::registerNewComment(size_t recordId, const QString& commentText) {
    if(!commentText.isEmpty()) {
        LotCommentsTable* commentsTable = static_cast<LotCommentsTable*>(m_recordsDb->Table(LOTCOMMENTS_TABLE_NAME));
        Comment comment(recordId, commentText, QDateTime::currentDateTime().date());
        size_t id = commentsTable->Api().InsertComment(comment);
        comment.SetId(id);
        return comment;
    }
    return Comment();
}
void MainWindow::setupPdfViewer(const QString& pdfPath) {
    m_pdfDocument.load(pdfPath);
    m_pdfViewerUi->w_pdfView->setDocument(&m_pdfDocument);
    m_pdfViewerUi->w_pdfView->setZoomMode(QPdfView::FitToWidth);
    m_pdfViewerUi->w_pdfView->setPageMode(QPdfView::MultiPage);
}
void MainWindow::setupFilters() {
    Database* db = DatabaseManager::Instance().GetSettingsDatabase();
    FilterSettingsTable* filterTable = static_cast<FilterSettingsTable*>(db->Table(FILTERSETTINGS_TABLE_NAME));
    try {
        if(filterTable->Api().IsFilterTurnedOn()) {
            m_customFilter = filterTable->Api().GetFilter();
        }
    } catch(const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification(QString("Loading filter failed.\nError: ") + err.what(), NotificationManager::Type::Error);
    }
    setupUpcommingFilter();
    setupEndedFilter();
    setupPastFilter();
    switch(m_currentTabButton) {
        case TabButton::Upcomming : m_mainUi->upcomingB->click(); break;
        case TabButton::Ended : m_mainUi->endedB->click(); break;
        case TabButton::Past : m_mainUi->pastB->click(); break;
        case TabButton::All : m_mainUi->allB->click(); break;
        case TabButton::Custom : m_mainUi->b_custom->click(); break;
    }
}
void MainWindow::setupUpcommingFilter() {
    LotFilterDialog::FilterCollection filterCollection;
    RangeFilter<QVariant>* dateFilter = new RangeFilter<QVariant>();
    dateFilter->AddData(QVariant(QDateTime::currentDateTime().date()), QVariant(QDate(9999, 12, 31)));
    SelectionFilter<QVariant>* selectionFilter = new SelectionFilter<QVariant>();
    selectionFilter->AddData(QVariant(SoldStatusTable::Pending));
    filterCollection.insert(LotFilterDialog::Field::SoldStatus, QSharedPointer<IFilter<QVariant>>(selectionFilter));
    filterCollection.insert(LotFilterDialog::Field::AuctionDate, QSharedPointer<IFilter<QVariant>>(dateFilter));

    m_upcommingFilter = filterCollection;
}
void MainWindow::setupEndedFilter() {
    LotFilterDialog::FilterCollection filterCollection1;
    RangeFilter<QVariant>* dateFilter1 = new RangeFilter<QVariant>();
    QDate today(QDateTime::currentDateTime().date());
    dateFilter1->AddData(QVariant(QDate(-4713, 1, 1)), QVariant(today.addDays(-1)));
    SelectionFilter<QVariant>* selectionFilter1 = new SelectionFilter<QVariant>();
    selectionFilter1->AddData(QVariant(SoldStatusTable::Pending));
    filterCollection1.insert(LotFilterDialog::Field::SoldStatus, QSharedPointer<IFilter<QVariant>>(selectionFilter1));
    filterCollection1.insert(LotFilterDialog::Field::AuctionDate, QSharedPointer<IFilter<QVariant>>(dateFilter1));

    LotFilterDialog::FilterCollection filterCollection2;
    RangeFilter<QVariant>* dateFilter2 = new RangeFilter<QVariant>();
    dateFilter2->AddData(QVariant(QDate(-4713, 1, 1)), QVariant(today));
    SelectionFilter<QVariant>* selectionFilter2 = new SelectionFilter<QVariant>();
    selectionFilter2->AddData(QVariant(SoldStatusTable::WA));
    filterCollection2.insert(LotFilterDialog::Field::SoldStatus, QSharedPointer<IFilter<QVariant>>(selectionFilter2));
    filterCollection2.insert(LotFilterDialog::Field::AuctionDate, QSharedPointer<IFilter<QVariant>>(dateFilter2));

    m_endedFilters.append(filterCollection1);
    m_endedFilters.append(filterCollection2);
}
void MainWindow::setupPastFilter() {
    LotFilterDialog::FilterCollection filterCollection1;
    RangeFilter<QVariant>* dateFilter = new RangeFilter<QVariant>();
    dateFilter->AddData(QVariant(QDate(-4713, 1, 1)), QVariant(QDateTime::currentDateTime().date()));
    SelectionFilter<QVariant>* selectionFilter1 = new SelectionFilter<QVariant>();
    selectionFilter1->AddData(QVariant(SoldStatusTable::Sold));
    selectionFilter1->AddData(QVariant(SoldStatusTable::NotSold));
    filterCollection1.insert(LotFilterDialog::Field::SoldStatus, QSharedPointer<IFilter<QVariant>>(selectionFilter1));
    filterCollection1.insert(LotFilterDialog::Field::AuctionDate, QSharedPointer<IFilter<QVariant>>(dateFilter));

    LotFilterDialog::FilterCollection filterCollection2;
    SelectionFilter<QVariant>* selectionFilter2 = new SelectionFilter<QVariant>();
    selectionFilter2->AddData(QVariant(SoldStatusTable::Moved));
    selectionFilter2->AddData(QVariant(SoldStatusTable::Canceled));
    filterCollection2.insert(LotFilterDialog::Field::SoldStatus, QSharedPointer<IFilter<QVariant>>(selectionFilter2));

    m_pastFilters.append(filterCollection1);
    m_pastFilters.append(filterCollection2);
}
