#include "Headers/Ui/CommentItem.h"
#include "Headers/Tools.h"
#include "Headers/Options.h"
#include "ui_CommentItemWidget.h"

CommentItem::CommentItem(const Comment& comment, QWidget* parent)
    : QWidget(parent)
    , m_ui(new Ui::commentItem)
{
    m_ui->setupUi(this);
    setupFields(comment);
}
CommentItem::~CommentItem() {
    delete m_ui;
}
QSize CommentItem::sizeHint() const {
    return QSize(width(), m_ui->te_comment->height() + 6);
}
void CommentItem::Resize(int w, int h) {
    Resize(QSize(w, h));
}
void CommentItem::Resize(const QSize& newSize) {
    m_ui->te_comment->resize(newSize.width() - c_textEditOffsetX, -1);
    updateTextEditHeight();
    setMaximumSize(QSize(QWIDGETSIZE_MAX, m_ui->te_comment->height() + 6));
    setFixedHeight(m_ui->te_comment->height() + 6); // Qt: Simple resize may cause not preoper resize of widget height.
    resize(newSize.width(), m_ui->te_comment->height() + 6);
    m_ui->m_delimiter->setFixedHeight(m_ui->te_comment->height() - 6);
}
void CommentItem::SetComment(const Comment& comment) {
    setupFields(comment);
}
void CommentItem::updateTextEditHeight() {
    int textWidth = m_ui->te_comment->width() - static_cast<int>(m_ui->te_comment->document()->documentMargin())*2 - 2;
    int calcHeight = Tools::TextRenderSize(m_ui->te_comment->toPlainText(), m_ui->te_comment->font(), textWidth).height();
    m_ui->te_comment->setFixedHeight(calcHeight);
}
void CommentItem::setupFields(const Comment& comment) {
    m_ui->l_date->setText(comment.CreatedDate().toString(Options::Instance().DateFormat()));
    comment.IsEdited() ? m_ui->l_edited->show() : m_ui->l_edited->hide();
    m_ui->te_comment->setHtml(comment.Text());
    m_ui->te_comment->setAlignment(Qt::AlignJustify);
}
