#include "Headers/Ui/PressLabel.h"
#include <QMouseEvent>
#include <QFontMetrics>
#include <QPaintEvent>
#include <QPainter>

PressLabel::PressLabel(QWidget* parent, Qt::WindowFlags f)
    : QLabel(parent, f)
{
    setStyleSheet("color: blue; text-decoration: underline;");
    setMouseTracking(true);
}
PressLabel::PressLabel(const QString& text, QWidget* parent, Qt::WindowFlags f)
    : QLabel(text, parent, f)
{
    setStyleSheet("color: blue; text-decoration: underline;");
    setMouseTracking(true);
}
void PressLabel::mouseMoveEvent(QMouseEvent* event) {
    QRect bRect = getTextComponentRectangle();
    m_mouseCoord = event->pos();
    if(bRect.contains(event->pos())) {
        setCursor(Qt::PointingHandCursor);
    } else {
        setCursor(Qt::ArrowCursor);
    }
    QLabel::mouseMoveEvent(event);
}
void PressLabel::mousePressEvent(QMouseEvent* event) {
    QRect bRect = getTextComponentRectangle();
    if(bRect.contains(event->pos())) {
        setStyleSheet("color: #00A0E0; text-decoration: underline;");
    }
    QLabel::mousePressEvent(event);
}
void PressLabel::mouseReleaseEvent(QMouseEvent* event) {
    setStyleSheet("color: blue; text-decoration: underline;");
    if(rect().contains(event->pos())) {
        QRect bRect = getTextComponentRectangle();
        if(bRect.contains(event->pos())) {
            emit clicked();
        }
    }
    QLabel::mouseReleaseEvent(event);
}
QRect PressLabel::getTextComponentRectangle() const {
     if(frameWidth() < 0) {
        throw std::runtime_error("Negative frame width.");
    }
    int effectiveIndent = indent();
    int trueMargin = margin();
    if(effectiveIndent < 0) {
        if(frameWidth() == 0 || margin() > 0) {
            effectiveIndent = 0;
        } else if(frameWidth() > 0) {
            QFontMetrics fm(font());
            effectiveIndent = fm.width(QChar('x')) / 2;
        }
        if(frameWidth() > 0 && margin() < 0) {
            trueMargin = 0;
        }
    }

    QFontMetrics fm(font());
    QRect bRect = fm.boundingRect(text());
    bRect.setWidth(fm.width(text()));

    int indentOffset = effectiveIndent + trueMargin + frameWidth();
    int offsetX = 0;
    int offsetY = 0;
    if(alignment() & Qt::AlignHCenter) {
        offsetX = rect().width() / 2 - bRect.width() / 2;
    } else if(alignment() & Qt::AlignRight) {
        offsetX = rect().width() - bRect.width() - indentOffset;
    } else if(alignment() & Qt::AlignJustify) {
        offsetX = trueMargin + frameWidth();
    } else if(alignment() & Qt::AlignLeft) {
        offsetX = indentOffset;
    }
    if(alignment() & Qt::AlignVCenter) {
        offsetY = rect().height() / 2 - bRect.height() / 2;
    } else if(alignment() & Qt::AlignBottom) {
        offsetY = rect().height() - bRect.height() - indentOffset;
    } else if(alignment() & Qt::AlignTop) {
        offsetY = indentOffset;
    }

    bRect.moveTopLeft(rect().topLeft());
    bRect.setX(bRect.x() + offsetX);
    bRect.setWidth(bRect.width() + offsetX);
    bRect.setY(bRect.y() + offsetY);
    bRect.setHeight(bRect.height() + offsetY);

    return bRect;
}
