#include "Headers/Ui/ShapeWidget.h"
#include "Headers/Options.h"
#include <QPainter>

ShapeWidget::ShapeWidget(Shape shape, QWidget* parent)
    : QWidget(parent)
    , m_shape(shape)
{
}
void ShapeWidget::ChangeColor(const QColor& color) {
    m_color = color;
}
void ShapeWidget::ChangeSize(const QSize& size) {
    setGeometry(x(), y(), size.width(), size.height());
}
void ShapeWidget::ChangeCenter(const QPoint& position) {
    setGeometry(position.x() - width()/2 - 1, position.y() - height()/2 - 1, width(), height());
}
void ShapeWidget::ChangeText(const QString& text) {
    m_text = text;
}
void ShapeWidget::paintEvent(QPaintEvent* event) {
    Q_UNUSED(event);

    QPainter painter(this);
    QBrush brush(m_color);
    painter.setBrush(brush);

    if(m_shape == Rectangle) {
        QPen borderPen(borderColor(), 2); // 2 is needed for borders at all sides visible with 1px width.
        painter.setPen(borderPen);
        painter.drawRect(rect());
    } else if(m_shape == Ellipse) {
        painter.setRenderHint(QPainter::Antialiasing);
        QPen borderPen(borderColor(), 1);
        painter.setPen(borderPen);
        painter.drawEllipse(rect());
    }

    QPen textPen(textColor(), 1);
    painter.setFont(textFont());
    painter.setPen(textPen);
    painter.drawText(rect(), Qt::AlignCenter, m_text);
}
QFont ShapeWidget::textFont() const {
    return QFont("Calibri Light", 9, QFont::Bold);
}
QColor ShapeWidget::textColor() const {
    int mid = m_color.saturation() * 127 - (m_color.value() - 128) * 255;
    if(mid >= 0) {
        return Qt::white;
    } else {
        return Qt::black;
    }
}
QColor ShapeWidget::borderColor() const {
    return QColor(m_color.red() / 4, m_color.green() / 4, m_color.blue() / 4);
}
