#include "Headers/Ui/DoubleClickableButton.h"

DoubleClickableButton::DoubleClickableButton(QWidget* parent)
    : QPushButton(parent)
{
}
DoubleClickableButton::DoubleClickableButton(const QString& text, QWidget* parent)
    : QPushButton(text, parent)
{
}
DoubleClickableButton::DoubleClickableButton(const QIcon& icon, const QString& text, QWidget* parent)
    : QPushButton(icon, text, parent)
{
}
DoubleClickableButton::~DoubleClickableButton()
{
}
void DoubleClickableButton::mouseDoubleClickEvent(QMouseEvent* event) {
    QPushButton::mouseDoubleClickEvent(event);
    emit doubleClicked();
}
