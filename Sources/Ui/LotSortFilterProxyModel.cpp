#include "Headers/Ui/LotSortFilterProxyModel.h"
#include "Headers/Ui/LotModel.h"
#include "Headers/LotComparator.h"
#include "Headers/Database/DatabaseManager.h"

LotSortFilterProxyModel::LotSortFilterProxyModel(QObject* parent)
    : QSortFilterProxyModel(parent)
    , m_rootModel(nullptr)
    , m_comparator(DatabaseManager::Instance().GetRecordsDatabase())
{
}
void LotSortFilterProxyModel::AddFilter(const LotFilterDialog::FilterCollection& filter) {
    m_filters.append(filter);
    invalidateFilter();
}
void LotSortFilterProxyModel::SetFilter(const LotFilterDialog::FilterCollection& filter) {
    m_filters.clear();
    m_filters.append(filter);
    invalidateFilter();
}
QAbstractItemModel *LotSortFilterProxyModel::SourceRootModel() const {
    return m_rootModel;
}
void LotSortFilterProxyModel::SetSourceRootModel(QAbstractItemModel* rootModel) {
    m_rootModel = rootModel;
}
void LotSortFilterProxyModel::MissedDescendingOrder(bool missedDescending) {
    Qt::SortOrder order;
    if(missedDescending) {
        order = Qt::DescendingOrder;
    } else {
        order = Qt::AscendingOrder;
    }
    m_comparator.FixField(LotRecordsTable::Fields::Missed);
    int column = static_cast<LotModel*>(m_rootModel)->GetColumnByField(m_comparator.GetFirstField());
    // Note: QSortFilterProxyModel::sort ignores sorting if column is -1 and also
    // if order and column are the same as previous, so we take the first column in chain.
    QSortFilterProxyModel::sort(column, order == Qt::AscendingOrder ? Qt::DescendingOrder : Qt::AscendingOrder);
    QSortFilterProxyModel::sort(column, order);
}
void LotSortFilterProxyModel::sort(int column, Qt::SortOrder order) {
    if(column == -1) {
        m_comparator.ResetFixedField();
        column = static_cast<LotModel*>(m_rootModel)->GetColumnByField(m_comparator.GetFirstField());
    } else {
        m_comparator.FixField(static_cast<LotModel*>(SourceRootModel())->GetFieldByColumn(column));
    }
    // Note: QSortFilterProxyModel::sort ignores sorting if column is -1 and also
    // if order and column are the same as previous, so we take the first column in chain.
    QSortFilterProxyModel::sort(column, order == Qt::AscendingOrder ? Qt::DescendingOrder : Qt::AscendingOrder);
    QSortFilterProxyModel::sort(column, order);
}
bool LotSortFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const {
    Q_UNUSED(sourceRow);
    Q_UNUSED(sourceParent);
    if(m_filters.count() == 0) {
        return true;
    }
    bool accept = false;
    for(auto filter : m_filters) {
        bool filterAccepted = true;
        if(filter.count() != 0) {
            Lot lot = static_cast<LotModel*>(SourceRootModel())->GetLotByRow(sourceRow);
            if(filter.contains(LotFilterDialog::Field::LotId)) {
                filterAccepted &= filter[LotFilterDialog::Field::LotId]->Accept(lot.LotId());
            }
            if(filter.contains(LotFilterDialog::Field::Make)) {
                filterAccepted &= filter[LotFilterDialog::Field::Make]->Accept(QVariant(lot.MakeId()));
            }
            if(filter.contains(LotFilterDialog::Field::Model)) {
                filterAccepted &= filter[LotFilterDialog::Field::Model]->Accept(lot.ModelId());
            }
            if(filter.contains(LotFilterDialog::Field::Year)) {
                filterAccepted &= filter[LotFilterDialog::Field::Year]->Accept(lot.Year());
            }
            if(filter.contains(LotFilterDialog::Field::Transmission)) {
                filterAccepted &= filter[LotFilterDialog::Field::Transmission]->Accept(lot.TransmissionId());
            }
            if(filter.contains(LotFilterDialog::Field::Body)) {
                filterAccepted &= filter[LotFilterDialog::Field::Body]->Accept(lot.BodyId());
            }
            if(filter.contains(LotFilterDialog::Field::Purpose)) {
                filterAccepted &= filter[LotFilterDialog::Field::Purpose]->Accept(lot.PurposeId());
            }
            if(filter.contains(LotFilterDialog::Field::Location)) {
                filterAccepted &= filter[LotFilterDialog::Field::Location]->Accept(QVariant(lot.LocationId()));
            }
            if(filter.contains(LotFilterDialog::Field::WatchDate)) {
                filterAccepted &= filter[LotFilterDialog::Field::WatchDate]->Accept(lot.WatchDate());
            }
            if(filter.contains(LotFilterDialog::Field::AuctionDate)) {
                filterAccepted &= filter[LotFilterDialog::Field::AuctionDate]->Accept(QVariant(lot.AuctionDate()));
            }
            if(filter.contains(LotFilterDialog::Field::SaleStatus)) {
                filterAccepted &= filter[LotFilterDialog::Field::SaleStatus]->Accept(lot.SaleStatusId());
            }
            if(filter.contains(LotFilterDialog::Field::SoldStatus)) {
                filterAccepted &= filter[LotFilterDialog::Field::SoldStatus]->Accept(QVariant(lot.SoldStatusId()));
            }
            if(filter.contains(LotFilterDialog::Field::SoldPrice)) {
                filterAccepted &= filter[LotFilterDialog::Field::SoldPrice]->Accept(lot.SoldPrice());
            }
            if(filter.contains(LotFilterDialog::Field::Wit)) {
                filterAccepted &= filter[LotFilterDialog::Field::Wit]->Accept(lot.WantItTodayPrice());
            }
            if(filter.contains(LotFilterDialog::Field::Missed)) {
                filterAccepted &= filter[LotFilterDialog::Field::Missed]->Accept(lot.IsAuctionMissed());
            }
        }
        accept |= filterAccepted;
        if(accept) {
            break;
        }
    }
    return accept;
}
bool LotSortFilterProxyModel::lessThan(const QModelIndex& left, const QModelIndex& right) const {
    Lot lot1 = static_cast<LotModel*>(SourceRootModel())->GetLotByRow(left.row());
    Lot lot2 = static_cast<LotModel*>(SourceRootModel())->GetLotByRow(right.row());
    int result = m_comparator.Compare(lot1, lot2);
    if(result == 2) {
        return true;
    }
    return false;
}
