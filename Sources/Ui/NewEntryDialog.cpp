#include "Headers/Ui/NewEntryDialog.h"
#include "Headers/FilePathValidator.h"
#include "Headers/Database/DatabaseManager.h"
#include "Headers/Tools.h"
#include "Headers/NewLotBuilder.h"
#include "Headers/NotificationManager.h"
#include "ui_NewEntryDialog.h"
#include "Headers/Options.h"
#include <QFileDialog>

QColor NewEntryDialog::m_labelColor(Qt::black);
QColor NewEntryDialog::m_notFilledLabelColor(Qt::red);

NewEntryDialog::NewEntryDialog(Database* database, const Lot& lot, QWidget* parent)
    : QDialog (parent)
    , m_ui(new Ui::newEntryDialog)
    , m_database(database)
    , m_originalLot(lot)
    , m_commentDoc(nullptr)
{
    m_ui->setupUi(this);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setFixedSize(geometry().size());
    SetupFields();

    QObject::connect(m_ui->b_addEntry, SIGNAL(clicked()), this, SLOT(onAcceptButtonClicked()));
    QObject::connect(m_ui->b_cancel, SIGNAL(clicked()), this, SLOT(reject()));
    QObject::connect(m_ui->cob_auctionDate, SIGNAL(currentIndexChanged(int)), this, SLOT(onAuctionDateVariantChanged(int)));
    QObject::connect(m_ui->le_worksheetFile, SIGNAL(textChanged(QString)), this, SLOT(onWorksheetPathChanged(QString)));
    QObject::connect(m_ui->b_worksheetFile, SIGNAL(clicked()), this, SLOT(onFileSelectButtonClicked()));
    m_ui->de_auctionDate->hide();
    m_ui->le_wantItToday->setValidator(new QIntValidator(m_ui->le_wantItToday));
    m_ui->le_worksheetFile->setValidator(new FilePathValidator(m_ui->le_worksheetFile, m_ui->le_worksheetFile));
}
NewEntryDialog::~NewEntryDialog() {
    if(m_commentDoc != nullptr) {
        delete m_commentDoc;
    }
}
void NewEntryDialog::SetupFields() {
    setupAuctionDateField();
    setupSaleStatusField();
}
void NewEntryDialog::onAcceptButtonClicked() {
    m_resultLot = constructLot();
    m_commentDoc = m_ui->pte_comment->document()->clone();
    if(!m_resultLot.IsValid()) {
        return;
    } else {
        accept();
    }
}
void NewEntryDialog::onAuctionDateVariantChanged(int index) {
    if(index == 0 || index == 1) {
        m_ui->de_auctionDate->hide();
    } else if(index == 2) {
        m_ui->de_auctionDate->show();
    }
}
void NewEntryDialog::onWorksheetPathChanged(const QString& newPath) {
    m_ui->le_worksheetFile->setToolTip(newPath);
}
void NewEntryDialog::onFileSelectButtonClicked() {
    QString fileName = QFileDialog::getOpenFileName(
                           this,
                           "Select Worksheet file",
                           QDir::homePath() + "\\Desktop",
                           "Worksheet Files (*.pdf);;All Files (*.*)"
                           // 0, QFileDialog::DontUseNativeDialog); // may work if file dialog hangs
                       );
    m_ui->le_worksheetFile->setText(fileName);
}
Lot NewEntryDialog::constructLot() {
    NewLotBuilder builder;
    builder.SetLot(m_originalLot);
    if(m_ui->cob_auctionDate->currentIndex() == 1) {
        builder.SetAuctionDate(QDate(9999, 12, 31));
    } else if(m_ui->cob_auctionDate->currentIndex() == 2) {
        builder.SetAuctionDate(m_ui->de_auctionDate->date());
    }
    builder.SetSaleStatusId(m_ui->cob_saleStatus->currentData().toUInt());
    builder.SetWantItTodayPrice(m_ui->le_wantItToday->text().toUInt());
    QString path = m_ui->le_worksheetFile->text();
    int unused(0);
    if(m_ui->le_worksheetFile->validator()->validate(path, unused) == QValidator::Acceptable) {
        builder.SetWorksheetPath(m_ui->le_worksheetFile->text());
    }

    try {
        return builder.ConstructLot();
    } catch (std::runtime_error err) {
        if(!builder.AreAllRequiredFieldsFilled()) {
            highlightUnfilled(builder.UnfilledRequiredFields());
        } else {
            resetLabelsColor();
        }
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return Lot();
    }
}
void NewEntryDialog::resetLabelsColor() const {
    QPalette palette;
    palette.setColor(QPalette::Foreground, m_labelColor);
    m_ui->l_auctionDate->setPalette(palette);
    m_ui->l_saleStatus->setPalette(palette);
    m_ui->l_wantItToday->setPalette(palette);
    m_ui->l_worksheetFile->setPalette(palette);
    m_ui->l_comment->setPalette(palette);
}
void NewEntryDialog::highlightUnfilled(const QList<LotRecordsTable::Fields>& fields) const {
    resetLabelsColor();
    QPalette palette;
    palette.setColor(QPalette::Foreground, m_notFilledLabelColor);
    for(auto f : fields) {
        if(f == LotRecordsTable::Fields::AuctionDate) {
            m_ui->l_auctionDate->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::SaleStatusId) {
            m_ui->l_saleStatus->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::WorksheetPath) {
            m_ui->l_worksheetFile->setPalette(palette);
        }
    }
}
void NewEntryDialog::setupAuctionDateField() {
    QDate todayDate = QDateTime::currentDateTime().date();
    m_ui->de_auctionDate->setDate(QDate(todayDate.year(), todayDate.month(), todayDate.day()));
    m_ui->de_auctionDate->setDisplayFormat(Options::Instance().DateFormat());
}
void NewEntryDialog::setupSaleStatusField() {
    SaleStatusTable* saleStatuses = static_cast<SaleStatusTable*>(m_database->Table(SALESTATUS_TABLE_NAME));
    QList<QMap<SaleStatusTable::Fields, QVariant>> saleStatusData = saleStatuses->Api().GetSaleStatusData();
    for(auto record : saleStatusData) {
        m_ui->cob_saleStatus->addItem(Tools::ChangeStringCase(record[SaleStatusTable::Fields::Name].toString(), Tools::StringCase::FirstUpper),
                                     record[SaleStatusTable::Fields::Id].toUInt());
    }
}
