#include "Headers/Ui/AboutDialog.h"
#include "ui_AboutDialog.h"
#include "Headers/Configuration/Version.h"

AboutDialog::AboutDialog(QWidget* parent)
    : QDialog(parent)
    , m_ui(new Ui::aboutDialog)
{
    m_ui->setupUi(this);
    m_ui->te_description->setAlignment(Qt::AlignJustify);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setFixedSize(geometry().size());
    m_ui->l_version->setText(Version_Full);
}
