#include "Headers/Ui/CommentItemView.h"
#include "Headers/Tools.h"
#include <QResizeEvent>
#include <QScrollBar>

CommentItemView::CommentItemView(QWidget* parent)
    : QListView(parent)
    , m_delegate(new CommentItemDelegate())
    , m_selectionMode(false)
{
    setItemDelegate(m_delegate);
    QObject::connect(m_delegate, SIGNAL(sigClickedOutside()), this, SLOT(onClearSelection()));
    QObject::connect(m_delegate, SIGNAL(sigClearSelections()), this, SLOT(onClearSelection()));
    QObject::connect(this, SIGNAL(sigSelectionChanged(QItemSelectionModel*)),
                     m_delegate, SLOT(OnSelectionChanged(QItemSelectionModel*)));
}
CommentItemView::~CommentItemView() {
    delete m_delegate;
}
void CommentItemView::setModel(QAbstractItemModel* model) {
    if(m_delegate != itemDelegate()) {
        throw std::runtime_error("Delegate was changed outside of view.");
    }
    QListView::setModel(model);
    // TODO: Check or insure that model is CommentItemModel.
    CommentItemModel* commentModel = static_cast<CommentItemModel*>(model);
    m_delegate->ResetModel(commentModel);
    for(int row = 0; row < commentModel->rowCount(); ++row) {
        m_delegate->NewItemAdded(commentModel->GetModelIndexAt(row));
    }
    QObject::connect(this->model(), SIGNAL(rowsInserted(const QModelIndex&, int, int)),
                     this, SLOT(onItemsAdded(const QModelIndex&, int, int)));
    QObject::connect(this->model(), SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&, const QVector<int>&)),
                     this, SLOT(onItemsChanged(const QModelIndex&, const QModelIndex&, const QVector<int>&)));
    QObject::connect(this->model(), SIGNAL(rowsRemoved(const QModelIndex&, int, int)),
                     this, SLOT(onItemsRemoved(const QModelIndex&, int, int)));
}
void CommentItemView::resizeEvent(QResizeEvent* event) {
    if(m_delegate != itemDelegate()) {
        throw std::runtime_error("Delegate was changed outside of view.");
    }
    m_delegate->UpdateItemsGeometry(event->size());
    QListView::resizeEvent(event);
    viewport()->update();
    if(verticalScrollBar()->isVisible()) {
        // Qt: ScrollBar maximum is not updated properly here after window restore.
        onRangeChanged(-1, -1);
        QObject::connect(verticalScrollBar(), SIGNAL(rangeChanged(int, int)), this, SLOT(onRangeChanged(int, int)));
    }
}
bool CommentItemView::event(QEvent* event) {
    if(event->type() == QEvent::KeyPress) {
        QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
        switch(keyEvent->key()) {
            case Qt::Key_Control : {
                m_selectionMode = true;
                break;
            }
        };
        return true;
    }
    if(event->type() == QEvent::KeyRelease) {
        QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
        switch(keyEvent->key()) {
            case Qt::Key_Control : {
                m_selectionMode = false;
                break;
            }
        };
        return true;
    }
    return QListView::event(event);
}
void CommentItemView::mouseMoveEvent(QMouseEvent* event) {
    if(event->buttons() == Qt::LeftButton) {
        return;
    }
    QListView::mouseMoveEvent(event);
}
void CommentItemView::mouseDoubleClickEvent(QMouseEvent* event) {
    QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
    QModelIndex modelIndex = indexAt(event->pos());
    QPoint clickPoint = mouseEvent->pos();
    int index = (modelIndex.isValid() ? modelIndex.row() : -1);
    bool clickAccepted = m_delegate->IsItemClicked(index, visualRect(modelIndex), clickPoint);
    if(clickAccepted) {
        m_delegate->SetEditorToEditMode(modelIndex);
        selectionModel()->select(modelIndex, QItemSelectionModel::ClearAndSelect);
    }
}
void CommentItemView::mousePressEvent(QMouseEvent* event) {
    m_pressedModelIndex = indexAt(event->pos());
}
void CommentItemView::mouseReleaseEvent(QMouseEvent* event) {
    QModelIndex modelIndex = indexAt(event->pos());
    QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
    QPoint clickPoint = mouseEvent->pos();
    int index = (modelIndex.isValid() ? modelIndex.row() : -1);
    bool clickAccepted = m_delegate->IsItemClicked(index, visualRect(modelIndex), clickPoint);
    if(modelIndex == m_pressedModelIndex) {
        if(clickAccepted && !m_selectionMode && (!selectionModel()->isSelected(modelIndex) || selectionModel()->selectedIndexes().size() > 1)) {
            selectionModel()->select(modelIndex, QItemSelectionModel::ClearAndSelect);
            emit sigSelectionChanged(selectionModel());
        } else if(clickAccepted && m_selectionMode) {
            selectionModel()->select(modelIndex, QItemSelectionModel::Toggle);
            emit sigSelectionChanged(selectionModel());
        } else if(!clickAccepted && !m_selectionMode) {
            //selectionModel()->select(modelIndex, QItemSelectionModel::Clear);
            clearSelection();
            emit sigSelectionChanged(selectionModel());
        } else if(!clickAccepted && m_selectionMode) {
            // NOP
        }
    }
}
void CommentItemView::dataChanged(const QModelIndex &tl, const QModelIndex &br, const QVector<int>& roles) {
    QListView::dataChanged(tl, br, roles);
}
void CommentItemView::SetDelegateEditor(CommentItemEditor* editor) {
    m_delegate->SetExternalEditor(editor);
}
void CommentItemView::onClearSelection() {
    clearSelection();
    emit sigSelectionChanged(selectionModel());
}
void CommentItemView::onItemsAdded(const QModelIndex& modelIndex, int first, int last) {
    Q_UNUSED(modelIndex);
    CommentItemModel* model = static_cast<CommentItemModel*>(this->model());
    for(int i = first; i <= last; ++i) {
        QModelIndex mIndex = model->GetModelIndexAt(i);
        m_delegate->NewItemAdded(mIndex);
        if(first == last) {
            break;
        }
    }
    scrollTo(model->GetModelIndexAt(model->rowCount() - 1), ScrollHint::PositionAtBottom);
}
void CommentItemView::onItemsChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles) {
    Q_UNUSED(roles);
    int first = topLeft.row();
    int last = bottomRight.row();
    for(int i = first; i <= last; ++i) {
        CommentItemModel* model = static_cast<CommentItemModel*>(this->model());
        QModelIndex mIndex = model->GetModelIndexAt(i);
        QString text = qvariant_cast<Comment>(mIndex.data()).Text();
        m_delegate->UpdateItem(mIndex);
        if(first == last) {
            break;
        }
    }
}
void CommentItemView::onItemsRemoved(const QModelIndex& modelIndex, int first, int last) {
    Q_UNUSED(modelIndex);
    for(int i = first; i <= last; ++i) {
        CommentItemModel* model = static_cast<CommentItemModel*>(this->model());
        QModelIndex mIndex = model->GetModelIndexAt(i);
        m_delegate->RemoveItem(mIndex);
        if(first == last) {
            break;
        }
    }
}
void CommentItemView::onRangeChanged(int min, int max) {
    Q_UNUSED(min);
    Q_UNUSED(max);
    if(verticalScrollBar()->isVisible()) {
        // TODO: Check or insure that model is CommentItemModel.
        CommentItemModel* commentModel = static_cast<CommentItemModel*>(model());
        scrollTo(commentModel->GetModelIndexAt(commentModel->rowCount() - 1), ScrollHint::PositionAtBottom);
    }
}
