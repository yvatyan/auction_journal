#include "Headers/Ui/LotFilterDialog.h"
#include "Headers/Database/DatabaseManager.h"
#include "Headers/NotificationManager.h"
#include "Headers/Ui/SelectionFilterWidget.h"
#include "Headers/Ui/RangeFilterWidget.h"
#include "Headers/ScrollEventEater.h"
#include "Headers/Tools.h"
#include "Headers/Options.h"
#include "ui_LotFilterDialog.h"
#include <QPushButton>
#include <QObject>

LotFilterDialog::LotFilterDialog(FilterCollection* filterCollection, QWidget* parent)
    : QDialog(parent)
    , m_ui(new Ui::lotFilterDialog)
{
    m_ui->setupUi(this);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    if(filterCollection != nullptr) {
        m_filterCollection = *filterCollection;
    }
    setupWidgets();

    QObject::connect(m_ui->buttonBox, SIGNAL(clicked(QAbstractButton*)), SLOT(onButtonBoxClicked(QAbstractButton*)));
}
LotFilterDialog::~LotFilterDialog() {
    delete m_ui;
}
const LotFilterDialog::FilterCollection& LotFilterDialog::GetResult() const {
    return m_filterCollection;
}
bool LotFilterDialog::ApplyFiltersOnStartup() const {
    return m_applyOnStartup;
}
void LotFilterDialog::onButtonBoxClicked(QAbstractButton* button) {
    if(static_cast<QPushButton*>(button) == m_ui->buttonBox->button(QDialogButtonBox::Apply)) {
        onApplyButtonClicked();
    } else if(static_cast<QPushButton*>(button) == m_ui->buttonBox->button(QDialogButtonBox::Cancel)) {
        onCancelButtonClicked();
    } else if(static_cast<QPushButton*>(button) == m_ui->buttonBox->button(QDialogButtonBox::Reset)) {
        onResetButtonClicked();
    }
}
void LotFilterDialog::onMakeSelectionAdded(QVariant makeData) {
    SelectionFilterWidget* modelFilter = m_ui->gb_model->findChild<SelectionFilterWidget*>("w_modelFilter");
    try {
        Database* db = DatabaseManager::Instance().GetRecordsDatabase();
        ModelTable* modelTable = static_cast<ModelTable*>(db->Table(MODEL_TABLE_NAME));
        QList<QMap<ModelTable::Fields, QVariant>> modelData = modelTable->Api().GetModelDataByMakeId(makeData.toUInt());
        Tools::SortStringsWithNumbers<ModelTable::Fields>(modelData,
                                                          ModelTable::Fields::Name,
                                                          Tools::SortOrder::Ascending);
        QList<QPair<QVariant, QVariant>> choiceData;
        for(auto d : modelData) {
            choiceData.append(QPair<QVariant, QVariant>(Tools::ChangeStringCase(d[ModelTable::Fields::Name].toString(),
                                                                                 Tools::StringCase::AllFirstUpper),
                                                        d[ModelTable::Fields::Id]));
        }
        static_cast<SelectionFilterWidget*>(modelFilter)->AddData(choiceData);
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification("Failed to setup \"model\" filter. Error: " + QString(err.what()),
                                                         NotificationManager::Type::Error);
    }
}
void LotFilterDialog::onMakeSelectionDeleted(QVariant makeData) {
    SelectionFilterWidget* modelFilter = m_ui->gb_model->findChild<SelectionFilterWidget*>("w_modelFilter");
    try {
        Database* db = DatabaseManager::Instance().GetRecordsDatabase();
        ModelTable* modelTable = static_cast<ModelTable*>(db->Table(MODEL_TABLE_NAME));
        QList<QMap<ModelTable::Fields, QVariant>> modelData = modelTable->Api().GetModelDataByMakeId(makeData.toUInt());
        Tools::SortStringsWithNumbers<ModelTable::Fields>(modelData,
                                                          ModelTable::Fields::Name,
                                                          Tools::SortOrder::Ascending);
        QList<QVariant> dataToRemove;
        for(auto d : modelData) {
            dataToRemove.append(d[ModelTable::Fields::Id]);
        }
        static_cast<SelectionFilterWidget*>(modelFilter)->RemoveData(dataToRemove);
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification("Failed to setup \"lot\" filter. Error: " + QString(err.what()),
                                                         NotificationManager::Type::Error);
    }
}
void LotFilterDialog::onApplyButtonClicked() {
    collectFieldData();
    m_applyOnStartup = m_ui->chB_applyOnStartup->isChecked();
    accept();
}
void LotFilterDialog::onCancelButtonClicked() {
    reject();
}
void LotFilterDialog::onResetButtonClicked() {
    m_ui->chB_applyOnStartup->setChecked(false);
    m_ui->gb_lot->findChild<SelectionFilterWidget*>("w_lotFilter")->Reset();
    m_ui->chB_lotExtended->setChecked(false);
    m_ui->gb_make->findChild<SelectionFilterWidget*>("w_makeFilter")->Reset();
    m_ui->chB_makeExtended->setChecked(false);
    m_ui->gb_model->findChild<SelectionFilterWidget*>("w_modelFilter")->Reset();
    m_ui->chB_modelExtended->setChecked(false);
    m_ui->gb_year->findChild<RangeFilterWidget*>("w_yearFilter")->Reset();
    m_ui->chB_yearExtended->setChecked(false);
    m_ui->gb_transmission->findChild<SelectionFilterWidget*>("w_transmissionFilter")->Reset();
    m_ui->chB_transmissionExtended->setChecked(false);
    m_ui->gb_body->findChild<SelectionFilterWidget*>("w_bodyFilter")->Reset();
    m_ui->chB_bodyExtended->setChecked(false);
    m_ui->gb_purpose->findChild<SelectionFilterWidget*>("w_purposeFilter")->Reset();
    m_ui->chB_purposeExtended->setChecked(false);
    m_ui->gb_location->findChild<SelectionFilterWidget*>("w_locationFilter")->Reset();
    m_ui->chB_locationExtended->setChecked(false);
    m_ui->gb_watchDate->findChild<RangeFilterWidget*>("w_watchDateFilter")->Reset();
    m_ui->chB_watchDateExtended->setChecked(false);
    m_ui->gb_auctionDate->findChild<RangeFilterWidget*>("w_auctionDateFilter")->Reset();
    m_ui->chB_auctionDateExtended->setChecked(false);
    m_ui->gb_saleStatus->findChild<SelectionFilterWidget*>("w_saleStatusFilter")->Reset();
    m_ui->chB_saleStatusExtended->setChecked(false);
    m_ui->gb_soldStatus->findChild<SelectionFilterWidget*>("w_soldStatusFilter")->Reset();
    m_ui->chB_soldStatusExtended->setChecked(false);
    m_ui->gb_soldPrice->findChild<RangeFilterWidget*>("w_soldPriceFilter")->Reset();
    m_ui->chB_soldPriceExtended->setChecked(false);
    m_ui->gb_wit->findChild<RangeFilterWidget*>("w_witFilter")->Reset();
    m_ui->chB_witExtended->setChecked(false);
    m_ui->coB_missedRecords->setCurrentIndex(0);
}
void LotFilterDialog::collectFieldData() {
    m_filterCollection.clear();
    SelectionFilterWidget* lotFilter = m_ui->gb_lot->findChild<SelectionFilterWidget*>("w_lotFilter");
    QSharedPointer<IFilter<QVariant>> lotFilterObj(lotFilter->GetFilterObject());
    if(lotFilterObj->IsValid()) {
        m_filterCollection.insert(Field::LotId, lotFilterObj);
    }
    SelectionFilterWidget* makeFilter = m_ui->gb_make->findChild<SelectionFilterWidget*>("w_makeFilter");
    QSharedPointer<IFilter<QVariant>> makeFilterObj(makeFilter->GetFilterObject());
    if(makeFilterObj->IsValid()) {
        m_filterCollection.insert(Field::Make, makeFilterObj);
    }
    SelectionFilterWidget* modelFilter = m_ui->gb_model->findChild<SelectionFilterWidget*>("w_modelFilter");
    QSharedPointer<IFilter<QVariant>> modelFilterObj(modelFilter->GetFilterObject());
    if(modelFilterObj->IsValid()) {
        m_filterCollection.insert(Field::Model, modelFilterObj);
    }
    RangeFilterWidget* yearFilter = m_ui->gb_year->findChild<RangeFilterWidget*>("w_yearFilter");
    QSharedPointer<IFilter<QVariant>> yearFilterObj(yearFilter->GetFilterObject());
    if(yearFilterObj->IsValid()) {
        m_filterCollection.insert(Field::Year, yearFilterObj);
    }
    SelectionFilterWidget* transmissionFilter = m_ui->gb_transmission->findChild<SelectionFilterWidget*>("w_transmissionFilter");
    QSharedPointer<IFilter<QVariant>> transmissionFilterObj(transmissionFilter->GetFilterObject());
    if(transmissionFilterObj->IsValid()) {
        m_filterCollection.insert(Field::Transmission, transmissionFilterObj);
    }
    SelectionFilterWidget* bodyFilter = m_ui->gb_body->findChild<SelectionFilterWidget*>("w_bodyFilter");
    QSharedPointer<IFilter<QVariant>> bodyFilterObj(bodyFilter->GetFilterObject());
    if(bodyFilterObj->IsValid()) {
        m_filterCollection.insert(Field::Body, bodyFilterObj);
    }
    SelectionFilterWidget* purposeFilter = m_ui->gb_purpose->findChild<SelectionFilterWidget*>("w_purposeFilter");
    QSharedPointer<IFilter<QVariant>> purposeFilterObj(purposeFilter->GetFilterObject());
    if(purposeFilterObj->IsValid()) {
        m_filterCollection.insert(Field::Purpose, purposeFilterObj);
    }
    SelectionFilterWidget* locationFilter = m_ui->gb_location->findChild<SelectionFilterWidget*>("w_locationFilter");
    QSharedPointer<IFilter<QVariant>> locationFilterObj(locationFilter->GetFilterObject());
    if(locationFilterObj->IsValid()) {
        m_filterCollection.insert(Field::Location, locationFilterObj);
    }
    RangeFilterWidget* watchDateFilter = m_ui->gb_watchDate->findChild<RangeFilterWidget*>("w_watchDateFilter");
    QSharedPointer<IFilter<QVariant>> watchDateFilterObj(watchDateFilter->GetFilterObject());
    if(watchDateFilterObj->IsValid()) {
        m_filterCollection.insert(Field::WatchDate, watchDateFilterObj);
    }
    RangeFilterWidget* auctionDateFilter = m_ui->gb_auctionDate->findChild<RangeFilterWidget*>("w_auctionDateFilter");
    QSharedPointer<IFilter<QVariant>> auctionDateFilterObj(auctionDateFilter->GetFilterObject());
    if(auctionDateFilterObj->IsValid()) {
        m_filterCollection.insert(Field::AuctionDate, auctionDateFilterObj);
    }
    SelectionFilterWidget* saleStatusFilter = m_ui->gb_saleStatus->findChild<SelectionFilterWidget*>("w_saleStatusFilter");
    QSharedPointer<IFilter<QVariant>> saleStatusFilterObj(saleStatusFilter->GetFilterObject());
    if(saleStatusFilterObj->IsValid()) {
        m_filterCollection.insert(Field::SaleStatus, saleStatusFilterObj);
    }
    SelectionFilterWidget* soldStatusFilter = m_ui->gb_soldStatus->findChild<SelectionFilterWidget*>("w_soldStatusFilter");
    QSharedPointer<IFilter<QVariant>> soldStatusFilterObj(soldStatusFilter->GetFilterObject());
    if(soldStatusFilterObj->IsValid()) {
        m_filterCollection.insert(Field::SoldStatus, soldStatusFilterObj);
    }
    RangeFilterWidget* soldPriceFilter = m_ui->gb_soldPrice->findChild<RangeFilterWidget*>("w_soldPriceFilter");
    QSharedPointer<IFilter<QVariant>> soldPriceFilterObj(soldPriceFilter->GetFilterObject());
    if(soldPriceFilterObj->IsValid()) {
        m_filterCollection.insert(Field::SoldPrice, soldPriceFilterObj);
    }
    RangeFilterWidget* witFilter = m_ui->gb_wit->findChild<RangeFilterWidget*>("w_witFilter");
    QSharedPointer<IFilter<QVariant>> witFilterObj(witFilter->GetFilterObject());
    if(witFilterObj->IsValid()) {
        m_filterCollection.insert(Field::Wit, witFilterObj);
    }
    QSharedPointer<SelectionFilter<QVariant>> missedFilterObject =
            QSharedPointer<SelectionFilter<QVariant>>(new SelectionFilter<QVariant>());
    if(m_ui->coB_missedRecords->currentIndex() == c_showMissedIndex) {
        missedFilterObject->AddData(QVariant(true));
    } else if(m_ui->coB_missedRecords->currentIndex() == c_showNotMissedIndex) {
        missedFilterObject->AddData(QVariant(false));
    }
    if(missedFilterObject->IsValid()) {
        m_filterCollection.insert(Field::Missed, static_cast<QSharedPointer<IFilter<QVariant>>>(missedFilterObject));
    }
}
void LotFilterDialog::setupWidgets() {
    setupLotFilter();
    setupModelFilter();
    setupMakeFilter();
    try {
        initModelFilter();
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification("Failed to initialize \"model\" filter. Error: " + QString(err.what()),
                                                         NotificationManager::Type::Error);
    }
    setupYearFilter();
    setupTransmissionFilter();
    setupBodyFilter();
    setupPurposeFilter();
    setupLocationFilter();
    setupWatchDateFilter();
    setupAuctionDateFilter();
    setupSaleStatusFilter();
    setupSoldStatusFilter();
    setupSoldPriceFilter();
    setupWitPriceFilter();

    if(m_filterCollection.contains(Field::Missed)) {
        QVariantList missed =
                qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::Missed])->GetData();
        if(missed.size() != 1) {
            throw std::runtime_error("Invalid filter data for \"Missed\" field");
        }
        bool onlyMissed = missed.at(0).toBool();
        if(onlyMissed) {
            m_ui->coB_missedRecords->setCurrentIndex(c_showMissedIndex);
        } else {
            m_ui->coB_missedRecords->setCurrentIndex(c_showNotMissedIndex);
        }
    } else {
        m_ui->coB_missedRecords->setCurrentIndex(c_showMissedAndNotIndex);
    }

    Database* db = DatabaseManager::Instance().GetSettingsDatabase();
    FilterSettingsTable* filterTable = static_cast<FilterSettingsTable*>(db->Table(FILTERSETTINGS_TABLE_NAME));
    try {
        if(filterTable->Api().IsFilterTurnedOn()) {
            m_ui->chB_applyOnStartup->setChecked(true);
        } else {
            m_ui->chB_applyOnStartup->setChecked(false);
        }
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification("Failed to get filter is saved status. Error: " + QString(err.what()),
                                                         NotificationManager::Type::Error);
    }

    QObject* scrollEater = new ScrollEventEater(this);
    m_ui->coB_missedRecords->installEventFilter(scrollEater);
    m_ui->gb_missedRecords->setStyleSheet("QGroupBox { font-weight: bold; }");

    m_ui->buttonBox->button(QDialogButtonBox::Apply)->setDefault(false);
    m_ui->buttonBox->button(QDialogButtonBox::Cancel)->setDefault(true);
    m_ui->buttonBox->button(QDialogButtonBox::Reset)->setDefault(false);
}
void LotFilterDialog::setupLotFilter() {
    m_ui->gb_lot->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_lotFilter = new SelectionFilterWidget(false, m_ui->gb_lot);
    w_lotFilter->setObjectName("w_lotFilter");
    QObject::connect(m_ui->chB_lotExtended, SIGNAL(stateChanged(int)), w_lotFilter, SLOT(OnEnableExtendedMode(int)));
    m_ui->vLay_lot->addWidget(w_lotFilter);

    try {
        Database* db = DatabaseManager::Instance().GetRecordsDatabase();
        LotRecordsTable* lotTable = static_cast<LotRecordsTable*>(db->Table(LOTRECORDS_TABLE_NAME));
        QList<size_t> lotData = lotTable->Api().GetAllDistinctLotIds();
        std::sort(lotData.begin(), lotData.end());
        QList<QPair<QVariant, QVariant>> choiceData;
        for(auto d : lotData) {
            choiceData.append(QPair<QVariant, QVariant>(d, d));
        }
        static_cast<SelectionFilterWidget*>(w_lotFilter)->AddData(choiceData);

        if(m_filterCollection.contains(Field::LotId)) {
            static_cast<SelectionFilterWidget*>(w_lotFilter)->SetFilterObject(m_filterCollection[Field::LotId]);
            if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::LotId])->Count() > 1) {
                m_ui->chB_lotExtended->setChecked(true);
            }
        }
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification("Failed to setup \"lot\" filter. Error: " + QString(err.what()),
                                                         NotificationManager::Type::Error);
    }
}
void LotFilterDialog::setupModelFilter() {
    m_ui->gb_model->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_modelFilter = new SelectionFilterWidget(false, m_ui->gb_model);
    w_modelFilter->setObjectName("w_modelFilter");
    QObject::connect(m_ui->chB_modelExtended, SIGNAL(stateChanged(int)), w_modelFilter, SLOT(OnEnableExtendedMode(int)));
    m_ui->vLay_model->addWidget(w_modelFilter);
}
void LotFilterDialog::setupMakeFilter() {
    m_ui->gb_make->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_makeFilter = new SelectionFilterWidget(false, m_ui->gb_make);
    w_makeFilter->setObjectName("w_makeFilter");
    QObject::connect(m_ui->chB_makeExtended, SIGNAL(stateChanged(int)), w_makeFilter, SLOT(OnEnableExtendedMode(int)));
    QObject::connect(static_cast<SelectionFilterWidget*>(w_makeFilter), SIGNAL(sigSelectionAdded(QVariant)), this, SLOT(onMakeSelectionAdded(QVariant)));
    QObject::connect(static_cast<SelectionFilterWidget*>(w_makeFilter), SIGNAL(sigSelectionDeleted(QVariant)), this, SLOT(onMakeSelectionDeleted(QVariant)));
    m_ui->vLay_make->addWidget(w_makeFilter);

    try {
        Database* db = DatabaseManager::Instance().GetRecordsDatabase();
        MakeTable* makeTable = static_cast<MakeTable*>(db->Table(MAKE_TABLE_NAME));
        QList<QMap<MakeTable::Fields, QVariant>> makeData = makeTable->Api().GetMakeData();
        Tools::SortStringsWithNumbers<MakeTable::Fields>(makeData,
                                                         MakeTable::Fields::Name,
                                                         Tools::SortOrder::Ascending);
        QList<QPair<QVariant, QVariant>> choiceData;
        for(auto d : makeData) {
            choiceData.append(QPair<QVariant, QVariant>(Tools::ChangeStringCase(d[MakeTable::Fields::Name].toString(),
                                                                                 Tools::StringCase::AllFirstUpper),
                                                        d[MakeTable::Fields::Id]));
        }
        static_cast<SelectionFilterWidget*>(w_makeFilter)->AddData(choiceData);

        if(m_filterCollection.contains(Field::Make)) {
            static_cast<SelectionFilterWidget*>(w_makeFilter)->SetFilterObject(m_filterCollection[Field::Make]);
            if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::Make])->Count() > 1) {
                m_ui->chB_makeExtended->setChecked(true);
            }
        }
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification("Failed to setup \"make\" filter. Error: " + QString(err.what()),
                                                         NotificationManager::Type::Error);
    }
}
void LotFilterDialog::initModelFilter() {
    SelectionFilterWidget* w_modelFilter = m_ui->gb_model->findChild<SelectionFilterWidget*>("w_modelFilter");
    if(m_filterCollection.contains(Field::Model)) {
        if(m_filterCollection.contains(Field::Make)) {
            QVariantList filterMakes = qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::Make])->GetData();
            QVariantList filterModels = qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::Model])->GetData();
            Database* db = DatabaseManager::Instance().GetRecordsDatabase();
            ModelTable* modelTable = static_cast<ModelTable*>(db->Table(MODEL_TABLE_NAME));
            for(auto model : filterModels) {
                int makeId = modelTable->Api().GetMakeIdByModelId(model.toInt());
                bool found = false;
                for(auto make : filterMakes) {
                    if(make.toInt() == makeId) {
                        found = true;
                        break;
                    }
                }
                if(!found) {
                    throw std::runtime_error("Make isn\'t exists for model in filter [Model ID=" + model.toString().toStdString() + "].");
                }
            }
            static_cast<SelectionFilterWidget*>(w_modelFilter)->SetFilterObject(m_filterCollection[Field::Model]);
            if(filterModels.size() > 1) {
                m_ui->chB_modelExtended->setChecked(true);
            }
        } else {
            throw std::runtime_error("Empty make list when model is given.");
        }
    }
}
void LotFilterDialog::setupYearFilter() {
    m_ui->gb_year->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_yearFilter = new RangeFilterWidget(RangeFilterWidget::RangeType::Number, false, m_ui->gb_year);
    w_yearFilter->setObjectName("w_yearFilter");
    m_ui->vLay_year->addWidget(w_yearFilter);
    QObject::connect(m_ui->chB_yearExtended, SIGNAL(stateChanged(int)), w_yearFilter, SLOT(OnEnableExtendedMode(int)));
    if(m_filterCollection.contains(Field::Year)) {
        static_cast<RangeFilterWidget*>(w_yearFilter)->SetFilterObject(m_filterCollection[Field::Year]);
        if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::Year])->Count() > 1) {
            m_ui->chB_yearExtended->setChecked(true);
        }
    }
}
void LotFilterDialog::setupTransmissionFilter() {
    m_ui->gb_transmission->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_transmissionFilter = new SelectionFilterWidget(false, m_ui->gb_transmission);
    w_transmissionFilter->setObjectName("w_transmissionFilter");
    QObject::connect(m_ui->chB_transmissionExtended, SIGNAL(stateChanged(int)), w_transmissionFilter, SLOT(OnEnableExtendedMode(int)));
    m_ui->vLay_transmission->addWidget(w_transmissionFilter);

    try {
        Database* db = DatabaseManager::Instance().GetRecordsDatabase();
        TransmissionTable* transmissionTable = static_cast<TransmissionTable*>(db->Table(TRANSMISSION_TABLE_NAME));
        QList<QMap<TransmissionTable::Fields, QVariant>> transmissionData = transmissionTable->Api().GetTransmissionData();
        QList<QPair<QVariant, QVariant>> choiceData;
        for(auto d : transmissionData) {
            choiceData.append(QPair<QVariant, QVariant>(Tools::ChangeStringCase(d[TransmissionTable::Fields::Name].toString(),
                                                                                 Tools::StringCase::FirstUpper),
                                                        d[TransmissionTable::Fields::Id]));
        }
        static_cast<SelectionFilterWidget*>(w_transmissionFilter)->AddData(choiceData);

        if(m_filterCollection.contains(Field::Transmission)) {
            static_cast<SelectionFilterWidget*>(w_transmissionFilter)->SetFilterObject(m_filterCollection[Field::Transmission]);
            if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::Transmission])->Count() > 1) {
                m_ui->chB_transmissionExtended->setChecked(true);
            }
        }
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification("Failed to setup \"transmission\" filter. Error: " + QString(err.what()),
                                                         NotificationManager::Type::Error);
    }
}
void LotFilterDialog::setupBodyFilter() {
    m_ui->gb_body->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_bodyFilter = new SelectionFilterWidget(false, m_ui->gb_body);
    w_bodyFilter->setObjectName("w_bodyFilter");
    QObject::connect(m_ui->chB_bodyExtended, SIGNAL(stateChanged(int)), w_bodyFilter, SLOT(OnEnableExtendedMode(int)));
    m_ui->vLay_body->addWidget(w_bodyFilter);

    try {
        Database* db = DatabaseManager::Instance().GetRecordsDatabase();
        BodyTable* bodyTable = static_cast<BodyTable*>(db->Table(BODY_TABLE_NAME));
        QList<QMap<BodyTable::Fields, QVariant>> bodyData = bodyTable->Api().GetBodyData();
        QList<QPair<QVariant, QVariant>> choiceData;
        for(auto d : bodyData) {
            choiceData.append(QPair<QVariant, QVariant>(d[BodyTable::Fields::Name].toString(),
                                                        d[BodyTable::Fields::Id]));
        }
        static_cast<SelectionFilterWidget*>(w_bodyFilter)->AddData(choiceData);

        if(m_filterCollection.contains(Field::Body)) {
            static_cast<SelectionFilterWidget*>(w_bodyFilter)->SetFilterObject(m_filterCollection[Field::Body]);
            if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::Body])->Count() > 1) {
                m_ui->chB_bodyExtended->setChecked(true);
            }
        }
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification("Failed to setup \"body\" filter. Error: " + QString(err.what()),
                                                         NotificationManager::Type::Error);
    }
}
void LotFilterDialog::setupPurposeFilter() {
    m_ui->gb_purpose->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_purposeFilter = new SelectionFilterWidget(false, m_ui->gb_purpose);
    w_purposeFilter->setObjectName("w_purposeFilter");
    QObject::connect(m_ui->chB_purposeExtended, SIGNAL(stateChanged(int)), w_purposeFilter, SLOT(OnEnableExtendedMode(int)));
    m_ui->vLay_purpose->addWidget(w_purposeFilter);

    try {
        Database* db = DatabaseManager::Instance().GetRecordsDatabase();
        PurposeTable* purposeTable = static_cast<PurposeTable*>(db->Table(PURPOSE_TABLE_NAME));
        QList<QMap<PurposeTable::Fields, QVariant>> purposeData = purposeTable->Api().GetPurposeData();
        QList<QPair<QVariant, QVariant>> choiceData;
        for(auto d : purposeData) {
            choiceData.append(QPair<QVariant, QVariant>(Tools::ChangeStringCase(d[PurposeTable::Fields::Name].toString(),
                                                                                 Tools::StringCase::FirstUpper),
                                                        d[PurposeTable::Fields::Id]));
        }
        static_cast<SelectionFilterWidget*>(w_purposeFilter)->AddData(choiceData);

        if(m_filterCollection.contains(Field::Purpose)) {
            static_cast<SelectionFilterWidget*>(w_purposeFilter)->SetFilterObject(m_filterCollection[Field::Purpose]);
            if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::Purpose])->Count() > 1) {
                m_ui->chB_purposeExtended->setChecked(true);
            }
        }
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification("Failed to setup \"purpose\" filter. Error: " + QString(err.what()),
                                                         NotificationManager::Type::Error);
    }
}
void LotFilterDialog::setupLocationFilter() {
    m_ui->gb_location->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_locationFilter = new SelectionFilterWidget(false, m_ui->gb_location);
    w_locationFilter->setObjectName("w_locationFilter");
    QObject::connect(m_ui->chB_locationExtended, SIGNAL(stateChanged(int)), w_locationFilter, SLOT(OnEnableExtendedMode(int)));
    m_ui->vLay_location->addWidget(w_locationFilter);

    try {
        Database* db = DatabaseManager::Instance().GetRecordsDatabase();
        LocationTable* locationTable = static_cast<LocationTable*>(db->Table(LOCATION_TABLE_NAME));
        QList<QMap<LocationTable::Fields, QVariant>> locationData = locationTable->Api().GetLocationData();
        QList<QPair<QVariant, QVariant>> choiceData;
        for(auto d : locationData) {
            choiceData.append(QPair<QVariant, QVariant>(Tools::ChangeStringCase(d[LocationTable::Fields::Abbreviation].toString(), Tools::StringCase::AllUpper) + " (" +
                                                        Tools::ChangeStringCase(d[LocationTable::Fields::Name].toString(), Tools::StringCase::AllFirstUpper) + ")",
                                                        d[LocationTable::Fields::Id]));

        }
        static_cast<SelectionFilterWidget*>(w_locationFilter)->AddData(choiceData);

        if(m_filterCollection.contains(Field::Location)) {
            static_cast<SelectionFilterWidget*>(w_locationFilter)->SetFilterObject(m_filterCollection[Field::Location]);
            if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::Location])->Count() > 1) {
                m_ui->chB_locationExtended->setChecked(true);
            }
        }
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification("Failed to setup \"location\" filter. Error: " + QString(err.what()),
                                                         NotificationManager::Type::Error);
    }
}
void LotFilterDialog::setupWatchDateFilter() {
    m_ui->gb_watchDate->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_watchDateFilter = new RangeFilterWidget(RangeFilterWidget::RangeType::Date, false, m_ui->gb_watchDate);
    w_watchDateFilter->setObjectName("w_watchDateFilter");
    QObject::connect(m_ui->chB_watchDateExtended, SIGNAL(stateChanged(int)), w_watchDateFilter, SLOT(OnEnableExtendedMode(int)));
    m_ui->vLay_watchDate->addWidget(w_watchDateFilter);

    if(m_filterCollection.contains(Field::WatchDate)) {
        static_cast<RangeFilterWidget*>(w_watchDateFilter)->SetFilterObject(m_filterCollection[Field::WatchDate]);
        if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::WatchDate])->Count() > 1) {
            m_ui->chB_watchDateExtended->setChecked(true);
        }
    }
}
void LotFilterDialog::setupAuctionDateFilter() {
    m_ui->gb_auctionDate->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_auctionDateFilter = new RangeFilterWidget(RangeFilterWidget::RangeType::Date, false, m_ui->gb_auctionDate);
    w_auctionDateFilter->setObjectName("w_auctionDateFilter");
    QObject::connect(m_ui->chB_auctionDateExtended, SIGNAL(stateChanged(int)), w_auctionDateFilter, SLOT(OnEnableExtendedMode(int)));
    m_ui->vLay_auctionDate->addWidget(w_auctionDateFilter);

    if(m_filterCollection.contains(Field::AuctionDate)) {
        static_cast<RangeFilterWidget*>(w_auctionDateFilter)->SetFilterObject(m_filterCollection[Field::AuctionDate]);
        if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::AuctionDate])->Count() > 1) {
            m_ui->chB_auctionDateExtended->setChecked(true);
        }
    }
}
void LotFilterDialog::setupSaleStatusFilter() {
    m_ui->gb_saleStatus->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_saleStatusFilter = new SelectionFilterWidget(false, m_ui->gb_saleStatus);
    w_saleStatusFilter->setObjectName("w_saleStatusFilter");
    QObject::connect(m_ui->chB_saleStatusExtended, SIGNAL(stateChanged(int)), w_saleStatusFilter, SLOT(OnEnableExtendedMode(int)));
    m_ui->vLay_saleStatus->addWidget(w_saleStatusFilter);

    try {
        Database* db = DatabaseManager::Instance().GetRecordsDatabase();
        SaleStatusTable* saleStatusTable = static_cast<SaleStatusTable*>(db->Table(SALESTATUS_TABLE_NAME));
        QList<QMap<SaleStatusTable::Fields, QVariant>> saleStatusData = saleStatusTable->Api().GetSaleStatusData();
        QList<QPair<QVariant, QVariant>> choiceData;
        for(auto d : saleStatusData) {
            choiceData.append(QPair<QVariant, QVariant>(Tools::ChangeStringCase(d[SaleStatusTable::Fields::Name].toString(),
                                                                                 Tools::StringCase::FirstUpper),
                                                        d[SaleStatusTable::Fields::Id]));
        }
        static_cast<SelectionFilterWidget*>(w_saleStatusFilter)->AddData(choiceData);

        if(m_filterCollection.contains(Field::SaleStatus)) {
            static_cast<SelectionFilterWidget*>(w_saleStatusFilter)->SetFilterObject(m_filterCollection[Field::SaleStatus]);
            if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::SaleStatus])->Count() > 1) {
                m_ui->chB_saleStatusExtended->setChecked(true);
            }
        }
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification("Failed to setup \"sale status\" filter. Error: " + QString(err.what()),
                                                         NotificationManager::Type::Error);
    }
}
void LotFilterDialog::setupSoldStatusFilter() {
    m_ui->gb_soldStatus->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_soldStatusFilter = new SelectionFilterWidget(false, m_ui->gb_soldStatus);
    w_soldStatusFilter->setObjectName("w_soldStatusFilter");
    QObject::connect(m_ui->chB_soldStatusExtended, SIGNAL(stateChanged(int)), w_soldStatusFilter, SLOT(OnEnableExtendedMode(int)));
    m_ui->vLay_soldStatus->addWidget(w_soldStatusFilter);

    try {
        Database* db = DatabaseManager::Instance().GetRecordsDatabase();
        SoldStatusTable* soldStatusTable = static_cast<SoldStatusTable*>(db->Table(SOLDSTATUS_TABLE_NAME));
        QList<QMap<SoldStatusTable::Fields, QVariant>> soldStatusData = soldStatusTable->Api().GetSoldStatusData();
        QList<QPair<QVariant, QVariant>> choiceData;
        for(auto d : soldStatusData) {
            choiceData.append(QPair<QVariant, QVariant>(Tools::ChangeStringCase(d[SoldStatusTable::Fields::Name].toString(),
                                                                                 Tools::StringCase::FirstUpper),
                                                        d[SoldStatusTable::Fields::Id]));
        }
        static_cast<SelectionFilterWidget*>(w_soldStatusFilter)->AddData(choiceData);

        if(m_filterCollection.contains(Field::SoldStatus)) {
            static_cast<SelectionFilterWidget*>(w_soldStatusFilter)->SetFilterObject(m_filterCollection[Field::SoldStatus]);
            if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::SoldStatus])->Count() > 1) {
                m_ui->chB_soldStatusExtended->setChecked(true);
            }
        }
    } catch (const std::runtime_error& err) {
        NotificationManager::Instance().ShowNotification("Failed to setup \"sold status\" filter. Error: " + QString(err.what()),
                                                         NotificationManager::Type::Error);
    }
}
void LotFilterDialog::setupSoldPriceFilter() {
    m_ui->gb_soldPrice->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_soldPriceFilter = new RangeFilterWidget(RangeFilterWidget::RangeType::Number, false, m_ui->gb_soldPrice);
    w_soldPriceFilter->setObjectName("w_soldPriceFilter");
    QObject::connect(m_ui->chB_soldPriceExtended, SIGNAL(stateChanged(int)), w_soldPriceFilter, SLOT(OnEnableExtendedMode(int)));
    m_ui->vLay_soldPrice->addWidget(w_soldPriceFilter);

    if(m_filterCollection.contains(Field::SoldPrice)) {
        static_cast<RangeFilterWidget*>(w_soldPriceFilter)->SetFilterObject(m_filterCollection[Field::SoldPrice]);
        if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::SoldPrice])->Count() > 1) {
            m_ui->chB_soldPriceExtended->setChecked(true);
        }
    }
}
void LotFilterDialog::setupWitPriceFilter() {
    m_ui->gb_wit->setStyleSheet("QGroupBox { font-weight: bold; }");
    QWidget* w_witFilter = new RangeFilterWidget(RangeFilterWidget::RangeType::Number, false, m_ui->gb_wit);
    w_witFilter->setObjectName("w_witFilter");
    QObject::connect(m_ui->chB_witExtended, SIGNAL(stateChanged(int)), w_witFilter, SLOT(OnEnableExtendedMode(int)));
    m_ui->vLay_wit->addWidget(w_witFilter);

    if(m_filterCollection.contains(Field::Wit)) {
        static_cast<RangeFilterWidget*>(w_witFilter)->SetFilterObject(m_filterCollection[Field::Wit]);
        if(qSharedPointerCast<SelectionFilter<QVariant>>(m_filterCollection[Field::Wit])->Count() > 1) {
            m_ui->chB_witExtended->setChecked(true);
        }
    }
}
