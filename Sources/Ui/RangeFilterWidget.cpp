#include "Headers/Ui/RangeFilterWidget.h"
#include "Headers/Ui/TemplateQWidget.h"
#include "Headers/ScrollEventEater.h"
#include "Headers/Options.h"
#include "Headers/Tools.h"
#include "ui_RangeFilterWidget.h"
#include "ui_RangeMultiFilterWidget.h"
#include "ui_DateRangeFilterWidget.h"
#include "ui_DateRangeMultiFilterWidget.h"

typedef QPair<int,int> IntegerPair;
typedef QPair<QDate,QDate> DatePair;
Q_DECLARE_METATYPE(IntegerPair)
Q_DECLARE_METATYPE(DatePair)

RangeFilterWidget::RangeFilterWidget(RangeFilterWidget::RangeType type, bool extended, QWidget* parent)
    : QWidget(parent)
    , m_stackedUi()
    , m_numberFilterIndex(-1)
    , m_numberMultiFilterIndex(-1)
    , m_dateFilterIndex(-1)
    , m_dateMultiFilterIndex(-1)
    , m_type(type)
{
    TemplateQWidget<Ui::rangeFilterWidget>* numberFilter = new TemplateQWidget<Ui::rangeFilterWidget>();
    TemplateQWidget<Ui::rangeMultiFilterWidget>* numberMultiFilter = new TemplateQWidget<Ui::rangeMultiFilterWidget>();
    TemplateQWidget<Ui::dateRangeFilterWidget>* dateFilter = new TemplateQWidget<Ui::dateRangeFilterWidget>();
    TemplateQWidget<Ui::dateRangeMultiFilterWidget>* dateMultiFilter = new TemplateQWidget<Ui::dateRangeMultiFilterWidget>();

    m_numberFilterUi = numberFilter->m_ui;
    m_numberMultiFilterUi = numberMultiFilter->m_ui;
    m_dateFilterUi = dateFilter->m_ui;
    m_dateMultiFilterUi = dateMultiFilter->m_ui;

    QObject::connect(m_numberMultiFilterUi->b_send, SIGNAL(clicked()),
                     this, SLOT(onSendButtonClicked()));
    QObject::connect(m_dateMultiFilterUi->b_send, SIGNAL(clicked()),
                     this, SLOT(onSendButtonClicked()));
    QObject::connect(m_numberMultiFilterUi->b_delete, SIGNAL(clicked()),
                     this, SLOT(onDeleteButtonClicked()));
    QObject::connect(m_dateMultiFilterUi->b_delete, SIGNAL(clicked()),
                     this, SLOT(onDeleteButtonClicked()));
    QObject::connect(m_numberMultiFilterUi->listWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)),
                     this, SLOT(onSelectedDoubleClicked(QListWidgetItem*)));
    QObject::connect(m_dateMultiFilterUi->listWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)),
                     this, SLOT(onSelectedDoubleClicked(QListWidgetItem*)));
    QObject::connect(m_numberFilterUi->le_from, SIGNAL(textChanged(const QString&)),
                     this, SLOT(onSingleNumberRangeChanged()));
    makeSingleDateRangeConnections();

    m_numberFilterUi->le_from->setValidator(new QIntValidator(m_numberFilterUi->le_from));
    m_numberFilterUi->le_to->setValidator(new QIntValidator(m_numberFilterUi->le_to));
    m_numberMultiFilterUi->le_from->setValidator(new QIntValidator(m_numberMultiFilterUi->le_from));
    m_numberMultiFilterUi->le_to->setValidator(new QIntValidator(m_numberMultiFilterUi->le_to));

    m_numberFilterIndex = m_stackedUi.addWidget(numberFilter);
    m_numberMultiFilterIndex = m_stackedUi.addWidget(numberMultiFilter);
    m_dateFilterIndex = m_stackedUi.addWidget(dateFilter);
    m_dateMultiFilterIndex = m_stackedUi.addWidget(dateMultiFilter);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    layout->addWidget(&m_stackedUi);
    setLayout(layout);

    Reset();
    OnEnableExtendedMode(extended);
    setupDateFilters();
}
void RangeFilterWidget::Reset() {
    if(m_type == RangeType::Date) {
        int year = QDateTime::currentDateTime().date().year();
        m_dateFilterUi->de_dateFrom->setDate(QDate(year, 1, 1));
        m_dateFilterUi->de_dateTo->setDate(QDate(year, 1, 1));
        m_dateFilterUi->cob_dateFrom->setCurrentIndex(0);
        m_dateFilterUi->cob_dateTo->setCurrentIndex(0);

        m_dateMultiFilterUi->de_dateFrom->setDate(QDate(year, 1, 1));
        m_dateMultiFilterUi->de_dateTo->setDate(QDate(year, 1, 1));
        m_dateMultiFilterUi->cob_dateFrom->setCurrentIndex(0);
        m_dateMultiFilterUi->cob_dateTo->setCurrentIndex(0);
        m_dateMultiFilterUi->listWidget->clear();
    } else if(m_type == RangeType::Number) {
        m_numberFilterUi->le_from->clear();
        m_numberFilterUi->le_to->clear();

        m_numberMultiFilterUi->le_from->clear();
        m_numberMultiFilterUi->le_to->clear();
        m_numberMultiFilterUi->listWidget->clear();
    }
}
QSharedPointer<IFilter<QVariant>> RangeFilterWidget::GetFilterObject() const {
    QSharedPointer<RangeFilter<QVariant>> filterObject =
            QSharedPointer<RangeFilter<QVariant>>(new RangeFilter<QVariant>());
    if(m_type == RangeType::Number) {
        if(m_stackedUi.currentIndex() == m_numberMultiFilterIndex) {
            for(int i = 0; i < m_numberMultiFilterUi->listWidget->count(); ++i) {
                IntegerPair range = qvariant_cast<IntegerPair>(m_numberMultiFilterUi->listWidget->item(i)->data(Qt::UserRole));
                filterObject->AddData(QVariant(range.first), QVariant(range.second));
            }
        } else {
            int from = INT_MIN;
            int to = INT_MAX;
            if(!m_numberFilterUi->le_from->text().isEmpty()) {
                from = m_numberFilterUi->le_from->text().toInt();
            }
            if(!m_numberFilterUi->le_to->text().isEmpty()) {
                to = m_numberFilterUi->le_to->text().toInt();
            }
            if(from != INT_MIN || to != INT_MAX) {
                filterObject->AddData(QVariant(from), QVariant(to));
            }
        }
    } else if(m_type == RangeType::Date) {
        if(m_stackedUi.currentIndex() == m_dateMultiFilterIndex) {
            for(int i = 0; i < m_dateMultiFilterUi->listWidget->count(); ++i) {
                DatePair range = qvariant_cast<DatePair>(m_dateMultiFilterUi->listWidget->item(i)->data(Qt::UserRole));
                filterObject->AddData(QVariant(range.first), QVariant(range.second));
            }
        } else {
            QDate from(-4713, 1, 1);
            QDate to(9999, 12, 31);
            if(m_dateFilterUi->cob_dateFrom->currentIndex() == 1) {
                from = QDate(9999, 12, 31);
            } else if(m_dateFilterUi->cob_dateFrom->currentIndex() == 2) {
                from = m_dateFilterUi->de_dateFrom->date();
            }
            if(m_dateFilterUi->cob_dateTo->currentIndex() == 1) {
                to = QDate(9999, 12, 31);
            } else if(m_dateFilterUi->cob_dateTo->currentIndex() == 2) {
                to = m_dateFilterUi->de_dateTo->date();
            }
            if(from.year() != -4713 || to.year() != 9999) {
                filterObject->AddData(QVariant(from), QVariant(to));
            }
        }
    }
    return static_cast<QSharedPointer<IFilter<QVariant>>>(filterObject);
}
void RangeFilterWidget::SetFilterObject(const QSharedPointer<IFilter<QVariant>>& filter) {
    selectData(qSharedPointerCast<RangeFilter<QVariant>>(filter)->GetData());
}
void RangeFilterWidget::OnEnableExtendedMode(int enable) {
    if(m_type == RangeType::Number) {
        if(enable == Qt::Checked) {
            m_stackedUi.setCurrentIndex(m_numberMultiFilterIndex);
            int minHeight = m_stackedUi.currentWidget()->minimumHeight();
            setFixedHeight(minHeight);
        } else {
            m_stackedUi.setCurrentIndex(m_numberFilterIndex);
            int minHeight = m_stackedUi.currentWidget()->minimumHeight();
            setFixedHeight(minHeight);
        }
    } else if(m_type == RangeType::Date) {
        if(enable == Qt::Checked) {
            m_stackedUi.setCurrentIndex(m_dateMultiFilterIndex);
            int minHeight = m_stackedUi.currentWidget()->minimumHeight();
            setFixedHeight(minHeight);
        } else {
            m_stackedUi.setCurrentIndex(m_dateFilterIndex);
            int minHeight = m_stackedUi.currentWidget()->minimumHeight();
            setFixedHeight(minHeight);
        }
    }
}


void RangeFilterWidget::setupDateFilters() {
    QObject::connect(m_dateFilterUi->cob_dateFrom, SIGNAL(currentIndexChanged(int)), this, SLOT(onDateVariantChanged(int)));
    QObject::connect(m_dateFilterUi->cob_dateTo, SIGNAL(currentIndexChanged(int)), this, SLOT(onDateVariantChanged(int)));
    QObject::connect(m_dateMultiFilterUi->cob_dateFrom, SIGNAL(currentIndexChanged(int)), this, SLOT(onDateVariantChanged(int)));
    QObject::connect(m_dateMultiFilterUi->cob_dateTo, SIGNAL(currentIndexChanged(int)), this, SLOT(onDateVariantChanged(int)));

    m_dateFilterUi->de_dateFrom->hide();
    m_dateFilterUi->de_dateTo->hide();
    m_dateMultiFilterUi->de_dateFrom->hide();
    m_dateMultiFilterUi->de_dateTo->hide();

    QObject* scrollEater = new ScrollEventEater(this);
    m_dateFilterUi->cob_dateFrom->installEventFilter(scrollEater);
    m_dateFilterUi->cob_dateTo->installEventFilter(scrollEater);
    m_dateMultiFilterUi->cob_dateFrom->installEventFilter(scrollEater);
    m_dateMultiFilterUi->cob_dateTo->installEventFilter(scrollEater);
}
QString RangeFilterWidget::getDisplayString(const QVariant &data) {
    QString displayStr;
    if(m_type == RangeType::Number) {
        int numberFrom = qvariant_cast<IntegerPair>(data).first;
        int numberTo = qvariant_cast<IntegerPair>(data).second;
        if(numberFrom == INT_MIN) {
            displayStr = "To " + QString::number(numberTo);
        } else if(numberTo == INT_MAX) {
            displayStr = "From " + QString::number(numberFrom);
        } else {
            if(numberFrom != numberTo) {
                displayStr = QString::number(numberFrom) + "  to  " + QString::number(numberTo);
            } else {
                displayStr = QString::number(numberFrom);
            }
        }
    } else if(m_type == RangeType::Date) {
        QDate dateFrom = qvariant_cast<DatePair>(data).first;
        QDate dateTo = qvariant_cast<DatePair>(data).second;
        QString dateFormat = Options::Instance().DateFormat();
        if(dateFrom.year() == -4713) {
            displayStr = "To " + (dateTo.year() == 9999 ? "future" : dateTo.toString(dateFormat));
        } else if(dateTo.year() == 9999 && dateFrom.year() != 9999) {
            displayStr = "From " + (dateFrom.year() == 9999 ? "future" : dateFrom.toString(dateFormat));
        } else {
            if(dateFrom == dateTo) {
                displayStr = (dateFrom.year() == 9999 ? "Future" : dateFrom.toString(dateFormat));
            } else {
                displayStr = (dateFrom.year() == 9999 ? "Future" : dateFrom.toString(dateFormat)) + " to " +
                             (dateTo.year() == 9999 ? "future" : dateTo.toString(dateFormat));
            }
        }
    }
    return displayStr;
}
void RangeFilterWidget::selectData(const QList<QPair<QVariant, QVariant>>& chosenData) {
    for(auto chosen : chosenData) {
        QListWidgetItem* item = new QListWidgetItem();
        QVariant data;
        switch(m_type) {
            case RangeType::Number : {
                data = QVariant::fromValue<IntegerPair>(IntegerPair(chosen.first.toInt(), chosen.second.toInt()));
                break;
            }
            case RangeType::Date : {
                data = QVariant::fromValue<DatePair>(DatePair(chosen.first.toDate(), chosen.second.toDate()));
                break;
            }
        };
        QString displayStr = getDisplayString(data);
        item->setData(Qt::UserRole, data);
        item->setText(displayStr);
        if(m_type == RangeType::Number) {
            m_numberMultiFilterUi->listWidget->addItem(item);
            m_numberMultiFilterUi->listWidget->scrollToBottom();
            QObject::disconnect(m_numberFilterUi->le_from, SIGNAL(textChanged(const QString&)),
                             this, SLOT(onSingleNumberRangeChanged()));
            QObject::disconnect(m_numberFilterUi->le_to, SIGNAL(textChanged(const QString&)),
                             this, SLOT(onSingleNumberRangeChanged()));
            if(m_numberMultiFilterUi->listWidget->count() == 1) {
                m_numberFilterUi->le_from->setText(QString::number(qvariant_cast<IntegerPair>(data).first));
                m_numberFilterUi->le_to->setText(QString::number(qvariant_cast<IntegerPair>(data).second));
            } else {
                m_numberFilterUi->le_from->setText("");
                m_numberFilterUi->le_to->setText("");
            }
            QObject::connect(m_numberFilterUi->le_from, SIGNAL(textChanged(const QString&)),
                          this, SLOT(onSingleNumberRangeChanged()));
            QObject::connect(m_numberFilterUi->le_to, SIGNAL(textChanged(const QString&)),
                          this, SLOT(onSingleNumberRangeChanged()));
        } else if(m_type == RangeType::Date) {
            m_dateMultiFilterUi->listWidget->addItem(item);
            m_dateMultiFilterUi->listWidget->scrollToBottom();
            breakSingleDateRangeConnections();
            if(m_dateMultiFilterUi->listWidget->count() == 1) {
                QDate dateFrom = qvariant_cast<DatePair>(data).first;
                QDate dateTo = qvariant_cast<DatePair>(data).second;
                if(dateFrom.year() == 9999) {
                    m_dateFilterUi->cob_dateFrom->setCurrentIndex(1);
                } else if(dateFrom.year() == -4713) {
                    m_dateFilterUi->cob_dateFrom->setCurrentIndex(0);
                } else {
                    m_dateFilterUi->cob_dateFrom->setCurrentIndex(2);
                    m_dateFilterUi->de_dateFrom->setDate(dateFrom);
                }
                if(dateTo.year() == 9999) {
                    m_dateFilterUi->cob_dateTo->setCurrentIndex(1);
                } else if(dateTo.year() == -4713) {
                    throw("Invalid Date \"To\" [p1]");
                } else {
                    m_dateFilterUi->cob_dateTo->setCurrentIndex(2);
                    m_dateFilterUi->de_dateTo->setDate(dateTo);
                }
            } else {
                m_dateFilterUi->cob_dateFrom->setCurrentIndex(0);
                m_dateFilterUi->cob_dateTo->setCurrentIndex(0);
                m_dateFilterUi->de_dateFrom->setDate(QDate(QDateTime::currentDateTime().date().year(), 1, 1));
                m_dateFilterUi->de_dateTo->setDate(QDate(QDateTime::currentDateTime().date().year(), 1, 1));
            }
            makeSingleDateRangeConnections();
        }
    }
}

void RangeFilterWidget::makeSingleDateRangeConnections() {
    QObject::connect(m_dateFilterUi->cob_dateFrom, SIGNAL(currentIndexChanged(int)),
                     this, SLOT(onSingleDateRangeChanged()));
    QObject::connect(m_dateFilterUi->de_dateFrom, SIGNAL(dateChanged(const QDate&)),
                     this, SLOT(onSingleDateRangeChanged()));
    QObject::connect(m_dateFilterUi->cob_dateTo, SIGNAL(currentIndexChanged(int)),
                     this, SLOT(onSingleDateRangeChanged()));
    QObject::connect(m_dateFilterUi->de_dateTo, SIGNAL(dateChanged(const QDate&)),
                     this, SLOT(onSingleDateRangeChanged()));
}
void RangeFilterWidget::breakSingleDateRangeConnections() {
    QObject::disconnect(m_dateFilterUi->cob_dateFrom, SIGNAL(currentIndexChanged(int)),
                        this, SLOT(onSingleDateRangeChanged()));
    QObject::disconnect(m_dateFilterUi->de_dateFrom, SIGNAL(dateChanged(const QDate&)),
                        this, SLOT(onSingleDateRangeChanged()));
    QObject::disconnect(m_dateFilterUi->cob_dateTo, SIGNAL(currentIndexChanged(int)),
                        this, SLOT(onSingleDateRangeChanged()));
    QObject::disconnect(m_dateFilterUi->de_dateTo, SIGNAL(dateChanged(const QDate&)),
                        this, SLOT(onSingleDateRangeChanged()));
}
void RangeFilterWidget::onSendButtonClicked() {
    QVariant data;
    QString displayStr;
    if(m_type == RangeType::Number) {
        int numberFrom, numberTo;
        if(m_numberMultiFilterUi->le_from->text().isEmpty()) {
            numberFrom = INT_MIN;
        } else {
            numberFrom = m_numberMultiFilterUi->le_from->text().toInt();
        }
        if(m_numberMultiFilterUi->le_to->text().isEmpty()) {
            numberTo = INT_MAX;
        } else {
            numberTo = m_numberMultiFilterUi->le_to->text().toInt();
        }
        if(numberTo < numberFrom || (numberFrom == INT_MIN && numberTo == INT_MAX)) {
            return;
        }
        data = QVariant::fromValue(IntegerPair(numberFrom, numberTo));
        displayStr = getDisplayString(data);
        for(int i = 0; i < m_numberMultiFilterUi->listWidget->count(); ++i) {
            if(m_numberMultiFilterUi->listWidget->item(i)->data(Qt::UserRole) == data) {
                return;
            }
        }
    } else if(m_type == RangeType::Date) {
        QDate dateFrom(-4713, 1, 1);
        QDate dateTo(9999, 12, 31);
        if(m_dateMultiFilterUi->cob_dateFrom->currentIndex() == 1) {
            dateFrom.setDate(9999, 12, 31);
        } else if(m_dateMultiFilterUi->cob_dateFrom->currentIndex() == 2) {
            dateFrom = m_dateMultiFilterUi->de_dateFrom->date();
        }
        if(m_dateMultiFilterUi->cob_dateTo->currentIndex() == 1) {
            dateTo.setDate(9999, 12, 31);
        } else if(m_dateMultiFilterUi->cob_dateTo->currentIndex() == 2) {
            dateTo = m_dateMultiFilterUi->de_dateTo->date();
        }
        if(dateTo < dateFrom || (m_dateMultiFilterUi->cob_dateFrom->currentIndex() == 0 && m_dateMultiFilterUi->cob_dateTo->currentIndex() == 0)) {
            return;
        }

        data = QVariant::fromValue(DatePair(dateFrom, dateTo));
        displayStr = getDisplayString(data);

        for(int i = 0; i < m_dateMultiFilterUi->listWidget->count(); ++i) {
            if(m_dateMultiFilterUi->listWidget->item(i)->data(Qt::UserRole) == data) {
                return;
            }
        }
    }
    QListWidgetItem* item = new QListWidgetItem();
    item->setData(Qt::UserRole, data);
    item->setText(displayStr);
    if(m_type == RangeType::Number) {
        m_numberMultiFilterUi->listWidget->addItem(item);
        m_numberMultiFilterUi->listWidget->scrollToBottom();
        QObject::disconnect(m_numberFilterUi->le_from, SIGNAL(textChanged(const QString&)),
                         this, SLOT(onSingleNumberRangeChanged()));
        QObject::disconnect(m_numberFilterUi->le_to, SIGNAL(textChanged(const QString&)),
                         this, SLOT(onSingleNumberRangeChanged()));
        if(m_numberMultiFilterUi->listWidget->count() == 1) {
            m_numberFilterUi->le_from->setText(m_numberMultiFilterUi->le_from->text());
            m_numberFilterUi->le_to->setText(m_numberMultiFilterUi->le_to->text());
        } else {
            m_numberFilterUi->le_from->setText("");
            m_numberFilterUi->le_to->setText("");
        }
        QObject::connect(m_numberFilterUi->le_from, SIGNAL(textChanged(const QString&)),
                         this, SLOT(onSingleNumberRangeChanged()));
        QObject::connect(m_numberFilterUi->le_to, SIGNAL(textChanged(const QString&)),
                         this, SLOT(onSingleNumberRangeChanged()));
    } else if(m_type == RangeType::Date) {
        m_dateMultiFilterUi->listWidget->addItem(item);
        m_dateMultiFilterUi->listWidget->scrollToBottom();
        breakSingleDateRangeConnections();
        if(m_dateMultiFilterUi->listWidget->count() == 1) {
            m_dateFilterUi->cob_dateFrom->setCurrentIndex(m_dateMultiFilterUi->cob_dateFrom->currentIndex());
            m_dateFilterUi->de_dateFrom->setDate(m_dateMultiFilterUi->de_dateFrom->date());
            m_dateFilterUi->cob_dateTo->setCurrentIndex(m_dateMultiFilterUi->cob_dateTo->currentIndex());
            m_dateFilterUi->de_dateTo->setDate(m_dateMultiFilterUi->de_dateTo->date());
        } else {
            m_dateFilterUi->cob_dateFrom->setCurrentIndex(0);
            m_dateFilterUi->cob_dateTo->setCurrentIndex(0);
        }
        makeSingleDateRangeConnections();
    }
}
void RangeFilterWidget::onDeleteButtonClicked() {
    QList<QListWidgetItem*> selectedItems;
    if(m_type == RangeType::Number) {
        selectedItems = m_numberMultiFilterUi->listWidget->selectedItems();
    } else if(m_type == RangeType::Date) {
        selectedItems = m_dateMultiFilterUi->listWidget->selectedItems();
    }
    for(auto item : selectedItems) {
        onSelectedDoubleClicked(item);
    }
}
void RangeFilterWidget::onSelectedDoubleClicked(QListWidgetItem* item) {
    if(m_type == RangeType::Number) {
        int row = m_numberMultiFilterUi->listWidget->row(item);
        m_numberMultiFilterUi->listWidget->takeItem(row);
        delete item;
        if(m_numberMultiFilterUi->listWidget->count() == 1) {
            IntegerPair data = qvariant_cast<IntegerPair>(m_numberMultiFilterUi->listWidget->item(0)->data(Qt::UserRole));
            m_numberFilterUi->le_from->setText(QString::number(data.first));
            m_numberFilterUi->le_to->setText(QString::number(data.second));
        } else {
            m_numberFilterUi->le_from->setText("");
            m_numberFilterUi->le_to->setText("");
        }
    } else if(m_type == RangeType::Date) {
        int row = m_dateMultiFilterUi->listWidget->row(item);
        m_dateMultiFilterUi->listWidget->takeItem(row);
        delete item;
        if(m_dateMultiFilterUi->listWidget->count() == 1) {
            DatePair data = qvariant_cast<DatePair>(m_dateMultiFilterUi->listWidget->item(0)->data(Qt::UserRole));
            if(data.first.year() == 9999) {
                m_dateFilterUi->cob_dateFrom->setCurrentIndex(1);
            } else if(data.first.year() == -4713) {
                m_dateFilterUi->cob_dateFrom->setCurrentIndex(0);
            } else {
                m_dateFilterUi->cob_dateFrom->setCurrentIndex(2);
                m_dateFilterUi->de_dateFrom->setDate(data.first);
            }
            if(data.second.year() == 9999) {
                m_dateFilterUi->cob_dateTo->setCurrentIndex(1);
            } else if(data.second.year() == -4713) {
                throw("Invalid Date \"To\" [p2]");
            } else {
                m_dateFilterUi->cob_dateTo->setCurrentIndex(2);
                m_dateFilterUi->de_dateTo->setDate(data.second);
            }
        } else {
            m_dateFilterUi->cob_dateFrom->setCurrentIndex(0);
            m_dateFilterUi->cob_dateTo->setCurrentIndex(0);
        }
    }
}
void RangeFilterWidget::onDateVariantChanged(int index) {
    QWidget* senderDateEdit = static_cast<QWidget*>(sender());

    auto lambda_alterVisibility = [index] (QWidget* widget) {
        if(index == 0 || index == 1) {
            widget->hide();
        } else if(index == 2) {
            widget->show();
        }
    };

    if(senderDateEdit == m_dateFilterUi->cob_dateFrom) {
        lambda_alterVisibility(m_dateFilterUi->de_dateFrom);
    } else if(senderDateEdit == m_dateFilterUi->cob_dateTo) {
        lambda_alterVisibility(m_dateFilterUi->de_dateTo);
    } else if(senderDateEdit == m_dateMultiFilterUi->cob_dateFrom) {
        lambda_alterVisibility(m_dateMultiFilterUi->de_dateFrom);
    } else if(senderDateEdit == m_dateMultiFilterUi->cob_dateTo) {
        lambda_alterVisibility(m_dateMultiFilterUi->de_dateTo);
    }
}
void RangeFilterWidget::onSingleNumberRangeChanged() {
    m_numberMultiFilterUi->listWidget->clear();
    m_numberMultiFilterUi->le_from->setText(m_numberFilterUi->le_from->text());
    m_numberMultiFilterUi->le_to->setText(m_numberFilterUi->le_to->text());
    onSendButtonClicked();
}
void RangeFilterWidget::onSingleDateRangeChanged() {
    m_dateMultiFilterUi->listWidget->clear();
    m_dateMultiFilterUi->cob_dateFrom->setCurrentIndex(m_dateFilterUi->cob_dateFrom->currentIndex());
    if(m_dateFilterUi->cob_dateFrom->currentIndex() == 2) {
        m_dateMultiFilterUi->de_dateFrom->setDate(m_dateFilterUi->de_dateFrom->date());
    }    m_dateMultiFilterUi->cob_dateTo->setCurrentIndex(m_dateFilterUi->cob_dateTo->currentIndex());
    if(m_dateFilterUi->cob_dateTo->currentIndex() == 2) {
        m_dateMultiFilterUi->de_dateTo->setDate(m_dateFilterUi->de_dateTo->date());
    }
    onSendButtonClicked();
}
