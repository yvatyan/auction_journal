#include "Headers/Ui/StartupWindow.h"
#include "Headers/Configuration/Version.h"
#include <QStyleOptionProgressBar>
#include <QPainter>

StartupWindow::StartupWindow(const QPixmap& pixmap, Qt::WindowFlags f)
    : QSplashScreen(pixmap, f)
{
}
StartupWindow::StartupWindow(QWidget* parent, const QPixmap& pixmap, Qt::WindowFlags f)
    : QSplashScreen(parent, pixmap, f)
{
}
void StartupWindow::SubscribeWorker(IWorker* worker) {
    QObject::connect(worker, SIGNAL(sigProgressChanged(const ProgressInfo&)),
                     this, SLOT(OnWorkerUpdated(const ProgressInfo&)));
    m_workers.append(worker);
}
void StartupWindow::SetBackgroundPixmap(const QString& pixmapPath) {
   setPixmap(QPixmap(pixmapPath));
}
void StartupWindow::SetForegroundPixmap(const QString& pixmapPath, QRect fgPosition) {
   m_fgPixmap = QPixmap(pixmapPath);
   m_fgPosition = fgPosition;
}
void StartupWindow::OnWorkerUpdated(const ProgressInfo& progressInfo) {
   m_progress = progressInfo;
   repaint();
}
void StartupWindow::drawContents(QPainter* painter) {
    QSplashScreen::drawContents(painter);
    painter->drawPixmap(m_fgPosition, m_fgPixmap);

    QPen dummyPen;
    dummyPen.setStyle(Qt::NoPen);
    painter->setPen(dummyPen);

    QRect progressFull = QRect(0, 237, 360, 3);
    if(m_progress.StepStatus() == ProgressInfo::Status::Failed) {
        painter->setBrush(QBrush(QColor(0x5f, 0x00, 0x0f)));
    } else {
        painter->setBrush(QBrush(QColor(0x00, 0x4f, 0xaf)));
    }
    painter->drawRect(progressFull);

    int w = static_cast<int>(m_progress.StepIndex() * 360 / m_progress.StepQuantity());
    QRect progressComplete = QRect(0, 237, w, 3);
    if(m_progress.StepStatus() == ProgressInfo::Status::Failed) {
        painter->setBrush(QBrush(QColor(0xff, 0x2f, 0x3f)));
    } else {
        painter->setBrush(QBrush(QColor(0x3f, 0x9f, 0xff)));
    }
    painter->drawRect(progressComplete);

    if(m_progress.StepStatus() == ProgressInfo::Status::Failed) {
        painter->setPen(QColor(0xff, 0x09, 0x09));
    } else {
        painter->setPen(QColor(0x0f, 0x0f, 0xff));
    }
    painter->setFont(QFont("Calibri Light", 10));
    QString msg = (m_progress.StepMessage().isEmpty() ? "" : " - " + m_progress.StepMessage());
    QString progress = QString::number(m_progress.StepIndex()) + "\\" + QString::number(m_progress.StepQuantity());
    QString text = " " + m_progress.JobName() + ": " + m_progress.StepName() + msg + " ...  (" + progress + ")";
    painter->drawText(0, 232, text);

    painter->setFont(QFont("Calibri Light", 7));
    painter->setPen(QColor(0x0f, 0x0f, 0xff));
    QString version(Version_Short);
    QFontMetrics fm(painter->font());
    painter->drawText(360 - fm.width(version) - 2, fm.height() - 3, version);
}
