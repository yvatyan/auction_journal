#include "Headers/Ui/LotWidget.h"
#include <QHeaderView>
#include <QMenu>
#include <QAction>

LotWidget::LotWidget(QWidget* parent)
    : QWidget(parent)
    , m_model(new LotModel(this))
    , m_identityProxy(new LotIdentityProxyModel(this))
    , m_proxyModel(new LotSortFilterProxyModel(this))
    , m_view(new QTableView(this))
    , m_sortReseted(true)
{
    setContextMenuPolicy(Qt::CustomContextMenu);
    m_identityProxy->SetSourceRootModel(m_model);
    m_proxyModel->SetSourceRootModel(m_model);
    m_identityProxy->setSourceModel(m_model);
    m_proxyModel->setSourceModel(m_identityProxy);
    m_view->setModel(m_proxyModel);
    m_view->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    m_view->setStyleSheet("QTableView::Item { margin: 25px;} ");
    m_view->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    m_view->setSelectionBehavior(QAbstractItemView::SelectRows);
    QPalette p = m_view->palette();
    p.setColor(QPalette::Highlight, QColor(0x40, 0x80, 0xff));
    m_view->setPalette(p);
    QObject::connect(m_view->selectionModel(), SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
                     this, SLOT(onSelectionChanged(const QItemSelection&, const QItemSelection&)));
    QObject::connect(m_view->horizontalHeader(), SIGNAL(sectionClicked(int)),
                     this, SLOT(onHorizontalHeaderClicked(int)));
    QObject::connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
                     this, SLOT(onContextMenuRequested(const QPoint&)));
}
LotWidget::~LotWidget() {
    delete m_view;
    delete m_proxyModel;
    delete m_identityProxy;
    delete m_model;
}
void LotWidget::SetDatabase(Database* database) {
    m_model->SetDatabase(database);
}
void LotWidget::AddNewRecord(const Lot& lot) {
    m_model->InsertLot(lot);
    correctColumnsWidth();
}
void LotWidget::DeleteSelectedRecords() {
    QItemSelectionModel* selection = m_view->selectionModel();
    QModelIndexList selectedIndices = selection->selectedRows();
    QMap<int, int> orderedRows;
    for(auto modelIndex : selectedIndices) {
        orderedRows.insert(modelIndex.row(), modelIndex.row());
    }
    QMapIterator<int, int> it(orderedRows);
    it.toBack();
    while(it.hasPrevious()) {
        it.previous();
        deleteRecord(it.value());
    }
    correctColumnsWidth();
}
bool LotWidget::HasSelection() const {
    QItemSelectionModel* selection = m_view->selectionModel();
    return selection->hasSelection();
}
QList<size_t> LotWidget::GetSelectedLotIds() const {
    QItemSelectionModel* selection = m_view->selectionModel();
    QModelIndexList selectedIndices = selection->selectedRows();
    QList<size_t> selectedLotIds;
    for(auto modelIndex : selectedIndices) {
        QModelIndex sourceIndex = m_proxyModel->mapToSource(modelIndex);
        selectedLotIds.append(m_model->GetLotByModelIndex(sourceIndex).LotId());
    }
    return selectedLotIds;
}
QList<size_t> LotWidget::GetSelectedRecordIds() const {
    QItemSelectionModel* selection = m_view->selectionModel();
    QModelIndexList selectedIndices = selection->selectedRows();
    QList<size_t> selectedRecordIds;
    for(auto modelIndex : selectedIndices) {
        QModelIndex sourceIndex = m_proxyModel->mapToSource(modelIndex);
        selectedRecordIds.append(m_model->GetLotByModelIndex(sourceIndex).GetRecordId());
    }
    return selectedRecordIds;
}
QList<Lot> LotWidget::GetSelectedLots() const {
    QItemSelectionModel* selection = m_view->selectionModel();
    QModelIndexList selectedIndices = selection->selectedRows();
    QList<Lot> selectedLots;
    for(auto modelIndex : selectedIndices) {
        QModelIndex sourceIndex = m_proxyModel->mapToSource(modelIndex);
        selectedLots.append(m_model->GetLotByModelIndex(sourceIndex));
    }
    return selectedLots;
}
Lot LotWidget::GetLotByRow(int row) const {
    QModelIndex proxyIndex = m_proxyModel->index(row, 0, QModelIndex());
    QModelIndex sourceIndex = m_proxyModel->mapToSource(proxyIndex);
    return m_model->GetLotByModelIndex(sourceIndex);
}
void LotWidget::UpdateRecordByRecordId(const Lot& lot) const {
    return m_model->UpdateRecord(lot.GetRecordId(), lot);
}
void LotWidget::ReplaceRecordByRecordId(size_t recordId, const Lot& lot) const {
    m_model->UpdateRecord(recordId, lot);
}
void LotWidget::ClearData() {
    m_model->DropData();
}
void LotWidget::AddFilter(const LotFilterDialog::FilterCollection& filter) {
    m_proxyModel->AddFilter(filter);
    correctColumnsWidth();
}
void LotWidget::SetFilter(const LotFilterDialog::FilterCollection& filter) {
    m_proxyModel->SetFilter(filter);
    correctColumnsWidth();
}
void LotWidget::DefaultSort() {
    m_view->horizontalHeader()->setSortIndicatorShown(false);
    m_proxyModel->sort(-1, Qt::DescendingOrder);
    m_sortReseted = true;
}
void LotWidget::resizeEvent(QResizeEvent* event) {
    m_view->resize(event->size());
    QWidget::resizeEvent(event);
    correctColumnsWidth();
}
void LotWidget::deleteRecord(int row) {
    QModelIndex proxyIndex = m_proxyModel->index(row, 0, QModelIndex());
    QModelIndex sourceIndex = m_proxyModel->mapToSource(proxyIndex);
    m_model->RemoveRow(sourceIndex.row());
}
void LotWidget::correctColumnsWidth() {
    m_view->resizeColumnsToContents();
    int colWidth = 0;
    for(int i = 0; i < m_view->model()->columnCount(); ++i) {
        colWidth += m_view->columnWidth(i);
    }
    if(colWidth < width()) {
        // Caution: May not work when table contains records because row number column.
        int delta = width() - colWidth;
        int deltaColumn = delta / m_view->model()->columnCount();
        int deltaLoss = delta - (deltaColumn * m_view->model()->columnCount()) - 2;
        m_view->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
        // Note: In order to hide progress bar in case of rounding error in deltaLoss.
        m_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        for(int i = 0; i < m_view->model()->columnCount(); ++i) {
            m_view->setColumnWidth(i, m_view->columnWidth(i) + deltaColumn + (deltaLoss > 0));
            deltaLoss --;
        }
    } else {
        m_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    }
}
void LotWidget::onSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected) {
    QModelIndexList selectedModelIndices(selected.indexes());
    for(auto modelIndex : selectedModelIndices) {
        m_selectedRows.insert(modelIndex.row());
    }
    QModelIndexList deselectedModelIndices(deselected.indexes());
    for(auto modelIndex : deselectedModelIndices) {
        m_selectedRows.remove(modelIndex.row());
    }
    if(m_selectedRows.size() == 0) {
        emit rowsDeselected();
    } else if(m_selectedRows.size() == 1) {
        emit oneRowSelected(m_selectedRows.toList().at(0));
    } else {
        emit multiRowsSelected(m_selectedRows.toList());
    }
}
void LotWidget::onHorizontalHeaderClicked(int columnIndex) {
    static int sortMode = 1;
    static int prevColumn = columnIndex;

    if(prevColumn != columnIndex || m_sortReseted) {
        sortMode = 1;
        prevColumn = columnIndex;
    }

    Qt::SortOrder sortOrder = Qt::DescendingOrder;
    if(sortMode == 0) {
        sortOrder = Qt::DescendingOrder;
        columnIndex = -1;
    } else if(sortMode == 1) {
        sortOrder = Qt::AscendingOrder;
    } else if(sortMode == 2) {
        sortOrder = Qt::DescendingOrder;
    }
    (++sortMode) %= 3;
    static_cast<QHeaderView*>(sender())->setSortIndicator(columnIndex, sortOrder);
    static_cast<QHeaderView*>(sender())->setSortIndicatorShown(columnIndex != -1);
    m_proxyModel->sort(columnIndex, sortOrder);
    m_sortReseted = false;
}
void LotWidget::onContextMenuRequested(const QPoint& position) {
    QMenu contextMenu(tr("Context menu"), this);

    QAction actionMissedAscending("Missed Ascending Order", this);
    connect(&actionMissedAscending, SIGNAL(triggered()), this, SLOT(onMissedAscending()));
    contextMenu.addAction(&actionMissedAscending);

    QAction actionMissedDescending("Missed Discending Order", this);
    connect(&actionMissedDescending, SIGNAL(triggered()), this, SLOT(onMissedDescending()));
    contextMenu.addAction(&actionMissedDescending);

    QAction actionDefaultSorting("Default Sorting Order", this);
    connect(&actionDefaultSorting, SIGNAL(triggered()), this, SLOT(onResetMissedSorting()));
    contextMenu.addAction(&actionDefaultSorting);

    contextMenu.exec(mapToGlobal(position));
}
void LotWidget::onMissedDescending() {
    m_view->horizontalHeader()->setSortIndicatorShown(false);
    m_sortReseted = true;
    m_proxyModel->MissedDescendingOrder(true);
}
void LotWidget::onMissedAscending() {
    m_view->horizontalHeader()->setSortIndicatorShown(false);
    m_sortReseted = true;
    m_proxyModel->MissedDescendingOrder(false);
}
void LotWidget::onResetMissedSorting() {
    DefaultSort();
}
