#include "Headers/Ui/CommentItemModel.h"

CommentItemModel::CommentItemModel(const QList<Comment>& list)
    : m_data(list)
{
}
int CommentItemModel::rowCount(const QModelIndex& parent) const {
    Q_UNUSED(parent);
    return m_data.size();
}
QVariant CommentItemModel::data(const QModelIndex& index, int role) const {
    switch(role) {
        case Qt::DisplayRole : {
            return QVariant::fromValue(m_data.at(index.row()));
        }
    }
    return QVariant();
}
bool CommentItemModel::setData(const QModelIndex& index, const QVariant& value, int role) {
    if(index.row() >= m_data.size()) {
        return false;
    }
    switch(role) {
        case Qt::DisplayRole : {
            m_data[index.row()] = qvariant_cast<Comment>(value);
            emit dataChanged(createIndex(index.row(), 0), createIndex(index.row(), 0), {Qt::DisplayRole});
            return true;
        }
    }
    return false;
}
Qt::ItemFlags CommentItemModel::flags(const QModelIndex& index) const {
    return QAbstractListModel::flags(index);// | Qt::ItemIsEditable;
}
const QModelIndex CommentItemModel::GetModelIndexAt(int index) const {
    return createIndex(index, 0);
}
void CommentItemModel::AddNewComment(const Comment& comment) {
    beginInsertRows(QModelIndex(), m_data.size(), m_data.size());
    m_data.append(comment);
    endInsertRows();
}
void CommentItemModel::ChangeComment(const QModelIndex& modelIndex, const Comment& comment) {
    m_data[modelIndex.row()] = comment;
    emit dataChanged(modelIndex, modelIndex);
}
void CommentItemModel::RemoveComment(const QModelIndex& modelIndex) {
    beginRemoveRows(QModelIndex(), modelIndex.row(), modelIndex.row());
    m_data.removeAt(modelIndex.row());
    endRemoveRows();
}
