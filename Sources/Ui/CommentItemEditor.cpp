#include "Headers/Ui/CommentItemEditor.h"
#include "Headers/NotificationManager.h"
#include "ui_CommentItemEditorWidget.h"

CommentItemEditor::CommentItemEditor(QWidget* parent)
    : QWidget(parent)
    , m_ui(new Ui::commentEditor)
    , m_mode(Mode::Add)
{
    m_ui->setupUi(this);
    m_placeholderText = m_ui->te_editor->placeholderText();
    m_ui->b_add->hide();
    m_ui->b_edit->hide();
    m_ui->b_delete->hide();
    QObject::connect(m_ui->b_add, SIGNAL(clicked()), this, SLOT(onAddButtonClicked()));
    QObject::connect(m_ui->b_edit, SIGNAL(clicked()), this, SLOT(onEditButtonClicked()));
    QObject::connect(m_ui->b_delete, SIGNAL(clicked()), this, SLOT(onDeleteButtonClicked()));
    QObject::connect(m_ui->te_editor, SIGNAL(textChanged()), this, SLOT(onTextChanged()));
}
CommentItemEditor::~CommentItemEditor() {
    delete m_ui;
}
void CommentItemEditor::EditComment() {
    if(m_selectedModelIndices.size() == 1) {
        m_ui->te_editor->setText(qvariant_cast<Comment>(m_selectedModelIndices.at(0).data()).Text());
        switchToEditMode();
    } else {
        throw std::runtime_error("More than one comments selected to edit.");
    }
}
void CommentItemEditor::CancelEditing() {
    switchToAddMode();
}
void CommentItemEditor::SetDeleteMode(bool enable) {
    if(enable) {
        switchToDeleteMode();
    } else {
        switchToAddMode();
    }
}
void CommentItemEditor::SetComments(const QModelIndexList& modelIndices) {
    m_selectedModelIndices.clear();
    for(auto modelIndex : modelIndices) {
        m_selectedModelIndices.push_back(modelIndex);
    }
}
void CommentItemEditor::switchToAddMode() {
    m_mode = Mode::Add;
    m_ui->te_editor->setText("");
    m_ui->b_add->hide();
    m_ui->b_edit->hide();
    m_ui->b_delete->hide();
    m_ui->te_editor->setEnabled(true);
    m_ui->te_editor->setPlaceholderText(m_placeholderText);
}
void CommentItemEditor::switchToEditMode() {
    m_mode = Mode::Edit;
    m_ui->b_add->show();
    if(m_ui->te_editor->document()->isEmpty()) {
        m_ui->b_add->setEnabled(false);
    } else {
        m_ui->b_add->setEnabled(true);
    }

    m_ui->b_edit->show();
    if(m_ui->te_editor->document()->isEmpty()) {
        m_ui->b_edit->setEnabled(false);
    } else {
        m_ui->b_edit->setEnabled(true);
    }

    m_ui->b_delete->show();
    m_ui->te_editor->setEnabled(true);
    m_ui->te_editor->setPlaceholderText(m_placeholderText);
}
void CommentItemEditor::switchToDeleteMode() {
    m_mode = Mode::Delete;
    m_ui->b_add->hide();
    m_ui->b_edit->hide();
    m_ui->b_delete->show();
    m_ui->te_editor->setEnabled(false);
    m_ui->te_editor->setPlaceholderText(c_placeholderTextInDeleteMode);
}
void CommentItemEditor::onAddButtonClicked() {
    if(!m_ui->te_editor->document()->isEmpty()) {
        emit sigNewTextSubmited(m_ui->te_editor->document()->toPlainText());
        emit sigNewStyledTextSubmited(m_ui->te_editor->document()->toHtml());
        emit sigReset();
    }
    switchToAddMode();
}
void CommentItemEditor::onEditButtonClicked() {
    if(!m_ui->te_editor->document()->isEmpty()) {
        emit sigEditedTextSubmited(m_selectedModelIndices.at(0), m_ui->te_editor->document()->toPlainText());
        emit sigEditedStyledTextSubmited(m_selectedModelIndices.at(0), m_ui->te_editor->document()->toHtml());
        emit sigReset();
    }
    switchToAddMode();
}
void CommentItemEditor::onDeleteButtonClicked() {
    static QString question = "Do you want to delete selected comment(s)?";
    if(NotificationManager::Instance().ShowNotification(question, NotificationManager::Type::Question)) {
        QModelIndexList selectedModelIndices;
        for(auto modelIndex : m_selectedModelIndices) {
            selectedModelIndices.push_back(modelIndex);
        }
        emit sigDeleteAccepted(selectedModelIndices);
        emit sigReset();
    }
    switchToAddMode();
}
void CommentItemEditor::onTextChanged() {
    if(m_mode == Mode::Add) {
        if(m_ui->te_editor->document()->isEmpty()) {
            m_ui->b_add->hide();
        } else {
            m_ui->b_add->setEnabled(true);
            m_ui->b_add->show();
        }
    } else if(m_mode == Mode::Edit) {
        if(m_ui->te_editor->document()->isEmpty()) {
            m_ui->b_add->setEnabled(false);
            m_ui->b_edit->setEnabled(false);
        } else {
            m_ui->b_add->setEnabled(true);
            m_ui->b_edit->setEnabled(true);
        }
    }
}
