#include "Headers/Ui/EndAuctionDialog.h"
#include "Headers/EndedLotBuilder.h"
#include "ui_EndAuctionDialog.h"
#include "Headers/NotificationManager.h"
#include "Headers/Database/DatabaseManager.h"
#include "Headers/Tools.h"

QColor EndAuctionDialog::m_labelColor(Qt::black);
QColor EndAuctionDialog::m_notFilledLabelColor(Qt::red);

EndAuctionDialog::EndAuctionDialog(Database* database, const Lot& lot, QWidget *parent)
    : QDialog(parent)
    , m_ui(new Ui::endAuctionDialog)
    , m_database(database)
    , m_originalLot(lot)
    , m_commentDoc(nullptr)
{
    m_ui->setupUi(this);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setFixedSize(geometry().size());
    SetupFields();

    QObject::connect(m_ui->endAuctionB, SIGNAL(clicked()), this, SLOT(onAcceptButtonClicked()));
    QObject::connect(m_ui->cancelB, SIGNAL(clicked()), this, SLOT(reject()));
    QObject::connect(m_ui->saleMissedChB, SIGNAL(stateChanged(int)), this, SLOT(saleMissedStateChanged(int)));
    QObject::connect(m_ui->saleEndedChB, SIGNAL(stateChanged(int)), this, SLOT(setupSoldStatus()));
    QObject::connect(m_ui->soldStatusCoB, SIGNAL(currentIndexChanged(int)), this, SLOT(onSoldStatusChanged(int)));
    saleMissedStateChanged(m_ui->saleMissedChB->checkState());
    m_ui->soldPriceLE->setValidator(new QIntValidator(m_ui->soldPriceLE));
}
EndAuctionDialog::~EndAuctionDialog() {
    if(m_commentDoc != nullptr) {
        delete m_commentDoc;
    }
    delete m_ui;
}
void EndAuctionDialog::SetupFields() {
    setupSoldStatus();
}
void EndAuctionDialog::onAcceptButtonClicked() {
    m_resultLot = constructLot();
    m_commentDoc = m_ui->commentTE->document()->clone();
    if(!m_resultLot.IsValid()) {
        return;
    } else {
        accept();
    }
}
void EndAuctionDialog::saleMissedStateChanged(int state) {
    if(m_ui->l_soldPrice->isEnabled()) {
        if(state == Qt::Checked) {
            m_ui->l_soldPrice->setText("Sold Price:");
        } else if(state == Qt::Unchecked) {
            m_ui->l_soldPrice->setText("Sold Price*:");
        }
    }
}
Lot EndAuctionDialog::constructLot() {
    EndedLotBuilder builder;
    builder.SetSoldPriceRequired(!m_ui->saleMissedChB->isChecked() && m_ui->soldPriceLE->isEnabled());
    builder.SetLot(m_originalLot);
    builder.SetSoldPrice(static_cast<unsigned int>(m_ui->soldPriceLE->text().toUInt()));
    builder.SetSoldStatusId(m_ui->soldStatusCoB->currentData().toUInt());
    builder.SetMissedFlag(m_ui->saleMissedChB->isChecked());
    try {
        return builder.ConstructLot();
    } catch (const std::runtime_error& err) {
        if(!builder.AreAllRequiredFieldsFilled()) {
            highlightUnfilled(builder.UnfilledRequiredFields());
        } else {
            resetLabelsColor();
        }
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return Lot();
    }
}
void EndAuctionDialog::resetLabelsColor() const {
    QPalette palette;
    palette.setColor(QPalette::Foreground, m_labelColor);
    m_ui->l_saleEnded->setPalette(palette);
    m_ui->l_saleMissed->setPalette(palette);
    m_ui->l_soldStatus->setPalette(palette);
    m_ui->l_soldPrice->setPalette(palette);
    m_ui->l_comment->setPalette(palette);
}
void EndAuctionDialog::highlightUnfilled(const QList<LotRecordsTable::Fields>& fields) const {
    resetLabelsColor();
    QPalette palette;
    palette.setColor(QPalette::Foreground, m_notFilledLabelColor);
    for(auto f : fields) {
        if(f == LotRecordsTable::Fields::SoldStatusId) {
            m_ui->l_soldStatus->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::SoldPrice) {
            m_ui->l_soldPrice->setPalette(palette);
        }
    }
}
void EndAuctionDialog::setupSoldStatus() {
    for(int i = m_ui->soldStatusCoB->count() - 1; i > 0; --i) {
        m_ui->soldStatusCoB->removeItem(i);
    }
    SoldStatusTable* table = static_cast<SoldStatusTable*>(m_database->Table(SOLDSTATUS_TABLE_NAME));
    if(m_ui->saleEndedChB->isChecked()) {
        QList<QMap<SoldStatusTable::Fields, QVariant>> soldStatusData = table->Api().GetFinishedSoldStatusData();
        for(auto record : soldStatusData) {
            bool c1 = m_originalLot.AuctionDate() > QDateTime::currentDateTime().date() && record[SoldStatusTable::Fields::Id].toUInt() == SoldStatusTable::Canceled;
            bool c2 = m_originalLot.AuctionDate() <= QDateTime::currentDateTime().date();
            bool condition = c1 || c2;
            if(condition) {
                m_ui->soldStatusCoB->addItem(Tools::ChangeStringCase(record[SoldStatusTable::Fields::Name].toString(),
                                                                     Tools::StringCase::FirstUpper),
                                             record[SoldStatusTable::Fields::Id]);
            }
        }
    } else {
        QList<QMap<SoldStatusTable::Fields, QVariant>> soldStatusData = table->Api().GetUnfinishedSoldStatusData();
        for(auto record : soldStatusData) {
            bool c1 = m_originalLot.AuctionDate() > QDateTime::currentDateTime().date() && record[SoldStatusTable::Fields::Id].toUInt() == SoldStatusTable::Moved;
            bool c2 = m_originalLot.AuctionDate() <= QDateTime::currentDateTime().date();
            bool c3 = record[SoldStatusTable::Fields::Id].toUInt() == SoldStatusTable::WA;
            bool c4 = c3 && (m_originalLot.SaleStatusId() == SaleStatusTable::OnApproval || m_originalLot.SaleStatusId() == SaleStatusTable::MinimumBid);
            bool condition = (c4 && c2) || ((c1 || c2) && !c3);
            if(condition) {
                m_ui->soldStatusCoB->addItem(Tools::ChangeStringCase(record[SoldStatusTable::Fields::Name].toString(),
                                                                     Tools::StringCase::FirstUpper),
                                             record[SoldStatusTable::Fields::Id]);
            }
        }
    }
}
void EndAuctionDialog::onSoldStatusChanged(int index) {
    size_t statusId = m_ui->soldStatusCoB->itemData(index).toUInt();
    if(statusId == SoldStatusTable::Moved || statusId == SoldStatusTable::Canceled) {
        m_ui->soldPriceLE->setEnabled(false);
        saleMissedStateChanged(Qt::Checked);
        m_ui->l_soldPrice->setEnabled(false);
    } else {
        m_ui->soldPriceLE->setEnabled(true);
        m_ui->l_soldPrice->setEnabled(true);
        saleMissedStateChanged(m_ui->saleMissedChB->checkState());
    }
}
