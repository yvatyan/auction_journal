#include "Headers/Ui/NewVehicleDialog.h"
#include "Headers/NewLotBuilder.h"
#include "Headers/NotificationManager.h"
#include "Headers/Database/DatabaseManager.h"
#include "Headers/LotValidator.h"
#include "Headers/FilePathValidator.h"
#include "Headers/Tools.h"
#include "Headers/Options.h"
#include "ui_NewVehicleDialog.h"
#include <QFileDialog>
#include <QIntValidator>

QColor NewVehicleDialog::m_labelColor(Qt::black);
QColor NewVehicleDialog::m_notFilledLabelColor(Qt::red);

NewVehicleDialog::NewVehicleDialog(Database* database, QWidget* parent)
    : QDialog(parent)
    , m_ui(new Ui::newVehicleDialog)
    , m_database(database)
    , m_commentDoc(nullptr)
{
    m_ui->setupUi(this);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setFixedSize(geometry().size());
    SetupFields();

    QObject::connect(m_ui->addVehicleB, SIGNAL(clicked()), this, SLOT(onAcceptButtonClicked()));
    QObject::connect(m_ui->cancelB, SIGNAL(clicked()), this, SLOT(reject()));
    QObject::connect(m_ui->cob_auctionDate, SIGNAL(currentIndexChanged(int)), this, SLOT(onAuctionDateVariantChanged(int)));
    QObject::connect(m_ui->worksheetFileLE, SIGNAL(textChanged(QString)), this, SLOT(onWorksheetPathChanged(QString)));
    QObject::connect(m_ui->selectWorksheetFileB, SIGNAL(clicked()), this, SLOT(onFileSelectButtonClicked()));

    m_ui->lotLE->setValidator(new LotValidator(m_ui->lotLE));
    m_ui->yearLE->setValidator(new QIntValidator(1900, QDateTime::currentDateTime().date().year() + 1, m_ui->yearLE));
    m_ui->auctionDateDE->hide();
    m_ui->wantItTodayLE->setValidator(new QIntValidator(m_ui->wantItTodayLE));
    m_ui->worksheetFileLE->setValidator(new FilePathValidator(m_ui->worksheetFileLE, m_ui->worksheetFileLE));
}
NewVehicleDialog::~NewVehicleDialog() {
    if(m_commentDoc != nullptr) {
        delete m_commentDoc;
    }
}
void NewVehicleDialog::SetupFields() {
    setupMakeField();
    setupModelField();
    setupTransmissionField();
    setupBodyField();
    setupPurposeField();
    setupLocationField();
    setupAuctionDateField();
    setupSaleStatusField();
}
void NewVehicleDialog::onAcceptButtonClicked() {
    m_resultLot = constructLot();
    m_commentDoc = m_ui->commentTE->document()->clone();
    if(!m_resultLot.IsValid()) {
        return;
    } else {
        accept();
    }
}
void NewVehicleDialog::onMakeChanged(int makeIndex) {
    for(int i = m_ui->modelCoB->count() - 1; i > 0; --i) {
        m_ui->modelCoB->removeItem(i);
    }
    if(makeIndex == 0) {
        return;
    }
    int makeId = m_ui->makeCoB->itemData(makeIndex, Qt::UserRole).toInt();
    if(makeId != -1) {
        ModelTable* models = static_cast<ModelTable*>(m_database->Table(MODEL_TABLE_NAME));
        QList<QMap<ModelTable::Fields, QVariant>> modelData =
                models->Api().GetModelDataByMakeId(static_cast<size_t>(makeId));

        Tools::SortStringsWithNumbers<ModelTable::Fields>(modelData,
                                                          ModelTable::Fields::Name,
                                                          Tools::SortOrder::Ascending);

        for(auto sortedRecord : modelData) {
            m_ui->modelCoB->addItem(Tools::ChangeStringCase(sortedRecord[ModelTable::Fields::Name].toString(),
                                                            Tools::StringCase::AllFirstUpper),
                                    sortedRecord[ModelTable::Fields::Id].toInt());
        }
    }
    m_ui->modelCoB->addItem("<enter new model>", -1);
    QObject::connect(m_ui->modelCoB, SIGNAL(currentIndexChanged(int)), this, SLOT(prepareComboBoxForInput(int)));
}
void NewVehicleDialog::onAuctionDateVariantChanged(int index) {
    if(index == 0 || index == 1) {
        m_ui->auctionDateDE->hide();
    } else if(index == 2) {
        m_ui->auctionDateDE->show();
    }
}
void NewVehicleDialog::onFileSelectButtonClicked() {
    QString fileName = QFileDialog::getOpenFileName(
                           this,
                           "Select Worksheet file",
                           QDir::homePath() + "\\Desktop",
                           "Worksheet Files (*.pdf);;All Files (*.*)"
                           // 0, QFileDialog::DontUseNativeDialog); // may work if file dialog hangs
                       );
    m_ui->worksheetFileLE->setText(fileName);
}
void NewVehicleDialog::onWorksheetPathChanged(const QString& newPath) {
    m_ui->worksheetFileLE->setToolTip(newPath);
}
void NewVehicleDialog::prepareComboBoxForInput(int index) {
    QComboBox* cobox = static_cast<QComboBox*>(sender());
    int id = cobox->itemData(index).toInt();
    if(id == -1) {
        cobox->setEditable(true);
        cobox->lineEdit()->setText("");
        cobox->lineEdit()->setPlaceholderText("Enter new record");
    } else {
        cobox->setEditable(false);
    }
}
void NewVehicleDialog::setupMakeField() {
    MakeTable* makes = static_cast<MakeTable*>(m_database->Table(MAKE_TABLE_NAME));
    QList<QMap<MakeTable::Fields, QVariant>> makeData = makes->Api().GetMakeData();

    Tools::SortStringsWithNumbers<MakeTable::Fields>(makeData,
                                                     MakeTable::Fields::Name,
                                                     Tools::SortOrder::Ascending);

    for(auto record : makeData) {
        m_ui->makeCoB->addItem(Tools::ChangeStringCase(record[MakeTable::Fields::Name].toString(),
                                                       Tools::StringCase::AllFirstUpper),
                               record[MakeTable::Fields::Id].toInt());
    }
    m_ui->makeCoB->addItem("<enter new make>", -1);
    QObject::connect(m_ui->makeCoB, SIGNAL(currentIndexChanged(int)), this, SLOT(prepareComboBoxForInput(int)));
}
void NewVehicleDialog::setupModelField() {
    QObject::connect(m_ui->makeCoB, SIGNAL(currentIndexChanged(int)), this, SLOT(onMakeChanged(int)));
}
void NewVehicleDialog::setupTransmissionField() {
    TransmissionTable* transmissions = static_cast<TransmissionTable*>(m_database->Table(TRANSMISSION_TABLE_NAME));
    QList<QMap<TransmissionTable::Fields, QVariant>> transmissionData = transmissions->Api().GetTransmissionData();
    for(auto record : transmissionData) {
        m_ui->transmissionCoB->addItem(Tools::ChangeStringCase(record[TransmissionTable::Fields::Name].toString(),
                                                               Tools::StringCase::FirstUpper),
                                       record[TransmissionTable::Fields::Id].toUInt());
    }
}
void NewVehicleDialog::setupBodyField() {
    BodyTable* bodies = static_cast<BodyTable*>(m_database->Table(BODY_TABLE_NAME));
    QList<QMap<BodyTable::Fields, QVariant>> bodyData = bodies->Api().GetBodyData();
    for(auto record : bodyData) {
        m_ui->bodyCoB->addItem(record[BodyTable::Fields::Name].toString(),
                               record[BodyTable::Fields::Id].toUInt());
    }
}
void NewVehicleDialog::setupPurposeField() {
    PurposeTable* purposes = static_cast<PurposeTable*>(m_database->Table(PURPOSE_TABLE_NAME));
    QList<QMap<PurposeTable::Fields, QVariant>> purposesData = purposes->Api().GetPurposeData();
    for(auto record : purposesData) {
        m_ui->purposeCoB->addItem(Tools::ChangeStringCase(record[PurposeTable::Fields::Name].toString(),
                                                          Tools::StringCase::FirstUpper),
                                  record[PurposeTable::Fields::Id].toUInt());
    }
}
void NewVehicleDialog::setupLocationField() {
    LocationTable* locations = static_cast<LocationTable*>(m_database->Table(LOCATION_TABLE_NAME));
    QList<QMap<LocationTable::Fields, QVariant>> locationData = locations->Api().GetLocationData();
    for(auto record : locationData) {
        m_ui->locationCoB->addItem(Tools::ChangeStringCase(record[LocationTable::Fields::Abbreviation].toString(), Tools::StringCase::AllUpper) + " (" +
                                   Tools::ChangeStringCase(record[LocationTable::Fields::Name].toString(), Tools::StringCase::AllFirstUpper) + ")",
                                   record[LocationTable::Fields::Id].toUInt());
    }
}
void NewVehicleDialog::setupAuctionDateField() {
    QDate todayDate = QDateTime::currentDateTime().date();
    m_ui->auctionDateDE->setDate(QDate(todayDate.year(), todayDate.month(), todayDate.day()));
    m_ui->auctionDateDE->setDisplayFormat(Options::Instance().DateFormat());
}
void NewVehicleDialog::setupSaleStatusField() {
    SaleStatusTable* saleStatuses = static_cast<SaleStatusTable*>(m_database->Table(SALESTATUS_TABLE_NAME));
    QList<QMap<SaleStatusTable::Fields, QVariant>> saleStatusData = saleStatuses->Api().GetSaleStatusData();
    for(auto record : saleStatusData) {
        m_ui->saleStatusCoB->addItem(Tools::ChangeStringCase(record[SaleStatusTable::Fields::Name].toString(), Tools::StringCase::FirstUpper),
                                     record[SaleStatusTable::Fields::Id].toUInt());
    }
}
Lot NewVehicleDialog::constructLot() const {
    LotRecordsTable* lotRecordsTable = static_cast<LotRecordsTable*>(m_database->Table(LOTRECORDS_TABLE_NAME));
    size_t lotId = m_ui->lotLE->text().toUInt();
    if(lotRecordsTable->Api().LotExistInDb(lotId)) {
        QString message("Record with Lot#: " + QString::number(lotId) + " already exist.");
        NotificationManager::Instance().ShowNotification(message, NotificationManager::Type::Error);
        return Lot();
    }
    NewLotBuilder builder;
    builder.SetLotId(lotId);
    int makeId = m_ui->makeCoB->currentData().toInt();
    if(makeId == -1) {
        if(m_ui->makeCoB->lineEdit() == nullptr) {
            throw std::runtime_error("Line edit of make combobox is NULL.");
        }
        builder.SetMakeName(Tools::ChangeStringCase(m_ui->makeCoB->lineEdit()->text(), Tools::StringCase::AllLower));
    } else {
        builder.SetMakeId(makeId);
    }

    int modelId = m_ui->modelCoB->currentData().toInt();
    if(modelId == -1) {
        if(m_ui->modelCoB->lineEdit() == nullptr) {
            throw std::runtime_error("Line edit for model combobox is NULL.");
        }
        builder.SetModelName(Tools::ChangeStringCase(m_ui->modelCoB->lineEdit()->text(), Tools::StringCase::AllLower));
    } else {
        builder.SetModelId(modelId);
    }

    builder.SetYear(m_ui->yearLE->text().toUInt());
    builder.SetTransmissionId(m_ui->transmissionCoB->currentData().toUInt());
    builder.SetBodyId(m_ui->bodyCoB->currentData().toUInt());
    builder.SetPurposeId(m_ui->purposeCoB->currentData().toUInt());
    builder.SetLocationId(m_ui->locationCoB->currentData().toUInt());
    if(m_ui->cob_auctionDate->currentIndex() == 1) {
        builder.SetAuctionDate(QDate(9999, 12, 31));
    } else if(m_ui->cob_auctionDate->currentIndex() == 2) {
        builder.SetAuctionDate(m_ui->auctionDateDE->date());
    }
    builder.SetSaleStatusId(m_ui->saleStatusCoB->currentData().toUInt());
    builder.SetWantItTodayPrice(m_ui->wantItTodayLE->text().toUInt());
    QString path = m_ui->worksheetFileLE->text();
    int unused(0);
    if(m_ui->worksheetFileLE->validator()->validate(path, unused) == QValidator::Acceptable) {
        builder.SetWorksheetPath(m_ui->worksheetFileLE->text());
    }

    try {
        return builder.ConstructLot();
    } catch (std::runtime_error err) {
        if(!builder.AreAllRequiredFieldsFilled()) {
            highlightUnfilled(builder.UnfilledRequiredFields());
        } else {
            resetLabelsColor();
        }
        NotificationManager::Instance().ShowNotification(err.what(), NotificationManager::Type::Error);
        return Lot();
    }
}
void NewVehicleDialog::resetLabelsColor() const {
    QPalette palette;
    palette.setColor(QPalette::Foreground, m_labelColor);
    m_ui->l_lot->setPalette(palette);
    m_ui->l_make->setPalette(palette);
    m_ui->l_model->setPalette(palette);
    m_ui->l_year->setPalette(palette);
    m_ui->l_transmission->setPalette(palette);
    m_ui->l_body->setPalette(palette);
    m_ui->l_purpose->setPalette(palette);
    m_ui->l_location->setPalette(palette);
    m_ui->l_auctionDate->setPalette(palette);
    m_ui->l_saleStatus->setPalette(palette);
    m_ui->l_wantItToday->setPalette(palette);
    m_ui->l_worksheet->setPalette(palette);
    m_ui->l_comment->setPalette(palette);
}
void NewVehicleDialog::highlightUnfilled(const QList<LotRecordsTable::Fields>& fields) const {
    resetLabelsColor();
    QPalette palette;
    palette.setColor(QPalette::Foreground, m_notFilledLabelColor);
    for(auto f : fields) {
        if(f == LotRecordsTable::Fields::LotId) {
            m_ui->l_lot->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::MakeId) {
            m_ui->l_make->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::ModelId) {
            m_ui->l_model->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::Year) {
            m_ui->l_year->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::TransmissionId) {
            m_ui->l_transmission->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::BodyId) {
            m_ui->l_body->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::PurposeId) {
            m_ui->l_purpose->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::LocationId) {
            m_ui->l_location->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::AuctionDate) {
            m_ui->l_auctionDate->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::SaleStatusId) {
            m_ui->l_saleStatus->setPalette(palette);
        } else if(f == LotRecordsTable::Fields::WorksheetPath) {
            m_ui->l_worksheet->setPalette(palette);
        }
    }
}
