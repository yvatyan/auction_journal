#include "Headers/Ui/CommentItemDelegate.h"
#include "Headers/Tools.h"

#include <QPushButton>
#include <QPainter>
#include <QKeyEvent>

CommentItemDelegate::CommentItemDelegate(QObject* parent)
    : QStyledItemDelegate(parent)
    , m_model(nullptr)
    , m_editor(nullptr)
    , m_selectionMode(false)
    , m_viewSize(QSize(0, 0))
{
}
CommentItemDelegate::~CommentItemDelegate() {
    deleteItemWidgetCache();
}
void CommentItemDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& modelIndex) const {
    painter->save();
    CommentItem* item(nullptr);
    QString err;
    if(modelIndex.row() < m_itemWidgetCache.size()) {
        item = m_itemWidgetCache.at(modelIndex.row());
    } else {
        throw std::runtime_error("Paint call: Invalid model index passed to delegate.");
    }
    int x = option.rect.topLeft().x() + static_cast<int>(option.rect.width() * c_horizontalSpaceInPercent);
    int y = option.rect.topLeft().y() + static_cast<int>(c_verticalSpaceInPixel);
    painter->translate(QPoint(x, y));

    if((option.state & QStyle::State_Selected)) {
        painter->setPen(QPen(QBrush(QColor(0xa0, 0xa0, 0xa0)), 1));
        painter->setBrush(QBrush(QColor(0xc0, 0xc0, 0xc0)));
    } else {
        painter->setPen(QPen(QBrush(QColor(0xe0, 0xe0, 0xe0)), 1));
        painter->setBrush(QBrush(QColor(0xe0, 0xe0, 0xe0)));
    }
    // NOTE: It may be drawn outside of view.
    painter->drawRoundedRect(QRect(QPoint(), QSize(item->width(), item->height())), 6, 6);
    // NOTE: Render call causes widgets in between of serial selected items not to appear.
    // item->render(painter, QPoint(), QRegion(), QWidget::DrawChildren);
    QPixmap pixmap = item->grab();
    painter->drawPixmap(QRect(QPoint(), QSize(item->width(), item->height())), pixmap);
    painter->restore();
}
QSize CommentItemDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& modelIndex) const {
    Q_UNUSED(option);
    if(modelIndex.row() < m_itemWidgetCache.size()) {
        QSize itemSize = m_itemWidgetCache.at(modelIndex.row())->sizeHint();
        itemSize.setHeight(itemSize.height() + static_cast<int>(2 * c_verticalSpaceInPixel));
        return itemSize;
    } else {
        throw std::runtime_error("Size hint call: Invalid model index passed to delegate.");
    }
}
bool CommentItemDelegate::editorEvent(QEvent* event, QAbstractItemModel* model, const QStyleOptionViewItem& option, const QModelIndex& modelIndex) {
    Q_UNUSED(event);
    Q_UNUSED(model);
    Q_UNUSED(option);
    Q_UNUSED(modelIndex)
    return false;
}
QWidget* CommentItemDelegate::createEditor(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& modelIndex) const {
    Q_UNUSED(editor);
    Q_UNUSED(option);
    Q_UNUSED(modelIndex)
    return nullptr;
}
bool CommentItemDelegate::IsItemClicked(int index, const QRect& itemRect, const QPoint& clickPoint) {
    if(index == -1) {
        return false;
    }
    int x = itemRect.topLeft().x() + static_cast<int>(itemRect.width() * c_horizontalSpaceInPercent);
    int y = itemRect.topLeft().y() + static_cast<int>(c_verticalSpaceInPixel);
    QRect widgetRect(m_itemWidgetCache.at(index)->rect());
    widgetRect.moveTo(x, y);
    return widgetRect.contains(clickPoint);
}
void CommentItemDelegate::UpdateItemsGeometry(const QSize& newSize) {
    m_viewSize = newSize;
    int w = m_viewSize.width() - static_cast<int>(m_viewSize.width() * 2 * c_horizontalSpaceInPercent);
    for(int i = 0; i < m_itemWidgetCache.size(); ++i) {
        m_itemWidgetCache[i]->Resize(w, -1);
        QModelIndex modelIndex = m_model->GetModelIndexAt(i);
        emit const_cast<CommentItemDelegate*>(this)->sizeHintChanged(modelIndex);
    }
}
void CommentItemDelegate::NewItemAdded(const QModelIndex& modelIndex) {
    if(m_model == nullptr) {
        m_model = static_cast<CommentItemModel*>(const_cast<QAbstractItemModel*>(modelIndex.model()));
    } else if(m_model != modelIndex.model()) {
        throw std::runtime_error("Deleagte: Model has been changed.");
    }
    Comment newComment = qvariant_cast<Comment>(modelIndex.data());
    CommentItem* item = new CommentItem(newComment);
    item->setStyleSheet("background-color: rgba(0,0,0,0)");
    int w = m_viewSize.width() - static_cast<int>(m_viewSize.width() * 2 * c_horizontalSpaceInPercent);
    item->Resize(w, -1);
    emit const_cast<CommentItemDelegate*>(this)->sizeHintChanged(modelIndex);

    m_itemWidgetCache.insert(modelIndex.row(), item);
}
void CommentItemDelegate::UpdateItem(const QModelIndex& modelIndex) {
    Comment comment = qvariant_cast<Comment>(modelIndex.data());
    m_itemWidgetCache[modelIndex.row()]->SetComment(comment);
    int w = m_viewSize.width() - static_cast<int>(m_viewSize.width() * 2 * c_horizontalSpaceInPercent);
    m_itemWidgetCache[modelIndex.row()]->Resize(w, -1);
    emit const_cast<CommentItemDelegate*>(this)->sizeHintChanged(modelIndex);
}
void CommentItemDelegate::RemoveItem(const QModelIndex& modelIndex) {
    CommentItem* cItem = m_itemWidgetCache[modelIndex.row()];
    delete cItem;
    m_itemWidgetCache.removeAt(modelIndex.row());
}
void CommentItemDelegate::SetEditorToEditMode(const QModelIndex& index) {
    if(index.isValid()) {
        m_editor->EditComment();
    }
}
void CommentItemDelegate::SetExternalEditor(CommentItemEditor* editor) {
    m_editor = editor;
    QObject::connect(m_editor, SIGNAL(sigReset()), this, SIGNAL(sigClearSelections()));
}
void CommentItemDelegate::ResetModel(CommentItemModel* newModel) {
    m_model = newModel;
    deleteItemWidgetCache();
}
void CommentItemDelegate::deleteItemWidgetCache() {
    for(int i = 0; i < m_itemWidgetCache.size(); ++i) {
        CommentItem* cItem = m_itemWidgetCache[i];
        delete cItem;
    }
    m_itemWidgetCache.clear();
}
void CommentItemDelegate::OnSelectionChanged(QItemSelectionModel* selectionModel) {
    int selectedQty = selectionModel->selectedIndexes().size();
    if(selectedQty == 0) {
        if(m_editor->IsInEditMode()) {
            m_editor->CancelEditing();
        } else if(m_editor->IsInDeleteMode()) {
            m_editor->SetDeleteMode(false);
        }
    } else {
        if(m_editor->IsInEditMode()) {
            m_editor->CancelEditing();
        }
        m_editor->SetDeleteMode(true);
    }
    m_editor->SetComments(selectionModel->selectedIndexes());
}
