#include "Headers/Ui/SelectionFilterWidget.h"
#include "Headers/Ui/TemplateQWidget.h"
#include "Headers/ScrollEventEater.h"
#include "Headers/Ui/CustomListWidgetItem.h"
#include "Headers/Tools.h"
#include "ui_SelectionFilterWidget.h"
#include "ui_SelectionMultiFilterWidget.h"

SelectionFilterWidget::SelectionFilterWidget(bool extended, QWidget* parent)
    : QWidget(parent)
    , m_stackedUi()
    , m_filterIndex(-1)
    , m_multiFilterIndex(-1)
{
    TemplateQWidget<Ui::selectionFilterWidget>* selectionFilter = new TemplateQWidget<Ui::selectionFilterWidget>();
    TemplateQWidget<Ui::selectionMultiFilterWidget>* selectionMultiFilter = new TemplateQWidget<Ui::selectionMultiFilterWidget>();

    m_selectionFilterUi = selectionFilter->m_ui;
    m_selectionMultiFilterUi = selectionMultiFilter->m_ui;

    QObject::connect(m_selectionMultiFilterUi->b_send, SIGNAL(clicked()),
                     this, SLOT(onSendButtonClicked()));
    QObject::connect(m_selectionMultiFilterUi->b_return, SIGNAL(clicked()),
                     this, SLOT(onReturnButtonClicked()));
    QObject::connect(m_selectionMultiFilterUi->w_listWidgetAvailable, SIGNAL(itemDoubleClicked(QListWidgetItem*)),
                     this, SLOT(onAvailbleDoubleClicked(QListWidgetItem*)));
    QObject::connect(m_selectionMultiFilterUi->w_listWidgetSelected, SIGNAL(itemDoubleClicked(QListWidgetItem*)),
                     this, SLOT(onSelectedDoubleClicked(QListWidgetItem*)));
    QObject::connect(m_selectionFilterUi->comboBox, SIGNAL(currentIndexChanged(int)),
                     this, SLOT(onComboboxIndexChange(int)));

    m_filterIndex = m_stackedUi.addWidget(selectionFilter);
    m_multiFilterIndex = m_stackedUi.addWidget(selectionMultiFilter);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(&m_stackedUi);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    setLayout(layout);

    OnEnableExtendedMode(extended);

    QObject* scrollEater = new ScrollEventEater(this);
    m_selectionFilterUi->comboBox->installEventFilter(scrollEater);
}
void SelectionFilterWidget::Reset() {
    m_selectionFilterUi->comboBox->setCurrentIndex(0);

    m_selectionMultiFilterUi->w_listWidgetSelected->selectAll();
    onReturnButtonClicked();
    m_selectionMultiFilterUi->w_listWidgetAvailable->clearSelection();
}
void SelectionFilterWidget::Clear() {
    for(int i = m_selectionFilterUi->comboBox->count() - 1; i > 0; --i) {
        m_selectionFilterUi->comboBox->removeItem(i);
    }

    m_selectionMultiFilterUi->w_listWidgetAvailable->clear();
    m_selectionMultiFilterUi->w_listWidgetSelected->clear();
}
void SelectionFilterWidget::AddData(const QList<QPair<QVariant, QVariant>>& data) {
    for(auto choice : data) {
        QListWidgetItem* item = new CustomListWidgetItem();
        item->setData(Qt::UserRole, choice.second);
        item->setText(choice.first.toString());
        m_selectionMultiFilterUi->w_listWidgetAvailable->addItem(item);
        m_selectionFilterUi->comboBox->addItem(choice.first.toString(), choice.second);
    }
}

void SelectionFilterWidget::RemoveData(const QVariantList& data) {
    for(auto d : data) {
        for(int i = 0; i < m_selectionMultiFilterUi->w_listWidgetAvailable->count(); ++i) {
            if(d == m_selectionMultiFilterUi->w_listWidgetAvailable->item(i)->data(Qt::UserRole)) {
                QListWidgetItem* item = m_selectionMultiFilterUi->w_listWidgetAvailable->takeItem(i);
                delete item;
                break;
            }
        }
        for(int i = 0; i < m_selectionMultiFilterUi->w_listWidgetSelected->count(); ++i) {
            if(d == m_selectionMultiFilterUi->w_listWidgetSelected->item(i)->data(Qt::UserRole)) {
                QListWidgetItem* item = m_selectionMultiFilterUi->w_listWidgetSelected->takeItem(i);
                delete item;
                break;
            }
        }
        for(int i = 1; i < m_selectionFilterUi->comboBox->count(); ++i) {
            if(d == m_selectionFilterUi->comboBox->itemData(i, Qt::UserRole)) {
                if(m_selectionFilterUi->comboBox->currentIndex() == i) {
                    m_selectionFilterUi->comboBox->setCurrentIndex(0);
                }
                m_selectionFilterUi->comboBox->removeItem(i);
                break;
            }
        }
    }
}
QSharedPointer<IFilter<QVariant>> SelectionFilterWidget::GetFilterObject() const {
    QSharedPointer<SelectionFilter<QVariant>> filterObject =
            QSharedPointer<SelectionFilter<QVariant>>(new SelectionFilter<QVariant>());
    if(m_stackedUi.currentIndex() == m_multiFilterIndex) {
        for(int i = 0; i < m_selectionMultiFilterUi->w_listWidgetSelected->count(); ++i) {
            filterObject->AddData(m_selectionMultiFilterUi->w_listWidgetSelected->item(i)->data(Qt::UserRole));
        }
    } else {
        if(m_selectionFilterUi->comboBox->currentIndex() != 0) {
            filterObject->AddData(m_selectionFilterUi->comboBox->currentData(Qt::UserRole));
        }
    }
    return static_cast<QSharedPointer<IFilter<QVariant>>>(filterObject);
}
void SelectionFilterWidget::SetFilterObject(const QSharedPointer<IFilter<QVariant> > &filter) {
    selectData(qSharedPointerCast<SelectionFilter<QVariant>>(filter)->GetData());
}
void SelectionFilterWidget::OnEnableExtendedMode(int enable) {
    if(enable == Qt::Checked) {
        m_stackedUi.setCurrentIndex(m_multiFilterIndex);
        int minHeight = m_stackedUi.currentWidget()->minimumHeight();
        setFixedHeight(minHeight);
    } else {
        m_stackedUi.setCurrentIndex(m_filterIndex);
        int minHeight = m_stackedUi.currentWidget()->minimumHeight();
        setFixedHeight(minHeight);
    }
}
void SelectionFilterWidget::selectData(const QList<QVariant>& data) {
    for(auto chosen : data) {
        for(int i = 0; i < m_selectionMultiFilterUi->w_listWidgetAvailable->count(); ++i) {
            if(chosen == m_selectionMultiFilterUi->w_listWidgetAvailable->item(i)->data(Qt::UserRole)) {
                QListWidgetItem* item = m_selectionMultiFilterUi->w_listWidgetAvailable->takeItem(i);
                onAvailbleDoubleClicked(item);
                break;
            }
        }
    }
}
void SelectionFilterWidget::onSendButtonClicked() {
    QList<QListWidgetItem*> availableItems = m_selectionMultiFilterUi->w_listWidgetAvailable->selectedItems();
    for(auto item : availableItems) {
        onAvailbleDoubleClicked(item);
    }
}
void SelectionFilterWidget::onReturnButtonClicked() {
    QList<QListWidgetItem*> selectedItems = m_selectionMultiFilterUi->w_listWidgetSelected->selectedItems();
    for(auto item : selectedItems) {
        onSelectedDoubleClicked(item);
    }
}
void SelectionFilterWidget::onAvailbleDoubleClicked(QListWidgetItem* item) {
    QListWidgetItem* newItem = new CustomListWidgetItem(*item);
    int row = m_selectionMultiFilterUi->w_listWidgetAvailable->row(item);
    m_selectionMultiFilterUi->w_listWidgetAvailable->takeItem(row);
    delete item;
    m_selectionMultiFilterUi->w_listWidgetSelected->addItem(newItem);
    m_selectionMultiFilterUi->w_listWidgetSelected->sortItems();
    emit sigSelectionAdded(newItem->data(Qt::UserRole));
    if(m_selectionMultiFilterUi->w_listWidgetSelected->count() == 1) {
        for(int i = 1; i < m_selectionFilterUi->comboBox->count(); ++i) {
            if(m_selectionFilterUi->comboBox->itemData(i) == newItem->data(Qt::UserRole)) {
                QObject::disconnect(m_selectionFilterUi->comboBox, SIGNAL(currentIndexChanged(int)),
                                    this, SLOT(onComboboxIndexChange(int)));
                m_selectionFilterUi->comboBox->setCurrentIndex(i);
                QObject::connect(m_selectionFilterUi->comboBox, SIGNAL(currentIndexChanged(int)),
                                    this, SLOT(onComboboxIndexChange(int)));
                break;
            }
        }
    } else {
        m_selectionFilterUi->comboBox->setCurrentIndex(0);
    }
}
void SelectionFilterWidget::onSelectedDoubleClicked(QListWidgetItem* item) {
    QListWidgetItem* newItem = new CustomListWidgetItem(*item);
    int row = m_selectionMultiFilterUi->w_listWidgetSelected->row(item);
    m_selectionMultiFilterUi->w_listWidgetSelected->takeItem(row);
    if(m_selectionMultiFilterUi->w_listWidgetSelected->count() == 1) {
        QListWidgetItem* selectedItem = m_selectionMultiFilterUi->w_listWidgetSelected->item(0);
        for(int i = 1; i < m_selectionFilterUi->comboBox->count(); ++i) {
            if(m_selectionFilterUi->comboBox->itemData(i) == selectedItem->data(Qt::UserRole)) {
                QObject::disconnect(m_selectionFilterUi->comboBox, SIGNAL(currentIndexChanged(int)),
                                    this, SLOT(onComboboxIndexChange(int)));
                m_selectionFilterUi->comboBox->setCurrentIndex(i);
                QObject::connect(m_selectionFilterUi->comboBox, SIGNAL(currentIndexChanged(int)),
                                    this, SLOT(onComboboxIndexChange(int)));
                break;
            }
        }
    } else {
        m_selectionFilterUi->comboBox->setCurrentIndex(0);
    }
    delete item;
    m_selectionMultiFilterUi->w_listWidgetAvailable->addItem(newItem);
    m_selectionMultiFilterUi->w_listWidgetAvailable->sortItems();
    emit sigSelectionDeleted(newItem->data(Qt::UserRole));
}
void SelectionFilterWidget::onComboboxIndexChange(int index) {
    if(index != 0) {
        m_selectionMultiFilterUi->w_listWidgetSelected->selectAll();
        onReturnButtonClicked();
        for(int i = 0; i < m_selectionMultiFilterUi->w_listWidgetAvailable->count(); ++i) {
            QListWidgetItem* item = m_selectionMultiFilterUi->w_listWidgetAvailable->item(i);
            if(item->data(Qt::UserRole) == m_selectionFilterUi->comboBox->itemData(index)) {
                onAvailbleDoubleClicked(item);
                break;
            }
        }
    }
}
