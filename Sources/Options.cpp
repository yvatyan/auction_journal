#include "Headers/Options.h"
#include <QStandardPaths>

const Options &Options::Instance() {
    static Options instance;
    return instance;
}
QString Options::StorageDbName() const {
    return "Storage.db";
}
QString Options::StorageDbPath() const {
    return appDataDirectory();
}
QString Options::SettingsDbName() const {
    return "Settings.db";
}
QString Options::SettingsDbPath() const {
    return appDataDirectory();
}
QString Options::LogFilePathAndName() const {
    return appDataDirectory() + "/AuctionJournal.log";
}
QString Options::DateFormat() const {
    return "dd-MMM-yyyy";
}
QIcon Options::ApplicationIcon() const {
    return QIcon(":/Icons/auction.ico");
}
QString Options::ApplicationName() const {
    return "Auction Journal";
}
int Options::ValidLotIdLength() const {
    return 8;
}
QFont Options::LotRecordsTableHeaderFont() const {
    return QFont("Calibri Light", 11, QFont::Bold, true);
}
QFont Options::LotRecordsTableCellFont() const {
    return QFont("Calibri Light", 11);
}
QString Options::StateLicensingFilePath() const {
    return StorageDirectory() + "/Docs/State Licensing.pdf";
}
QString Options::StorageDirectory() const {
    return appDataDirectory() + "/Storage";
}
QString Options::MemberFeesFilePath() const {
   return StorageDirectory() + "/Docs/Member Fees.pdf";
}
Options::Options()
{
}
Options::Options(const Options& copy) {
    Q_UNUSED(copy);
}
const Options& Options::operator=(const Options& copy) {
    Q_UNUSED(copy);
    return *this;
}
QString Options::appDataDirectory() const {
    QString userDataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    int pos = userDataPath.lastIndexOf(QChar('/'));
    return userDataPath.left(pos) + '/' + ApplicationName();
}
