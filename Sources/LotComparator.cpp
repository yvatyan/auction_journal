#include "Headers/LotComparator.h"
#include "Headers/Tools.h"

LotComparator::LotComparator(Database* m_db)
    : m_db(m_db)
    , m_comparisonChain(nullptr)
    , m_dummyComparatorIndex(INT_MAX)
    , m_firstComparatorField(LotRecordsTable::Fields::Dummy)
{
    init();
}
LotComparator::~LotComparator() {
    for(int i = static_cast<int>(c_lotFieldCount); i >= 0; --i) {
        delete m_comparisonChain[i];
    }
    delete[]  m_comparisonChain;
}
void LotComparator::FixField(LotRecordsTable::Fields field) {
    ResetFixedField();
    for(size_t i = 0; i <= c_lotFieldCount; ++i) {
        if(m_comparisonChain[i]->Field() == field) {
            std::swap(m_comparisonChain[0], m_comparisonChain[i]);
            m_dummyComparatorIndex = i;
            break;
        }
    }
}
void LotComparator::ResetFixedField() {
    std::swap(m_comparisonChain[0], m_comparisonChain[m_dummyComparatorIndex]);
    m_dummyComparatorIndex = 0;
}
LotRecordsTable::Fields LotComparator::GetFirstField() const {
    return m_firstComparatorField;
}
int LotComparator::Compare(const Lot& a, const Lot& b) const {
    int result = 0;
    LotResolver resolvedA(a, m_db);
    LotResolver resolvedB(b, m_db);
    for(size_t i = 0; i <= c_lotFieldCount; ++i) {
        if(m_comparisonChain[i]->Compare(resolvedA, resolvedB, result)) {
            break;
       }
    }
    return result;
}
void LotComparator::init() {
    m_comparisonChain = new IComparisonNode*[c_lotFieldCount + 1];

    m_comparisonChain[0] = new IComparisonNode();
    m_comparisonChain[1] = new DateComparisonNode(LotRecordsTable::Fields::AuctionDate);
    m_comparisonChain[2] = new NumberComparisonNode(LotRecordsTable::Fields::Missed);
    m_comparisonChain[3] = new NumberComparisonNode(LotRecordsTable::Fields::Year);
    m_comparisonChain[4] = new NumberComparisonNode(LotRecordsTable::Fields::LotId);
    m_comparisonChain[5] = new StringWithNumberComparisonNode(LotRecordsTable::Fields::MakeId);
    m_comparisonChain[6] = new StringWithNumberComparisonNode(LotRecordsTable::Fields::ModelId);
    m_comparisonChain[7] = new StringComparisonNode(LotRecordsTable::Fields::LocationId);
    m_comparisonChain[8] = new StringComparisonNode(LotRecordsTable::Fields::PurposeId);
    m_comparisonChain[9] = new StringComparisonNode(LotRecordsTable::Fields::BodyId);
    m_comparisonChain[10] = new StringComparisonNode(LotRecordsTable::Fields::TransmissionId);
    m_comparisonChain[11] = new DateComparisonNode(LotRecordsTable::Fields::WatchDate);
    m_comparisonChain[12] = new StringComparisonNode(LotRecordsTable::Fields::SaleStatusId);
    m_comparisonChain[13] = new StringComparisonNode(LotRecordsTable::Fields::SoldStatusId);
    m_comparisonChain[14] = new NumberComparisonNode(LotRecordsTable::Fields::SoldPrice);
    m_comparisonChain[15] = new NumberComparisonNode(LotRecordsTable::Fields::WitPrice);

    m_dummyComparatorIndex = 0;
    m_firstComparatorField = m_comparisonChain[1]->Field();
}
LotComparator::IComparisonNode::IComparisonNode()
    : m_fieldName(LotRecordsTable::Fields::Dummy)
{
}
LotComparator::IComparisonNode::IComparisonNode(LotRecordsTable::Fields fieldName)
    : m_fieldName(fieldName)
{
}
LotComparator::IComparisonNode::~IComparisonNode()
{
}
bool LotComparator::IComparisonNode::Compare(const LotResolver& lot1, const LotResolver& lot2, int& out_comparisonResult) {
    Q_UNUSED(lot1);
    Q_UNUSED(lot2);
    out_comparisonResult = 0;
    bool handled = false;
    return handled;
}
LotComparator::NumberComparisonNode::NumberComparisonNode(LotRecordsTable::Fields fieldName)
    : IComparisonNode(fieldName)
{
}
bool LotComparator::NumberComparisonNode::Compare(const LotResolver& lot1, const LotResolver& lot2, int& out_comparisonResult) {
    int val1 = qvariant_cast<int>(lot1.GetField(Field()));
    int val2 = qvariant_cast<int>(lot2.GetField(Field()));
    if(val1 > val2) {
        out_comparisonResult = 1;
        return true;
    } else if(val1 < val2) {
        out_comparisonResult = 2;
        return true;
    } else {
        out_comparisonResult = 0;
        return false;
    }
}
LotComparator::StringComparisonNode::StringComparisonNode(LotRecordsTable::Fields fieldName)
    : IComparisonNode(fieldName)
{
}
bool LotComparator::StringComparisonNode::Compare(const LotResolver& lot1, const LotResolver& lot2, int& out_comparisonResult) {
    QString val1 = qvariant_cast<QString>(lot1.GetField(Field()));
    QString val2 = qvariant_cast<QString>(lot2.GetField(Field()));
    if(val1 > val2) {
        out_comparisonResult = 1;
        return true;
    } else if(val1 < val2) {
        out_comparisonResult = 2;
        return true;
    } else {
        out_comparisonResult = 0;
        return false;
    }
}
LotComparator::StringWithNumberComparisonNode::StringWithNumberComparisonNode(LotRecordsTable::Fields fieldName)
    : IComparisonNode(fieldName)
{
}
bool LotComparator::StringWithNumberComparisonNode::Compare(const LotResolver& lot1, const LotResolver& lot2, int& out_comparisonResult) {
    QString val1 = qvariant_cast<QString>(lot1.GetField(Field()));
    QString val2 = qvariant_cast<QString>(lot2.GetField(Field()));
    out_comparisonResult = Tools::CompareNumberedStrings(val1, val2);
    return out_comparisonResult != 0;
}
LotComparator::DateComparisonNode::DateComparisonNode(LotRecordsTable::Fields fieldName)
    : IComparisonNode(fieldName)
{
}
bool LotComparator::DateComparisonNode::Compare(const LotResolver& lot1, const LotResolver& lot2, int& out_comparisonResult) {
    QDate val1 = qvariant_cast<QDate>(lot1.GetField(Field()));
    QDate val2 = qvariant_cast<QDate>(lot2.GetField(Field()));
    if(val1 > val2) {
        out_comparisonResult = 1;
        return true;
    } else if(val1 < val2) {
        out_comparisonResult = 2;
        return true;
    } else {
        out_comparisonResult = 0;
        return false;
    }
}
