#include "Headers/FilePathValidator.h"
#include <QFileInfo>

FilePathValidator::FilePathValidator(QWidget* target, QObject* parent)
    : QValidator(parent)
    , m_targetWidget(target)
{
}
FilePathValidator::~FilePathValidator()
{
}
QValidator::State FilePathValidator::validate(QString& input, int& pos) const {
    Q_UNUSED(pos);
    if(!QFileInfo::exists(input)) {
        QPalette palette(m_targetWidget->palette());
        palette.setColor(QPalette::Text, QColor(0xaa, 0x00, 0x00));
        m_targetWidget->setPalette(palette);
        return QValidator::Intermediate;
    } else {
        QPalette palette(m_targetWidget->palette());
        palette.setColor(QPalette::Text, QColor(0x00, 0xaa, 0x00));
        m_targetWidget->setPalette(palette);
        return QValidator::Acceptable;
    }
}
