#include "Headers/LotValidator.h"
#include "Headers/Options.h"

LotValidator::LotValidator(QObject* parent)
    : QValidator(parent)
{
}
LotValidator::~LotValidator()
{
}
QValidator::State LotValidator::validate(QString& input, int& pos) const {
    bool isDigit = (pos > 0 ? input[pos - 1].isDigit() : true);
    if(input.size() > Options::Instance().ValidLotIdLength() || !isDigit) {
        return QValidator::Invalid;
    } else if(input.size() < Options::Instance().ValidLotIdLength()) {
        return QValidator::Intermediate;
    } else {
        return QValidator::Acceptable;
    }
}
