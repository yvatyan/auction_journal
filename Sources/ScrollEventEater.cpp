#include <Headers/ScrollEventEater.h>
#include <QEvent>

ScrollEventEater::ScrollEventEater(QObject* parent)
    : QObject(parent)
{
}
bool ScrollEventEater::eventFilter(QObject* obj, QEvent* event) {
    if(event->type() == QEvent::Wheel) {
        return true;
    } else {
        return QObject::eventFilter(obj, event);
    }
}
