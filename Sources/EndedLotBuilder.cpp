#include "Headers/EndedLotBuilder.h"

EndedLotBuilder::EndedLotBuilder()
    : m_temporaryLot()
    , m_product()
    , m_modified(true)
    , m_soldPriceIsRequired(true)
    , m_requiredFieldsMask(ResetMask)
    , m_missedFlag(false)
{
}
Lot EndedLotBuilder::ConstructLot() {
    if(!AreAllRequiredFieldsFilled()) {
        throw std::runtime_error("Not all required fields are filled.");
    }
    if(!m_temporaryLot.IsValid()) {
        throw std::runtime_error("Original lot is not set.");
    }
    if(m_modified) {
        m_product = Lot(
                        m_temporaryLot.LotId(),
                        m_temporaryLot.MakeId(),
                        m_temporaryLot.ModelId(),
                        m_temporaryLot.Year(),
                        m_temporaryLot.TransmissionId(),
                        m_temporaryLot.BodyId(),
                        m_temporaryLot.PurposeId(),
                        m_temporaryLot.LocationId(),
                        m_temporaryLot.WatchDate(),
                        m_temporaryLot.AuctionDate(),
                        m_temporaryLot.SaleStatusId(),
                        m_soldStatusId,
                        m_soldPrice,
                        m_temporaryLot.WantItTodayPrice(),
                        m_temporaryLot.WorksheetPath(),
                        m_missedFlag
                    );
        m_product.SetRecordId(m_temporaryLot.GetRecordId());
        m_modified = false;
    }
    return m_product;
}
void EndedLotBuilder::SetLot(const Lot &lot) {
    m_modified = true;
    m_temporaryLot = lot;
}
void EndedLotBuilder::ResetBuilder() {
    m_temporaryLot = Lot();
    m_modified = true;
    m_soldPriceIsRequired = true;
    m_requiredFieldsMask = ResetMask;
    // NOTE: Required field data is not nulled, because
    //       new object can be built only when all required fields are set again.
    m_soldPrice = 0; // Is semi-required so it is nulled
    m_missedFlag = false;
}
bool EndedLotBuilder::AreAllRequiredFieldsFilled() const {
    return !((m_soldPriceIsRequired && m_requiredFieldsMask != AllowedMask)
            || (!m_soldPriceIsRequired && m_requiredFieldsMask != (AllowedMask & ~SoldPrice)));
}
QList<LotRecordsTable::Fields> EndedLotBuilder::UnfilledRequiredFields() const {
    QList<LotRecordsTable::Fields> result;
    if(m_soldPriceIsRequired && (m_requiredFieldsMask & SoldPrice) == 0) {
        result.append(LotRecordsTable::Fields::SoldPrice);
    }
    if((m_requiredFieldsMask & SoldStatus) == 0) {
        result.append(LotRecordsTable::Fields::SoldStatusId);
    }
    return result;
}
void EndedLotBuilder::SetSoldStatusId(unsigned int soldStatusId) {
    if(soldStatusId == 0) {
        return;
    }
    m_modified = true;
    m_requiredFieldsMask |= SoldStatus;
    m_soldStatusId = soldStatusId;
}
void EndedLotBuilder::SetSoldPrice(unsigned int soldPrice) {
    if(m_soldPriceIsRequired && soldPrice == 0) {
        return;
    }
    m_modified = true;
    if(soldPrice != 0 && m_soldPriceIsRequired) {
        m_requiredFieldsMask |= SoldPrice;
    } else {
        m_requiredFieldsMask &= ~SoldPrice;
    }
    m_soldPrice = soldPrice;
}
void EndedLotBuilder::SetMissedFlag(bool isMissed) {
    m_modified = true;
    m_missedFlag = isMissed;
}
void EndedLotBuilder::SetSoldPriceRequired(bool isRequired) {
    m_soldPriceIsRequired = isRequired;
}
