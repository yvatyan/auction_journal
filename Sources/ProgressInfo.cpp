#include "Headers/ProgressInfo.h"

ProgressInfo::ProgressInfo() {
   m_stepStatus = Status::Invalid;
}
ProgressInfo::ProgressInfo(const QString& jobName, size_t stepQty, const QString& stepName, size_t stepIndex, const QString& stepMessage, Status stepStatus)
    : m_jobName(jobName)
    , m_stepQty(stepQty)
    , m_stepName(stepName)
    , m_stepIndex(stepIndex)
    , m_stepMessage(stepMessage)
    , m_stepStatus(stepStatus)
{
}
