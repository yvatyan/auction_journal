# **Git commands' cheatsheet**
### *Updated 23 June 2018*

**Global setup**
```git
git config --global user.name "Yuri Vatyan"
git config --global user.email "vatyan.1996@gmail.com"
```
**Create a new repository**
```git
git clone https://gitlab.com/yvatyan/toaster.git D:/Projects/Toaster
cd Toaster
touch README.md
git add README.md
git commit -m "Initial commit"
git push -u origin master
```
**Create from existing folder**
```git
cd existing_folder
git init
git remote add origin https://gitlab.com/yvatyan/toaster.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
**Create from existing repository**
```git
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/yvatyan/toaster.git
git push -u origin --all
git push -u origin --tags
```
**Make a commit to remote _(add file to commit, check shortened status, make commit, push to remote)_**
```git
git add file.cpp
git status -s
git commit -m "Issue 13"
git push
```
**Unstage files (revert "git add" operation)**
```git
git reset
```
**Create tag for bd65e9 commit and push to remote**
```git
git tag -a v.0.00.0.α1 -m "v.0.00.0-03.2018.α" bd65e9
git push origin v.0.00.0.α1
```
**Delete tag from local and remote repository**
```git
git tag -d v.0.00.0.α1
git push origin :refs/tags/v.0.00.0.α1
```
**List tags, filter tags to see only alpha versions, show information about tag**
```git
git tag
git tag -l "*.α"
git show v.0.00.2.δ
```
**See all commits in one line**
```git
git log --pretty=oneline
# or for shortened checksums
git log --oneline
```
**See all commits and also tags with branch pointers**
```git
git log --oneline --decorate
```
**See all commits and also tags with branch pointers**
```git
git log --oneline --decorate
```
**See all commits in graph**
```git
git log --oneline --graph
```
**Update if commit has not been done locally yet and push to remote**
```git
git add file.cpp
git pull # should be fast-forward
git commit -m "Issue 13: Failed to allocate memory"
git push
```
**Update if commit has been done locally and push to remote**
```git
git add file.cpp
git commit -m "Issue 13: Failed to allocate memory"
git fetch
git merge origin/develop -m "Merge, Issue 13: Failed to allocate memory"
git push
```
**Update using rebase if commit has been done locally and push to remote**
```git
git add file.cpp
git commit -m "Issue 13: Failed to allocate memory"
git fetch
git rebase origin/develop
git push
```
**Checkout a single file**
```git
git checkout -- file.cpp
```
**Create a branch**
```git
git branch branch_1
```
**Create a branch based on another branch and switch to it**
```git
git checkout -b branch_1 another_branch
```
**To list all branches with last commits and tracking remotes**
```git
git branch -vv
```
**Switch between branches _(<span style="color:red;">NOTE!</span> every modifed file will be lost)_**
```git
git checkout branch_1
```
**Save, View and Apply your changes while swiching to another branch**
```git
git stash -u
git stash list
git stash apply stash@{2}
```
**Retrieve changed file names between local and remote branches (files that were not commited are not shown in diff)**
```git
git diff develop origin/develop --stat # or --name-only to see ONLY filenames
```
**Delete local branch\delete remote branch**
```git
git branch -d branch_1
git push origin --delete branch_1
```
**Remove untracked files**
```git
git clean -d -x # specify -n option to see what will be removed
```
**Commiting hotfix _(new branch by guest can be commited the same way but only must contain Issue name (like commiting to develop))_**
```git
git checkout -b Hotfix_Issue13 master
git add file.cpp
git commit -m "Issue 13: Failed to allocate memory"
git push --set-upstream origin Hotfix_Issue13
```
**Merging hotfix to master after updating local repository**
```git
git checkout master
git merge Hotfix_Issue13 # should be fast-forward
git push origin --delete Hotfix_Issue13
```
**Creating new release (merge develop to master and back)**
```git
git checkout master
git merge develop -m "Release v.1.013.123-03.2018.γ" --no-ff
git push
git checkout develop
git merge master # should be fast-forward
```
**TODO**
```git
git submodule
git cherry-pick
and others
```
