﻿Commit: 760fca0 Major refactoring of data retrievement, sorting and registration
+ 1. Check while adding new lot to be unique, show message box if such a lot already exist.
+ 2. In LotMode l class , create mapping <ENUM = column_id, column_name> to use in switches.
+ 3. Add to Options class: Header Font, Table Items' Font.
+ 4. Separate in different functions new vehicle dialog fields' setup.
+ 5. In SetupFields functions for new vehicle dialog sort only changable data.
+ 6. Store field names in table classes' maps with mapping of <ENUM = column_id, column_name>
+ 7. TableApi classes should return maps mapping field names with values.
+ 8. Remove comments member from Lot class, in future the comments maube will return to Lot class as separate object-member.
+ 9. Change order of Lot parameters acording to the order in database.
+ 10. Change Tools::Sort to take and return the same type that is returned from sql commands.
+ 11. Common Sort order for api retrieving data - Sort by Id ascending.
+ 12. LotBuilder class should have method to construct Lot object based on sql commands' result data. LotBuilder reset should null all fields. Check for required field and setting to null. A comfortable solution without code duplication is to create new lot with NewLotBuilder and then end it with EndedLotBuilder.
+ 14. Mark non-changable and changebale enum tables in order to note for further additions to write migration that adds new values in alphabeticly sorted order in non-changable tables.
Changable tables' data is sorted by Names in FieldsSetup functions.
+ 15. Divide SqlSelect command's constructor in methods.

==== Main tasks ====
+ 1. Replace ids in tables with names in LotRecordTable.
+ 2. "Delete record" button and Api.
+ 4. Select the whole row when table item is clicked.

+ 6. Show in bubbles quantity of timeline and comments when a row is selected.

+ 5. Highlight fields in dialogs that are not filled. 
    + 5.1. Add ability to select Future auction date (currently year 9999 is used as future).
+ 3.1. Correct required fields and combobox variants based on sale ended and missed check boxes. Sold price and sold status are not required if vehicle sale is missed.

+ 10. Implement End Auction dialog.

+ 3. Integrate "Missed status" (add column in database, Lot object and write it with sold status).
+ 8. Link top buttons together to create tabs effect.
+ 14. [Bug:] Wrong coloring when future records are finalized.

+ 7. Add option in comboboxes corresponding the dynamic enum tables, to add new values (in new vehicle and auction end dialogs).

+ 11. Implement Comments menu and every stuff related to Comments.

+ 12. Implement PdfViewer integration.
+	12.1. Add view state license, member fee pdfs and about button.

+ 13. Implement Loading startup window.

+ 9. Implement ProxyModel to sort and filter LotRecordTable.
	+ 9.1. Group in correct order when new created records are moved back in time. (e.g. future gets spesefied date, current behavior it is shown instead of actual pending record).
- 9.2 Fixed columns

10. - Restucture files in subdirectories.
	~ 10.1 Git script for adding tags. Git hook script to replace version in sources according checkout tag. Add readme with corrected version naming (revision for non δ releases is number of δ in between).
	 - 10.2 Check comments in code and mark some of them as INFO or NOTE.
	 + 10.3 Push to remote repo and add tasks below as issues.

==== Further refactoring goals and tasks ====
1. HTML formating is lost after editing row text, new lines made with Enter key are not work, <br> should be used, text is shown HTML stylized.
* 2. Other method to calculate comment item height, rather then creating QTextEdit, check whether setGeometry warning is eliminated.
+ 3. Transition between windows is bad is blinking.
+ 4.  Migration registration should be moved to class DatabaseMangar class. Also databasees should be retrieved from DatabaseManager. All incluedes for TableApis in NewVehicleDialog.cpp and MainWindow.cpp should be moved to DatabaseManager.
5. Revise Tools class to perform code refactoring and optimization.
6. Check for QVariant to be able to convert and cast with qvariant_cast<>.
7. Revise row colors and selection colors.
* 8. Adjust table width to widget width if it is less than widget width, otherwise simply use scrollbars and resize to contents for columns.
9. In Migrate() functions check for value order correctness.
	9.1. RollBack of migrations.
10. Shorten code in TableApis by using loops instead of by element initialization.
11. Store all colores together in QStyle, or in Options subclass(namespace).
12. Is it better to attach worksheet before auction, not at registaration?
13. New vehicle dialog and new entry dialog have some methods dulicated but differ in members that makes imposible their inheritance. Code duplication shoul be eliminated in some way.
14. Color missed records with zebra gradient.
15. Bug: When migration fails app continues to work encounting more errors.
16. Write custom year validator from 1900 to curent year + 1 to work properly and check for validy before setting year in builder.
17. If some task comments\lot insertion fails lot\comments should not be added.
18. Add ProgressInfoBuilder to get rid of huge constructor calling, Initialize function break into Small functions by blocks (e.g. createDatabaseProxy) and pass them rhe same builder.
19. Check for all new coresponding delete existance.
20. GetSoldStatus(Unfinished\Finished\All)Data code assemble in one function (code duplication).
21. Remove conversion from unsigned integers to signed (change interfaces).
22. Update models in filter using newly added or deleted make information not fully reset model data and fill again.
23. Separate GetFilter function in small subfunctions and add checks to every thing (e.g. is date valid, is conversion from string to number valid, does list element exist after split).
23.5.crash after closing application on windows 10 64 bit build

24. Change readme, fill isues, remove this file.
25. Deploy.bat rewrite for bash.
26. Remove v letter before version (but it should remain in app UI).
27. Replace α, β, γ by δ in version if build is modified.
28. Add publisher significance in order to windows not block setup.
29. Remove worksheet file from storage if record is not created.

==== Further discusions ====
1. A better approch for organazing migrations and working with tables (in-code api synchronization with sqlite database).
2. Close sale for approval with a dialog that contains a palce for comment.
3. Maybe it is better to attach pdf before auction?
4. A better approach to proxy model chaining in order to use methods from root source model. How obtain index of root model bypassing proxy models before?
5. Do I need to fix some columns?

==== SBugs ====
1. Sometimes switching between tabs (also selectecting lot in them) disables side buttons.
2. Sometimes selection in lotwidget is lost, but side buttons remain enabled.
3. Sometimes crash after closing application showing comments. The crash is in CommentsItemView destructor. Also 
	terminate called after throwing an instance of 'std::bad_array_new_length'
  	what():  std::bad_array_new_length.
