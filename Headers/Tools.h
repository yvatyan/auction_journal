#ifndef TOOLS_H
#define TOOLS_H

#include <QString>
#include <QList>
#include <QPair>
#include <QFont>
#include <QMouseEvent>

class Tools {
public:
    enum class StringCase {
        AllLower,
        AllUpper,
        FirstUpper,
        AllFirstUpper
    };
    enum class SortOrder {
        Ascending,
        Descending
    };

    static QString ChangeStringCase(const QString &str, StringCase mode);
    template<typename Field>
    static void SortStringsWithNumbers(QList<QMap<Field, QVariant>>& data, Field sortBy, SortOrder order);
    static QSize StringRenderSize(const QString& str, const QFont& font);
    static QSize TextRenderSizeOld(const QString& text, const QFont& font, int widthLimit);
    static QSize TextRenderSize(const QString& text, const QFont& font, int widthLimit);
    static bool IsQMouseEvent(QEvent* event);
    static int CompareNumberedStrings(const QString& str1, const QString& str2);
    static bool IsInStorageDir(const QString path);
    static QString GenerateRandomName(int length, int portion = 0, const QString& delemiter = "", const QString& extension = "");
    static QString FileExtension(const QString& path);
private:
    static bool isSpecialCharacter(char ch);
    static bool isSpaceCharacter(char ch);
    static int extractIntegerAt(const QString& str, int index, int& length);
    static QString applySpecialSlashB(const QString& str);
};

template<typename Field>
void Tools::SortStringsWithNumbers(QList<QMap<Field, QVariant>>& data, Field sortBy, Tools::SortOrder order) {
    for(int i = 0; i < data.size() - 1; ++i) {
        QString str1 = data.at(i)[sortBy].toString();
        for(int j = i + 1; j < data.size(); ++j) {
           QString str2 = data.at(j)[sortBy].toString();
           int cmp = CompareNumberedStrings(str1, str2);
           if((order == SortOrder::Ascending && cmp == 1) ||
              (order == SortOrder::Descending && cmp == 2)) {
                data.swap(i, j);
                str1 = str2;
           }
        }
    }
}

uint qHash(const QVariant& v, uint seed = 0);

#endif // TOOLS_H
