#ifndef IWORKER_H
#define IWORKER_H

#include <QObject>
#include "Headers/ProgressInfo.h"

class IWorker : public QObject {
    Q_OBJECT
signals:
    void sigProgressChanged(const ProgressInfo& progress);
};

#endif // IWORKER_H
