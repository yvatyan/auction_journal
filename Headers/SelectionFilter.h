#ifndef SELECTIONFILTER_H
#define SELECTIONFILTER_H

#include "Headers/IFilter.h"
#include <QSet>

template<typename T>
class SelectionFilter : public IFilter<T> {
public:
    ~SelectionFilter();

    void AddData(const T& data);
    QList<T> GetData() const;
    virtual bool Accept(const T& object) const;
    virtual bool IsValid() const;
    virtual unsigned int Count() const;
private:
    QSet<T> m_selections;
};

template<typename T>
SelectionFilter<T>::~SelectionFilter<T>()
{
}
template<typename T>
void SelectionFilter<T>::AddData(const T& data) {
    m_selections.insert(data);
    if(m_selections.size() > 0) {
        IFilter<T>::m_isValid = true;
    }
}
template<typename T>
QList<T> SelectionFilter<T>::GetData() const {
    return m_selections.toList();
}
template<typename T>
bool SelectionFilter<T>::Accept(const T& object) const {
    if(IsValid()) {
        bool b = m_selections.contains(object);
        return b;
    } else {
        return true;
    }
}
template<typename T>
bool SelectionFilter<T>::IsValid() const {
    return IFilter<T>::m_isValid;
}
template<typename T>
unsigned int SelectionFilter<T>::Count() const {
    return m_selections.count();
}

#endif // SELECTIONFILTER_H
