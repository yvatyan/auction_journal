#ifndef SQLDELETE_H
#define SQLDELETE_H

#include "Headers/Database/SqlCommands/ISqlCommand.h"

class SqlDelete : public ISqlCommand {
public:
    SqlDelete(Database* database,
              const QString& tableName,
              const QString& condition);

    QList<QVariantList> Execute();
private:
    QString m_tableName;
    QString m_condition;
};

#endif // SQLDELETE_H
