#ifndef SQLSELECT_H
#define SQLSELECT_H

#include "Headers/Database/SqlCommands/ISqlCommand.h"

class SqlSelect : public ISqlCommand {
public:
    SqlSelect(Database* database,
              const QString& selectWhat,
              const QString& tableName);

    QList<QVariantList> Execute();
    void SetCondition(const QString& condition);
    void SetOrderBy(const QString& orderBy);
    void SetGroupBy(const QString& groupBy);
    void SetDistinctFlag(bool turnOn);
private:
    QString m_selectWhat;
    QString m_tableName;
    QString m_condition;
    QString m_orderBy;
    QString m_groupBy;
    bool m_isDistinct;
};

#endif // SQLSELECT_H
