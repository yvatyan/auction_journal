#ifndef SQLCREATETABLE_H
#define SQLCREATETABLE_H

#include "Headers/Database/SqlCommands/ISqlCommand.h"
#include "Headers/Database/SqlTypes.h"
#include <QList>

class SqlCreateTable : public ISqlCommand {
public:
    SqlCreateTable(Database* database,
                   const QString& tableName,
                   const QString& primaryKey,
                   const FieldVector& fields,
                   const ForeignKeyList& foreignKeys = {});

    QList<QVariantList> Execute();
private:
    QString m_tableName;
    QString m_primaryKey;
    FieldVector m_fields;
    ForeignKeyList m_foreignKeys;
};

#endif // SQLCREATETABLE_H
