#ifndef ISQLCOMMAND_H
#define ISQLCOMMAND_H

#include "Headers/Database/Database.h"
#include "Headers/Database/SqlTypes.h"
#include <QVariant>
#include <QtSql/QSqlQuery>

class ISqlCommand {
public:
    ISqlCommand(Database* database);
    virtual ~ISqlCommand();

    virtual QList<QVariantList> Execute() = 0;

    bool IsValid() const;
protected:
    bool m_state;
    QSqlQuery executeQuery(const QString& queryString);
private:
    Database* m_db;
};

#endif // ISQLCOMMAND_H
