#ifndef SQLUPDATE_H
#define SQLUPDATE_H

#include "Headers/Database/SqlCommands/ISqlCommand.h"

class SqlUpdate : public ISqlCommand {
public:
    SqlUpdate(Database* database,
              const QString& tableName,
              const QList<QPair<QString, QString>>& fields);

    QList<QVariantList> Execute();
    void SetCondition(const QString& condition);
private:
    QString m_tableName;
    QList<QPair<QString, QString>> m_fields;
    QString m_condition;
};

#endif // SQLUPDATE_H
