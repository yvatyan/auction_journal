#ifndef SQLINSERT_H
#define SQLINSERT_H

#include "Headers/Database/SqlCommands/ISqlCommand.h"
#include "Headers/Database/SqlTypes.h"

class SqlInsert : public ISqlCommand {
public:
    SqlInsert(Database* database,
              const QString& tableName,
              const Values& values);

    QList<QVariantList> Execute();
private:
    QString m_tableName;
    Values m_values;
};

#endif // SQLINSERT_H
