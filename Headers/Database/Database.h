#ifndef DATABASE_H
#define DATABASE_H

#include "Headers/IWorker.h"
#include <QtSql/QSqlDatabase>
#include <QMap>
#include <QString>

class ITableApi;
class MigrationTable;
class Database : public IWorker {
public:
    Database(const QString& name);
    ~Database();

    void OpenConnection(const QString& path);
    void AddTable(ITableApi* table);             // Note: Gains pointing resource managment
    ITableApi* Table(const QString& name) const;
    MigrationTable* GetMigrationTable();
    const QSqlDatabase& SqlDatabase() const;
    void Migrate();
private:
    QSqlDatabase m_db;
    QMap<QString, ITableApi*> m_tables;
    QList<ITableApi*> m_migrationApplyHistory;
    QString m_name;

    bool migrationTableExistsInDb() const;
};

#endif // DATABASE_H
