#ifndef IMIGRATION_H
#define IMIGRATION_H

#include "Headers/Database/Database.h"
#include <QString>

class IMigration {
public:
    IMigration(ITableApi* owningTable);
    virtual ~IMigration();

    virtual bool Migrate() const = 0;
    virtual bool Rollback() const = 0;
    virtual QString Description() const = 0;
    virtual size_t Id() const = 0;
protected:
    ITableApi* m_table;
};

#endif // IMIGRATION_H
