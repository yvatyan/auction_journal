#ifndef SQLTYPES_H
#define SQLTYPES_H

#include <QString>
#include <QList>
#include <tuple>

enum class SqlType {
    Integer,
    Blob,
    Text,
    Real,
    Numeric,
    Date,
    Boolean
};
namespace SqlTypes {
    QString ToString(SqlType type);
}

enum class SqlAttribute {
    Unique,
    NotNull,
    AutoIncrement
};
namespace SqlAttributes {
    QString ToString(SqlAttribute attrinute);
}

typedef QPair<QStringList, QStringList>                   Values;
typedef std::tuple<QString, QString, QString>             ForeignKey;
typedef std::tuple<QString, SqlType, QList<SqlAttribute>> Field;
typedef QList<SqlAttribute> 							  AttributeList;
typedef QVector<Field>									  FieldVector;
typedef QList<ForeignKey>								  ForeignKeyList;

#endif // SQLTYPES_H
