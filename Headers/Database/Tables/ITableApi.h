#ifndef ITABLEAPI_H
#define ITABLEAPI_H

#include "Headers/Database/Database.h"
#include "Headers/Database/IMigration.h"
#include "Headers/Database/SqlTypes.h"
#include <QList>

// Aadding new values via migraitons to NON-DYNAMIC tables should be done in right order
// And changed in already existed records,
// Because that table data is not sorting after retrieving from database.
class ITableApi {
public:
    ITableApi(IMigration* migration, Database* database);
    virtual ~ITableApi();

    virtual QString Name() const = 0;
    virtual void ApplyMigration();
    bool ExistsInDb();

    Database* GetDatabase();
    unsigned int MigrationId() const;
    QString MigrationDescription() const;

    bool CreateDataTable(const FieldVector& fields,
                         const QList<QStringList>& data,
                         const QString& primaryKey,
                         const ForeignKeyList& foreignKeys = {}) const;
protected:
    IMigration* m_migration;
    Database* m_database;
};

#endif // ITABLEAPI_H
