#ifndef MODELTABLE_H
#define MODELTABLE_H

#include "Headers/Database/Tables/ITableApi.h"
#include "Headers/Database/IMigration.h"

// Dynamic table
class ModelTable : public ITableApi {
public:
    enum class Fields;
private:
    class Migration : public IMigration {
    public:
        Migration(ITableApi* table);
        bool Migrate() const;
        bool Rollback() const;
        QString Description() const;
        size_t Id() const;
    };
    class IApi {
    public:
        IApi(ITableApi* owningTable);

        QList<QMap<Fields, QVariant>> GetModelDataByMakeId(size_t makeId) const;
        QString GetModelNameById(size_t id) const;
        void AddNewModel(int makeId, const QString& modelName) const;
        int GetModelIdByName(const QString& modelName) const;
        int GetMakeIdByModelId(int modelId) const;
    private:
        ITableApi* m_table;
    };
    IApi m_api;
    static QMap<Fields, QPair<size_t, QString>> m_fields;
public:
    enum class Fields {
        Id,
        Name,
        MakeId
    };
    ModelTable(Database* database);

    QString Name() const;
    const IApi& Api() const;
    static size_t FieldIndex(Fields f);
    static QString FieldName(Fields f);
};

#endif // MODELTABLE_H
