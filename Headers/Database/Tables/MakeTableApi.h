#ifndef MAKETABLE_H
#define MAKETABLE_H

#include "Headers/Database/Tables/ITableApi.h"
#include "Headers/Database/IMigration.h"

// Dynamic table
class MakeTable : public ITableApi {
public:
    enum class Fields;
private:
    class Migration : public IMigration {
    public:
        Migration(ITableApi* table);
        bool Migrate() const;
        bool Rollback() const;
        QString Description() const;
        size_t Id() const;
    };
    class IApi {
    public:
        IApi(ITableApi* owningTable);

        QList<QMap<Fields, QVariant>> GetMakeData() const;
        QString GetMakeNameById(size_t id) const;
        void AddNewMake(const QString& makeName) const;
        int GetMakeIdByName(const QString& makeName) const;
    private:
        ITableApi* m_table;
    };
    IApi m_api;
    static QMap<Fields, QPair<size_t, QString>> m_fields;
public:
    enum class Fields {
        Id,
        Name
    };
    MakeTable(Database* database);

    QString Name() const;
    const IApi& Api() const;
    static size_t FieldIndex(Fields f);
    static QString FieldName(Fields f);
};

#endif // MAKETABLE_H
