#ifndef FILTERSETTINGSTABLEAPI_H
#define FILTERSETTINGSTABLEAPI_H

#include "Headers/Database/Tables/ITableApi.h"
#include "Headers/Database/IMigration.h"
#include "Headers/Ui/LotFilterDialog.h"
#include <QMap>

// Dynamic table
class FilterSettingsTable : public ITableApi {
public:
    enum class Fields;
private:
    class Migration : public IMigration {
    public:
        Migration(ITableApi* table);
        bool Migrate() const;
        bool Rollback() const;
        QString Description() const;
        size_t Id() const;
    };
    class IApi {
    public:
        IApi(ITableApi* owningTable);

       void SaveFilter(const LotFilterDialog::FilterCollection& filter) const;
       LotFilterDialog::FilterCollection GetFilter() const;
       void TurnOffFilter() const;
       bool IsFilterTurnedOn() const;
    private:
        ITableApi* m_table;

        void packFilterData(const LotFilterDialog::FilterCollection& filterData, QMap<Fields, QString>& packedFilter) const;
        QString fieldsToRetrieve() const;
    };
    IApi m_api;
    static QMap<Fields, QPair<size_t, QString>> m_fields;
public:
    enum class Fields {
        Id,
        UseFilter,
        LotIds,
        MakeIds,
        ModelIds,
        Years,
        TransmissionIds,
        BodyIds,
        PurposeIds,
        LocationIds,
        WatchDates,
        AuctionDates,
        SaleStatusIds,
        SoldStatusIds,
        SoldPrices,
        WitPrices,
        Missed
    };
    FilterSettingsTable(Database* database);

    QString Name() const;
    const IApi& Api() const;
    static size_t FieldIndex(Fields f);
    static QString FieldName(Fields f);
};

#endif // FILTERSETTINGSTABLEAPI_H
