#ifndef LOCATIONTABLE_H
#define LOCATIONTABLE_H

#include "Headers/Database/Tables/ITableApi.h"
#include "Headers/Database/IMigration.h"

// Non-dynamic table
class LocationTable : public ITableApi {
public:
    enum class Fields;
private:
    class Migration : public IMigration {
    public:
        Migration(ITableApi* table);
        bool Migrate() const;
        bool Rollback() const;
        QString Description() const;
        size_t Id() const;
    };
    class IApi {
    public:
        IApi(ITableApi* owningTable);

        QList<QMap<Fields, QVariant>> GetLocationData() const;
        QString GetLocationAbbreviationById(size_t id) const;
        QString GetLocationNameById(size_t id) const;
    private:
        ITableApi* m_table;
    };
    IApi m_api;
    static QMap<Fields, QPair<size_t, QString>> m_fields;
public:
    enum class Fields {
        Id,
        Abbreviation,
        Name
    };
    LocationTable(Database* database);
    ~LocationTable();

    QString Name() const;
    const IApi& Api() const;
    static size_t FieldIndex(Fields f);
    static QString FieldName(Fields f);
};

#endif // LOCATIONTABLE_H
