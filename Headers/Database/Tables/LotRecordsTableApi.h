#ifndef LOTRECORDSTABLE_H
#define LOTRECORDSTABLE_H

#include "Headers/Database/Tables/ITableApi.h"
#include "Headers/Database/IMigration.h"
#include "Headers/Lot.h"

// Dynamic table
class LotRecordsTable : public ITableApi {
public:
    enum class Fields;
private:
    class Migration : public IMigration {
    public:
        Migration(ITableApi* table);
        bool Migrate() const;
        bool Rollback() const;
        QString Description() const;
        size_t Id() const;
    };
    class IApi {
    public:
        IApi(ITableApi* owningTable);

        size_t InsertLot(const Lot& lot) const;
        QList<Lot> GetAllLastLots() const;
        bool LotExistInDb(size_t lotId) const;
        void RemoveLotEntries(size_t lotId) const;
        void RemoveLotRecord(size_t recordId) const;
        size_t GetLotEntriesQty(size_t lotId) const;
        QList<Lot> GetAllLotEntries(size_t lotId) const;
        void AlterFinalFields(const Lot& finalizedLot) const;
        QList<size_t> GetRecordIdsForLotId(size_t lotId) const;
        QString GetWorksheetPathByRecordId(size_t recordId) const;
        QList<size_t> GetAllDistinctLotIds() const;
    private:
        ITableApi* m_table;

        QString lotFieldsToRetrieve() const;
        QList<Lot> buildLot(const QList<QVariantList>& commandResult, int fieldOffset) const;
    };
    IApi m_api;
    static QMap<Fields, QPair<size_t, QString>> m_fields;
public:
    enum class Fields {
        Dummy,
        Id,
        LotId,
        MakeId,
        ModelId,
        Year,
        TransmissionId,
        BodyId,
        PurposeId,
        LocationId,
        WatchDate,
        AuctionDate,
        SaleStatusId,
        SoldStatusId,
        SoldPrice,
        WitPrice,
        WorksheetPath,
        Missed
    };
    LotRecordsTable(Database* database);
    ~LotRecordsTable();

    QString Name() const;
    const IApi& Api() const;
    static size_t FieldIndex(Fields f);
    static QString FieldName(Fields f);
};

#endif // LOTRECORDSTABLE_H
