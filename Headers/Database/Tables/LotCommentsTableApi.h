#ifndef LOTCOMMENTSTABLE_H
#define LOTCOMMENTSTABLE_H

#include "Headers/Database/Tables/ITableApi.h"
#include "Headers/Database/IMigration.h"
#include "Headers/Comment.h"

// Dynamic table
class LotCommentsTable : public ITableApi {
public:
    enum class Fields;
private:
    class Migration : public IMigration {
    public:
        Migration(ITableApi* table);
        bool Migrate() const;
        bool Rollback() const;
        QString Description() const;
        size_t Id() const;
    };
    class IApi {
    public:
        IApi(ITableApi* owningTable);

        QList<Comment> GetCommentsByRecordId(size_t recordId) const;
        Comment GetCommentById(size_t id) const;
        void RemoveCommentByRecordId(size_t recordId) const;
        void RemoveCommentById(size_t id) const;
        void EditComment(size_t id, const QString& newText, const QDate& editDate) const;
        size_t InsertComment(const Comment& comment) const;
        size_t GetCommentQtyForRecord(const size_t recordId) const;
    private:
        ITableApi* m_table;

        QString commentFieldsToRetrieve() const;
        Comment buildComment(const QVariantList& rawComment) const;
    };
    IApi m_api;
    static QMap<Fields, QPair<size_t, QString>> m_fields;
public:
    enum class Fields {
        Id,
        RecordId,
        CommentDate,
        Comment,
        EditDate
    };
    LotCommentsTable(Database* database);
    ~LotCommentsTable();

    QString Name() const;
    const IApi& Api() const;
    static size_t FieldIndex(Fields f);
    static QString FieldName(Fields f);
};

#endif // LOTCOMMENTSTABLE_H
