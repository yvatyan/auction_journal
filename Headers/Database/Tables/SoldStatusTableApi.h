#ifndef SOLDSTATUSTABLE_H
#define SOLDSTATUSTABLE_H

#include "Headers/Database/Tables/ITableApi.h"
#include "Headers/Database/IMigration.h"

// Non-dynamic table
class SoldStatusTable : public ITableApi {
public:
    enum class Fields;
private:
    class Migration : public IMigration {
    public:
        Migration(ITableApi* table);
        bool Migrate() const;
        bool Rollback() const;
        QString Description() const;
        size_t Id() const;
    };
    class IApi {
    public:
        IApi(ITableApi* owningTable);

        QString GetSoldStatusNameById(size_t id) const;
        QList<QMap<Fields, QVariant>> GetSoldStatusData() const;
        QList<QMap<Fields, QVariant>> GetFinishedSoldStatusData() const;
        QList<QMap<Fields, QVariant>> GetUnfinishedSoldStatusData() const;
    private:
        ITableApi* m_table;
    };
    IApi m_api;
    static QMap<Fields, QPair<size_t, QString>> m_fields;
public:
    enum class Fields {
        Id,
        Name,
        Finished
    };
    static const size_t Sold = 1;
    static const size_t NotSold = 2;
    static const size_t WA = 3;
    static const size_t Pending = 4;
    static const size_t Canceled = 5;
    static const size_t Moved = 6;
    SoldStatusTable(Database* database);

    QString Name() const;
    const IApi& Api() const;
    static size_t FieldIndex(Fields f);
    static QString FieldName(Fields f);
};

#endif // SOLDSTATUSTABLE_H
