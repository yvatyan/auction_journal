#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include "Headers/IWorker.h"
#include "Headers/Database/Database.h"
#include "Headers/Database/MigrationList.h"
#include "Headers/Database/Tables/MigrationTableApi.h"
#include "Headers/Database/Tables/TransmissionTableApi.h"
#include "Headers/Database/Tables/BodyTableApi.h"
#include "Headers/Database/Tables/LocationTableApi.h"
#include "Headers/Database/Tables/SaleStatusTableApi.h"
#include "Headers/Database/Tables/SoldStatusTableApi.h"
#include "Headers/Database/Tables/PurposeTableApi.h"
#include "Headers/Database/Tables/MakeTableApi.h"
#include "Headers/Database/Tables/ModelTableApi.h"
#include "Headers/Database/Tables/LotRecordsTableApi.h"
#include "Headers/Database/Tables/LotCommentsTableApi.h"
#include "Headers/Database/Tables/FilterSettingsTableApi.h"

class DatabaseManager : public IWorker {
    Q_OBJECT
public:
    static DatabaseManager& Instance();
    DatabaseManager();
    ~DatabaseManager();

    bool Initialize();
    inline Database* GetRecordsDatabase();
    inline Database* GetSettingsDatabase();
private:
    Database m_recordsDb;
    Database m_settingsDb;

    bool initializeStorageDatabase(const QString& jobName, size_t stepQty, size_t stepOffset);
    bool initializeSettingsDatabase(const QString& jobName, size_t stepQty, size_t stepOffset);

    bool createStorageDatabase();
    bool createTableForStorageMigrations();
    bool createTableForTransmission();
    bool createTableForBody();
    bool createTableForLocation();
    bool createTableForSaleStatus();
    bool createTableForSoldStatus();
    bool createTableForPurpose();
    bool createTableForMake();
    bool createTableForModel();
    bool createTableForLotRecords();
    bool createTableForLotComments();
    bool migrateStorageDatabase();

    bool createSettingsDatabase();
    bool createTableForSettingsMigrations();
    bool createTableForFilter();
    bool migrateSettingsDatabase();
};

inline Database* DatabaseManager::GetRecordsDatabase() {
    return &m_recordsDb;
}
inline Database* DatabaseManager::GetSettingsDatabase() {
    return &m_settingsDb;
}

#endif // DATABASEMANAGER_H
