#ifndef MIGRATIONLIST_H
#define MIGRATIONLIST_H

// Common tables
#define MIGRATION_TABLE_NAME "migration"
#define MIGRATION_TABLE_M_v1_DESCRIPTION "Initial migration of migrations\' table."
#define MIGRATION_TABLE_M_v1_ID 1

// storage.db
#define TRANSMISSION_TABLE_NAME "transmission"
#define TRANSMISSION_TABLE_M_v1_DESCRIPTION "Initial migration of transmissions\' table."
#define TRANSMISSION_TABLE_M_v1_ID 2

#define BODY_TABLE_NAME "body"
#define BODY_TABLE_M_v1_DESCRIPTION "Initial migration of bodies\' table."
#define BODY_TABLE_M_v1_ID 3

#define LOCATION_TABLE_NAME "location"
#define LOCATION_TABLE_M_v1_DESCRIPTION "Initial migration of locations\' table."
#define LOCATION_TABLE_M_v1_ID 4

#define SALESTATUS_TABLE_NAME "sale_status"
#define SALESTATUS_TABLE_M_v1_DESCRIPTION "Initial migration of sale statuses\' table."
#define SALESTATUS_TABLE_M_v1_ID 5

#define SOLDSTATUS_TABLE_NAME "sold_status"
#define SOLDSTATUS_TABLE_M_v1_DESCRIPTION "Initial migration of sold statuses\' table."
#define SOLDSTATUS_TABLE_M_v1_ID 6

#define PURPOSE_TABLE_NAME "purpose"
#define PURPOSE_TABLE_M_v1_DESCRIPTION "Initial migration of purposes\' table."
#define PURPOSE_TABLE_M_v1_ID 7

#define MAKE_TABLE_NAME "make"
#define MAKE_TABLE_M_v1_DESCRIPTION "Initial migration of makes\' table."
#define MAKE_TABLE_M_v1_ID 8

#define MODEL_TABLE_NAME "model"
#define MODEL_TABLE_M_v1_DESCRIPTION "Initial migration of models\' table."
#define MODEL_TABLE_M_v1_ID 9

#define LOTRECORDS_TABLE_NAME "lot_records"
#define LOTRECORDS_TABLE_M_v1_DESCRIPTION "Initial migration of lot records\' table."
#define LOTRECORDS_TABLE_M_v1_ID 10

#define LOTCOMMENTS_TABLE_NAME "lot_comments"
#define LOTCOMMENTS_TABLE_M_v1_DESCRIPTION "Initial migration of lot comments\' table."
#define LOTCOMMENTS_TABLE_M_v1_ID 11

#define STORAGE_DB_TABLE_QTY 11

// settings.db
#define FILTERSETTINGS_TABLE_NAME "filter"
#define FILTERSETTINGS_TABLE_M_v1_DESCRIPTION "Initial migration of filter settings\' table."
#define FILTERSETTINGS_TABLE_M_v1_ID 2

#define SETTINGS_DB_TABLE_QTY 2

#endif // MIGRATIONLIST_H
