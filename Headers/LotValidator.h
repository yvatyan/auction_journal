#ifndef LOTVALIDATOR_H
#define LOTVALIDATOR_H

#include <QValidator>

class LotValidator : public QValidator {
public:
    explicit LotValidator(QObject* parent = nullptr);
    ~LotValidator();

    QValidator::State validate(QString& input, int& pos) const;
};

#endif // LOTVALIDATOR_H
