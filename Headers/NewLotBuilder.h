#ifndef NEWLOTBUILDER_H
#define NEWLOTBUILDER_H

#include "Headers/ILotBuilder.h"

class NewLotBuilder : public ILotBuilder {
public:
    NewLotBuilder();

    Lot ConstructLot();
    void SetLot(const Lot& lot);
    void ResetBuilder();
    bool AreAllRequiredFieldsFilled() const;
    QList<LotRecordsTable::Fields> UnfilledRequiredFields() const;

    void SetLotId(unsigned int lotId);
    void SetMakeId(int makeId);
    void SetMakeName(const QString& makeName);
    void SetModelId(int modelId);
    void SetModelName(const QString& modelName);
    void SetYear(unsigned int year);
    void SetTransmissionId(unsigned int transmissionId);
    void SetBodyId(unsigned int bodyId);
    void SetPurposeId(unsigned int purposeId);
    void SetLocationId(unsigned int locationId);
    void SetAuctionDate(const QDate& auctionDate);
    void SetSaleStatusId(unsigned int saleStatusId);
    void SetWantItTodayPrice(unsigned int wantItTodayPrice);
    void SetWorksheetPath(const QString& worksheetPath);
private:
    enum RequiredFields {
        ResetMask = 0x0000,

        LotId = 0x0001,
        Make = 0x0002,
        Model = 0x0004,
        Year = 0x0008,
        Transmission = 0x0010,
        Body = 0x0020,
        Purpose = 0x0040,
        Location = 0x0080,
        AuctionDate = 0x0100,
        SaleStatus = 0x0200,
        WorksheetFile = 0x0400,

        AllowedMask = 0x0001 | 0x0002 | 0x0004 | 0x0008 |
                      0x0010 | 0x0020 | 0x0040 | 0x0080 |
                      0x0100 | 0x0200 | 0x0400
    };
    Lot m_product;
    bool m_modified;
    unsigned short m_requiredFieldsMask;

    unsigned int m_lotId;
    int m_makeId;
    QString m_makeName;
    int m_modelId;
    QString m_modelName;
    unsigned int m_year;
    unsigned int m_transmissionId;
    unsigned int m_bodyId;
    unsigned int m_purposeId;
    unsigned int m_locationId;
    QDate m_auctionDate;
    unsigned int m_saleStatusId;
    unsigned int m_wantItTodayPrice;
    QString m_worksheetPath;
};

#endif // NEWLOTBUILDER_H
