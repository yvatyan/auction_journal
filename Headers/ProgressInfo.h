#ifndef PROGRESSINFO_H
#define PROGRESSINFO_H

#include <QString>

class ProgressInfo {
public:
    enum class Status {
        Invalid,
        Pending,
        Running,
        Succeed,
        Failed
    };
    ProgressInfo();
    ProgressInfo(const QString& jobName,
                 size_t stepQty,
                 const QString& stepName,
                 size_t stepIndex,
                 const QString& stepMessage,
                 Status stepStatus);

    inline const QString& JobName() const;
    inline size_t StepQuantity() const;
    inline const QString& StepName() const;
    inline size_t StepIndex() const;
    inline const QString& StepMessage() const;
    inline Status StepStatus() const;
    inline bool IsValid() const;
private:
    QString m_jobName;
    size_t m_stepQty;
    QString m_stepName;
    size_t m_stepIndex;
    QString m_stepMessage;
    Status m_stepStatus;
};
inline const QString& ProgressInfo::JobName() const {
    return m_jobName;
}
inline size_t ProgressInfo::StepQuantity() const {
    return m_stepQty;
}
inline const QString& ProgressInfo::StepName() const {
    return m_stepName;
}
inline size_t ProgressInfo::StepIndex() const {
    return m_stepIndex;
}
inline const QString& ProgressInfo::StepMessage() const {
    return m_stepMessage;
}
inline ProgressInfo::Status ProgressInfo::StepStatus() const {
   return m_stepStatus;
}
inline bool ProgressInfo::IsValid() const {
    return m_stepStatus != Status::Invalid;
}

#endif // PROGRESSINFO_H
