#ifndef LOT_H
#define LOT_H

#include <QDate>
#include <QList>
#include <QString>

class Lot {
public:
    Lot();
    Lot(
        unsigned int lotId,
        int makeId,
        int modelId,
        unsigned int year,
        unsigned int transmissionId,
        unsigned int bodyId,
        unsigned int purposeId,
        unsigned int locationId,
        QDate watchDate,
        QDate auctionDate,
        unsigned int saleStatusId,
        unsigned int soldStatusId,
        unsigned int soldPrice,
        unsigned int wantItTodayPrice,
        QString worksheetPath,
        bool isAuctionMissed
    );
    inline unsigned int LotId() const;
    inline int MakeId() const;
    inline int ModelId() const;
    inline unsigned int Year() const;
    inline unsigned int TransmissionId() const;
    inline unsigned int BodyId() const;
    inline unsigned int PurposeId() const;
    inline unsigned int LocationId() const;
    inline const QDate& WatchDate() const;
    inline const QDate& AuctionDate() const;
    inline unsigned int SaleStatusId() const;
    inline unsigned int SoldStatusId() const;
    inline unsigned int SoldPrice() const;
    inline unsigned int WantItTodayPrice() const;
    inline const QString& WorksheetPath() const;
    inline bool IsAuctionMissed() const;
    inline bool IsValid() const;
    inline void SetRecordId(size_t recordId);
    inline size_t GetRecordId() const;
    bool IsEnded() const;
    inline void SetNewMakeName(const QString& makeName);
    inline void SetNewModelName(const QString& modelName);
    inline const QString& GetNewMakeName() const;
    inline const QString& GetNewModelName() const;
    void UpdateMakeId(int makeId);
    void UpdateModelId(int modelId);
    inline bool ContainsUnregisteredData() const;
private:
    unsigned int m_lotId;
    int m_makeId;
    int m_modelId;
    unsigned int m_year;
    unsigned int m_transmissionId;
    unsigned int m_bodyId;
    unsigned int m_purposeId;
    unsigned int m_locationId;
    QDate m_watchDate;
    QDate m_auctionDate;
    unsigned int m_saleStatusId;
    unsigned int m_soldStatusId;
    unsigned int m_soldPrice;
    unsigned int m_wantItTodayPrice;
    QString m_worksheetPath;
    bool m_missed;
    bool m_isValid;
    size_t m_recordId;
    QString m_newMakeName;
    QString m_newModelName;
};
inline unsigned int Lot::LotId() const {
    return m_lotId;
}
inline int Lot::MakeId() const {
    return m_makeId;
}
inline int Lot::ModelId() const {
    return m_modelId;
}
inline unsigned int Lot::Year() const {
    return m_year;
}
inline unsigned int Lot::TransmissionId() const {
    return m_transmissionId;
}
inline unsigned int Lot::BodyId() const {
    return m_bodyId;
}
inline unsigned int Lot::PurposeId() const {
    return m_purposeId;
}
inline unsigned int Lot::LocationId() const {
    return m_locationId;
}
inline const QDate& Lot::WatchDate() const {
    return m_watchDate;
}
inline const QDate& Lot::AuctionDate() const {
    return m_auctionDate;
}
inline unsigned int Lot::SaleStatusId() const {
    return m_saleStatusId;
}
inline unsigned int Lot::SoldStatusId() const {
    return m_soldStatusId;
}
inline unsigned int Lot::SoldPrice() const {
    return m_soldPrice;
}
inline unsigned int Lot::WantItTodayPrice() const {
    return m_wantItTodayPrice;
}
inline const QString& Lot::WorksheetPath() const {
    return m_worksheetPath;
}
inline bool Lot::IsAuctionMissed() const {
    return m_missed;
}
inline bool Lot::IsValid() const {
    return m_isValid;
}
inline void Lot::SetRecordId(size_t recordId) {
    m_recordId = recordId;
}
inline size_t Lot::GetRecordId() const {
    return m_recordId;
}
inline void Lot::SetNewMakeName(const QString& makeName) {
    m_newMakeName = makeName;
}
inline void Lot::SetNewModelName(const QString& modelName) {
    m_newModelName = modelName;
}
inline const QString& Lot::GetNewMakeName() const {
    return m_newMakeName;
}
inline const QString& Lot::GetNewModelName() const {
    return m_newModelName;
}
inline bool Lot::ContainsUnregisteredData() const {
    return MakeId() == -1 || ModelId() == -1;
}

#endif // LOT_H
