#ifndef ILOTBUILDER_H
#define ILOTBUILDER_H

#include "Headers/Lot.h"
#include "Headers/Database/Tables/LotRecordsTableApi.h"

class ILotBuilder {
public:
    virtual Lot ConstructLot() = 0;
    virtual void SetLot(const Lot& lot) = 0;
    virtual void ResetBuilder() = 0;
    virtual bool AreAllRequiredFieldsFilled() const = 0;
    virtual QList<LotRecordsTable::Fields> UnfilledRequiredFields() const = 0;
    virtual ~ILotBuilder();
};

#endif // ILOTBUILDER_H
