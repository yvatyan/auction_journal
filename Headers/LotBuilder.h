#ifndef LOTBUILDER_H
#define LOTBUILDER_H

#include "Headers/NewLotBuilder.h"
#include "Headers/EndedLotBuilder.h"

class LotBuilder : public NewLotBuilder {
public:
    LotBuilder();

    Lot ConstructLot();
    void SetLot(const Lot& lot);
    void ResetBuilder();
    bool AreAllRequiredFieldsFilled() const;
    QList<LotRecordsTable::Fields> UnfilledRequiredFields() const;

    void SetWatchDate(const QDate& watchDate);
    void SetSoldStatusId(unsigned int soldStatusId);
    void SetSoldPrice(unsigned int soldPrice);
    void SetMissedFlag(bool isMissed);
private:
    enum RequiredFields {
        ResetMask = 0x0000,

        WatchDate = 0x0001,

        AllowedMask = 0x0001
    };
    Lot m_product;
    bool m_modified;
    unsigned short m_requiredFieldsMask;

    QDate m_watchDate;
    unsigned int m_soldStatusId;
    unsigned int m_soldPrice;
    bool m_missedFlag;
};

#endif // LOTBUILDER_H
