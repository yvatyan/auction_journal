#ifndef NEWVEHICLEDIALOG_H
#define NEWVEHICLEDIALOG_H

#include "Headers/Lot.h"
#include "Headers/Database/Database.h"
#include "Headers/Database/Tables/LotRecordsTableApi.h"
#include <QDialog>
#include <QTextDocument>

namespace Ui {
    class newVehicleDialog;
}
class NewVehicleDialog : public QDialog {
    Q_OBJECT
public:
    NewVehicleDialog(Database* database, QWidget* parent = nullptr);
    ~NewVehicleDialog();

    inline const Lot& GetResult() const;
    inline const QTextDocument* GetCommentDocument() const;
    void SetupFields();
private slots:
    void onAcceptButtonClicked();
    void onMakeChanged(int make);
    void onAuctionDateVariantChanged(int index);
    void onFileSelectButtonClicked();
    void onWorksheetPathChanged(const QString& newPath);
    void prepareComboBoxForInput(int index);
private:
    Ui::newVehicleDialog* m_ui;
    Database* m_database;
    Lot m_resultLot;
    QTextDocument* m_commentDoc;
    static QColor m_labelColor;
    static QColor m_notFilledLabelColor;

    void setupMakeField();
    void setupModelField();
    void setupTransmissionField();
    void setupBodyField();
    void setupPurposeField();
    void setupLocationField();
    void setupAuctionDateField();
    void setupSaleStatusField();
    Lot constructLot() const;
    void resetLabelsColor() const;
    void highlightUnfilled(const QList<LotRecordsTable::Fields>& fields) const;
};

inline const Lot& NewVehicleDialog::GetResult() const {
   return m_resultLot;
}
inline const QTextDocument* NewVehicleDialog::GetCommentDocument() const {
   return m_commentDoc;
}

#endif // NEWVEHICLEDIALOG_H
