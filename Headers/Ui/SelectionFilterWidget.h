#ifndef SELECTIONFILTERWIDGET_H
#define SELECTIONFILTERWIDGET_H

#include <QWidget>
#include <QStackedWidget>
#include <QListWidgetItem>
#include <QSharedPointer>
#include "Headers/SelectionFilter.h"

namespace Ui {
    class selectionFilterWidget;
    class selectionMultiFilterWidget;
}
class SelectionFilterWidget : public QWidget {
    Q_OBJECT
public:
    SelectionFilterWidget(bool extended = false, QWidget* parent = nullptr);

    void Reset();
    void Clear();
    void AddData(const QList<QPair<QVariant, QVariant>>& data);
    void RemoveData(const QVariantList& data);
    QSharedPointer<IFilter<QVariant>> GetFilterObject() const;
    void SetFilterObject(const QSharedPointer<IFilter<QVariant>>& filter);
public slots:
    void OnEnableExtendedMode(int enable);
signals:
    void sigSelectionAdded(QVariant data);
    void sigSelectionDeleted(QVariant data);
private:
    Ui::selectionFilterWidget* m_selectionFilterUi;
    Ui::selectionMultiFilterWidget* m_selectionMultiFilterUi;
    QStackedWidget m_stackedUi;

    int m_filterIndex;
    int m_multiFilterIndex;

    void selectData(const QList<QVariant>& data);
private slots:
    void onSendButtonClicked();
    void onReturnButtonClicked();
    void onAvailbleDoubleClicked(QListWidgetItem* item);
    void onSelectedDoubleClicked(QListWidgetItem* item);
    void onComboboxIndexChange(int index);
};

#endif // SELECTIONFILTERWIDGET_H
