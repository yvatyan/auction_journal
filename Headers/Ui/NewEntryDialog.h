#ifndef NEWENTRYDIALOG_H
#define NEWENTRYDIALOG_H

#include "Headers/Database/Tables/LotRecordsTableApi.h"
#include "Headers/Database/Database.h"
#include "Headers/Lot.h"
#include <QDialog>
#include <QTextDocument>

namespace Ui {
    class newEntryDialog;
}
class NewEntryDialog : public QDialog {
    Q_OBJECT
public:
    NewEntryDialog(Database* database, const Lot& lot, QWidget* parent = nullptr);
    ~NewEntryDialog();

    inline const Lot& GetResult() const;
    inline const QTextDocument* GetCommentDocument() const;
    void SetupFields();
private slots:
    void onAcceptButtonClicked();
    void onAuctionDateVariantChanged(int index);
    void onWorksheetPathChanged(const QString& newPath);
    void onFileSelectButtonClicked();
private:
    Ui::newEntryDialog* m_ui;
    Database* m_database;
    Lot m_resultLot;
    Lot m_originalLot;
    QTextDocument* m_commentDoc;
    static QColor m_labelColor;
    static QColor m_notFilledLabelColor;

    Lot constructLot();
    void resetLabelsColor() const;
    void highlightUnfilled(const QList<LotRecordsTable::Fields>& fields) const;
    void setupAuctionDateField();
    void setupSaleStatusField();
};

inline const Lot& NewEntryDialog::GetResult() const {
    return m_resultLot;
}
inline const QTextDocument* NewEntryDialog::GetCommentDocument() const {
    return m_commentDoc;
}
#endif // NEWENTRYDIALOG_H
