#ifndef COMMENTITEMVIEW_H
#define COMMENTITEMVIEW_H

#include "Headers/Ui/CommentItemDelegate.h"
#include <QListView>

class CommentItemView : public QListView {
    Q_OBJECT
public:
    CommentItemView(QWidget* parent = nullptr);
    ~CommentItemView() override;

    void setModel(QAbstractItemModel* model) override;
    void resizeEvent(QResizeEvent* event) override;

    void SetDelegateEditor(CommentItemEditor* editor);
signals:
    void sigSelectionChanged(QItemSelectionModel* selectionModel);
protected:
    bool event(QEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void mouseDoubleClickEvent(QMouseEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
protected slots:
    void dataChanged(const QModelIndex& tl, const QModelIndex& br, const QVector<int>& roles) override;
private:
    CommentItemDelegate* m_delegate;
    QModelIndex m_pressedModelIndex;
    bool m_selectionMode;
private slots:
    void onClearSelection();
    void onItemsAdded(const QModelIndex& modelIndex, int first, int last);
    void onItemsChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles);
    void onItemsRemoved(const QModelIndex& modelIndex, int first, int last);
    void onRangeChanged(int min, int max);
};

#endif // COMMENTITEMVIEW_H
