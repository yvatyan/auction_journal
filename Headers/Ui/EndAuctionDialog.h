#ifndef ENDAUCTIONDIALOG_H
#define ENDAUCTIONDIALOG_H

#include "Headers/Lot.h"
#include "Headers/Database/Tables/LotRecordsTableApi.h"
#include <QDialog>
#include <QTextDocument>

namespace Ui {
    class endAuctionDialog;
}
class EndAuctionDialog : public QDialog {
    Q_OBJECT
public:
    EndAuctionDialog(Database* database, const Lot& lot, QWidget* parent = nullptr);
    ~EndAuctionDialog();

    inline const Lot& GetResult() const;
    inline const QTextDocument* GetCommentDocument() const;
    void SetupFields();
private slots:
    void onAcceptButtonClicked();
    void saleMissedStateChanged(int state);
    void setupSoldStatus();
    void onSoldStatusChanged(int index);
private:
    Ui::endAuctionDialog* m_ui;
    Database* m_database;
    Lot m_resultLot;
    Lot m_originalLot;
    QTextDocument* m_commentDoc;
    static QColor m_labelColor;
    static QColor m_notFilledLabelColor;

    Lot constructLot();
    void resetLabelsColor() const;
    void highlightUnfilled(const QList<LotRecordsTable::Fields>& fields) const;
};

inline const Lot& EndAuctionDialog::GetResult() const {
    return m_resultLot;
}
inline const QTextDocument* EndAuctionDialog::GetCommentDocument() const {
    return m_commentDoc;
}

#endif // ENDAUCTIONDIALOG_H
