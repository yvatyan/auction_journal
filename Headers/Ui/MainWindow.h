#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtPdf/QPdfDocument>
#include <QStackedWidget>

#include "Headers/Lot.h"
#include "Headers/Database/Database.h"
#include "Headers/Ui/ShapeWidget.h"
#include "Headers/Ui/CommentItemModel.h"
#include "Headers/Ui/LotFilterDialog.h"

namespace Ui {
    class mainWindow;
    class commentsWindow;
    class pdfViewerWindow;
}
class Ui_newVehicleDialog;

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit MainWindow(Database* recordsDb, QWidget* parent = nullptr);
    ~MainWindow();

    bool CriticalErrorOccurred() const;
protected:
    void resizeEvent(QResizeEvent* event);
private slots:
    void switchToMainWindow();
    void switchToCommentsWindow();
    void switchToPdfViewerWindow(const QString& pdfPath = "");
    void onAddNewVehicleButtonClicked();
    void onAuctionEndButtonClicked();
    void onTimelineButtonClicked();
    void onCommentsButtonClicked();
    void onPdfViewerButtonClicked();
    void onDeleteRecordButtonClicked();
    void onExitButtonClicked();
    void adjustCountersPosition();
    void onTableWidgetDeselected();
    void onTableWidgetOneRowSelected(int row);
    void onTableWidgetMultiRowsSelected(QList<int> rows);
    void onNewComment(const QString& text);
    void onEditComment(const QModelIndex& modelIndex, const QString& text);
    void onDeleteComment(const QModelIndexList& modelIndices);
    void onStateLicensingClicked();
    void onMemberFeesClicked();
    void onAboutClicked();
    void onUpcommingTabButtonClicked();
    void onEndedTabButtonClicked();
    void onPastTabButtonClicked();
    void onAllTabButtonClicked();
    void onFilterTabButtonClicked();
    void onFilterTabButtonDoubleClicked();
    void onCloseTimelineButtonClicked();
private:
    Ui::mainWindow* m_mainUi;
    Ui::commentsWindow* m_commentsUi;
    Ui::pdfViewerWindow* m_pdfViewerUi;
    Database* m_recordsDb;
    bool m_criticalErrorOccurred;
    ShapeWidget* m_timelineCount;
    ShapeWidget* m_commentCount;
    bool m_timelineMode;
    LotFilterDialog::FilterCollection m_upcommingFilter;
    QList<LotFilterDialog::FilterCollection> m_endedFilters;
    QList<LotFilterDialog::FilterCollection> m_pastFilters;
    LotFilterDialog::FilterCollection m_customFilter;
    enum class TabButton {
        Upcomming,
        Ended,
        Past,
        All,
        Custom
    } m_currentTabButton;

    CommentItemModel* m_commentModel;
    size_t m_commentRecordId;

    QPdfDocument m_pdfDocument;

    QStackedWidget m_stackedUi;
    int m_mainUiIndex;
    int m_commentsUiIndex;
    int m_pdfViewerUiIndex;

    void setupUiStack();
    void setupMainWindow();
    void setupCommentsWindow();
    void setupPdfViewerWindow();
    void clearAllUi();
    void loadRecordTable();
    void finalizeWaitingApprovalLot(const Lot& targetLot);
    void finalizeCommonLot(const Lot& targetLot);
    void saleApproved(const Lot& targetLot) const;
    void saleRejected(const Lot& targetLot);
    void saleEnded(const Lot& finalizedLot) const;
    void saleContinues(const Lot& finalizedLot);
    void addNewEntry(const Lot& finalizedLot);
    void setupMainUiExclusiveButtonGroup();
    void registerNewDataFromLot(const Lot& lot);
    void updateUnregisteredDataForLot(Lot& lot);
    Comment registerNewComment(size_t recordId, const QString& commentText);
    void setupPdfViewer(const QString& pdfPath);
    void setupFilters();
    void setupUpcommingFilter();
    void setupEndedFilter();
    void setupPastFilter();
};

#endif // MAINWINDOW_H
