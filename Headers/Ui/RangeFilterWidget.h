#ifndef RANGEFILTERWIDGET_H
#define RANGEFILTERWIDGET_H

#include <QWidget>
#include <QStackedWidget>
#include <QListWidgetItem>
#include <QSharedPointer>
#include "Headers/RangeFilter.h"

namespace Ui {
    class rangeFilterWidget;
    class rangeMultiFilterWidget;
    class dateRangeFilterWidget;
    class dateRangeMultiFilterWidget;
}
class RangeFilterWidget : public QWidget {
    Q_OBJECT
public:
    enum class RangeType {
        Number,
        Date
    };
    RangeFilterWidget(RangeType type = RangeType::Number, bool extended = false, QWidget* parent = nullptr);

    void Reset();
    QSharedPointer<IFilter<QVariant>> GetFilterObject() const;
    void SetFilterObject(const QSharedPointer<IFilter<QVariant>>& filter);
public slots:
    void OnEnableExtendedMode(int enable);
private:
    Ui::rangeFilterWidget* m_numberFilterUi;
    Ui::rangeMultiFilterWidget* m_numberMultiFilterUi;
    Ui::dateRangeFilterWidget* m_dateFilterUi;
    Ui::dateRangeMultiFilterWidget* m_dateMultiFilterUi;
    QStackedWidget m_stackedUi;
    int m_numberFilterIndex;
    int m_numberMultiFilterIndex;
    int m_dateFilterIndex;
    int m_dateMultiFilterIndex;
    RangeType m_type;

    void setupDateFilters();
    QString getDisplayString(const QVariant& data);
    void selectData(const QList<QPair<QVariant, QVariant>>& chosenData);
    void makeSingleDateRangeConnections();
    void breakSingleDateRangeConnections();
private slots:
    void onSendButtonClicked();
    void onDeleteButtonClicked();
    void onSelectedDoubleClicked(QListWidgetItem* item);
    void onDateVariantChanged(int index);
    void onSingleNumberRangeChanged();
    void onSingleDateRangeChanged();
};

#endif // RANGEFILTERWIDGET_H
