#ifndef PRESSLABEL_H
#define PRESSLABEL_H

#include <QLabel>

class PressLabel : public QLabel {
    Q_OBJECT
public:
    PressLabel(QWidget* parent = nullptr, Qt::WindowFlags f = Qt::Widget);
    PressLabel(const QString& text, QWidget* parent = nullptr, Qt::WindowFlags f = Qt::Widget);
protected:
    void mouseMoveEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
signals:
    void clicked();
private:
    QPoint m_mouseCoord;
    QRect getTextComponentRectangle() const;
};

#endif // PRESSLABEL_H
