#ifndef LOTRECORDWIDGET_H
#define LOTRECORDWIDGET_H

#include "Headers/Lot.h"
#include "Headers/Database/Database.h"
#include "Headers/Ui/LotIdentityProxyModel.h"
#include "Headers/Ui/LotSortFilterProxyModel.h"
#include "Headers/Ui/LotModel.h"
#include "Headers/Ui/LotFilterDialog.h"
#include <QWidget>
#include <QTableView>

class LotWidget : public QWidget {
    Q_OBJECT
public:
    LotWidget(QWidget* parent = nullptr);
    virtual ~LotWidget();

    void SetDatabase(Database* database);
    void AddNewRecord(const Lot& lot);
    void DeleteSelectedRecords();
    bool HasSelection() const;
    QList<size_t> GetSelectedLotIds() const;
    QList<size_t> GetSelectedRecordIds() const;
    QList<Lot> GetSelectedLots() const;
    Lot GetLotByRow(int row) const;
    void UpdateRecordByRecordId(const Lot& lot) const;
    void ReplaceRecordByRecordId(size_t recordId, const Lot& lot) const;
    void ClearData();
    void AddFilter(const LotFilterDialog::FilterCollection& filter);
    void SetFilter(const LotFilterDialog::FilterCollection& filter);
    void DefaultSort();
protected:
    void resizeEvent(QResizeEvent* event);
signals:
    void oneRowSelected(int row);
    void multiRowsSelected(QList<int> rowList);
    void rowsDeselected();
private:
    LotModel* m_model;
    LotIdentityProxyModel* m_identityProxy;
    LotSortFilterProxyModel* m_proxyModel;
    QTableView* m_view;
    QSet<int> m_selectedRows; // TODO: Move this to custom selection model or custom view
    bool m_sortReseted;

    void deleteRecord(int row);
    void correctColumnsWidth();
private slots:
    void onSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected);
    void onHorizontalHeaderClicked(int columnIndex);
    void onContextMenuRequested(const QPoint& position);
    void onMissedDescending();
    void onMissedAscending();
    void onResetMissedSorting();
};

#endif // LOTRECORDWIDGET_H
