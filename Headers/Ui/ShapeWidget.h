#ifndef SHAPEWIDGET_H
#define SHAPEWIDGET_H

#include <QWidget>

class ShapeWidget : public QWidget {
Q_OBJECT
public:
    enum Shape {
        Rectangle,
        Ellipse
    };
    ShapeWidget(Shape shape, QWidget* parent = nullptr);
public slots:
    void ChangeColor(const QColor& color);
    void ChangeSize(const QSize& size);
    void ChangeCenter(const QPoint& position);
    void ChangeText(const QString& text);
protected:
    void paintEvent(QPaintEvent* event);
private:
    QColor  m_color;
    QString m_text;
    Shape   m_shape;

    QFont textFont() const;
    QColor textColor() const;
    QColor borderColor() const;
};

#endif // SHAPEWIDGET_H
