#ifndef TEMPLATEQMAINWINDOW_H
#define TEMPLATEQMAINWINDOW_H

#include <QMainWindow>

template<typename Ui>
class TemplateQMainWindow : public QMainWindow {
public:
    Ui* m_ui;
    TemplateQMainWindow(QWidget* parent = nullptr);
    ~TemplateQMainWindow();
};

template<typename Ui>
TemplateQMainWindow<Ui>::TemplateQMainWindow(QWidget* parent)
    : QMainWindow(parent)
    , m_ui(new Ui)
{
    m_ui->setupUi(this);
}
template<typename Ui>
TemplateQMainWindow<Ui>::~TemplateQMainWindow() {
    delete m_ui;
}

#endif // TEMPLATEQMAINWINDOW_H
