#ifndef LOTIDENTITYPROXYMODEL_H
#define LOTIDENTITYPROXYMODEL_H

#include <QIdentityProxyModel>

class LotIdentityProxyModel : public QIdentityProxyModel {
public:
    LotIdentityProxyModel(QObject* parent = nullptr);
    QVariant data(const QModelIndex& index, int role) const;
    QAbstractItemModel* SourceRootModel() const;
    void SetSourceRootModel(QAbstractItemModel* rootModel);
private:
    QAbstractItemModel* m_rootModel;
};

#endif // LOTIDENTITYPROXYMODEL_H
