#ifndef TEMPLATEQWIDGET_H
#define TEMPLATEQWIDGET_H

#include <QWidget>

template<typename Ui>
class TemplateQWidget : public QWidget {
public:
    Ui* m_ui;
    TemplateQWidget(QWidget* parent = nullptr);
    ~TemplateQWidget();
};

template<typename Ui>
TemplateQWidget<Ui>::TemplateQWidget(QWidget* parent)
    : QWidget(parent)
    , m_ui(new Ui)
{
    m_ui->setupUi(this);
}
template<typename Ui>
TemplateQWidget<Ui>::~TemplateQWidget() {
    delete m_ui;
}

#endif // TEMPLATEQWIDGET_H
