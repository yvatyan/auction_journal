#ifndef COMMENTEDITOR_H
#define COMMENTEDITOR_H

#include "Headers/Comment.h"
#include <QWidget>
#include <QModelIndexList>

namespace Ui {
    class commentEditor;
}
class CommentItemEditor : public QWidget {
    Q_OBJECT
public:
    CommentItemEditor(QWidget* parent = nullptr);
    ~CommentItemEditor();
    void EditComment();
    void CancelEditing();
    void SetDeleteMode(bool enable);
    void SetComments(const QModelIndexList& modelIndices);

    inline bool IsInEditMode() const;
    inline bool IsInDeleteMode() const;
signals:
    void sigNewTextSubmited(const QString& text);
    void sigNewStyledTextSubmited(const QString& styledText);
    void sigEditedTextSubmited(const QModelIndex& modelIndex, const QString& text);
    void sigEditedStyledTextSubmited(const QModelIndex& modelIndex, const QString& styledText);
    void sigDeleteAccepted(const QModelIndexList& modelIndicesToDelete);
    void sigReset();
private:
    enum class Mode {
        Add,
        Edit,
        Delete
    };

    Ui::commentEditor* m_ui;
    Mode m_mode;
    QList<QModelIndex> m_selectedModelIndices;
    QString m_placeholderText;

    const QString c_placeholderTextInDeleteMode = "Double click the item to edit.";
private slots:
    void switchToAddMode();
    void switchToEditMode();
    void switchToDeleteMode();
    void onAddButtonClicked();
    void onEditButtonClicked();
    void onDeleteButtonClicked();
    void onTextChanged();
};

inline bool CommentItemEditor::IsInEditMode() const {
    return m_mode == Mode::Edit;
}
inline bool CommentItemEditor::IsInDeleteMode() const {
    return m_mode == Mode::Delete;
}
#endif // COMMENTEDITOR_H
