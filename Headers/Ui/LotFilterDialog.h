#ifndef LOTFILTERDIALOG_H
#define LOTFILTERDIALOG_H

#include <QDialog>
#include <QVariant>
#include <QAbstractButton>
#include <QSharedPointer>
#include "Headers/IFilter.h"
#include "Headers/RangeFilter.h"
#include "Headers/SelectionFilter.h"

namespace Ui {
    class lotFilterDialog;
}

class LotFilterDialog : public QDialog {
    Q_OBJECT
public:
    enum class Field {
        LotId,
        Make,
        Model,
        Year,
        Transmission,
        Body,
        Purpose,
        Location,
        WatchDate,
        AuctionDate,
        SaleStatus,
        SoldStatus,
        SoldPrice,
        Wit,
        Missed
    };
    typedef QMap<Field, QSharedPointer<IFilter<QVariant>>> FilterCollection;

    explicit LotFilterDialog(FilterCollection *filterCollection = nullptr, QWidget* parent = nullptr);
    virtual ~LotFilterDialog();

    const FilterCollection& GetResult() const;
    bool ApplyFiltersOnStartup() const;
private slots:
    void onButtonBoxClicked(QAbstractButton* button);
    void onMakeSelectionAdded(QVariant makeData);
    void onMakeSelectionDeleted(QVariant makeData);
private:
    Ui::lotFilterDialog* m_ui;
    FilterCollection m_filterCollection;
    bool m_applyOnStartup;

    const int c_showMissedAndNotIndex = 0;
    const int c_showMissedIndex = 1;
    const int c_showNotMissedIndex = 2;

    void setupWidgets();
    void setupLotFilter();
    void setupModelFilter();
    void setupMakeFilter();
    void initModelFilter();
    void setupYearFilter();
    void setupTransmissionFilter();
    void setupBodyFilter();
    void setupPurposeFilter();
    void setupLocationFilter();
    void setupWatchDateFilter();
    void setupAuctionDateFilter();
    void setupSaleStatusFilter();
    void setupSoldStatusFilter();
    void setupSoldPriceFilter();
    void setupWitPriceFilter();

    void onApplyButtonClicked();
    void onCancelButtonClicked();
    void onResetButtonClicked();

    void collectFieldData();
};

#endif // LOTFILTERDIALOG_H
