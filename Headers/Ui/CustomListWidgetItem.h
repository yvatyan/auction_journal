#ifndef CUSTOMLISTWIDGETITEM_H
#define CUSTOMLISTWIDGETITEM_H

#include <QListWidgetItem>

class CustomListWidgetItem : public QListWidgetItem {
public:
    CustomListWidgetItem(QListWidget* parent = nullptr, int type = Type);
    CustomListWidgetItem(const QString& text, QListWidget* parent = nullptr, int type = Type);
    CustomListWidgetItem(const QIcon& icon, const QString& text, QListWidget* parent = nullptr, int type = Type);
    CustomListWidgetItem(const QListWidgetItem& other);
    virtual	~CustomListWidgetItem();

    bool operator<(const QListWidgetItem& other) const;
};

#endif // CUSTOMLISTWIDGETITEM_H
