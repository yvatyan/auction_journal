#ifndef LOTMODEL_H
#define LOTMODEL_H

#include "Headers/Lot.h"
#include "Headers/Database/Database.h"
#include "Headers/Database/Tables/LotRecordsTableApi.h"
#include <QAbstractTableModel>

class LotModel : public QAbstractTableModel {
public:
    LotModel(QObject* parent);

    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    void SetDatabase(Database* database);
    QDate AuctionDateByRow(int row);
    size_t SoldStatusIdByRow(int row);
    void InsertLot(const Lot& lot);
    void RemoveRow(int index);
    Lot GetLotByModelIndex(const QModelIndex& mIndex) const;
    Lot GetLotByRow(int row) const;
    void UpdateRecord(size_t recordId, const Lot& lot);
    void DropData();
    LotRecordsTable::Fields GetFieldByColumn(int column) const;
    int GetColumnByField(LotRecordsTable::Fields field) const;
private:
    QList<Lot> m_lotRows;
    Database* m_database;
    QMap<int, QPair<LotRecordsTable::Fields, QString>> m_columns;

    void initializeColumnDescription();
};

#endif // LOTMODEL_H
