#ifndef STARTUPWINDOW_H
#define STARTUPWINDOW_H

#include <QSplashScreen>
#include <QProgressBar>
#include "Headers/IWorker.h"

class StartupWindow : public QSplashScreen {
    Q_OBJECT
public:
    StartupWindow(const QPixmap& pixmap = QPixmap(), Qt::WindowFlags f = Qt::Widget);
    StartupWindow(QWidget* parent, const QPixmap &pixmap = QPixmap(), Qt::WindowFlags f = Qt::Widget);

    void SubscribeWorker(IWorker* worker);
    void SetBackgroundPixmap(const QString& pixmapPath);
    void SetForegroundPixmap(const QString& pixmapPath, QRect fgPosition);
public slots:
    void OnWorkerUpdated(const ProgressInfo& progressInfo);
protected:
    void drawContents(QPainter* painter);
private:
    QList<IWorker*> m_workers;
    ProgressInfo m_progress;
    QPixmap m_fgPixmap;
    QRect m_fgPosition;
};

#endif // STARTUPWINDOW_H
