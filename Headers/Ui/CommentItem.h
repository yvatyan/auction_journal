#ifndef COMMENTLISTITEM_H
#define COMMENTLISTITEM_H

#include "Headers/Comment.h"
#include <QWidget>
#include <QDate>

namespace Ui {
    class commentItem;
}
class CommentItem : public QWidget {
    Q_OBJECT
public:
    CommentItem(const Comment& comment, QWidget* parent = nullptr);
    ~CommentItem();

    QSize sizeHint() const;
    void Resize(int w, int h);
    void Resize(const QSize& newSize);
    void SetComment(const Comment& comment);
private:
    Ui::commentItem* m_ui;

    int c_textEditOffsetX = 81; // Note: Is taken from form.

    void updateTextEditHeight();
    void setupFields(const Comment& comment);
};

Q_DECLARE_METATYPE(CommentItem*)

#endif // COMMENTLISTITEM_H
