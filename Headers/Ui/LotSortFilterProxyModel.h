#ifndef LOTSORTFILTERPROXYMODEL_H
#define LOTSORTFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>
#include "Headers/Ui/LotFilterDialog.h"
#include "Headers/LotComparator.h"

class LotSortFilterProxyModel : public QSortFilterProxyModel {
public:
    LotSortFilterProxyModel(QObject* parent = nullptr);

    void AddFilter(const LotFilterDialog::FilterCollection& filter);
    void SetFilter(const LotFilterDialog::FilterCollection& filter);
    QAbstractItemModel* SourceRootModel() const;
    void SetSourceRootModel(QAbstractItemModel* rootModel);
    void MissedDescendingOrder(bool missedDescending);
    void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override;
protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override;
    bool lessThan(const QModelIndex& left, const QModelIndex& right) const override;
private:
    QAbstractItemModel* m_rootModel;
    QList<LotFilterDialog::FilterCollection> m_filters;
    LotComparator m_comparator;
};

#endif // LOTSORTFILTERPROXYMODEL_H
