#ifndef COMMENTITEMMODEL_H
#define COMMENTITEMMODEL_H

#include "Headers/Comment.h"
#include <QAbstractListModel>

class CommentItemModel : public QAbstractListModel {
public:
    CommentItemModel(const QList<Comment>& list);

    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::DisplayRole);
    Qt::ItemFlags flags(const QModelIndex& index) const;

    const QModelIndex GetModelIndexAt(int index) const;
    void AddNewComment(const Comment& comment);
    void ChangeComment(const QModelIndex& modelIndex, const Comment& comment);
    void RemoveComment(const QModelIndex& modelIndex);
private:
    QList<Comment> m_data;
};

#endif // COMMENTITEMMODEL_H
