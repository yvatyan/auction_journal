#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <QDialog>

namespace Ui {
    class aboutDialog;
}
class AboutDialog : public QDialog {
public:
    AboutDialog(QWidget* parent = nullptr);
private:
    Ui::aboutDialog* m_ui;
};

#endif // ABOUTDIALOG_H
