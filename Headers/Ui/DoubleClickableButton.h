#ifndef DOUBLECLICKABLEBUTTON_H
#define DOUBLECLICKABLEBUTTON_H

#include <QPushButton>

class DoubleClickableButton : public QPushButton {
    Q_OBJECT
public:
    DoubleClickableButton(QWidget* parent = nullptr);
    DoubleClickableButton(const QString& text, QWidget* parent = nullptr);
    DoubleClickableButton(const QIcon& icon, const QString& text, QWidget* parent = nullptr);
    virtual ~DoubleClickableButton();
protected:
    void mouseDoubleClickEvent(QMouseEvent* event);
signals:
    void doubleClicked();
};

#endif // DOUBLECLICKABLEBUTTON_H
