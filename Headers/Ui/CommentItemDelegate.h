#ifndef COMMENTITEMDELEGATE_H
#define COMMENTITEMDELEGATE_H

#include "Headers/Ui/CommentItem.h"
#include "Headers/Ui/CommentItemModel.h"
#include "Headers/Ui/CommentItemEditor.h"
#include <QMap>
#include <QStyledItemDelegate>
#include <QItemSelectionModel>

class CommentItemDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    CommentItemDelegate(QObject* parent = nullptr);
    ~CommentItemDelegate() override;

    void paint(QPainter *painter,
               const QStyleOptionViewItem& option,
               const QModelIndex& modelIndex) const override;
    QSize sizeHint(const QStyleOptionViewItem& option,
                   const QModelIndex& modelIndex ) const override;
    bool editorEvent(QEvent* event,
                     QAbstractItemModel* model,
                     const QStyleOptionViewItem& option,
                     const QModelIndex& modelIndex) override;
    QWidget* createEditor(QWidget* editor,
                          const QStyleOptionViewItem& option,
                          const QModelIndex& modelIndex) const  override;

    bool IsItemClicked(int index, const QRect& itemRect, const QPoint& clickPoint);
    void UpdateItemsGeometry(const QSize& newSize);
    void NewItemAdded(const QModelIndex& modelIndex);
    void UpdateItem(const QModelIndex& modelIndex);
    void RemoveItem(const QModelIndex& modelIndex);
    void SetEditorToEditMode(const QModelIndex& index);
    void SetExternalEditor(CommentItemEditor* editor);
    void ResetModel(CommentItemModel* newModel);
public slots:
    void OnSelectionChanged(QItemSelectionModel* selectionModel);
signals:
    void sigClickedOutside();
    void sigClearSelections();
private:
    QList<CommentItem*> m_itemWidgetCache;
    CommentItemModel* m_model;
    CommentItemEditor* m_editor;
    bool m_selectionMode;
    QSize m_viewSize;

    const double c_verticalSpaceInPixel = 25.;
    const double c_horizontalSpaceInPercent = 15./100.;

    void deleteItemWidgetCache();
};

#endif // COMMENTITEMDELEGATE_H
