#ifndef RESIZENOTIFYINGBUTTON_H
#define RESIZENOTIFYINGBUTTON_H

#include <QPushButton>

class ResizeNotifyingButton : public QPushButton {
    Q_OBJECT
public:
    ResizeNotifyingButton(QWidget* parent = nullptr);
    ResizeNotifyingButton(const QString& text, QWidget* parent = nullptr);
    ResizeNotifyingButton(const QIcon& icon, const QString& text, QWidget* parent = nullptr);
    virtual ~ResizeNotifyingButton();
protected:
    void resizeEvent(QResizeEvent* event);
signals:
    void buttonResized();
};

#endif // RESIZENOTIFYINGBUTTON_H
