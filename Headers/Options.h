#ifndef OPTIONS_H
#define OPTIONS_H

#include <QString>
#include <QIcon>
#include <QFont>

class Options {
public:
    static const Options& Instance();

    QString StorageDbName() const;
    QString StorageDbPath() const;
    QString SettingsDbName() const;
    QString SettingsDbPath() const;
    QString LogFilePathAndName() const;
    QString DateFormat() const;
    QIcon   ApplicationIcon() const;
    QString ApplicationName() const;
    int     ValidLotIdLength() const;
    QFont	LotRecordsTableHeaderFont() const;
    QFont	LotRecordsTableCellFont() const;
    QString StateLicensingFilePath() const;
    QString StorageDirectory() const;
    QString MemberFeesFilePath() const;
private:
    Options();
    Options(const Options& copy);
    const Options& operator=(const Options& copy);

    QString appDataDirectory() const;

};

#endif // OPTIONS_H
