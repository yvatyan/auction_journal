#ifndef ENDEDLOTBUILDER_H
#define ENDEDLOTBUILDER_H

#include "Headers/ILotBuilder.h"

class EndedLotBuilder : public ILotBuilder {
public:
    EndedLotBuilder();

    Lot ConstructLot();
    void SetLot(const Lot& lot);
    void ResetBuilder();
    bool AreAllRequiredFieldsFilled() const;
    QList<LotRecordsTable::Fields> UnfilledRequiredFields() const;

    void AddComment(const QString& comment);
    void SetSoldStatusId(unsigned int soldStatusId);
    void SetSoldPrice(unsigned int soldPrice);
    void SetSoldPriceRequired(bool isRequired);
    void SetMissedFlag(bool isMissed);
private:
    enum RequiredFields {
        ResetMask = 0x0000,

        SoldStatus = 0x0001,
        SoldPrice = 0x0002,

        AllowedMask = 0x0001 | 0x0002
    };
    Lot m_temporaryLot;
    Lot m_product;
    bool m_modified;
    bool m_soldPriceIsRequired;
    unsigned short m_requiredFieldsMask;

    unsigned int m_soldStatusId;
    unsigned int m_soldPrice;
    bool m_missedFlag;
};

#endif // ENDEDLOTBUILDER_H
