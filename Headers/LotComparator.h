#ifndef LOTCOMPARATOR_H
#define LOTCOMPARATOR_H

#include "Headers/LotResolver.h"
#include "Headers/Database/Tables/LotRecordsTableApi.h"

class LotComparator {
public:
    LotComparator(Database* m_db);
    ~LotComparator();

    void FixField(LotRecordsTable::Fields field);
    void ResetFixedField();
    LotRecordsTable::Fields GetFirstField() const;
    int Compare(const Lot& a, const Lot& b) const;
private:
    class IComparisonNode {
    public:
        IComparisonNode();
        IComparisonNode(LotRecordsTable::Fields fieldName);
        virtual ~IComparisonNode();

        virtual bool Compare(const LotResolver& lot1, const LotResolver& lot2, int& out_comparisonResult);

        inline LotRecordsTable::Fields Field() const;
    private:
        LotRecordsTable::Fields m_fieldName;
    };
    class NumberComparisonNode : public IComparisonNode {
    public:
        NumberComparisonNode(LotRecordsTable::Fields fieldName);

        bool Compare(const LotResolver& lot1, const LotResolver& lot2, int& out_comparisonResult);
    };
    class StringComparisonNode : public IComparisonNode {
    public:
        StringComparisonNode(LotRecordsTable::Fields fieldName);

        bool Compare(const LotResolver& lot1, const LotResolver& lot2, int& out_comparisonResult);
    };
    class StringWithNumberComparisonNode : public IComparisonNode {
    public:
        StringWithNumberComparisonNode(LotRecordsTable::Fields fieldName);

        bool Compare(const LotResolver& lot1, const LotResolver& lot2, int& out_comparisonResult);
    };
    class DateComparisonNode : public IComparisonNode {
    public:
        DateComparisonNode(LotRecordsTable::Fields fieldName);

        bool Compare(const LotResolver& lot1, const LotResolver& lot2, int& out_comparisonResult);
    };
    Database* m_db;
    IComparisonNode** m_comparisonChain;
    size_t m_dummyComparatorIndex;
    LotRecordsTable::Fields m_firstComparatorField;

    const size_t c_lotFieldCount = 15;

    void init();
};

inline LotRecordsTable::Fields LotComparator::IComparisonNode::Field() const {
    return m_fieldName;
}

#endif // LOTCOMPARATOR_H
