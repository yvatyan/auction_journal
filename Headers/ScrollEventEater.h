#ifndef SCROLLEVENTEATER_H
#define SCROLLEVENTEATER_H

#include <QObject>

class ScrollEventEater : public QObject {
    Q_OBJECT
public:
    ScrollEventEater(QObject* parent);
protected:
    bool eventFilter(QObject* obj, QEvent* event) override;
};

#endif // SCROLLEVENTEATER_H
