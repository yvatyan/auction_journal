#ifndef COMMENT_H
#define COMMENT_H

#include <QString>
#include <QDate>
#include <QMetaType>

class Comment {
public:
    Comment();
    Comment(
        size_t recordId,
        const QString& text,
        const QDate& createdDate,
        const bool edited = false,
        const QDate& editedDate = QDate()
    );

    inline size_t RecordId() const;
    inline QString Text() const;
    inline QDate CreatedDate() const;
    inline bool IsEdited() const;
    inline QDate EditedDate() const;
    inline void SetId(size_t id);
    inline size_t GetId() const;
private:
    size_t  m_recordId;
    QString m_text;
    QDate   m_createdDate;
    bool 	m_edited;
    QDate 	m_editedDate;
    size_t  m_id;
};

inline size_t Comment::RecordId() const {
    return m_recordId;
}
inline QString Comment::Text() const {
    return m_text;
}
inline QDate Comment::CreatedDate() const {
    return m_createdDate;
}
inline bool Comment::IsEdited() const {
    return m_edited;
}
inline QDate Comment::EditedDate() const {
    return m_editedDate;
}
inline void Comment::SetId(size_t id) {
    m_id = id;
}
inline size_t Comment::GetId() const {
    return m_id;
}

Q_DECLARE_METATYPE(Comment)

#endif // COMMENT_H
