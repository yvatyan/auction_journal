#ifndef LOTRESOLVER_H
#define LOTRESOLVER_H

#include "Headers/Lot.h"
#include "Headers/Database/Database.h"
#include "Headers/Database/Tables/LotRecordsTableApi.h"

class LotResolver {
public:
    LotResolver(const Lot& lot, Database *db);

    size_t LotId() const;
    QString MakeName() const;
    QString ModelName() const;
    size_t Year() const;
    QString TransmissionName() const;
    QString BodyName() const;
    QString PurposeName() const;
    QString LocationName() const;
    QString LocationAbbreviation() const;
    QDate WatchDate() const;
    QDate AuctionDate() const;
    QString SaleStatus() const;
    QString SoldStatus() const;
    size_t SoldPrice() const;
    size_t WitPrice() const;
    bool IsMissed() const;
    QVariant GetField(LotRecordsTable::Fields fieldName) const;
private:
    Lot m_lot;
    Database* m_db;
};

#endif // LOTRESOLVER_H
