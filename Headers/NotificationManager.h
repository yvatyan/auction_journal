#ifndef NOTIFICATIONMANAGER_H
#define NOTIFICATIONMANAGER_H

#include <QString>
#include <QMessageBox>
#include <QFile>

class NotificationManager {
public:
    enum class Type {
        Question,
        Question3,
        Warning,
        Error,
        Notification
    };
    static const NotificationManager& Instance();

    int ShowNotification(const QString& message, Type type) const;
    void QtMessagesLogger(QtMsgType type, const QMessageLogContext& context, const QString& msg) const;
private:
    mutable QFile m_logFile;

    NotificationManager();
    NotificationManager(const NotificationManager& copy);
    const NotificationManager& operator=(const NotificationManager& copy);

    int showQuestion(const QString& messages, bool triState = false) const;
    bool showWarning(const QString& message) const;
    bool showError(const QString& message) const;
    bool showNotification(const QString& message) const;
    void showNotificationMessageBox(QMessageBox::Icon icon, const QString& title, const QString& message) const;
};

#endif // NOTIFICATIONMANAGER_H
