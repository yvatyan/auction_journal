#ifndef FILEPATHVALIDATOR_H
#define FILEPATHVALIDATOR_H

#include <QValidator>
#include <QWidget>

class FilePathValidator : public QValidator {
public:
    FilePathValidator(QWidget* target, QObject* parent = nullptr);
    ~FilePathValidator();

    QValidator::State validate(QString& input, int& pos) const;
private:
    QWidget* m_targetWidget;
};

#endif // FILEPATHVALIDATOR_H
