#ifndef RANGEFILTER_H
#define RANGEFILTER_H

#include <Headers/IFilter.h>
#include <QSet>

template<typename T>
class RangeFilter : public IFilter<T> {
public:
    ~RangeFilter();

    void AddData(const T& from, const T& to);
    QList<QPair<T, T>> GetData() const;
    virtual bool Accept(const T& object) const;
    virtual bool IsValid() const;
    virtual unsigned int Count() const;
private:
    QSet<QPair<T, T>> m_ranges;
};

template<typename T>
RangeFilter<T>::~RangeFilter()
{
}
template<typename T>
void RangeFilter<T>::AddData(const T& from, const T& to) {
    m_ranges.insert(QPair<T, T>(from, to));
    if(m_ranges.size() > 0) {
        IFilter<T>::m_isValid = true;
    }
}
template<typename T>
QList<QPair<T, T> > RangeFilter<T>::GetData() const {
    return m_ranges.toList();
}
template<typename T>
bool RangeFilter<T>::Accept(const T& object) const {
    if(IsValid()) {
        for(auto range : m_ranges) {
            if(object >= range.first && object <= range.second) {
                return true;
            }
        }
        return false;
    } else {
        return true;
    }
}
template<typename T>
bool RangeFilter<T>::IsValid() const {
    return IFilter<T>::m_isValid;
}
template<typename T>
unsigned int RangeFilter<T>::Count() const {
    return m_ranges.count();
}

#endif // RANGEFILTER_H
