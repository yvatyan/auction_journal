#ifndef IFILTER_H
#define IFILTER_H

template<typename T>
class IFilter {
public:
    IFilter();
    virtual ~IFilter();

    virtual bool Accept(const T& object) const = 0;
    virtual bool IsValid() const = 0;
    virtual unsigned int Count() const = 0;
protected:
    bool m_isValid;
};

template<typename T>
IFilter<T>::~IFilter()
{
}
template<typename T>
IFilter<T>::IFilter()
    : m_isValid(false)
{
}

#endif // IFILTER_H
