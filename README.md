## **Description**

Auction statistics storage application.
TODO more details.

## **Build requirements**

1. Qt 5.9.1 with MinGW 32bit gcc 5.3.0
2. Inno Setup 5.6.1 Unicode

### **Building & Deploying**

TODO

## **Commits**

1. Commit mesages should contain issue number and name e.g. "Issue 13: Failed to allocate memory" and the corresponding issue should be closed.
2. For every commit maintainer creates version tags with full and shortened names.

## **Version naming**

    v.<major>.<minor>.<revision>\<changes amount>-<month>.<year>.<stage>
	
**e.g. v.0.00.267-05.2018.β**

1. “Major version is a number without leading zero that is zero for α and β stages. Number is incremented when new features are added to application in γ stage.
2. “Minor version” is a number with leading zero that is zero for α and one and greater for β stages. Number is incremented in γ and β stage when is done:
  * Bug fixing,
  * Improvement or optimization,
  * Customization changes.
3. “Revision” number is started from zero and incremented on every commit in δ stage. Minor and major versions are determinate based on which release version current development commit is based on.
4. “Changes amount” is quantity of changes such as bug fixing and improvements that were done in between last and current releases. This number is written when application stage is γ, i.e. in α and β stages it is 0.
5. “Month” is 2-digit month of application code revision.
6. “Year” is 4-digit year of application code revision.
7. “Stage” is one of the following Greek letters based on what stage is application in:
  * α – Refers to application that is partly developed and testing has not started yet. Application in α stage is released only once a month to save work done in that month. In one month there can’t be done more than one commit. Versions are differentiated only by date components as minor and major components are always zero.
  * β – Refers to application that has been finished but currently is in testing stage. Its major version is always zero and minor version is greater than one.
  * γ – Refers to application that has been tested and released. The first application that enters this stage has its minor and major version set to 1.
  * δ – Refers to application that is under development. Application in this stage is not released and has its revision change when something is fixed or changed. After releasing new application that is in this stage will have revision set to 0.

#### Shortened version

Shortened version doesn’t contain date components. For α versions additional number is attached to stage indicating α versions count:

**e.g. v.0.00.0.α2**

δ versions based on α release contain α stage component instead of major version and α versions count instead of minor version:

**e.g. v.α.2.13.δ**

Full version is name of annotated tag and the shortened one is lightweight tag.
