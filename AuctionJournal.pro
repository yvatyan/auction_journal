#-------------------------------------------------
#
# Project created by QtCreator 2018-09-19T10:21:19
#
#-------------------------------------------------

include(ThirdParty/ThirdParty.pri)

QT       += core gui widgets sql pdfwidgets

TARGET = AuctionJournal
!exists( Sources/Configuration/Version.cpp ) {
      error("Version.cpp missing. Run Scripts/Versioning.sh first.")
}
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        Sources/Main.cpp \
        Sources/Ui/MainWindow.cpp \
        Sources/Ui/NewVehicleDialog.cpp \
    	Sources/Lot.cpp \
    	Sources/NewLotBuilder.cpp \
    	Sources/EndedLotBuilder.cpp \
        Sources/Ui/EndAuctionDialog.cpp \
        Sources/Database/Database.cpp \
        Sources/Database/SqlCommands/ISqlCommand.cpp \
        Sources/Database/SqlCommands/SqlCreateTable.cpp \
        Sources/Options.cpp \
        Sources/Database/Tables/ITableApi.cpp \
        Sources/Database/Tables/MigrationTableApi.cpp \
        Sources/Database/IMigration.cpp \
        Sources/Database/SqlCommands/SqlSelect.cpp \
        Sources/Database/SqlCommands/SqlInsert.cpp \
        Sources/NotificationManager.cpp \
        Sources/ILotBuilder.cpp \
        Sources/Database/Tables/TransmissionTableApi.cpp \
        Sources/Database/Tables/BodyTableApi.cpp \
        Sources/Database/Tables/LocationTableApi.cpp \
        Sources/Database/Tables/SaleStatusTableApi.cpp \
        Sources/Database/Tables/SoldStatusTableApi.cpp \
        Sources/Database/SqlTypes.cpp \
        Sources/Database/Tables/PurposeTableApi.cpp \
        Sources/Database/Tables/MakeTableApi.cpp \
        Sources/Database/Tables/ModelTableApi.cpp \
        Sources/Database/Tables/LotRecordsTableApi.cpp \
        Sources/Database/Tables/LotCommentsTableApi.cpp \
        Sources/LotValidator.cpp \
        Sources/FilePathValidator.cpp \
        Sources/Tools.cpp \
        Sources/Ui/LotIdentityProxyModel.cpp \
        Sources/LotBuilder.cpp \
        Sources/Ui/LotModel.cpp \
        Sources/Ui/LotWidget.cpp \
        Sources/Database/SqlCommands/SqlDelete.cpp \
        Sources/Ui/ShapeWidget.cpp \
        Sources/Ui/ResizeNotifyingButton.cpp \
        Sources/Database/SqlCommands/SqlUpdate.cpp \
        Sources/Ui/NewEntryDialog.cpp \
        Sources/Ui/CommentItemDelegate.cpp \
        Sources/Comment.cpp \
        Sources/Ui/CommentItemView.cpp \
        Sources/Ui/CommentItemModel.cpp \
        Sources/Ui/PressLabel.cpp \
        Sources/Ui/AboutDialog.cpp \
        Sources/Database/DatabaseManager.cpp \
        Sources/ProgressInfo.cpp \
        Sources/Ui/StartupWindow.cpp \
        Sources/Ui/LotSortFilterProxyModel.cpp \
        Sources/Ui/LotFilterDialog.cpp \
        Sources/Ui/RangeFilterWidget.cpp \
        Sources/Ui/SelectionFilterWidget.cpp \
        Sources/Ui/CommentItemEditor.cpp \
        Sources/Ui/CommentItem.cpp \
        Sources/ScrollEventEater.cpp \
        Sources/Ui/CustomListWidgetItem.cpp \
        Sources/Ui/DoubleClickableButton.cpp \
        Sources/Database/Tables/FilterSettingsTableApi.cpp \
        Sources/LotComparator.cpp \
        Sources/LotResolver.cpp \
        Sources/Configuration/Version.cpp

HEADERS += \
        Headers/Ui/MainWindow.h \
        Headers/Ui/NewVehicleDialog.h \
    	Headers/Lot.h \
    	Headers/NewLotBuilder.h \
    	Headers/ILotBuilder.h \
    	Headers/EndedLotBuilder.h \
        Headers/Ui/EndAuctionDialog.h \
        Headers/Database/SqlCommands/ISqlCommand.h \
        Headers/Database/SqlCommands/SqlCreateTable.h \
        Headers/Database/Database.h \
        Headers/Database/Tables/ITableApi.h \
        Headers/Database/Tables/MigrationTableApi.h \
        Headers/Database/IMigration.h \
        Headers/Options.h \
        Headers/Database/SqlCommands/SqlSelect.h \
        Headers/Database/MigrationList.h \
        Headers/Database/SqlCommands/SqlInsert.h \
        Headers/NotificationManager.h \
        Headers/Database/Tables/TransmissionTableApi.h \
        Headers/Database/Tables/BodyTableApi.h \
        Headers/Database/Tables/LocationTableApi.h \
        Headers/Database/Tables/SaleStatusTableApi.h \
        Headers/Database/Tables/SoldStatusTableApi.h \
        Headers/Database/SqlTypes.h \
        Headers/Database/Tables/PurposeTableApi.h \
        Headers/Database/Tables/MakeTableApi.h \
        Headers/Database/Tables/ModelTableApi.h \
        Headers/Database/Tables/LotRecordsTableApi.h \
        Headers/Database/Tables/LotCommentsTableApi.h \
        Headers/LotValidator.h \
        Headers/FilePathValidator.h \
        Headers/Tools.h \
        Headers/Ui/LotIdentityProxyModel.h \
        Headers/LotBuilder.h \
        Headers/Ui/LotModel.h \
        Headers/Ui/LotWidget.h \
        Headers/Database/SqlCommands/SqlDelete.h \
        Headers/Ui/ShapeWidget.h \
        Headers/Ui/ResizeNotifyingButton.h \
        Headers/Database/SqlCommands/SqlUpdate.h \
        Headers/Ui/NewEntryDialog.h \
        Headers/Ui/CommentItemDelegate.h \
        Headers/Comment.h \
        Headers/Ui/CommentItemEditor.h \
        Headers/Ui/CommentItemView.h \
        Headers/Ui/CommentItemModel.h \
        Headers/Ui/PressLabel.h \
        Headers/Ui/AboutDialog.h \
        Headers/Database/DatabaseManager.h \
        Headers/ProgressInfo.h \
        Headers/Ui/StartupWindow.h \
        Headers/IWorker.h \
        Headers/Ui/LotSortFilterProxyModel.h \
        Headers/Ui/LotFilterDialog.h \
        Headers/Ui/RangeFilterWidget.h \
        Headers/Ui/SelectionFilterWidget.h \
        Headers/Ui/CommentItem.h \
        Headers/Ui/TemplateQWidget.h \
        Headers/ScrollEventEater.h \
        Headers/Ui/CustomListWidgetItem.h \
        Headers/IFilter.h \
        Headers/SelectionFilter.h \
        Headers/RangeFilter.h \
        Headers/Ui/DoubleClickableButton.h \
        Headers/Database/Tables/FilterSettingsTableApi.h \
        Headers/LotComparator.h \
        Headers/LotResolver.h \
        Headers/Ui/TemplateQMainWindow.h \
        Headers/Configuration/Version.h

FORMS += \
        Resources/Forms/MainWindow.ui \
        Resources/Forms/NewVehicleDialog.ui \
        Resources/Forms/EndAuctionDialog.ui \
        Resources/Forms/PdfViewerWindow.ui \
        Resources/Forms/NewEntryDialog.ui \
        Resources/Forms/CommentWindow.ui \
        Resources/Forms/AboutDialog.ui \
        Resources/Forms/CommentItemEditorWidget.ui \
        Resources/Forms/CommentItemWidget.ui \
        Resources/Forms/LotFilterDialog.ui \
        Resources/Forms/FilterWidgets/DateRangeFilterWidget.ui \
        Resources/Forms/FilterWidgets/DateRangeMultiFilterWidget.ui \
        Resources/Forms/FilterWidgets/RangeMultiFilterWidget.ui \
        Resources/Forms/FilterWidgets/SelectionFilterWidget.ui \
        Resources/Forms/FilterWidgets/SelectionMultiFilterWidget.ui \
        Resources/Forms/FilterWidgets/RangeFilterWidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
        Resources/Resources.qrc

RC_FILE = \
        Resources/Resources.rc

#INCLUDEPATH += \
#        ThirdParty/qtpdf/include \

#LIBS += \
#        -L"ThirdParty/qtpdf/lib" -lQt5Pdf

# === OR ===

#QMAKESPEC += \
#        ThirdParty/qtpdf/mkspecs/modules

#%{Qt:QMAKE_MKSPECS} = ThirdParty/qtpdf/mkspecs/qt_lib_pdf.pri
