#!/bin/bash

project_path="`dirname $0`/../../AuctionJournal"
version_template_file="$project_path/Sources/Configuration/Version_template.cpp"
version_file="$project_path/Sources/Configuration/Version.cpp"
inno_setup_template_file="$project_path/Scripts/Setup/setup_template.iss"
inno_setup_file="$project_path/Scripts/Setup/setup.iss"

cp $version_template_file $version_file
cp $inno_setup_template_file $inno_setup_file

short_tag=$(git describe --tags)
full_tag=$(git show --quiet $short_tag | sed "5q;d")

sed "/.*Version_Full/ s/=.*/= \"$full_tag\";/" $version_file -i
sed "/.*Version_Short/ s/=.*/= \"$short_tag\";/" $version_file -i
sed "s/^#define MyAppVersion .*$/#define MyAppVersion \"$full_tag\"/" $inno_setup_file -i
