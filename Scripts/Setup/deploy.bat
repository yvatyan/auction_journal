@echo off
::Should be changed MANUALLY
SET qtPath=C:\Qt
SET platformPath=5.12.1\mingw73_64\bin

SET projectPath=%cd%\..\..
SET Path=%projectPath%\Setup;%Path%

mkdir %projectPath%\Setup
del %projectPath%\Setup\* /Q /S /F
for /D %%p in ("Setup\*.*") do rmdir "%%p" /s /q
echo.
echo "Coping files to %projectPath%\Setup directory"
echo.
copy %projectPath%\Build\release\AuctionJournal.exe %projectPath%\Setup
copy "%projectPath%\Resources\Documents\Member Fees.pdf" %projectPath%\Setup
copy "%projectPath%\Resources\Documents\State Licensing.pdf" %projectPath%\Setup
echo ----------------------------------------------------------
echo Deploying application %projectPath%\Setup\AuctionJournal.exe
echo.
%qtPath%\%platformPath%\windeployqt.exe --force --libdir %projectPath%\Setup --release --compiler-runtime --no-system-d3d-compiler --no-translations --no-network --no-svg %projectPath%\Setup\AuctionJournal.exe
pause